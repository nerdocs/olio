# Olio

#### Installation

Unter Ubuntu muss libmysqlclient-dev und python3-mysqldb installiert sein,
damit per pip mysqlclient installiert werden kann.

### Prerequisites

    apt install -y libmysqlclient-dev python3-mysqldb

    mkvirtualenv -p /usr/bin/python3 django-olio
    # workon django-olio # should do that automatically

    git clone git@gitlab.com:nerdocs/olio.git
    cd olio
    pip install -r requirements.txt
    cp olio/settings.py.example olio/settings.py

Die DB-Einstellungen sollten dann vorgenommen werden: Dazu bitte die
`DATABASES`-Variable in *olio/settings.py* dementsprechend anpassen.

#### ACHTUNG

**Der Benutzer für die acetoweb-Datenbank muss Read-Only sein**,
da es sonst passieren kann, dass olio Daten in der ACETOmed-Datenbank
überschreibt und diese damit unbrauchbar macht.

    ./manage.py migrate
    ./manage.py createsuperuser
    ./manage.py runserver


#### Deployment

Unter /var/www/olio ist das öffentliche Verzeichnis, darin kommt `olio/` als Projektverzeichnis

    git clone https://gitlab.com/nerdocs/olio.git

Unter Ubuntu sollte man gunicorn als Service installieren:

/etc/systemd/system/gunicorn.service:

    [Unit]
    Description=gunicorn daemon
    After=network.target

    [Service]
    PIDFile=/run/gunicorn/pid
    User=olio
    Group=olio
    WorkingDirectory=/var/www/olio/olio
    ExecStart=/var/www/olio/env/bin/gunicorn \
        --pid /run/gunicorn/pid \
        --access-logfile - --workers 3 \
        --bind unix:/run/gunicorn/socket olio.wsgi
    ExecReload=/bin/kill -s HUP $MAINPID
    ExecStop=/bin/kill -s TERM $MAINPID
    PrivateTmp=true

    [Install]
    WantedBy=multi-user.target

/etc/systemd/system/gunicorn.socket:

    [Unit]
    Description=gunicorn socket

    [Socket]
    ListenStream=/run/gunicorn/socket

    [Install]
    WantedBy=sockets.target

/etc/tmpfiles.d/gunicorn.conf:

    d /run/gunicorn 0755 olio olio -



Alles starten:

    systemctl enable gunicorn.socket
    systemctl start gunicorn.socket

    systemctl enable nginx.service
    systemctl start nginx
