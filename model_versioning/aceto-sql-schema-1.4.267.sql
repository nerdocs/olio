-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: kerberos    Database: acetoweb
-- ------------------------------------------------------
-- Server version	5.6.33-0ubuntu0.14.04.1-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ABRECHNUNG_BUFFER`
--

DROP TABLE IF EXISTS `ABRECHNUNG_BUFFER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ABRECHNUNG_BUFFER` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYP` int(11) DEFAULT NULL,
  `MANDAND_ID` int(11) DEFAULT NULL,
  `AbrechnungsstelleKennung` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AbrechnungsKassenKennungen` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AbrechnungsKassenTypen` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Erstellung` datetime DEFAULT NULL,
  `ZeitraumJahr` int(11) DEFAULT NULL,
  `ZeitraumKennung` int(11) DEFAULT NULL,
  `DATUM_START` date DEFAULT NULL,
  `DATUM_ENDE` date DEFAULT NULL,
  `Text` mediumtext COLLATE utf8_unicode_ci,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `TYP` (`TYP`),
  KEY `MANDAND_ID` (`MANDAND_ID`),
  KEY `AbrechnungsstelleKennung` (`AbrechnungsstelleKennung`)
) ENGINE=MyISAM AUTO_INCREMENT=305 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ABRECHNUNG_HL7_ZUORDNUNG`
--

DROP TABLE IF EXISTS `ABRECHNUNG_HL7_ZUORDNUNG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ABRECHNUNG_HL7_ZUORDNUNG` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `HL7_SEGMENT` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HL7_ERGEBNIS_TYP` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HL7_ERGEBNIS_BESCHREIBUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HL7_ERGEBNIS_IDENTIFIER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KASSE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZIFFER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZIFFER_ANZAHL_FAKTOR` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ACETO_EINSTELLUNGEN`
--

DROP TABLE IF EXISTS `ACETO_EINSTELLUNGEN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACETO_EINSTELLUNGEN` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WERT` longtext COLLATE utf8_unicode_ci,
  `WERTELISTE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STATUS` int(11) NOT NULL DEFAULT '-1',
  `PROTOKOLL` mediumtext COLLATE utf8_unicode_ci,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LETZTE_AENDERUNG` datetime DEFAULT NULL,
  `TYP` int(11) NOT NULL DEFAULT '0',
  `BEREICH` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KENNUNG_EINSTELLUNGSATZ` int(11) NOT NULL DEFAULT '-1',
  `MANDANT_ID` int(11) DEFAULT NULL,
  `BENUTZER_ID` int(11) DEFAULT NULL,
  `RECHNER_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PATIENT_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `NAME` (`NAME`),
  KEY `TYP` (`TYP`),
  KEY `FREMD_ID` (`KENNUNG_EINSTELLUNGSATZ`),
  KEY `KENNUNG_EINSTELLUNGSATZ` (`KENNUNG_EINSTELLUNGSATZ`)
) ENGINE=MyISAM AUTO_INCREMENT=245 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ACETO_KONTAKT`
--

DROP TABLE IF EXISTS `ACETO_KONTAKT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ACETO_KONTAKT` (
  `WEB_ID` int(11) NOT NULL DEFAULT '0',
  `DATUM` datetime DEFAULT NULL,
  `XML` text COLLATE utf8_unicode_ci NOT NULL,
  `PATIENT_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`WEB_ID`),
  UNIQUE KEY `WEB_ID` (`WEB_ID`),
  KEY `DATUM` (`DATUM`),
  KEY `PATIENT_ID` (`PATIENT_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ADRESSE`
--

DROP TABLE IF EXISTS `ADRESSE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ADRESSE` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYP` int(11) NOT NULL DEFAULT '1',
  `STRASSE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HAUSNUMMER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ADRESSZUSATZ` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PLZ` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ORT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BUNDESLAND` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAND_CODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `POSTFACH` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `POSTFACH_PLZ` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `POSTFACH_ORT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `POSTFACH_LAND_CODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LETZTE_AENDERUNG` datetime DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STATUS` int(11) DEFAULT NULL,
  `TABELLE` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `TABELLE` (`TABELLE`),
  KEY `TYP` (`TYP`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ALLERGENE`
--

DROP TABLE IF EXISTS `ALLERGENE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ALLERGENE` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYP` int(11) DEFAULT NULL,
  `KUERZEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEZEICHNUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BESCHREIBUNG` text COLLATE utf8_unicode_ci,
  `GRUPPE` int(11) DEFAULT NULL,
  `MANDANTEN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEARBEITER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LETZTE_AENDERUNG` datetime DEFAULT NULL,
  `PROTOKOLL` text COLLATE utf8_unicode_ci,
  `PRICK` int(11) DEFAULT NULL,
  `PRICK_PRICK` int(11) DEFAULT NULL,
  `EPICUTAN` int(11) DEFAULT NULL,
  `INTRACUTAN` int(11) DEFAULT NULL,
  `SCRATCH` int(11) DEFAULT NULL,
  `BLUTTEST_INTERN` int(11) DEFAULT NULL,
  `BLUTTEST_EXTERN` int(11) DEFAULT NULL,
  `IMPFUNG` int(11) DEFAULT NULL,
  `TYP_PROFIL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROFIL_PARAMETER_IDS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FAVORIT` int(11) DEFAULT NULL,
  `FAVORIT_POSITION` int(11) DEFAULT NULL,
  `INDIVIDUELL` int(11) DEFAULT NULL,
  `LABOR_AUFTRAGPARAMETER_CODEIMLABOR` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LABORAUFTRAG_ZUWEISER_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `KUERZEL_GRUPPE` (`KUERZEL`,`GRUPPE`),
  KEY `ZEILE_LOCK` (`ZEILE_LOCK`),
  KEY `LABOR_AUFTRAGPARAMETER` (`LABOR_AUFTRAGPARAMETER_CODEIMLABOR`,`LABORAUFTRAG_ZUWEISER_ID`),
  KEY `MANDANTEN` (`MANDANTEN`),
  KEY `PRICK` (`PRICK`),
  KEY `PRICK_PRICK` (`PRICK_PRICK`),
  KEY `EPICUTAN` (`EPICUTAN`),
  KEY `INTRACUTAN` (`INTRACUTAN`),
  KEY `SCRATCH` (`SCRATCH`),
  KEY `BLUTTEST_INTERN` (`BLUTTEST_INTERN`),
  KEY `TYP_PROFIL` (`TYP_PROFIL`),
  KEY `LABORAUFTRAG_ZUWEISER_ID` (`LABORAUFTRAG_ZUWEISER_ID`),
  KEY `LABOR_AUFTRAGPARAMETER_CODEIMLABOR` (`LABOR_AUFTRAGPARAMETER_CODEIMLABOR`),
  KEY `KUERZEL` (`KUERZEL`),
  KEY `GRUPPE` (`GRUPPE`),
  FULLTEXT KEY `BESCHREIBUNG` (`BESCHREIBUNG`)
) ENGINE=MyISAM AUTO_INCREMENT=430 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ALLERGENTEST`
--

DROP TABLE IF EXISTS `ALLERGENTEST`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ALLERGENTEST` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ALLERGENTESTDOKUID` int(11) DEFAULT NULL,
  `GESAMTIGE` int(11) DEFAULT NULL,
  `START_ZEIT` datetime DEFAULT NULL,
  `END_ZEIT` datetime DEFAULT NULL,
  `TIMER_TYP` int(11) DEFAULT NULL,
  `STATUS` int(11) DEFAULT NULL,
  `BEMERKUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ALLERGENTESTDOKUID` (`ALLERGENTESTDOKUID`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ALLERGENTESTWERT`
--

DROP TABLE IF EXISTS `ALLERGENTESTWERT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ALLERGENTESTWERT` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ALLERGENTESTDOKUID` int(11) DEFAULT NULL,
  `ALLERGEN_ID` int(11) DEFAULT NULL,
  `ERGEBNISWERT_1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ERGEBNISWERT_2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BLUTTEST` int(11) DEFAULT NULL,
  `TESTTYP` int(11) DEFAULT NULL,
  `ANAMNESE` int(11) DEFAULT NULL,
  `BESCHREIBUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GRUPPE` int(11) DEFAULT NULL,
  `POSITION` int(11) DEFAULT NULL,
  `BEAUFTRAGT` int(2) DEFAULT NULL,
  `REFERENZ` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ANAMNESEBOGENSTAMM`
--

DROP TABLE IF EXISTS `ANAMNESEBOGENSTAMM`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ANAMNESEBOGENSTAMM` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BEZEICHNUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ERMAECHTIGUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ERMAECHTIGUNG` (`ERMAECHTIGUNG`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ANGEBOT`
--

DROP TABLE IF EXISTS `ANGEBOT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ANGEBOT` (
  `ANGEBOTDOKU_ID` int(11) NOT NULL DEFAULT '0',
  `UNTERSUCHUNG_ID` int(11) NOT NULL DEFAULT '-1',
  `ANGEBOT_NR` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ANGEBOT_TEXT` text COLLATE utf8_unicode_ci,
  `ANSCHRIFT` text COLLATE utf8_unicode_ci,
  `ANSCHRIFT_XML` text COLLATE utf8_unicode_ci,
  `ABSENDE_ANSCHRIFT` text COLLATE utf8_unicode_ci,
  `POSITIONEN` text COLLATE utf8_unicode_ci,
  `ANZAHL_POS` int(11) NOT NULL DEFAULT '-1',
  `SUMME_NETTO` double DEFAULT NULL,
  `SUMME_NETTO_1` double DEFAULT NULL,
  `SUMME_NETTO_2` double DEFAULT NULL,
  `SUMME_BRUTTO` double DEFAULT NULL,
  `SUMME_BRUTTO_1` double DEFAULT NULL,
  `SUMME_BRUTTO_2` double DEFAULT NULL,
  `MWST_1` double DEFAULT NULL,
  `MWST_2` double DEFAULT NULL,
  `SUMME_MWST` double DEFAULT NULL,
  `SUMME_MWST_1` double DEFAULT NULL,
  `SUMME_MWST_2` double DEFAULT NULL,
  `WAEHRUNG` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UNTERSCHREIBER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SIGNED` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OFFENE_SUMME_BRUTTO` double NOT NULL DEFAULT '0',
  `ZAEHLER` int(10) NOT NULL DEFAULT '-1',
  `DATUM_VERSENDET` date DEFAULT NULL,
  `DATUM_STORNIERT` date DEFAULT NULL,
  `STATUS_ZUSATZ` mediumtext COLLATE utf8_unicode_ci,
  `MAHN_STUFE` int(11) NOT NULL DEFAULT '-1',
  `DATUM_ABGELEHNT` date DEFAULT NULL,
  PRIMARY KEY (`ANGEBOTDOKU_ID`),
  UNIQUE KEY `RE_NR` (`ANGEBOT_NR`),
  KEY `RE_ZAEHLER` (`ZAEHLER`),
  KEY `MAHN_STUFE` (`MAHN_STUFE`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ANGEBOTPOSITION`
--

DROP TABLE IF EXISTS `ANGEBOTPOSITION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ANGEBOTPOSITION` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ANGEBOTSDOKU_ID` int(11) DEFAULT NULL,
  `POS_NR` int(11) DEFAULT NULL,
  `ANZAHL` double DEFAULT NULL,
  `POSITION_DATUM` date DEFAULT NULL,
  `EINZELPREIS` double DEFAULT NULL,
  `MWST_SATZ` int(11) DEFAULT NULL,
  `KONTIERUNG` int(11) DEFAULT NULL,
  `POS_TEXT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `POS_ZUSATZ_TEXT` text COLLATE utf8_unicode_ci,
  `MWST_PREIS` double DEFAULT NULL,
  `BRUTTO_PREIS` double DEFAULT NULL,
  `BEARBEITER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SYSTEM_DATUM` date DEFAULT NULL,
  `SYSTEM_UHRZEIT` time DEFAULT NULL,
  `FAKTOR` double NOT NULL DEFAULT '1',
  `ABZUG` double NOT NULL DEFAULT '0',
  `TYP_KOSTEN` int(11) NOT NULL DEFAULT '0',
  `ANGEBOTSGRUPPE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ANGEBOTBILD` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROPERTIES` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `RECHNUNGDOKU_ID` (`ANGEBOTSDOKU_ID`),
  KEY `TYP_KOSTEN` (`TYP_KOSTEN`),
  KEY `POSITION_DATUM` (`POSITION_DATUM`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ARBEITSLISTE`
--

DROP TABLE IF EXISTS `ARBEITSLISTE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ARBEITSLISTE` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PATIENT_ID` int(11) NOT NULL DEFAULT '-1',
  `ERMAECHTIGUNG_ID` int(11) NOT NULL DEFAULT '-1',
  `PATIENT_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DOKU_ID` int(11) DEFAULT NULL,
  `BEZEICHNUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ARBEITSLISTE_NR` int(11) DEFAULT NULL,
  `TYP` int(11) DEFAULT NULL,
  `STATUS` int(11) NOT NULL DEFAULT '-1',
  `EINSTELLUNGEN` text COLLATE utf8_unicode_ci,
  `PLAN_START_DATUM` date DEFAULT NULL,
  `PLAN_START_UHRZEIT` time DEFAULT NULL,
  `PLAN_ENDE_DATUM` date DEFAULT NULL,
  `PLAN_ENDE_UHRZEIT` time DEFAULT NULL,
  `LETZTE_AENDERUNG_DATUM` date DEFAULT NULL,
  `LETZTE_AENDERUNG_UHRZEIT` time DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEARBEITER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROTOKOLL` text COLLATE utf8_unicode_ci,
  `ERLEDIGT` int(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ARZTBRIEF`
--

DROP TABLE IF EXISTS `ARZTBRIEF`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ARZTBRIEF` (
  `ARZTBRIEF_ID` int(11) NOT NULL DEFAULT '-1',
  `STATUS_DIKTAT` int(3) DEFAULT NULL,
  `ANSCHRIFT` text COLLATE utf8_unicode_ci,
  `KONTROLLE_ABSCHNITT` longtext COLLATE utf8_unicode_ci,
  `DATUM_VERSENDET` date DEFAULT NULL,
  `STATUS_ARBEITSFLUSS` int(11) DEFAULT NULL,
  `FACHBEFUND_ABSCHNITT` longtext COLLATE utf8_unicode_ci,
  `OPERATION_ABSCHNITT` longtext COLLATE utf8_unicode_ci,
  `AB_UNTERSCHREIBER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AB_SIGNED` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATUM_STORNIERT` date DEFAULT NULL,
  `BEMERKUNG_STORNIERUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FREMDBEFUND_ABSCHNITT` text COLLATE utf8_unicode_ci,
  `INVITRO_ABSCHNITT` text COLLATE utf8_unicode_ci,
  `AUGEN_ABSCHNITT` text COLLATE utf8_unicode_ci,
  UNIQUE KEY `ARZTBRIEF_ID` (`ARZTBRIEF_ID`),
  KEY `STATUS_DIKTAT` (`STATUS_DIKTAT`),
  KEY `STATUS_ARBEITSFLUSS` (`STATUS_ARBEITSFLUSS`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ARZTBRIEFLISTE`
--

DROP TABLE IF EXISTS `ARZTBRIEFLISTE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ARZTBRIEFLISTE` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PATIENT_ID` int(11) NOT NULL DEFAULT '-1',
  `ERMAECHTIGUNG_ID` int(11) NOT NULL DEFAULT '-1',
  `PATIENT_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DOKU_ID` int(11) DEFAULT NULL,
  `BEZEICHNUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ARBEITSLISTE_NR` int(11) DEFAULT NULL,
  `TYP` int(11) DEFAULT NULL,
  `STATUS` int(11) NOT NULL DEFAULT '-1',
  `EINSTELLUNGEN` text COLLATE utf8_unicode_ci,
  `PLAN_START_DATUM` date DEFAULT NULL,
  `PLAN_START_UHRZEIT` time DEFAULT NULL,
  `PLAN_ENDE_DATUM` date DEFAULT NULL,
  `PLAN_ENDE_UHRZEIT` time DEFAULT NULL,
  `LETZTE_AENDERUNG_DATUM` date DEFAULT NULL,
  `LETZTE_AENDERUNG_UHRZEIT` time DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEARBEITER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROTOKOLL` text COLLATE utf8_unicode_ci,
  `ERLEDIGT` int(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AT_PUNKTWERTE`
--

DROP TABLE IF EXISTS `AT_PUNKTWERTE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AT_PUNKTWERTE` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYP` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BUNDESLAND` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MANDANT_ID` int(11) DEFAULT NULL,
  `KUERZEL` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BESCHREIBUNG` text COLLATE utf8_unicode_ci,
  `PUNKTWERT` double NOT NULL DEFAULT '0',
  `LETZTE_AENDERUNG` datetime DEFAULT NULL,
  `STAND` date DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GUELTIG_VON_PKTSTAND` double NOT NULL DEFAULT '0',
  `GUELTIG_BIS_PKTSTAND` double NOT NULL DEFAULT '0',
  `FACHGEBIET` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `PKT_SCHLUESSEL` (`MANDANT_ID`,`KUERZEL`,`TYP`,`FACHGEBIET`)
) ENGINE=MyISAM AUTO_INCREMENT=123 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AT_SISX_ATC`
--

DROP TABLE IF EXISTS `AT_SISX_ATC`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AT_SISX_ATC` (
  `ATC` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `ATCBEZ` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ATCBEZE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DT` datetime DEFAULT NULL,
  PRIMARY KEY (`ATC`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AT_SISX_BREVIER`
--

DROP TABLE IF EXISTS `AT_SISX_BREVIER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AT_SISX_BREVIER` (
  `MONO` int(11) NOT NULL DEFAULT '0',
  `EWD` text COLLATE utf8_unicode_ci,
  `AGD` text COLLATE utf8_unicode_ci,
  `AAD` text COLLATE utf8_unicode_ci,
  `AHD` text COLLATE utf8_unicode_ci,
  `DOD` text COLLATE utf8_unicode_ci,
  `GAD` text COLLATE utf8_unicode_ci,
  `SSTD` text COLLATE utf8_unicode_ci,
  `NWD` text COLLATE utf8_unicode_ci,
  `WWD` text COLLATE utf8_unicode_ci,
  `GED` text COLLATE utf8_unicode_ci,
  `WHD` text COLLATE utf8_unicode_ci,
  `VTWD` text COLLATE utf8_unicode_ci,
  `LHD` text COLLATE utf8_unicode_ci,
  `HKD` text COLLATE utf8_unicode_ci,
  `DT` datetime DEFAULT NULL,
  PRIMARY KEY (`MONO`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AT_SISX_CODE`
--

DROP TABLE IF EXISTS `AT_SISX_CODE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AT_SISX_CODE` (
  `CDTYP` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `CDVAL` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `DSCRSD` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DSCRMD` text COLLATE utf8_unicode_ci,
  `DSCRD` text COLLATE utf8_unicode_ci,
  `DT` datetime DEFAULT NULL,
  PRIMARY KEY (`CDTYP`,`CDVAL`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AT_SISX_COMPANY`
--

DROP TABLE IF EXISTS `AT_SISX_COMPANY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AT_SISX_COMPANY` (
  `PRTNO` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `NAML` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ADNAM` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STRT` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZIP` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CITY` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CNTRY` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PBOX` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TEL` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FAX` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CNTCT` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EMAIL` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WWW` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EAN` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DT` datetime DEFAULT NULL,
  PRIMARY KEY (`PRTNO`),
  KEY `NAML` (`NAML`),
  KEY `PRTNO` (`PRTNO`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AT_SISX_DAR`
--

DROP TABLE IF EXISTS `AT_SISX_DAR`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AT_SISX_DAR` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `REFERENZ` int(11) DEFAULT NULL,
  `TYP` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `DARREICHUNG_CODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEZEICHNUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DT` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AT_SISX_DOK`
--

DROP TABLE IF EXISTS `AT_SISX_DOK`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AT_SISX_DOK` (
  `DOKNUMMER` int(10) NOT NULL DEFAULT '0',
  `DOKTYP` int(10) NOT NULL DEFAULT '0',
  `DOKTITEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DOKTXT` longtext COLLATE utf8_unicode_ci,
  `DOKTXK` longtext COLLATE utf8_unicode_ci,
  `DT` datetime DEFAULT NULL,
  PRIMARY KEY (`DOKNUMMER`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AT_SISX_DOK_IND`
--

DROP TABLE IF EXISTS `AT_SISX_DOK_IND`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AT_SISX_DOK_IND` (
  `INDNR` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `DOKNUMMER` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`INDNR`,`DOKNUMMER`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AT_SISX_INDGRST`
--

DROP TABLE IF EXISTS `AT_SISX_INDGRST`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AT_SISX_INDGRST` (
  `INDNR` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `INDBEZ` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `INDNR2` varchar(10) COLLATE utf8_unicode_ci DEFAULT '',
  `ART` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`INDNR`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AT_SISX_INTER`
--

DROP TABLE IF EXISTS `AT_SISX_INTER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AT_SISX_INTER` (
  `IXNO` int(11) NOT NULL DEFAULT '0',
  `GRP1ID` int(11) DEFAULT NULL,
  `GRP2ID` int(11) DEFAULT NULL,
  `INTERNR2` int(11) DEFAULT NULL,
  `EFFD` mediumtext COLLATE utf8_unicode_ci,
  `RLV` int(5) DEFAULT NULL,
  `RLVD` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EFFTXTD` mediumtext COLLATE utf8_unicode_ci,
  `MECHD` mediumtext COLLATE utf8_unicode_ci,
  `MEASD` mediumtext COLLATE utf8_unicode_ci,
  `REMD` mediumtext COLLATE utf8_unicode_ci,
  `LIT` mediumtext COLLATE utf8_unicode_ci,
  `IXMCH_TYP` int(11) DEFAULT NULL,
  `DT` datetime DEFAULT NULL,
  PRIMARY KEY (`IXNO`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AT_SISX_IXCPT`
--

DROP TABLE IF EXISTS `AT_SISX_IXCPT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AT_SISX_IXCPT` (
  `IXNO` int(11) NOT NULL DEFAULT '0',
  `PRTNO` int(11) NOT NULL DEFAULT '0',
  `CPTNO` int(5) NOT NULL DEFAULT '0',
  `GRP` int(5) NOT NULL DEFAULT '0',
  `ART` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`IXNO`,`PRTNO`,`GRP`,`ART`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AT_SISX_IXSUB`
--

DROP TABLE IF EXISTS `AT_SISX_IXSUB`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AT_SISX_IXSUB` (
  `IXNO` int(11) NOT NULL DEFAULT '0',
  `SUBNO` int(11) NOT NULL DEFAULT '0',
  `SUBNO1` int(5) DEFAULT '0',
  `GRP` int(5) DEFAULT NULL,
  `ART` int(5) DEFAULT NULL,
  PRIMARY KEY (`IXNO`,`SUBNO`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AT_SISX_LIM`
--

DROP TABLE IF EXISTS `AT_SISX_LIM`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AT_SISX_LIM` (
  `LIMCD` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `LIMTYP` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `DSCRD` mediumtext COLLATE utf8_unicode_ci,
  `DT` datetime DEFAULT NULL,
  PRIMARY KEY (`LIMCD`,`LIMTYP`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AT_SISX_PRODUCT`
--

DROP TABLE IF EXISTS `AT_SISX_PRODUCT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AT_SISX_PRODUCT` (
  `PRDNO` int(11) NOT NULL DEFAULT '0',
  `DSCRD` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BNAMD` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ATC` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IT` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `A_TRADE` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRTNO` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `A_VERTR` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `A_ZLINH` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `A_MONO` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `A_ANZAHL` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `A_BDAT` date DEFAULT NULL,
  `A_VERFALL` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `A_VERFALL2` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `A_LAGER` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `A_SUCHT` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `A_WARTEZ` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MONO` int(11) DEFAULT NULL,
  `CDGALD` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `A_ZLOESEN` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `A_ZABGABE` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `A_ZLNUMM` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `A_ZRP` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `A_WARN` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DT` datetime DEFAULT NULL,
  PRIMARY KEY (`PRDNO`),
  KEY `A_ZLNUMM` (`A_ZLNUMM`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AT_SISX_PRO_CPT`
--

DROP TABLE IF EXISTS `AT_SISX_PRO_CPT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AT_SISX_PRO_CPT` (
  `PRDNO` int(11) NOT NULL DEFAULT '0',
  `CPTLNO` int(11) NOT NULL DEFAULT '0',
  `GALF` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `A_ZEDAT5` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EXCIPQ` double DEFAULT NULL,
  `EXCIPU` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EXCIPCD` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EXCIPQ2` double DEFAULT NULL,
  `EXCIPU2` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EXCIPCD2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PQTY` double DEFAULT NULL,
  `PQTYU` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEZGR2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DT` datetime DEFAULT NULL,
  PRIMARY KEY (`PRDNO`,`CPTLNO`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AT_SISX_PRO_CPTAPP`
--

DROP TABLE IF EXISTS `AT_SISX_PRO_CPTAPP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AT_SISX_PRO_CPTAPP` (
  `PRDNO` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `CPTLNO` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `APPNO` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `METH` int(11) DEFAULT NULL,
  `MODE` int(11) DEFAULT NULL,
  `STATE` int(10) DEFAULT NULL,
  `LCT` int(255) DEFAULT NULL,
  `PRP` int(11) DEFAULT NULL,
  PRIMARY KEY (`PRDNO`,`CPTLNO`,`APPNO`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AT_SISX_PRO_CPTCMP`
--

DROP TABLE IF EXISTS `AT_SISX_PRO_CPTCMP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AT_SISX_PRO_CPTCMP` (
  `PRDNO` int(11) NOT NULL DEFAULT '0',
  `CPTLNO` int(11) NOT NULL DEFAULT '0',
  `LINE` int(11) NOT NULL DEFAULT '0',
  `SUBNO` int(11) DEFAULT NULL,
  `QTY` double DEFAULT NULL,
  `QTYU` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SUFFD` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRAED` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WHK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IXREL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SUBNO1` int(11) DEFAULT NULL,
  PRIMARY KEY (`PRDNO`,`CPTLNO`,`LINE`),
  KEY `IXREL` (`IXREL`),
  KEY `SUBNO` (`SUBNO`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AT_SISX_PRO_CPTTB`
--

DROP TABLE IF EXISTS `AT_SISX_PRO_CPTTB`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AT_SISX_PRO_CPTTB` (
  `PRDNO` int(11) NOT NULL DEFAULT '0',
  `CPTLNO` int(11) NOT NULL DEFAULT '0',
  `TB_EE` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TB_EE_ZUS` mediumtext COLLATE utf8_unicode_ci,
  `TB_GD` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TB_GD_ZUS` mediumtext COLLATE utf8_unicode_ci,
  `KA_OEF` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KA_OEF_ZUS` mediumtext COLLATE utf8_unicode_ci,
  `AL_SUS` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AL_SUS_ZUS` mediumtext COLLATE utf8_unicode_ci,
  `ZMOE` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZMOE_ZUS` mediumtext COLLATE utf8_unicode_ci,
  `SONDE` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SONDE_ZUS` mediumtext COLLATE utf8_unicode_ci,
  `CMR` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CMR_ZUS` mediumtext COLLATE utf8_unicode_ci,
  `LICHT` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LICHT_ZUS` mediumtext COLLATE utf8_unicode_ci,
  `TB` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TB_ZUS` mediumtext COLLATE utf8_unicode_ci,
  `TB_BR` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TB_BR_ZUS` mediumtext COLLATE utf8_unicode_ci,
  `AZP` mediumtext COLLATE utf8_unicode_ci,
  KEY `PRDNO` (`PRDNO`,`CPTLNO`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AT_SISX_SUB`
--

DROP TABLE IF EXISTS `AT_SISX_SUB`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AT_SISX_SUB` (
  `SUBNO` int(11) NOT NULL DEFAULT '0',
  `NAMD` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `ANAMD` mediumtext COLLATE utf8_unicode_ci,
  `ABDANO` int(11) DEFAULT NULL,
  `WSTHST` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SUBNO2` int(11) DEFAULT NULL,
  `DT` datetime DEFAULT NULL,
  PRIMARY KEY (`SUBNO`),
  KEY `IXREL` (`SUBNO2`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AT_SISX_SUBST`
--

DROP TABLE IF EXISTS `AT_SISX_SUBST`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AT_SISX_SUBST` (
  `INTERGR` int(11) NOT NULL DEFAULT '0',
  `INTERGR1` int(11) DEFAULT NULL,
  `GRBEZ` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `ART` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DT` datetime DEFAULT NULL,
  PRIMARY KEY (`INTERGR`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AUFTRAG`
--

DROP TABLE IF EXISTS `AUFTRAG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AUFTRAG` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FRAGESTELLUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SYMPTOMATIK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZUWEISER_ID` int(11) DEFAULT NULL,
  `REGION_ID` int(11) DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AUFTRAG_GRUPPE`
--

DROP TABLE IF EXISTS `AUFTRAG_GRUPPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AUFTRAG_GRUPPE` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FRAGESTELLUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SYMPTOMATIK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZUWEISER_ID` int(11) DEFAULT NULL,
  `REGION_ID` int(11) DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AUGE_BRILLE`
--

DROP TABLE IF EXISTS `AUGE_BRILLE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AUGE_BRILLE` (
  `DOKU_ID` int(11) NOT NULL DEFAULT '0',
  `L_SPHAERE_FERN` double DEFAULT NULL,
  `L_ZYLINDER_FERN` double DEFAULT NULL,
  `L_ACHSE_FERN` double DEFAULT NULL,
  `L_ADDITION` double DEFAULT NULL,
  `L_PRISMA` double DEFAULT NULL,
  `L_BASIS` double DEFAULT NULL,
  `PUPILLARDISTANZ` double DEFAULT NULL,
  `L_HORNHAUTSCHEITELABSTAND` double DEFAULT NULL,
  `R_SPHAERE_FERN` double DEFAULT NULL,
  `R_ZYLINDER_FERN` double DEFAULT NULL,
  `R_ACHSE_FERN` double DEFAULT NULL,
  `R_ADDITION` double DEFAULT NULL,
  `R_PRISMA` double DEFAULT NULL,
  `R_BASIS` double DEFAULT NULL,
  `R_HORNHAUTSCHEITELABSTAND` double DEFAULT NULL,
  `PRODUKTIONSJAHR` int(6) DEFAULT NULL,
  `L_SPHAERE_NAH` double DEFAULT NULL,
  `L_ZYLINDER_NAH` double DEFAULT NULL,
  `L_ACHSE_NAH` double DEFAULT NULL,
  `R_SPHAERE_NAH` double DEFAULT NULL,
  `R_ZYLINDER_NAH` double DEFAULT NULL,
  `R_ACHSE_NAH` double DEFAULT NULL,
  `BEMERKUNG_MESSUNG` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`DOKU_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AUGE_DRUCK`
--

DROP TABLE IF EXISTS `AUGE_DRUCK`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AUGE_DRUCK` (
  `DOKU_ID` int(11) NOT NULL DEFAULT '0',
  `L_AUGENDRUCK` double DEFAULT NULL,
  `R_AUGENDRUCK` double DEFAULT NULL,
  `BEMERKUNG_MESSUNG` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`DOKU_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AUGE_KERATOMETRIE`
--

DROP TABLE IF EXISTS `AUGE_KERATOMETRIE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AUGE_KERATOMETRIE` (
  `DOKU_ID` int(11) NOT NULL DEFAULT '0',
  `L_R1_HORNHAUTKRUEMMUNG` double DEFAULT NULL,
  `L_R2_HORNHAUTKRUEMMUNG` double DEFAULT NULL,
  `L_AVE_HORNHAUTKRUEMMUNG` double DEFAULT NULL,
  `L_R1_BRECHWERT` double DEFAULT NULL,
  `L_R2_BRECHWERT` double DEFAULT NULL,
  `L_AVE_BRECHWERT` double DEFAULT NULL,
  `L_R1_ZYLINDERACHSE` double DEFAULT NULL,
  `L_R2_ZYLINDERACHSE` double DEFAULT NULL,
  `L_CYL_ZYLINDERWERT` double DEFAULT NULL,
  `L_CYL_ZYLINDERACHSE` double DEFAULT NULL,
  `R_R1_HORNHAUTKRUEMMUNG` double DEFAULT NULL,
  `R_R2_HORNHAUTKRUEMMUNG` double DEFAULT NULL,
  `R_AVE_HORNHAUTKRUEMMUNG` double DEFAULT NULL,
  `R_R1_BRECHWERT` double DEFAULT NULL,
  `R_R2_BRECHWERT` double DEFAULT NULL,
  `R_AVE_BRECHWERT` double DEFAULT NULL,
  `R_R1_ZYLINDERACHSE` double DEFAULT NULL,
  `R_R2_ZYLINDERACHSE` double DEFAULT NULL,
  `R_CYL_ZYLINDERWERT` double DEFAULT NULL,
  `R_CYL_ZYLINDERACHSE` double DEFAULT NULL,
  `BEMERKUNG_MESSUNG` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`DOKU_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AUGE_REFRAKTION`
--

DROP TABLE IF EXISTS `AUGE_REFRAKTION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AUGE_REFRAKTION` (
  `DOKU_ID` int(11) DEFAULT NULL,
  `TYP` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `L_SPHAERE_FERN` double DEFAULT NULL,
  `L_ZYLINDER_FERN` double DEFAULT NULL,
  `L_ACHSE_FERN` double DEFAULT NULL,
  `L_ADDITION` double DEFAULT NULL,
  `L_PRISMA` double DEFAULT NULL,
  `L_BASIS` double DEFAULT NULL,
  `PUPILLARDISTANZ` double DEFAULT NULL,
  `R_SPHAERE_FERN` double DEFAULT NULL,
  `R_ZYLINDER_FERN` double DEFAULT NULL,
  `R_ACHSE_FERN` double DEFAULT NULL,
  `R_ADDITION` double DEFAULT NULL,
  `R_PRISMA` double DEFAULT NULL,
  `R_BASIS` double DEFAULT NULL,
  `CYCLOPLEGIE` int(1) DEFAULT NULL,
  `CYCLOPLEGIE_TROPFEN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `L_VISUS_SC` double DEFAULT NULL,
  `L_VISUS_CC` double DEFAULT NULL,
  `L_SPHAERE_NAH` double DEFAULT NULL,
  `L_ZYLINDER_NAH` double DEFAULT NULL,
  `L_ACHSE_NAH` double DEFAULT NULL,
  `R_VISUS_SC` double DEFAULT NULL,
  `R_VISUS_CC` double DEFAULT NULL,
  `R_SPHAERE_NAH` double DEFAULT NULL,
  `R_ZYLINDER_NAH` double DEFAULT NULL,
  `R_ACHSE_NAH` double DEFAULT NULL,
  `DOMINANZ` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEMERKUNG_MESSUNG` text COLLATE utf8_unicode_ci,
  UNIQUE KEY `DOKU_ID` (`DOKU_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AUSWERTUNG_ERGEBNISLISTEN`
--

DROP TABLE IF EXISTS `AUSWERTUNG_ERGEBNISLISTEN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AUSWERTUNG_ERGEBNISLISTEN` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `BESCHREIBUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AUSWERTUNG_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TYP` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ID_LISTE` text COLLATE utf8_unicode_ci,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATUM_ERSTELLT` date DEFAULT NULL,
  `AKTIV` int(2) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `NAME` (`NAME`),
  KEY `AUSWERTUNG_NAME` (`AUSWERTUNG_NAME`),
  KEY `TYP` (`TYP`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AUSWERTUNG_KRITERIEN`
--

DROP TABLE IF EXISTS `AUSWERTUNG_KRITERIEN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AUSWERTUNG_KRITERIEN` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `BESCHREIBUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AUSWERTUNG_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `SUCHFILTER_XML` text COLLATE utf8_unicode_ci,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NUMMER` int(1) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NUMMER` (`AUSWERTUNG_NAME`,`NUMMER`),
  KEY `NAME` (`NAME`),
  KEY `AUSWERTUNG_NAME` (`AUSWERTUNG_NAME`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AUT_ABRECHNUNGSSTELLE`
--

DROP TABLE IF EXISTS `AUT_ABRECHNUNGSSTELLE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AUT_ABRECHNUNGSSTELLE` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MANDANT_ID` int(11) DEFAULT NULL,
  `BEZEICHNUNG` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KENNUNG` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BUNDESLAND` int(11) DEFAULT NULL,
  `KASSEN_TYP_LISTE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KASSEN_KENNUNG_LISTE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LETZTE_AENDERUNG` datetime DEFAULT NULL,
  `EINSTELLUNGEN` text COLLATE utf8_unicode_ci,
  `ELEKTRONISCHE_ADRESSE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DIENSTLEISTER_NR` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `BEZEICHNUNG` (`BEZEICHNUNG`,`MANDANT_ID`) USING BTREE,
  KEY `MANDANT_ID` (`MANDANT_ID`),
  KEY `KENNUNG` (`KENNUNG`),
  KEY `KASSEN_TYP_LISTE` (`KASSEN_TYP_LISTE`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AUT_ABRECHNUNG_STATISTIK`
--

DROP TABLE IF EXISTS `AUT_ABRECHNUNG_STATISTIK`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AUT_ABRECHNUNG_STATISTIK` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYP` int(11) DEFAULT NULL,
  `MANDAND_ID` int(11) DEFAULT NULL,
  `AbrechnungsstelleKennung` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AbrechnungsKassenKennungen` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AbrechnungsKassenTypen` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Erstellung` datetime DEFAULT NULL,
  `ZeitraumJahr` int(11) DEFAULT NULL,
  `ZeitraumKennung` int(11) DEFAULT NULL,
  `DATUM_START` date DEFAULT NULL,
  `DATUM_ENDE` date DEFAULT NULL,
  `AnzahlScheine` int(11) DEFAULT NULL,
  `Gesamtsumme` double DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `INDEX1` (`MANDAND_ID`,`AbrechnungsstelleKennung`,`DATUM_START`,`DATUM_ENDE`)
) ENGINE=MyISAM AUTO_INCREMENT=288 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AUT_ABSMEDIKAMENT`
--

DROP TABLE IF EXISTS `AUT_ABSMEDIKAMENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AUT_ABSMEDIKAMENT` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PATIENT_ID` int(11) NOT NULL DEFAULT '-1',
  `ABSDOKU_ID` int(11) NOT NULL DEFAULT '-1',
  `MEDIKAMENT_ID` int(11) DEFAULT NULL,
  `BEGRUENDUNG` text COLLATE utf8_unicode_ci,
  `DIAGNOSE` text COLLATE utf8_unicode_ci,
  `LANGZEITVERORDNUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROTOKOLL` text COLLATE utf8_unicode_ci,
  `ANTRAG_MEDIKAMENT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHARMANUMMER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MAGISTRALREZEPTUR` text COLLATE utf8_unicode_ci,
  `ANTRAG_ANZAHL` double DEFAULT NULL,
  `DOSISSCHEMA` text COLLATE utf8_unicode_ci,
  `BEWILLIGT_ANZAHL` double DEFAULT NULL,
  `BEWILLIGT_MEDIKAMENT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEWILLIGT_LZV_MONATE` int(11) DEFAULT NULL,
  `BEWILLIGT_LZV_ANZAHL` int(11) DEFAULT NULL,
  `ENTSCHEIDUNG` text COLLATE utf8_unicode_ci,
  `INFO_TEXT` text COLLATE utf8_unicode_ci,
  `ABS_ANFRAGE_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEWILLIGT_LZV_OFFEN` int(11) DEFAULT NULL,
  `BEWILLIGT_LZV_BIS` date DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `PATIENT_ID` (`PATIENT_ID`),
  KEY `ABSDOKU_ID` (`ABSDOKU_ID`),
  KEY `MEDIKAMENT_ID` (`MEDIKAMENT_ID`),
  KEY `ABS_ANFRAGE_ID` (`ABS_ANFRAGE_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=720 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AUT_AP_WARENVERZEICHNIS`
--

DROP TABLE IF EXISTS `AUT_AP_WARENVERZEICHNIS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AUT_AP_WARENVERZEICHNIS` (
  `PHARMA_NR` int(11) NOT NULL DEFAULT '0',
  `ARTIKEL_NR` int(11) DEFAULT NULL,
  `AENDERUNG_CODE` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GUELTIG_AB` date DEFAULT NULL,
  `HERSTELLER_CODE` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AP_EK_PREIS` double DEFAULT NULL,
  `KASSEN_PREIS` double DEFAULT NULL,
  `VERKAUFS_PREIS` double DEFAULT NULL,
  `BOX` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ARTIKEL_KENNZEICHEN` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAGERVORSCHRIFT` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `REZEPTZEICHEN` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RZPT_GEBUEHR` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KURZTEXT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MENGE` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MENGENART` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KASSENZEICHEN` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MWST_SATZ` int(11) DEFAULT NULL,
  `LIEFER_CODE` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GETRAENKE_STEUER` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WARENGRUPPE` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KASSENZEICHEN_ZUSATZ` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `REGISTERNUMMER` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PREISKENNZEICHEN` int(1) DEFAULT NULL,
  PRIMARY KEY (`PHARMA_NR`),
  KEY `KASSENPREIS` (`KASSEN_PREIS`),
  KEY `BOX` (`BOX`),
  KEY `KURZTEXT` (`KURZTEXT`),
  KEY `HERSTELLER_CODE` (`HERSTELLER_CODE`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AUT_AU_MELDUNGEN`
--

DROP TABLE IF EXISTS `AUT_AU_MELDUNGEN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AUT_AU_MELDUNGEN` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DOKU_ID` int(11) NOT NULL DEFAULT '-1',
  `MELDUNGSART` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AUSSTELLUNGSDATUM` date DEFAULT NULL,
  `QUITTUNG_ID` int(11) DEFAULT NULL,
  `QUITTUNG_VERSION` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MELDUNGSDATEN` text COLLATE utf8_unicode_ci,
  `PERSONENDATEN` text COLLATE utf8_unicode_ci,
  `LETZTE_AENDERUNG` datetime DEFAULT NULL,
  `BEARBEITER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AU_VON` date DEFAULT NULL,
  `AU_BIS` date DEFAULT NULL,
  `STATUS` int(11) DEFAULT NULL,
  `VERSICHERUNGSNUMMER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`),
  UNIQUE KEY `QUITTUNG_ID` (`QUITTUNG_ID`),
  KEY `DOKU_ID` (`DOKU_ID`),
  KEY `AU_VON` (`AU_VON`),
  KEY `AU_BIS` (`AU_BIS`),
  KEY `AUSSTELLUNGSDATUM` (`AUSSTELLUNGSDATUM`)
) ENGINE=MyISAM AUTO_INCREMENT=727 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AUT_ECARD_MELDUNGEN`
--

DROP TABLE IF EXISTS `AUT_ECARD_MELDUNGEN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AUT_ECARD_MELDUNGEN` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYP` int(11) DEFAULT NULL,
  `DATUM` date DEFAULT NULL,
  `UHRZEIT` time DEFAULT NULL,
  `MELDUNG` text COLLATE utf8_unicode_ci,
  `CATEGORY` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATEN` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=1031 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AUT_ECARD_QUITTUNGEN`
--

DROP TABLE IF EXISTS `AUT_ECARD_QUITTUNGEN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AUT_ECARD_QUITTUNGEN` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FORMULAR_DOKU_ID` int(11) NOT NULL DEFAULT '-1',
  `ANNAHMEZEITPUNKT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATENBLATT_TYP` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SIGNATUR` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SV_NUMMER` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `SVT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UNTERSUCHUNGSDATUM` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VP_NUMMER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SYSTEM_ZEIT` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `BEARBEITER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FORMULAR_DOKU_ID` (`FORMULAR_DOKU_ID`),
  KEY `SV_NUMMER` (`SV_NUMMER`)
) ENGINE=MyISAM AUTO_INCREMENT=277 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AUT_GNW_AUFENTHALT`
--

DROP TABLE IF EXISTS `AUT_GNW_AUFENTHALT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AUT_GNW_AUFENTHALT` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PATIENT_ID` int(11) DEFAULT NULL,
  `PATIENT_NAME_AUFENTHALT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PATIENT_VORNAME_AUFENTHALT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PATIENT_GEBDATUM_AUFENTHALT` date DEFAULT NULL,
  `PATIENT_VERSICHNR_AUFENTHALT` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AUFNAHMEZEIT` datetime DEFAULT NULL,
  `ENTLASSUNGSZEIT` datetime DEFAULT NULL,
  `BETRIEBSSTELLEKEY` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BETRIEBSSTELLETEXT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AUFENTHALTSKEY` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AUFENTHALTSART` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AUFENTHALTSPFAD` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `BETRIEBSSTELLEKEY` (`BETRIEBSSTELLEKEY`,`AUFENTHALTSKEY`),
  KEY `PATIENT_ID` (`PATIENT_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AUT_GNW_AUFENTHALTABFRAGE`
--

DROP TABLE IF EXISTS `AUT_GNW_AUFENTHALTABFRAGE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AUT_GNW_AUFENTHALTABFRAGE` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYP` int(11) NOT NULL DEFAULT '-1',
  `BEFUNDABFRAGE_ID` int(11) NOT NULL DEFAULT '-1',
  `BETRIEBSSTELLEKEY` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AUFENTHALTSKEY` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MASTERSERVER_BETRKEY` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MASTERSERVER_EMAIL` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ANFRAGE_KENNUNG` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ANFRAGE_STATUS` int(11) NOT NULL DEFAULT '-1',
  `ANFRAGE_ZEIT` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ANFRAGE_KENNUNG` (`ANFRAGE_KENNUNG`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AUT_KONSULTATION`
--

DROP TABLE IF EXISTS `AUT_KONSULTATION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AUT_KONSULTATION` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `KSCHEIN_ID` int(11) NOT NULL DEFAULT '-1',
  `ABRECHNUNGSPERIODE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ABSTIMMUNGSBELEG` text COLLATE utf8_unicode_ci,
  `ANSPRUCHSART` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEARBEITUNGSDATUM` date DEFAULT NULL,
  `BEHANDLUNGSDATUM` date DEFAULT NULL,
  `BEHANDLUNGSFALLCODE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEZUGSBEREICH` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FACHGEBIETSCODE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KONSULTATIONSARTCODE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LIMITGEPRUEFT` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZUSTAENDIGESVT` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ORDINATIONSNR` int(11) DEFAULT NULL,
  `ORIGINALSVTCODE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STATUS` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SVNUMMER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ABGELEITETESVNR` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TXNNUMMER` bigint(20) DEFAULT NULL,
  `VERRECHNUNGSSVTNR` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERSICHERTENARTCODE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERSICHERTENKATEGORIE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERSION` int(11) DEFAULT NULL,
  `VERTRAGSPARTNERNR` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KSTANTEILBEFREIT` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OVERLIMIT` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RZPTGEBFREI` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STORNO` int(11) DEFAULT '-1',
  `BEARBEITUNGSUHRZEIT` time DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `KSCHEIN_ID` (`KSCHEIN_ID`),
  KEY `SVNUMMER` (`SVNUMMER`),
  KEY `BEHANDLUNGSDATUM` (`BEHANDLUNGSDATUM`),
  KEY `STATUS` (`STATUS`),
  KEY `CODE` (`KONSULTATIONSARTCODE`),
  KEY `BEARBEITUNGSUHRZEIT` (`BEARBEITUNGSUHRZEIT`),
  KEY `VERTRAGSPARTNERNR` (`VERTRAGSPARTNERNR`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AUT_KOSTENTRAEGER`
--

DROP TABLE IF EXISTS `AUT_KOSTENTRAEGER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AUT_KOSTENTRAEGER` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYP` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KENNUNG` int(11) DEFAULT NULL,
  `KENNUNG_S` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEZEICHNUNG` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GRUPPE` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KUERZEL` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NOTIZEN` text COLLATE utf8_unicode_ci,
  `STATUS` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GUELTIG_VON` date DEFAULT NULL,
  `GUELTIG_BIS` date DEFAULT NULL,
  `VERSION` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NIEDERLASSUNG` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BUNDESLAND` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SORTIERUNG` int(11) DEFAULT NULL,
  `PROTOKOLL` text COLLATE utf8_unicode_ci,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LETZTE_AENDERUNG_DATUM` date DEFAULT NULL,
  `LETZTE_AENDERUNG_UHRZEIT` time DEFAULT NULL,
  `STRASSE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PLZ` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ORT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `USTID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FALLPAUSCHALE` double DEFAULT NULL,
  `LEERERSCHEIN` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `KENNUNG_S` (`KENNUNG_S`),
  KEY `SORTIERUNG` (`SORTIERUNG`)
) ENGINE=MyISAM AUTO_INCREMENT=114 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AUT_KOSTENTRAEGER_BEWILLIGUNGSSTELLEN`
--

DROP TABLE IF EXISTS `AUT_KOSTENTRAEGER_BEWILLIGUNGSSTELLEN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AUT_KOSTENTRAEGER_BEWILLIGUNGSSTELLEN` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BEWILLIGUNGSSTELLEN_ID` int(11) NOT NULL,
  `AUT_KOSTENTRAEGER_ID` int(11) NOT NULL,
  `AUT_KSCHEIN_VERSICH_KATEGORIE_VON` int(11) NOT NULL DEFAULT '0',
  `AUT_KSCHEIN_VERSICH_KATEGORIE_BIS` int(11) NOT NULL DEFAULT '99',
  PRIMARY KEY (`ID`),
  KEY `BEWILLIGUNGSSTELLEN_ID` (`BEWILLIGUNGSSTELLEN_ID`),
  KEY `AUT_KOSTENTRAEGER_ID` (`AUT_KOSTENTRAEGER_ID`),
  KEY `AUT_KSCHEIN_VERSICH_KATEGORIE_VON` (`AUT_KSCHEIN_VERSICH_KATEGORIE_VON`),
  KEY `AUT_KSCHEIN_VERSICH_KATEGORIE_BIS` (`AUT_KSCHEIN_VERSICH_KATEGORIE_BIS`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AUT_KSCHEIN`
--

DROP TABLE IF EXISTS `AUT_KSCHEIN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AUT_KSCHEIN` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PATIENT_ID` int(11) NOT NULL DEFAULT '0',
  `ORGEINHEIT_ID` int(11) NOT NULL DEFAULT '0',
  `ERMAECHTIGUNG_ID` int(11) NOT NULL DEFAULT '0',
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STATUS` int(11) NOT NULL DEFAULT '0',
  `KSCHEIN_NR` int(11) NOT NULL DEFAULT '0',
  `ABGABE_DATUM` date DEFAULT NULL,
  `GUELTIGKEIT` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GUELTIGKEIT_JAHR` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KSCHEIN_ART` int(11) DEFAULT NULL,
  `VERSICH_KATEGORIE` int(11) DEFAULT NULL,
  `KENNZEICHEN` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZUW_NR` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZUW_KOM_ANREDE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZUW_ADRESSE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZUW_KOMMUNIKATION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZUW_FACHGEBIET` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UEBERW_DATUM` date DEFAULT NULL,
  `GRUVU` int(11) DEFAULT NULL,
  `KAUTION` double DEFAULT NULL,
  `KAUTION_WAEHRUNG` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KST_CODE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KASSE_ID` int(11) DEFAULT NULL,
  `KASSE_TYP` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KASSE_BLAND` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RZPTGEB_BIS` date DEFAULT NULL,
  `VERS_NEHM_VERS_NR` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERS_NEHM_GEBDATUM` date DEFAULT NULL,
  `VERS_NEHM_KOM_ANREDE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERS_NEHM_ADRESSE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERS_NEHM_KOMMUNIKATION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERW_VERH` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZUS_VERS_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DIENST_KOM_ANREDE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DIENST_ADRESSE` text COLLATE utf8_unicode_ci,
  `DIENST_KOMMUNIKATION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NOTIZEN` text COLLATE utf8_unicode_ci,
  `PROTOKOLL` text COLLATE utf8_unicode_ci,
  `PATIENTEN_ADRESSE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LETZTE_AENDERUNG` datetime DEFAULT NULL,
  `PROPERTIES` text COLLATE utf8_unicode_ci,
  `TYP` int(5) DEFAULT NULL,
  `ABR_MONAT` int(11) DEFAULT NULL,
  `ABR_JAHR` int(11) DEFAULT NULL,
  `VERTRAGSART` int(11) DEFAULT NULL,
  `ZUWEISER_ID` int(11) DEFAULT NULL,
  `ZUWEISUNGSTEXT` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `PATIENT_ID` (`PATIENT_ID`),
  KEY `ERMAECHTIGUNG_ID` (`ERMAECHTIGUNG_ID`),
  KEY `STATUS` (`STATUS`),
  KEY `ZEILE_LOCK` (`ZEILE_LOCK`),
  KEY `GUELTIGKEIT` (`GUELTIGKEIT`),
  KEY `GUELTIGKEIT_JAHR` (`GUELTIGKEIT_JAHR`),
  KEY `TYP` (`TYP`),
  KEY `ZUWEISER_ID` (`ZUWEISER_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=4134 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AUT_LABORSTAMM_ZIFFER_ZUORDNUNG`
--

DROP TABLE IF EXISTS `AUT_LABORSTAMM_ZIFFER_ZUORDNUNG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AUT_LABORSTAMM_ZIFFER_ZUORDNUNG` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `STAMMDATEN_ID` int(11) NOT NULL DEFAULT '0',
  `ANZAHL` int(11) NOT NULL DEFAULT '1',
  `TYP` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZIFFERN_KASSE_ID` int(11) DEFAULT NULL,
  `ZIFFERN_PRIVAT_ID` int(11) DEFAULT NULL,
  `STATUS` double DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERTRAGSART_KENNUNG` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `AUT_ZIFFERN_KASSE_ID` (`ZIFFERN_KASSE_ID`) USING BTREE,
  KEY `AUT_ZIFFERN_PRIVAT_ID` (`ZIFFERN_PRIVAT_ID`) USING BTREE,
  KEY `STAMMDATEN_ID` (`STAMMDATEN_ID`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AUT_THERAPIE_ZUORDNUNG`
--

DROP TABLE IF EXISTS `AUT_THERAPIE_ZUORDNUNG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AUT_THERAPIE_ZUORDNUNG` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PHYS_THERAPIE_ID` int(11) NOT NULL,
  `ANZAHL` int(11) NOT NULL DEFAULT '1',
  `TYP` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AUT_ZIFFERN_KASSE_ID` int(11) DEFAULT NULL,
  `ZIFFERN_PRIVAT_ID` int(11) DEFAULT NULL,
  `STATUS` double DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERTRAGSART_KENNUNG` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `PHYS_THERAPIE_ID` (`PHYS_THERAPIE_ID`),
  KEY `AUT_ZIFFERN_KASSE_ID` (`AUT_ZIFFERN_KASSE_ID`),
  KEY `AUT_ZIFFERN_PRIVAT_ID` (`ZIFFERN_PRIVAT_ID`),
  KEY `ZIFFERN_PRIVAT_ID` (`ZIFFERN_PRIVAT_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AUT_ZIFFERN_KASSE`
--

DROP TABLE IF EXISTS `AUT_ZIFFERN_KASSE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AUT_ZIFFERN_KASSE` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYP` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SUBTYP` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZIFFER` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ZIFFER_TEXT` mediumtext COLLATE utf8_unicode_ci,
  `ZIFFER_TEXT_KURZ` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PUNKTE` double NOT NULL DEFAULT '0',
  `GRUPPE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KUERZEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NOTIZEN` mediumtext COLLATE utf8_unicode_ci,
  `PROTOKOLL` mediumtext COLLATE utf8_unicode_ci,
  `STATUS` double DEFAULT NULL,
  `ZIFFER_VON` date DEFAULT NULL,
  `ZIFFER_BIS` date DEFAULT NULL,
  `BETRAG` double NOT NULL DEFAULT '0',
  `WAEHRUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERSION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZEITSTEMPEL` datetime DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `REGEL_L` mediumtext COLLATE utf8_unicode_ci,
  `ERSTATTUNG_1` double DEFAULT NULL,
  `ERSTATTUNG_2` double DEFAULT NULL,
  `FACHGEBIETE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BETRAG2` double DEFAULT NULL,
  `PUNKTWERT_KUERZEL` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EINSTELLUNGEN` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ZIFFER_START` (`ZIFFER`,`TYP`,`SUBTYP`,`ZIFFER_VON`),
  UNIQUE KEY `ZIFFER_ENDE` (`ZIFFER`,`TYP`,`SUBTYP`,`ZIFFER_BIS`),
  KEY `TYP` (`TYP`),
  KEY `SUBTYP` (`SUBTYP`),
  KEY `ZIFFER` (`ZIFFER`),
  KEY `ZIFFER_VON` (`ZIFFER_VON`),
  KEY `ZIFFER_BIS` (`ZIFFER_BIS`)
) ENGINE=MyISAM AUTO_INCREMENT=14494 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BAUSTEINE_TEXT`
--

DROP TABLE IF EXISTS `BAUSTEINE_TEXT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BAUSTEINE_TEXT` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYP` int(11) NOT NULL DEFAULT '-1',
  `POSITION` int(11) NOT NULL DEFAULT '0',
  `KUERZEL` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LANGTEXT` mediumtext COLLATE utf8_unicode_ci,
  `TB_KATEGORIE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WAHLARZT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KASSE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEARBEITER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MANDANTEN` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '-1',
  `BG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERTRAGSART` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SYSTEM_ZEIT` datetime DEFAULT NULL,
  `LISTE` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `TYP` (`TYP`),
  KEY `KUERZEL` (`KUERZEL`),
  KEY `VERTRAGSART` (`VERTRAGSART`),
  KEY `WAHLARZT` (`WAHLARZT`),
  KEY `KASSE` (`KASSE`),
  KEY `BG` (`BG`),
  KEY `MANDANTEN` (`MANDANTEN`),
  KEY `TB_KATEGORIE` (`TB_KATEGORIE`)
) ENGINE=MyISAM AUTO_INCREMENT=148 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BAUSTEINE_UNTERSUCHUNG`
--

DROP TABLE IF EXISTS `BAUSTEINE_UNTERSUCHUNG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BAUSTEINE_UNTERSUCHUNG` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ELTER_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KUERZEL` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LANGTEXT` mediumtext COLLATE utf8_unicode_ci,
  `TYP` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WAHLARZT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KASSE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZIFFER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DIAGNOSE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `THERAPIE` text COLLATE utf8_unicode_ci,
  `BEFUND` text COLLATE utf8_unicode_ci,
  `LETZTE_AENDERUNG` datetime DEFAULT NULL,
  `BEARBEITER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MANDANTEN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UNTERSUCHUNG` text COLLATE utf8_unicode_ci,
  `ANAMNESE` text COLLATE utf8_unicode_ci,
  `FORMULAR_1_ID` int(11) DEFAULT NULL,
  `EINSTELLUNGEN` text COLLATE utf8_unicode_ci,
  `ROENTGEN` text COLLATE utf8_unicode_ci,
  `SONO` text COLLATE utf8_unicode_ci,
  `FORMULAR_2_ID` int(11) DEFAULT NULL,
  `BG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERTRAGSART` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `REZEPT` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `KUERZEL` (`KUERZEL`),
  KEY `KASSE` (`KASSE`),
  KEY `WAHLARZT` (`WAHLARZT`),
  KEY `BG` (`BG`),
  KEY `VERTRAGSART` (`VERTRAGSART`),
  KEY `MANDANTEN` (`MANDANTEN`)
) ENGINE=MyISAM AUTO_INCREMENT=554 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BEHANDLUNGEN_EINZELLEISTUNGEN`
--

DROP TABLE IF EXISTS `BEHANDLUNGEN_EINZELLEISTUNGEN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BEHANDLUNGEN_EINZELLEISTUNGEN` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BEHANDLUNGEN_GRUPPEN_ID` int(11) NOT NULL DEFAULT '-1',
  `THERAPIE_ID` int(11) NOT NULL DEFAULT '-1',
  `REGION_ID` int(11) DEFAULT NULL,
  `BE_BEMERKUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERTRAGSART` int(11) DEFAULT NULL,
  `BE_REIHE` smallint(6) NOT NULL DEFAULT '-1',
  `BE_PRIVATPREIS` double DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `BEHANDLUNGEN_GRUPPEN_ID` (`BEHANDLUNGEN_GRUPPEN_ID`),
  KEY `THERAPIE_ID` (`THERAPIE_ID`),
  KEY `REGION_ID` (`REGION_ID`),
  KEY `BE_REIHE` (`BE_REIHE`),
  KEY `VERTRAGSART` (`VERTRAGSART`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BEHANDLUNGEN_GRUPPEN`
--

DROP TABLE IF EXISTS `BEHANDLUNGEN_GRUPPEN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BEHANDLUNGEN_GRUPPEN` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DOKU_ID` int(11) NOT NULL DEFAULT '-1',
  `UNTERSUCHUNG_ID` int(11) NOT NULL DEFAULT '-1',
  `BG_ANZAHL` smallint(6) NOT NULL DEFAULT '10',
  `BG_ERHALTEN` smallint(6) NOT NULL DEFAULT '0',
  `BG_STATUS` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `BG_VORIGE_ID` int(11) DEFAULT NULL,
  `BENUTZER_ID` int(11) DEFAULT NULL,
  `BG_AKTIVIERBAR_BIS` datetime DEFAULT NULL,
  `BG_AENR` smallint(6) NOT NULL DEFAULT '0',
  `BG_NR` smallint(6) NOT NULL DEFAULT '1',
  `BG_NAME` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FAXSTATUS` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEARBEITUNGS_STATUS` int(11) NOT NULL DEFAULT '-1',
  `BEARBEITUNGS_INSTANZ` int(11) NOT NULL DEFAULT '1',
  `BEWILLIGUNGS_STATUS` int(11) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`ID`),
  KEY `PHYSIOTHERAPIEDOKU_ID` (`DOKU_ID`),
  KEY `UNTERSUCHUNG_ID` (`UNTERSUCHUNG_ID`),
  KEY `BG_STATUS` (`BG_STATUS`),
  KEY `BG_VORIGE_ID` (`BG_VORIGE_ID`),
  KEY `FAXSTATUS` (`FAXSTATUS`),
  KEY `BENUTZER_ID` (`BENUTZER_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BEHANDLUNGSPLANEINHEIT`
--

DROP TABLE IF EXISTS `BEHANDLUNGSPLANEINHEIT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BEHANDLUNGSPLANEINHEIT` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BEHANDLUNGSPLANDOKU_ID` int(11) DEFAULT NULL,
  `DATEN` text COLLATE utf8_unicode_ci,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `BEHANDLUNGSPLANDOKU_ID` (`BEHANDLUNGSPLANDOKU_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BEHANDLUNGSPLANEINHEIT_STAMM`
--

DROP TABLE IF EXISTS `BEHANDLUNGSPLANEINHEIT_STAMM`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BEHANDLUNGSPLANEINHEIT_STAMM` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BEHANDLUNGSPLAN_ID` int(11) DEFAULT NULL,
  `KUERZEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TYP` int(11) DEFAULT NULL,
  `SORTIERUNG` tinyint(4) DEFAULT NULL,
  `BESCHREIBUNG` text COLLATE utf8_unicode_ci,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `KUERZEL_BEHANDLUNGSPLAN_ID` (`KUERZEL`,`BEHANDLUNGSPLAN_ID`),
  KEY `TYP` (`TYP`),
  KEY `BEHANDLUNGSPLAN_ID` (`BEHANDLUNGSPLAN_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BEHANDLUNGSPLAN_STAMM`
--

DROP TABLE IF EXISTS `BEHANDLUNGSPLAN_STAMM`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BEHANDLUNGSPLAN_STAMM` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BEZEICHNUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MANDANT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `MANDANT` (`MANDANT`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BENUTZER`
--

DROP TABLE IF EXISTS `BENUTZER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BENUTZER` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `KUE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ANREDE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TITEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VORNAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WVORNAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GEBNAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAMENSZUSATZ` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAMENSVORSATZ` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GEBDATUM` date DEFAULT NULL,
  `BILD` blob,
  `NOTIZEN` text COLLATE utf8_unicode_ci,
  `PROTOKOLL` text COLLATE utf8_unicode_ci,
  `STRASSE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PLZ` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ORT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAND` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Adresse_ID1` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Adresse_ID2` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Adresse_ID3` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Adresse_ID4` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TELEFON` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TELEFON_2` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FAX` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MOBIL` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EMAIL` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BANK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BANK_PLZ` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BANK_ORT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BANK_LAND` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KTO_NR` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BLZ` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KTO_INHABER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STATUS` int(11) NOT NULL DEFAULT '0',
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KENNWORT` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LETZTE_KENNWOERTER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GUELTIG_AB` date DEFAULT NULL,
  `GUELTIG_BIS` date DEFAULT NULL,
  `ORGANISATION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MANDANT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GRUPPE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EINSTELLUNG` mediumtext COLLATE utf8_unicode_ci,
  `SPRACHE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WERKZEUGLEISTE` mediumtext COLLATE utf8_unicode_ci,
  `ARZTNUMMER` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UNTERSCHRIFT_BILDPFAD` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `KUE` (`KUE`),
  KEY `STATUS` (`STATUS`),
  KEY `GRUPPE` (`GRUPPE`),
  KEY `MANDANT` (`MANDANT`),
  KEY `ARZTNUMMER` (`ARZTNUMMER`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BERECHTIGUNG`
--

DROP TABLE IF EXISTS `BERECHTIGUNG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BERECHTIGUNG` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ROLE` int(11) NOT NULL DEFAULT '0',
  `BENUTZER_ID` int(11) NOT NULL DEFAULT '0',
  `ERMAECHTIGUNG_ID` int(11) NOT NULL DEFAULT '0',
  `ORGANISATION_ID` int(11) NOT NULL DEFAULT '0',
  `AKTION` int(11) NOT NULL DEFAULT '0',
  `BERECHTIGUNG` int(11) NOT NULL DEFAULT '0',
  `BEZEICHNUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BEWILLIGUNGSLISTE_FAX_BARCODE_ZAEHLER`
--

DROP TABLE IF EXISTS `BEWILLIGUNGSLISTE_FAX_BARCODE_ZAEHLER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BEWILLIGUNGSLISTE_FAX_BARCODE_ZAEHLER` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FAX_BARCODE` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BEWILLIGUNGSSTELLEN`
--

DROP TABLE IF EXISTS `BEWILLIGUNGSSTELLEN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BEWILLIGUNGSSTELLEN` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `KUERZEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEZEICHNUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FAXNUMMER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FORMULARE_ID` int(11) DEFAULT NULL,
  `KOSTENTRAEGER_POSITIONSNUMMERN_ERMITTELN` tinyint(4) NOT NULL DEFAULT '0',
  `UHRZEIT_ERSTES_FAX` datetime DEFAULT NULL,
  `UHRZEIT_LETZTES_FAX` datetime DEFAULT NULL,
  `WIEDERHOLUNG_INTERVALL` int(11) DEFAULT NULL,
  `AUTO_VOR_ERSTER_LEISTUNG` tinyint(4) NOT NULL DEFAULT '1',
  `AUTO_BUNDESLAND_NICHT` tinyint(4) NOT NULL DEFAULT '0',
  `AUTO_MIN_BEHANDLUNGEN_KALENDERJAHR` int(11) NOT NULL DEFAULT '0',
  `AUTO_EINZELLEISTUNGEN_PRO_BEHANDLUNG_KALENDERJAHR` int(11) NOT NULL DEFAULT '0',
  `AUTOMATISCH_FAXEN` tinyint(4) NOT NULL DEFAULT '1',
  `SAMMELSICHT` tinyint(4) NOT NULL DEFAULT '1',
  `STATUS` int(11) NOT NULL DEFAULT '-1',
  `BENUTZER_ID` int(11) DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FORMULARE_ID` (`FORMULARE_ID`),
  KEY `STATUS` (`STATUS`),
  KEY `BENUTZER_ID` (`BENUTZER_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BLZ_AUT`
--

DROP TABLE IF EXISTS `BLZ_AUT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BLZ_AUT` (
  `Lfd_Nr` int(11) NOT NULL AUTO_INCREMENT,
  `BLZ` double DEFAULT NULL,
  `EIGEN_BLZ` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BISHERIGE_BLZ` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LZB_GIROKONTO` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LOESCHUNGSDATUM` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HINWEIS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEZEICHNUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KURZBEZEICHNUNG` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STRASSE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PLZ` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ORT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BUNDESLAND` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TELEFON` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FAX` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BTX_BEZEICHNUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PAN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VEROEF` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BIC` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRUEFZIFFER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`Lfd_Nr`),
  KEY `BLZ` (`BLZ`),
  KEY `KURZBEZEICHNUNG` (`KURZBEZEICHNUNG`)
) ENGINE=MyISAM AUTO_INCREMENT=5558 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BLZ_DE`
--

DROP TABLE IF EXISTS `BLZ_DE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BLZ_DE` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BLZ` int(11) DEFAULT NULL,
  `MERKMAL` int(11) DEFAULT NULL,
  `BEZEICHNUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PLZ` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ORT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KURZBEZEICHNUNG` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PAN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BIC` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRUEFZIFFER` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATENSATZ_ID` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AENDERUNG_KZ` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LOESCHUNG` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NACHFOLGE_BLZ` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HINWEIS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TELEFON` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FAX` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `BLZ` (`BLZ`),
  KEY `KURZBEZEICHNUNG` (`KURZBEZEICHNUNG`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CDA_DOKUMENT`
--

DROP TABLE IF EXISTS `CDA_DOKUMENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CDA_DOKUMENT` (
  `DOKUMENT_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATUM` datetime DEFAULT NULL,
  `FORMAT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STATUS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PATIENT_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`DOKUMENT_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `COMBO`
--

DROP TABLE IF EXISTS `COMBO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `COMBO` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TEXT_KURZ_DE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TEXT_KURZ_EN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TEXT_KURZ_FR` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TEXT_KURZ_SP` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TEXT_DE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TEXT_EN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TEXT_FR` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TEXT_SP` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KENNUNG` int(11) DEFAULT NULL,
  `ZUSATZ_1` mediumtext COLLATE utf8_unicode_ci,
  `ZUSATZ_2` mediumtext COLLATE utf8_unicode_ci,
  `ZUSATZ_3` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZUSATZ_4` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KENNUNG_S` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `KENNUNGS_S` (`NAME`,`KENNUNG_S`),
  UNIQUE KEY `1_KENNUNG_S` (`NAME`,`KENNUNG_S`),
  UNIQUE KEY `1_KENNUNG` (`NAME`,`KENNUNG`),
  KEY `NAME` (`NAME`),
  KEY `TEXT_KURZ_DE` (`TEXT_KURZ_DE`),
  KEY `KENNUNG` (`KENNUNG`),
  KEY `KENNUNG_S` (`KENNUNG_S`)
) ENGINE=MyISAM AUTO_INCREMENT=25421 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DERMATEST`
--

DROP TABLE IF EXISTS `DERMATEST`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DERMATEST` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PATIENT_ID` int(11) DEFAULT NULL,
  `UNTERSUCHUNG_ID` int(11) DEFAULT NULL,
  `TYP` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATUM` date DEFAULT NULL,
  `TEST` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TESTERGEBNIS` int(11) DEFAULT NULL,
  `BEMERKUNG` text COLLATE utf8_unicode_ci,
  `FREITEXT1` text COLLATE utf8_unicode_ci,
  `FREITEXT2` text COLLATE utf8_unicode_ci,
  `STATUS` int(11) DEFAULT NULL,
  `SYSTEM_DATUM` date DEFAULT NULL,
  `SYSTEM_UHRZEIT` time DEFAULT NULL,
  `BEARBEITER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROTOKOLL` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DIAGNOSE`
--

DROP TABLE IF EXISTS `DIAGNOSE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DIAGNOSE` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PATIENT_ID` int(11) NOT NULL DEFAULT '-1',
  `DIAGNOSEDOKU_ID` int(11) NOT NULL DEFAULT '-1',
  `UNTERSUCHUNG_ID` int(11) NOT NULL DEFAULT '-1',
  `TYP` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STATUS` int(11) NOT NULL DEFAULT '-1',
  `DATUM` date DEFAULT NULL,
  `UHRZEIT` time DEFAULT NULL,
  `SCHLUESSEL` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DIAGNOSE_TEXT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DIAGNOSE_DATUM` date DEFAULT NULL,
  `DIAGNOSE_UHRZEIT` time DEFAULT NULL,
  `BEARBEITER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROTOKOLL` text COLLATE utf8_unicode_ci,
  `NOTIZEN` text COLLATE utf8_unicode_ci,
  `KUERZEL` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZUSATZ` text COLLATE utf8_unicode_ci,
  `SICHERHEIT` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LOKALISATION` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ERLAUTERUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SCHLUESSEL_TYP` int(10) NOT NULL DEFAULT '0',
  `SORT` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `DIAGNOSEDOKU_ID` (`DIAGNOSEDOKU_ID`),
  KEY `UNTERSUCHUNG_ID` (`UNTERSUCHUNG_ID`),
  KEY `PATIENT_ID` (`PATIENT_ID`),
  KEY `ZEILE_LOCK` (`ZEILE_LOCK`),
  KEY `TYP` (`TYP`),
  KEY `DATUM` (`DATUM`),
  KEY `STATUS` (`STATUS`),
  KEY `SCHLUESSEL_TYP` (`SCHLUESSEL_TYP`)
) ENGINE=MyISAM AUTO_INCREMENT=16674 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DOKUMENTATION`
--

DROP TABLE IF EXISTS `DOKUMENTATION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DOKUMENTATION` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYP` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FREMD_ID` int(11) NOT NULL DEFAULT '0',
  `PATIENT_ID` int(11) NOT NULL DEFAULT '0',
  `FALL_NR` int(11) NOT NULL DEFAULT '0',
  `UNTERSUCHUNG_NR` int(11) NOT NULL DEFAULT '0',
  `ORGEINHEIT_ID` int(11) NOT NULL DEFAULT '0',
  `ERMAECHTIGUNG_ID` int(11) NOT NULL DEFAULT '0',
  `KSCHEIN_ID` int(11) NOT NULL DEFAULT '0',
  `DATUM` date DEFAULT NULL,
  `DOKU_TEXT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROTOKOLL` text COLLATE utf8_unicode_ci,
  `NOTIZEN` text COLLATE utf8_unicode_ci,
  `STATUS` int(11) NOT NULL DEFAULT '0',
  `KENNWORT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FALL_ZAEHLER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UNTERS_ZAEHLER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FREITEXT1` text COLLATE utf8_unicode_ci,
  `FREITEXT2` text COLLATE utf8_unicode_ci,
  `FREITEXT3` text COLLATE utf8_unicode_ci,
  `FREITEXT4` text COLLATE utf8_unicode_ci,
  `DOKU_PROPERTIES` text COLLATE utf8_unicode_ci,
  `BEARBEITER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ARZT1` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ARZT2` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SUBTYP` int(11) NOT NULL DEFAULT '0',
  `LETZTE_AENDERUNG_DATUM` date DEFAULT NULL,
  `LETZTE_AENDERUNG_UHRZEIT` time DEFAULT NULL,
  `ABRECHNUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DOKU_UHRZEIT` time DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UNTERS_ZAEHLER` (`UNTERS_ZAEHLER`),
  UNIQUE KEY `FALL_ZAEHLER` (`FALL_ZAEHLER`),
  KEY `FREMD_ID` (`FREMD_ID`),
  KEY `PATIENT_ID` (`PATIENT_ID`),
  KEY `KSCHEIN_ID` (`KSCHEIN_ID`),
  KEY `STATUS` (`STATUS`),
  KEY `ZEILE_LOCK` (`ZEILE_LOCK`),
  KEY `TYP` (`TYP`),
  KEY `DATUM` (`DATUM`),
  KEY `ERMAECHTIGUNG_ID` (`ERMAECHTIGUNG_ID`),
  KEY `ABRECHNUNG` (`ABRECHNUNG`),
  KEY `ARZT1` (`ARZT1`),
  KEY `ARZT2` (`ARZT2`),
  KEY `SUBTYP` (`SUBTYP`)
) ENGINE=MyISAM AUTO_INCREMENT=57539 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DOKU_TYP`
--

DROP TABLE IF EXISTS `DOKU_TYP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DOKU_TYP` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DOKU_TYP` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DOKU_SORTIERUNG` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=199 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DRUCKEREINSTELLUNG`
--

DROP TABLE IF EXISTS `DRUCKEREINSTELLUNG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DRUCKEREINSTELLUNG` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYP` int(11) NOT NULL DEFAULT '0',
  `REFERENZ_ID` int(11) NOT NULL DEFAULT '0',
  `RECHNER_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `DRUCKEREINSTELLUNG` text COLLATE utf8_unicode_ci,
  `BEARBEITER` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `LETZTE_AENDERUNG` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `PROTOKOLL` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `REFERENZ_ID` (`REFERENZ_ID`),
  KEY `TYP` (`TYP`)
) ENGINE=MyISAM AUTO_INCREMENT=124 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `EHMV_ATC`
--

DROP TABLE IF EXISTS `EHMV_ATC`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EHMV_ATC` (
  `ATC` varchar(7) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `Bedeutung` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ATC`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `EHMV_HINWEIS`
--

DROP TABLE IF EXISTS `EHMV_HINWEIS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EHMV_HINWEIS` (
  `Pharmanummer` int(11) NOT NULL DEFAULT '0',
  `Hinweise` mediumtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`Pharmanummer`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `EHMV_INDTEXT`
--

DROP TABLE IF EXISTS `EHMV_INDTEXT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EHMV_INDTEXT` (
  `Pharmanummer` int(11) NOT NULL DEFAULT '0',
  `Kriterien` mediumtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`Pharmanummer`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `EHMV_MEDIKAMENT`
--

DROP TABLE IF EXISTS `EHMV_MEDIKAMENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EHMV_MEDIKAMENT` (
  `Pharmanummer` int(11) NOT NULL DEFAULT '0',
  `RegNrPre` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RegNr` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RegNrEU` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Box` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Kassenzeichen` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Menge` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Mengenart` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KVP` varchar(9) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KVP_Einheit` varchar(9) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Darreichung` varchar(9) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Teilbarkeit` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Refaktie` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Abgabeanzahl` int(3) DEFAULT NULL,
  `Hinweis` varchar(28) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Langzeitbewilligung` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Suchtgift` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `Pharmanummer` (`Pharmanummer`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `EHMV_PHARMA`
--

DROP TABLE IF EXISTS `EHMV_PHARMA`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EHMV_PHARMA` (
  `Pharmanummer` int(11) DEFAULT NULL,
  `Position` int(11) DEFAULT NULL,
  `PharmanummerVgl` int(11) DEFAULT NULL,
  `PositionVgl` int(11) DEFAULT NULL,
  `KennzeichenVgl` int(11) DEFAULT NULL,
  KEY `Pharmanummer` (`Pharmanummer`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `EHMV_REGELTEXTE`
--

DROP TABLE IF EXISTS `EHMV_REGELTEXTE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EHMV_REGELTEXTE` (
  `Pharmanummer` int(11) NOT NULL DEFAULT '-1',
  `Hinweis` mediumtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`Pharmanummer`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `EHMV_WIRKSTOFF`
--

DROP TABLE IF EXISTS `EHMV_WIRKSTOFF`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EHMV_WIRKSTOFF` (
  `Pharmanummer` int(7) NOT NULL DEFAULT '0',
  `ATC_WHO` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Laufnummer` int(2) DEFAULT NULL,
  `ATC_ERG` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Staerke` double(20,10) DEFAULT NULL,
  `Dimension` varchar(9) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Eigenschaft` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `Pharmanummer` (`Pharmanummer`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `EMAIL`
--

DROP TABLE IF EXISTS `EMAIL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EMAIL` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EMAILDOKU_ID` int(11) DEFAULT NULL,
  `TYP` int(11) DEFAULT NULL,
  `ZEIT` datetime DEFAULT NULL,
  `CONTENT_TYPE` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '-1',
  `MIME_VERSION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ABSENDER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EMPFAENGER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MESSAGE_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BETREFF` text COLLATE utf8_unicode_ci,
  `NACHRICHT_TEXT` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `EMAILDOKU_ID` (`EMAILDOKU_ID`) USING BTREE,
  KEY `DATUM` (`ZEIT`),
  KEY `ABSENDER` (`ABSENDER`),
  KEY `EMPFAENGER` (`EMPFAENGER`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ERINNERUNG_TERMIN`
--

DROP TABLE IF EXISTS `ERINNERUNG_TERMIN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ERINNERUNG_TERMIN` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MANDANT_ID` int(11) DEFAULT NULL,
  `PATIENT_ID` int(11) DEFAULT NULL,
  `TYP` int(11) DEFAULT NULL,
  `DATUM` date DEFAULT NULL,
  `BEZEICHNUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEMERKUNG` mediumtext COLLATE utf8_unicode_ci,
  `STATUS` int(11) DEFAULT NULL,
  `SYSTEM_ZEIT` datetime DEFAULT NULL,
  `ANLAGE_ZEIT` datetime DEFAULT NULL,
  `BEARBEITER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROTOKOLL` mediumtext COLLATE utf8_unicode_ci,
  `ERINNERUNG_KENNUNG` int(11) DEFAULT NULL,
  `REFERENZDOKU_ID` int(11) DEFAULT NULL,
  `WILL_EMAIL_ERINNERUNG` tinyint(4) DEFAULT NULL,
  `WILL_SMS_ERINNERUNG` tinyint(4) DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `PATIENT_ID` (`PATIENT_ID`),
  KEY `TYP` (`TYP`),
  KEY `ERINNERUNG_KENNUNG` (`ERINNERUNG_KENNUNG`)
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `FACHBEFUND`
--

DROP TABLE IF EXISTS `FACHBEFUND`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FACHBEFUND` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PATIENT_ID` int(11) NOT NULL DEFAULT '-1',
  `DOKU_ID` int(11) NOT NULL DEFAULT '-1',
  `UNTERSUCHUNG_ID` int(11) NOT NULL DEFAULT '-1',
  `TYP` int(11) DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STATUS` int(11) DEFAULT NULL,
  `ANSCHRIFT` text COLLATE utf8_unicode_ci,
  `EMAIL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEGRUESSUNG` text COLLATE utf8_unicode_ci,
  `EINLEITUNG` text COLLATE utf8_unicode_ci,
  `ANAMNESE` text COLLATE utf8_unicode_ci,
  `TEXT_1` text COLLATE utf8_unicode_ci,
  `TEXT_2` text COLLATE utf8_unicode_ci,
  `TEXT_3` text COLLATE utf8_unicode_ci,
  `TEXT_4` text COLLATE utf8_unicode_ci,
  `TEXT_5` text COLLATE utf8_unicode_ci,
  `TEXT_6` text COLLATE utf8_unicode_ci,
  `TEXT_7` text COLLATE utf8_unicode_ci,
  `TEXT_8` text COLLATE utf8_unicode_ci,
  `TEXT_9` text COLLATE utf8_unicode_ci,
  `TEXT_10` text COLLATE utf8_unicode_ci,
  `TEXT_11` text COLLATE utf8_unicode_ci,
  `TEXT_12` text COLLATE utf8_unicode_ci,
  `TEXT_13` text COLLATE utf8_unicode_ci,
  `TEXT_14` text COLLATE utf8_unicode_ci,
  `TEXT_15` text COLLATE utf8_unicode_ci,
  `TEXT_16` text COLLATE utf8_unicode_ci,
  `SCHLUSSFORMEL` text COLLATE utf8_unicode_ci,
  `BEARBEITER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NOTIZEN` text COLLATE utf8_unicode_ci,
  `PROTOKOLL` text COLLATE utf8_unicode_ci,
  `LETZTE_AENDERUNG_DATUM` date DEFAULT NULL,
  `LETZTE_AENDERUNG_UHRZEIT` time DEFAULT NULL,
  `ZUSATZ` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `DIAGNOSEDOKU_ID` (`DOKU_ID`),
  KEY `UNTERSUCHUNG_ID` (`UNTERSUCHUNG_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `FALLNUMMER`
--

DROP TABLE IF EXISTS `FALLNUMMER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FALLNUMMER` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FALLNUMMER` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `PATIENT_ID` int(11) NOT NULL DEFAULT '-1',
  `AUFNAHME_ZEIT` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `FALLNUMMER` (`FALLNUMMER`,`PATIENT_ID`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `FIRMA`
--

DROP TABLE IF EXISTS `FIRMA`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FIRMA` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ABTEILUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KURZNAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KUERZEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ADRESSE` int(11) NOT NULL DEFAULT '-1',
  `POST_ADRESSE` int(11) NOT NULL DEFAULT '-1',
  `LIEFER_ADRESSE` int(11) NOT NULL DEFAULT '-1',
  `RECHNUNG_ADRESSE` int(11) NOT NULL DEFAULT '-1',
  `KOMMUNIKATION` int(11) NOT NULL DEFAULT '-1',
  `KONTO` int(11) NOT NULL DEFAULT '-1',
  `ANSPRECHPARTNER` int(11) NOT NULL DEFAULT '-1',
  `NOTIZEN` text COLLATE utf8_unicode_ci,
  `PROTOKOLL` text COLLATE utf8_unicode_ci,
  `LETZTE_AENDERUNG` datetime DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STATUS` int(11) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`ID`),
  KEY `NAME` (`NAME`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `FORMULARE`
--

DROP TABLE IF EXISTS `FORMULARE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FORMULARE` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ACETO_NR` int(11) DEFAULT NULL,
  `POSITION` int(11) DEFAULT NULL,
  `NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BESCHREIBUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `REPORT` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FARBE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LOGO` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EINSTELLUNGEN` mediumtext COLLATE utf8_unicode_ci,
  `FELDER` mediumtext COLLATE utf8_unicode_ci,
  `WAHLARZT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KASSE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MANDANTEN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEARBEITER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROTOKOLL` text COLLATE utf8_unicode_ci,
  `LETZTE_AENDERUNG` datetime DEFAULT NULL,
  `SCHNELL_KNOPF` tinyint(4) DEFAULT NULL,
  `TYP` int(11) DEFAULT NULL,
  `VERSION` double DEFAULT NULL,
  `PDFTITEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PDFDOKUMENTFORMAT` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PDFCSSEXPERT` text COLLATE utf8_unicode_ci,
  `HTMLDOKUMENT` text COLLATE utf8_unicode_ci,
  `LOGO_SEITE2` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NOTIZEN` text COLLATE utf8_unicode_ci,
  `DRUCK_CSS` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EMAIL_CSS` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FAX_CSS` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SERIENBRIEF_TYPEN` text COLLATE utf8_unicode_ci,
  `SERIENBRIEF_ADRESSAT_TYP` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERTRAGSART` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SPRACHE` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GRUPPE` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SCHNELL_KNOPF` (`SCHNELL_KNOPF`),
  UNIQUE KEY `ACETO_NR` (`ACETO_NR`),
  KEY `POSITION` (`POSITION`),
  KEY `KASSE` (`KASSE`),
  KEY `MANDANTEN` (`MANDANTEN`),
  KEY `WAHLARZT` (`WAHLARZT`),
  KEY `VERTRAGSART` (`VERTRAGSART`),
  KEY `SPRACHE` (`SPRACHE`),
  KEY `GRUPPE` (`GRUPPE`)
) ENGINE=MyISAM AUTO_INCREMENT=1221 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `FTP_ARCHIV`
--

DROP TABLE IF EXISTS `FTP_ARCHIV`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FTP_ARCHIV` (
  `FTP_ARCHIV_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATEI_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PFAD` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZUGRIFF_VON` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATUM_ZUGRIFF` date DEFAULT NULL,
  `UHRZEIT_ZUGRIFF` time DEFAULT NULL,
  `ZUGRIFF_COMPUTERNAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AKTION` int(11) DEFAULT NULL,
  PRIMARY KEY (`FTP_ARCHIV_ID`),
  KEY `DATEI_NAME` (`DATEI_NAME`),
  KEY `PFAD` (`PFAD`),
  KEY `ZEILE_LOCK` (`ZEILE_LOCK`),
  KEY `ZUGRIFF_VON` (`ZUGRIFF_VON`),
  KEY `ZUGRIFF_COMPUTERNAME` (`ZUGRIFF_COMPUTERNAME`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `GERAETE`
--

DROP TABLE IF EXISTS `GERAETE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GERAETE` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GT_TYP` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GR_KUERZEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEZEICHNUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GR_BESCHREIBUNG` text COLLATE utf8_unicode_ci,
  `GRUPPE` int(11) DEFAULT NULL,
  `ERMAECHTIGUNGEN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEARBEITER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LETZTE_AENDERUNG` datetime DEFAULT NULL,
  `PROTOKOLL` text COLLATE utf8_unicode_ci,
  `STATUS` int(11) DEFAULT '-1',
  `GR_TIMESTART` datetime NOT NULL DEFAULT '1899-12-30 07:00:00',
  `GR_RECHENRASTER` int(11) NOT NULL DEFAULT '30',
  `GR_GM` int(11) NOT NULL DEFAULT '30',
  `MODALITAET` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `KUERZEL` (`GR_KUERZEL`),
  KEY `GT_TYP` (`GT_TYP`),
  KEY `ZEILE_LOCK` (`ZEILE_LOCK`),
  KEY `MODALITAET` (`MODALITAET`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `GERAETE_CODE`
--

DROP TABLE IF EXISTS `GERAETE_CODE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GERAETE_CODE` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GERAET_ID` int(11) DEFAULT NULL,
  `PROCEDURE_CODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BESCHREIBUNG` text COLLATE utf8_unicode_ci,
  `LETZTE_AENDERUNG` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `GERAET_ID` (`GERAET_ID`,`PROCEDURE_CODE`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `GERAETE_TYPEN`
--

DROP TABLE IF EXISTS `GERAETE_TYPEN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GERAETE_TYPEN` (
  `GT_TYP` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`GT_TYP`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `GRAFIK`
--

DROP TABLE IF EXISTS `GRAFIK`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GRAFIK` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PATIENT_ID` int(11) DEFAULT NULL,
  `TYP` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATUM` date DEFAULT NULL,
  `BILD` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `POSITION` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MARKIERUNG_TEXT` text COLLATE utf8_unicode_ci,
  `FREITEXT1` text COLLATE utf8_unicode_ci,
  `FREITEXT2` text COLLATE utf8_unicode_ci,
  `STATUS` int(11) DEFAULT NULL,
  `SYSTEM_DATUM` date DEFAULT NULL,
  `SYSTEM_UHRZEIT` time DEFAULT NULL,
  `BEARBEITER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROTOKOLL` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `GRUPPEN`
--

DROP TABLE IF EXISTS `GRUPPEN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GRUPPEN` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYP` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STATUS` int(11) DEFAULT NULL,
  `KENNUNG` int(11) DEFAULT '-1',
  `BESCHREIBUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `NAME` (`NAME`),
  KEY `KENNUNG` (`KENNUNG`)
) ENGINE=MyISAM AUTO_INCREMENT=47 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `HEILMITTEL_STAMM`
--

DROP TABLE IF EXISTS `HEILMITTEL_STAMM`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HEILMITTEL_STAMM` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYP` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `POSITION` int(11) DEFAULT NULL,
  `KUERZEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEZEICHNUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GRUPPENTHERAPIE` tinyint(4) DEFAULT NULL,
  `STATUS` int(11) DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LETZTE_AENDERUNG` datetime DEFAULT NULL,
  `BEARBEITER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WAHLARZT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERTRAGSART` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KASSE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MANDANTEN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `KUERZEL` (`KUERZEL`,`TYP`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `HILFSMITTEL_STAMM`
--

DROP TABLE IF EXISTS `HILFSMITTEL_STAMM`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HILFSMITTEL_STAMM` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `POSITION` int(5) NOT NULL DEFAULT '-1',
  `TYP` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KUERZEL` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `BEZEICHNUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `REZEPTUR` mediumtext COLLATE utf8_unicode_ci,
  `STATUS` int(11) DEFAULT NULL,
  `LETZTE_AENDERUNG_DATUM` date DEFAULT NULL,
  `LETZTE_AENDERUNG_UHRZEIT` time DEFAULT NULL,
  `BEARBEITER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROTOKOLL` mediumtext COLLATE utf8_unicode_ci,
  `DOSISSCHEMA` text COLLATE utf8_unicode_ci,
  `MANDANTEN` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '-1',
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KASSE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRIVAT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EINSTELLUNG` text COLLATE utf8_unicode_ci,
  `MEDIKAMENT_BOX` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERTRAGSART` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `KUERZEL` (`KUERZEL`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `HISTORIE`
--

DROP TABLE IF EXISTS `HISTORIE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HISTORIE` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DOKU_TYP` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DOKU_ID` int(11) DEFAULT NULL,
  `ZEIT` datetime DEFAULT NULL,
  `BENUTZER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `DOKU_ID` (`DOKU_ID`),
  KEY `BENUTZER` (`BENUTZER`),
  KEY `ZEIT` (`ZEIT`),
  KEY `DOKU_TYP` (`DOKU_TYP`)
) ENGINE=MyISAM AUTO_INCREMENT=43734 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `HISTO_BEFUNDE`
--

DROP TABLE IF EXISTS `HISTO_BEFUNDE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HISTO_BEFUNDE` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PATIENT_ID` int(11) DEFAULT NULL,
  `UNTERSUCHUNG_ID` int(11) DEFAULT NULL,
  `TYP` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATUM` date DEFAULT NULL,
  `ABSTRICH_FREITEXT` mediumtext COLLATE utf8_unicode_ci,
  `BEFUND_ERGEBNIS` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEFUND_FREITEXT` mediumtext COLLATE utf8_unicode_ci,
  `BEMERKUNG` mediumtext COLLATE utf8_unicode_ci,
  `STATUS` int(11) DEFAULT NULL,
  `SYSTEM_DATUM` date DEFAULT NULL,
  `SYSTEM_UHRZEIT` time DEFAULT NULL,
  `BEARBEITER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROTOKOLL` mediumtext COLLATE utf8_unicode_ci,
  `ERMAECHTIGUNG_ID` tinyint(5) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `HL7_AUFTRAGSDATEN`
--

DROP TABLE IF EXISTS `HL7_AUFTRAGSDATEN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HL7_AUFTRAGSDATEN` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYP` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TRIGGER_EVENT` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MESSAGE_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATEINAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PATIENT_ID` int(11) DEFAULT NULL,
  `LETZTE_AENDERUNG` datetime DEFAULT NULL,
  `MESSAGE` text COLLATE utf8_unicode_ci,
  `STATUS` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `DATEISTAMMNAME` (`DATEINAME`),
  KEY `PATIENT_ID` (`PATIENT_ID`),
  KEY `MESSAGE_TYP` (`TRIGGER_EVENT`),
  KEY `TYP` (`TYP`),
  KEY `TRIGGER_EVENT` (`TRIGGER_EVENT`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `HL7_LEISTUNGSDATEN`
--

DROP TABLE IF EXISTS `HL7_LEISTUNGSDATEN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HL7_LEISTUNGSDATEN` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYP` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TRIGGER_EVENT` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SEGMENT` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PATIENT_ID` int(11) NOT NULL DEFAULT '-1',
  `DATUM` date DEFAULT '0000-00-00',
  `PRODUCER_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ERGEBNIS` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ERGEBNIS_TYP` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ERGEBNIS_IDENTIFIER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ERGEBNIS_STATUS` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ERGEBNIS_EINHEIT` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BESCHREIBUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STATUS` int(11) NOT NULL DEFAULT '-1',
  `ERMAECHTIGUNG_ID` int(11) NOT NULL DEFAULT '-1',
  `DATEINAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LETZTE_AENDERUNG` timestamp NULL DEFAULT NULL,
  `BEARBEITER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Producer_ID` (`PRODUCER_ID`),
  KEY `DATEISTAMMNAME` (`DATEINAME`),
  KEY `PATIENT_ID` (`PATIENT_ID`),
  KEY `DOKU_ID` (`STATUS`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `HL7_SENDEPROTOKOLL`
--

DROP TABLE IF EXISTS `HL7_SENDEPROTOKOLL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HL7_SENDEPROTOKOLL` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYP` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TRIGGER_EVENT` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATEINAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PATIENT_ID` int(11) DEFAULT NULL,
  `DOKU_ID` int(11) DEFAULT NULL,
  `ZEIT` datetime DEFAULT NULL,
  `BEARBEITER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NACHRICHT` text COLLATE utf8_unicode_ci,
  `STATUS` int(11) DEFAULT NULL,
  `PROTOKOLL` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `DATEISTAMMNAME` (`DATEINAME`),
  KEY `PATIENT_ID` (`PATIENT_ID`),
  KEY `DOKU_ID` (`DOKU_ID`),
  KEY `MESSAGE_TYP` (`TRIGGER_EVENT`),
  KEY `TYP` (`TYP`),
  KEY `TRIGGER_EVENT` (`TRIGGER_EVENT`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ICD_KAPITEL`
--

DROP TABLE IF EXISTS `ICD_KAPITEL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ICD_KAPITEL` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `HIERARCHIE` int(5) NOT NULL DEFAULT '0',
  `NUMMER` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VON_ICD` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BIS_ICD` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEZEICHNUNG` text COLLATE utf8_unicode_ci,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LETZTE_AENDERUNG` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=16324 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ICD_KATALOG`
--

DROP TABLE IF EXISTS `ICD_KATALOG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ICD_KATALOG` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYP` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ICD_KODE` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ICD_TEXT` text COLLATE utf8_unicode_ci,
  `ICD_TYP` int(11) DEFAULT NULL,
  `NOTATION` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GESCHLECHTSBEZUG` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ALTER_UNTEN` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ALTER_OBEN` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ALTER_FEHLER` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EXOTIK` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KODE_BELEGT` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MELDEPFLICHT` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EBM_KOMBI` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GRUPPE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KUERZEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NOTIZEN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROTOKOLL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STATUS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEARBEITER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZIFFER_VON` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZIFFER_BIS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERSION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LETZTE_AENDERUNG` datetime DEFAULT NULL,
  `GESCHLECHT_FEHLER` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `AKR_REF` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `ICD_KODE` (`ICD_KODE`),
  KEY `ZEILE_LOCK` (`ZEILE_LOCK`)
) ENGINE=MyISAM AUTO_INCREMENT=16061 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ICD_THESAURUS`
--

DROP TABLE IF EXISTS `ICD_THESAURUS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ICD_THESAURUS` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ICD_KODE` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `THESAURUS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HAUSARZTKODIERUNG` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `THESAURUS` (`THESAURUS`),
  KEY `ICD_KODE` (`ICD_KODE`),
  KEY `HAUSARZTKODIERUNG` (`HAUSARZTKODIERUNG`)
) ENGINE=MyISAM AUTO_INCREMENT=77962 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `IMPFBUCH`
--

DROP TABLE IF EXISTS `IMPFBUCH`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `IMPFBUCH` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ERMAECHTIGUNG_ID` int(11) NOT NULL DEFAULT '-1',
  `PATIENT_ID` int(11) NOT NULL DEFAULT '-1',
  `UNTERSUCHUNG_ID` int(11) DEFAULT NULL,
  `TYP` int(11) NOT NULL DEFAULT '-1',
  `SUBTYP` int(11) NOT NULL DEFAULT '-1',
  `DATUM` date DEFAULT NULL,
  `BEZEICHNUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ERGEBNIS` int(11) NOT NULL DEFAULT '-1',
  `BEMERKUNG` mediumtext COLLATE utf8_unicode_ci,
  `FREITEXT1` mediumtext COLLATE utf8_unicode_ci,
  `FREITEXT2` mediumtext COLLATE utf8_unicode_ci,
  `STATUS` int(11) NOT NULL DEFAULT '-1',
  `SYSTEM_DATUM` date DEFAULT NULL,
  `SYSTEM_UHRZEIT` time DEFAULT NULL,
  `BEARBEITER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROTOKOLL` mediumtext COLLATE utf8_unicode_ci,
  `CHARGENNR` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IMPFART` int(11) NOT NULL DEFAULT '-1',
  `IMPF_KENNUNG` int(11) NOT NULL DEFAULT '-1',
  `IMPFTYP` int(11) NOT NULL DEFAULT '-1',
  `IMPFSTOFF` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HERSTELLER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DOSIERUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DOSIERUNG2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IMPF_DOKUID` int(11) DEFAULT NULL,
  `IMPFER` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `PATIENT_ID` (`PATIENT_ID`),
  KEY `IMPF_KENNUNG` (`IMPF_KENNUNG`),
  KEY `TYP` (`TYP`)
) ENGINE=MyISAM AUTO_INCREMENT=150 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `IMPORT`
--

DROP TABLE IF EXISTS `IMPORT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `IMPORT` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ALTSYSTEM_ID` bigint(20) DEFAULT NULL,
  `TEXT` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `IMPORT2`
--

DROP TABLE IF EXISTS `IMPORT2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `IMPORT2` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ALTSYSTEM_ID` bigint(20) DEFAULT NULL,
  `DATUM` date DEFAULT NULL,
  `TEXT` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `IVF_EMBRYOKRYO`
--

DROP TABLE IF EXISTS `IVF_EMBRYOKRYO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `IVF_EMBRYOKRYO` (
  `EMBRYOKRYO_DOKUID` int(11) DEFAULT NULL,
  `KOMMENTAR` text COLLATE utf8_unicode_ci,
  `LABOR` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EXTERNEKRYO` int(1) DEFAULT NULL,
  `INVITRO_ZYKLUS_ID` int(11) DEFAULT NULL,
  `EMBRYONEN_ANZAHL` int(2) DEFAULT NULL,
  `EMBRYONEN_QUALITAET` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEFUND` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEFUND_SONSTIGES` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAGER_TYP` int(1) DEFAULT NULL,
  `LAGER_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAGER_CODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAGER_BEHAELTER` int(2) DEFAULT NULL,
  `LAGER_POSITION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TAG` int(1) DEFAULT NULL,
  `LAGER_ANZAHL` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAGER_TYPFARBE` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAGER_POSITIONFARBE` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `METHODE_GEFRIEREN_PBS_MIN` int(2) DEFAULT NULL,
  `METHODE_GEFRIEREN_PBS_SEK` int(2) DEFAULT NULL,
  `METHODE_GEFRIEREN_55_MIN` int(2) DEFAULT NULL,
  `METHODE_GEFRIEREN_55_SEK` int(2) DEFAULT NULL,
  `METHODE_GEFRIEREN_1010_MIN` int(2) DEFAULT NULL,
  `METHODE_GEFRIEREN_1010_SEK` int(2) DEFAULT NULL,
  `METHODE_GEFRIEREN_1515_MIN` int(2) DEFAULT NULL,
  `METHODE_GEFRIEREN_1515_SEK` int(2) DEFAULT NULL,
  `METHODE_GEFRIEREN_2020_MIN` int(2) DEFAULT NULL,
  `METHODE_GEFRIEREN_2020_SEK` int(2) DEFAULT NULL,
  `METHODE_AUFTAUEN_SUC1M_MIN` int(2) DEFAULT NULL,
  `METHODE_AUFTAUEN_SUC1M_SEK` int(2) DEFAULT NULL,
  `METHODE_AUFTAUEN_SUC05M_MIN` int(2) DEFAULT NULL,
  `METHODE_AUFTAUEN_SUC05M_SEK` int(2) DEFAULT NULL,
  `METHODE_AUFTAUEN_SUC025M_MIN` int(2) DEFAULT NULL,
  `METHODE_AUFTAUEN_SUC025M_SEK` int(2) DEFAULT NULL,
  `METHODE_AUFTAUEN_PBS_MIN` int(2) DEFAULT NULL,
  `METHODE_AUFTAUEN_PBS_SEK` int(2) DEFAULT NULL,
  `METHODE_AUFTAUEN_TEMPERATUR` int(1) DEFAULT NULL,
  `METHODE_AUFTAUEN_SUC0125M_MIN` int(2) DEFAULT NULL,
  `METHODE_AUFTAUEN_SUC0125M_SEK` int(2) DEFAULT NULL,
  `METHODE_AUFTAUEN_LASERHATCHING` int(1) DEFAULT NULL,
  `METHODE_GEFRIEREN_TEMPERATUR` int(1) DEFAULT NULL,
  `KRYO_ABGESCHLOSSEN` int(1) DEFAULT NULL,
  UNIQUE KEY `EMBRYOKRYO_DOKUID` (`EMBRYOKRYO_DOKUID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `IVF_EMBRYOTRANSFER`
--

DROP TABLE IF EXISTS `IVF_EMBRYOTRANSFER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `IVF_EMBRYOTRANSFER` (
  `EMBRYOTRANSFER_DOKUID` int(11) DEFAULT NULL,
  `KOMMENTAR` text COLLATE utf8_unicode_ci,
  `INVITRO_ZYKLUS_ID` int(11) DEFAULT NULL,
  `TAG` int(1) DEFAULT NULL,
  `LABOR` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EMBRYONEN_ANZ` int(1) DEFAULT NULL,
  `EMBRYONEN_TYP` int(1) DEFAULT NULL,
  `EMBRYONEN_QUALITAET` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `INTERN` text COLLATE utf8_unicode_ci,
  `SONDENLAENGE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `POSITION_UTERUS` int(1) DEFAULT NULL,
  `SCHWIERIGKEIT` int(1) DEFAULT NULL,
  `TRANSFERSET` int(1) DEFAULT NULL,
  UNIQUE KEY `EMBRYOTRANSFER_DOKUID` (`EMBRYOTRANSFER_DOKUID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `IVF_INVITRO_ZYKLUS`
--

DROP TABLE IF EXISTS `IVF_INVITRO_ZYKLUS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `IVF_INVITRO_ZYKLUS` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PATIENT_ID` int(11) DEFAULT NULL,
  `DATUM` date DEFAULT NULL,
  `MANDANT_ID` int(11) DEFAULT NULL,
  `BEARBEITER_ID` int(11) DEFAULT NULL,
  `ARZT_ID` int(11) DEFAULT NULL,
  `START_DOWN_REG` date DEFAULT NULL,
  `DOWN_REG_BEMERKUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LETZTE_MENSTRUATION` date DEFAULT NULL,
  `START_STIMULATION` date DEFAULT NULL,
  `ENDE_STIMULATION` date DEFAULT NULL,
  `ULTRASCHALL_1` date DEFAULT NULL,
  `ULTRASCHALL_IMHAUS` int(2) DEFAULT NULL,
  `AKTIVIERUNG_DATUM` date DEFAULT NULL,
  `AKTIVIERUNG_UHRZEIT` time DEFAULT NULL,
  `PUNKTION_DATUM` date DEFAULT NULL,
  `PUNKTION_UHRZEIT` time DEFAULT NULL,
  `PUNKTION_IMHAUS` int(2) DEFAULT NULL,
  `BEHANDLUNGEN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEMERKUNGEN` longtext COLLATE utf8_unicode_ci,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `THERAPIEPLAN_TITEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `THERAPIEPLAN_NUMMER` int(5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`),
  KEY `PATIENT_ID` (`PATIENT_ID`),
  KEY `ZEILE_LOCK` (`ZEILE_LOCK`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `IVF_INVITRO_ZYKLUS_TABELLENEINTRAEGE`
--

DROP TABLE IF EXISTS `IVF_INVITRO_ZYKLUS_TABELLENEINTRAEGE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `IVF_INVITRO_ZYKLUS_TABELLENEINTRAEGE` (
  `ZYKLUS_ID` int(11) DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `US_SEITE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATUM_STRING` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WERT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TYP` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZEILENPOSITION` int(2) DEFAULT NULL,
  KEY `ZYKLUS_ID` (`ZYKLUS_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `IVF_KULTUR`
--

DROP TABLE IF EXISTS `IVF_KULTUR`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `IVF_KULTUR` (
  `KULTUR_DOKUID` int(11) DEFAULT NULL,
  `KOMMENTAR` text COLLATE utf8_unicode_ci,
  `INVITRO_ZYKLUS_ID` int(11) DEFAULT NULL,
  `PUNKTIONDOKU_ID` int(1) DEFAULT NULL,
  UNIQUE KEY `KULTUR_DOKUID` (`KULTUR_DOKUID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `IVF_KULTUR_DATEN`
--

DROP TABLE IF EXISTS `IVF_KULTUR_DATEN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `IVF_KULTUR_DATEN` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `KULTUR_DOKUID` int(11) DEFAULT NULL,
  `EIZELLE_NUMMER` int(3) DEFAULT NULL,
  `TAG0_DATUM` date DEFAULT NULL,
  `TAG0_UHRZEIT` time DEFAULT NULL,
  `TAG0_DATEN` longtext COLLATE utf8_unicode_ci,
  `TAG1_DATUM` date DEFAULT NULL,
  `TAG1_UHRZEIT` time DEFAULT NULL,
  `TAG1_DATEN` longtext COLLATE utf8_unicode_ci,
  `TAG2_DATUM` date DEFAULT NULL,
  `TAG2_UHRZEIT` time DEFAULT NULL,
  `TAG2_DATEN` longtext COLLATE utf8_unicode_ci,
  `TAG3_DATUM` date DEFAULT NULL,
  `TAG3_UHRZEIT` time DEFAULT NULL,
  `TAG3_DATEN` longtext COLLATE utf8_unicode_ci,
  `TAG4_DATUM` date DEFAULT NULL,
  `TAG4_UHRZEIT` time DEFAULT NULL,
  `TAG4_DATEN` longtext COLLATE utf8_unicode_ci,
  `TAG5_DATUM` date DEFAULT NULL,
  `TAG5_UHRZEIT` time DEFAULT NULL,
  `TAG5_DATEN` longtext COLLATE utf8_unicode_ci,
  `TAG6_DATUM` date DEFAULT NULL,
  `TAG6_UHRZEIT` time DEFAULT NULL,
  `TAG6_DATEN` longtext COLLATE utf8_unicode_ci,
  `ENDABLAUF_TRANSFER` int(1) DEFAULT NULL,
  `ENDABLAUF_HATCH` int(1) DEFAULT NULL,
  `ENDABLAUF_KRYO` int(1) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `EIZELLE` (`KULTUR_DOKUID`,`EIZELLE_NUMMER`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `IVF_PUNKTION`
--

DROP TABLE IF EXISTS `IVF_PUNKTION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `IVF_PUNKTION` (
  `PUNKTION_DOKUID` int(11) NOT NULL,
  `KOMMENTAR` text COLLATE utf8_unicode_ci,
  `LABOR` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PUNKTIONEN` longtext COLLATE utf8_unicode_ci,
  `INVITRO_ZYKLUS_ID` int(11) DEFAULT NULL,
  `HYALURONIDASE` int(1) DEFAULT NULL,
  `UHRZEIT` time DEFAULT NULL,
  UNIQUE KEY `PUNKTION_DOKUID` (`PUNKTION_DOKUID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `IVF_SAMENKRYO`
--

DROP TABLE IF EXISTS `IVF_SAMENKRYO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `IVF_SAMENKRYO` (
  `SAMENKRYO_DOKUID` int(11) DEFAULT NULL,
  `KOMMENTAR` longtext COLLATE utf8_unicode_ci,
  `LABOR` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EXTERNEKRYO` int(1) DEFAULT NULL,
  `INVITRO_ZYKLUS_ID` int(11) DEFAULT NULL,
  `BEFUND` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEFUND_SONSTIGES` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAGER_TYP` int(1) DEFAULT NULL,
  `LAGER_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAGER_CODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAGER_BEHAELTER` int(2) DEFAULT NULL,
  `LAGER_POSITION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAGER_ANZAHL` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAGER_TYPFARBE` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAGER_POSITIONFARBE` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `METHODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `METHODE_KOMMENTAR` text COLLATE utf8_unicode_ci,
  `SPERMIOGRAMM_ID` int(11) DEFAULT NULL,
  `TESE` int(1) DEFAULT NULL,
  `TESE_50ER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TESE_90ER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TESE_70ER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TESE_ERGEBNISSE` longtext COLLATE utf8_unicode_ci,
  `TESE_UEBERSTAND` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KRYO_ABGESCHLOSSEN` int(1) DEFAULT NULL,
  UNIQUE KEY `SAMENKRYO_DOKUID` (`SAMENKRYO_DOKUID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `IVF_SPERMIOGRAMM`
--

DROP TABLE IF EXISTS `IVF_SPERMIOGRAMM`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `IVF_SPERMIOGRAMM` (
  `SPERMIOGRAMM_DOKUID` int(11) DEFAULT NULL,
  `KOMMENTAR` text COLLATE utf8_unicode_ci,
  `INVITRO_ZYKLUS_ID` int(11) DEFAULT NULL,
  `LABOR` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SPERMIENGEWINNUNG` int(2) DEFAULT NULL,
  `SPERMIENGEWINNUNG_SONTIGES` text COLLATE utf8_unicode_ci,
  `SAMENPRAEPARATIONFUER` int(1) DEFAULT NULL,
  `3GRADIENTENSPERMA` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GENERATION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TESE_OBSTRUKTION` int(1) DEFAULT NULL,
  `TESE_ERGEBNIS` longtext COLLATE utf8_unicode_ci,
  `SAMEN_VOLUMEN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SAMEN_VISKOSITAET` int(1) DEFAULT NULL,
  `SAMEN_VERFLUESSIGEN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SAMEN_KONZENTRATION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SAMEN_KONZENTRATION_EINHEIT` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SAMEN_TOTALEMOTILITAETPROZENT` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SAMEN_SCHNELLPROGRESSIVPROZENT` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SAMEN_LANGSAMPROGRESSIVPROZENT` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SAMEN_BEWEGLICHPROZENT` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SAMEN_UNBEWEGLICHPROZENT` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SAMEN_PATHOLOGISCHPROZENT` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SAMEN_RUNDZELLENLEUKOSANZAHL` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SAMEN_AGGLUTINATION` int(2) DEFAULT NULL,
  `TESE_SEITE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IMSI_KLASSE1PROZENT` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IMSI_KLASSE2PROZENT` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IMSI_KLASSE3PROZENT` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  UNIQUE KEY `SPERMIOGRAMM_DOKUID` (`SPERMIOGRAMM_DOKUID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `KALENDERSTAMM`
--

DROP TABLE IF EXISTS `KALENDERSTAMM`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `KALENDERSTAMM` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BESCHREIBUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KENNUNG` int(11) DEFAULT NULL,
  `ZUSATZ_1` mediumtext COLLATE utf8_unicode_ci,
  `ZUSATZ_2` mediumtext COLLATE utf8_unicode_ci,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `KENNUNG` (`KENNUNG`),
  KEY `NAME` (`NAME`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `KALENDERVORLAGE`
--

DROP TABLE IF EXISTS `KALENDERVORLAGE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `KALENDERVORLAGE` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `KALENDER_KENNUNG` int(11) NOT NULL DEFAULT '0',
  `VORLAGEN_KENNUNG` int(11) NOT NULL DEFAULT '0',
  `GUELTIG_AB` date DEFAULT NULL,
  `GUELTIG_BIS` date DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `KALENDER_KENNUNG` (`KALENDER_KENNUNG`),
  KEY `VORLAGEN_KENNUNG` (`VORLAGEN_KENNUNG`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `KATALOG_ICD`
--

DROP TABLE IF EXISTS `KATALOG_ICD`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `KATALOG_ICD` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYP` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TYP_1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TYP_2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZIFFER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZIFFER_TEXT_1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZIFFER_TEXT_2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZIFFER_TEXT_3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZIFFER_TEXT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZIFFER_TEXT_KURZ` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PUNKTE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GRUPPE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KUERZEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NOTIZEN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROTOKOLL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STATUS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZIFFER_VON` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZIFFER_BIS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BETRAG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WAEHRUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERSION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZEITSTEMPEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=27111 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `KATALOG_KAPITEL`
--

DROP TABLE IF EXISTS `KATALOG_KAPITEL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `KATALOG_KAPITEL` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYP` int(11) NOT NULL DEFAULT '-1',
  `INDEX` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEREICH` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KAPITEL` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ABSCHNITT` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UABSCHNITT` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BLOCK` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TITEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ZIFFER` (`INDEX`,`TYP`),
  KEY `TYP` (`TYP`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `KONTRAINDIKATIONENAUFTRAG`
--

DROP TABLE IF EXISTS `KONTRAINDIKATIONENAUFTRAG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `KONTRAINDIKATIONENAUFTRAG` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ANAMNESEFRAGENSTAMM_ID` int(11) NOT NULL DEFAULT '0',
  `AUFTRAGSTAMM_ID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `KEY` (`ANAMNESEFRAGENSTAMM_ID`,`AUFTRAGSTAMM_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `KREISLAUF`
--

DROP TABLE IF EXISTS `KREISLAUF`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `KREISLAUF` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PATIENT_ID` int(11) NOT NULL DEFAULT '0',
  `TYP` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `DATUM` date DEFAULT NULL,
  `SYSTOLE` double DEFAULT NULL,
  `DIASTOLE` double DEFAULT NULL,
  `PULS` double DEFAULT NULL,
  `BEMERKUNG` mediumtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=855 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `KULTUR`
--

DROP TABLE IF EXISTS `KULTUR`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `KULTUR` (
  `KULTUR_DOKUID` int(11) DEFAULT NULL,
  `KOMMENTAR` text COLLATE utf8_unicode_ci,
  `INVITRO_ZYKLUS_ID` int(11) DEFAULT NULL,
  `TAG0_DATUM` date DEFAULT NULL,
  `TAG0_UHRZEIT` time DEFAULT NULL,
  `TAG0_DATEN` longtext COLLATE utf8_unicode_ci,
  `TAG1_DATUM` date DEFAULT NULL,
  `TAG1_UHRZEIT` time DEFAULT NULL,
  `TAG1_DATEN` longtext COLLATE utf8_unicode_ci,
  `TAG2_DATUM` date DEFAULT NULL,
  `TAG2_UHRZEIT` time DEFAULT NULL,
  `TAG2_DATEN` longtext COLLATE utf8_unicode_ci,
  `TAG3_DATUM` date DEFAULT NULL,
  `TAG3_UHRZEIT` time DEFAULT NULL,
  `TAG3_DATEN` longtext COLLATE utf8_unicode_ci,
  `TAG4_DATUM` date DEFAULT NULL,
  `TAG4_UHRZEIT` time DEFAULT NULL,
  `TAG4_DATEN` longtext COLLATE utf8_unicode_ci,
  `TAG5_DATUM` date DEFAULT NULL,
  `TAG5_UHRZEIT` time DEFAULT NULL,
  `TAG5_DATEN` longtext COLLATE utf8_unicode_ci,
  `TAG6_DATUM` date DEFAULT NULL,
  `TAG6_UHRZEIT` time DEFAULT NULL,
  `TAG6_DATEN` longtext COLLATE utf8_unicode_ci,
  `ENDABLAUF_TRANSFER` int(1) DEFAULT NULL,
  `ENDABLAUF_HATCH` int(1) DEFAULT NULL,
  `ENDABLAUF_KRYO` int(1) DEFAULT NULL,
  UNIQUE KEY `KULTUR_DOKUID` (`KULTUR_DOKUID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `LABORAUFTRAG`
--

DROP TABLE IF EXISTS `LABORAUFTRAG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LABORAUFTRAG` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LABORAUFTRAGDOKU_ID` int(11) NOT NULL DEFAULT '-1',
  `DATUM_VERSENDET` date DEFAULT NULL,
  `STATUS` int(11) NOT NULL DEFAULT '-1',
  `DIAGNOSE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KOMMENTAR` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AUFTRAGSNUMMER` int(11) DEFAULT NULL,
  `PROBENENTNAHME_DATUM` date DEFAULT NULL,
  `ETIKETTEN_ZAEHLER` int(4) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `DOKU_ID` (`LABORAUFTRAGDOKU_ID`),
  UNIQUE KEY `LABORAUFTRAGDOKU_ID` (`LABORAUFTRAGDOKU_ID`),
  UNIQUE KEY `AUFTRAGSNUMMER` (`AUFTRAGSNUMMER`) USING BTREE,
  KEY `STATUS` (`STATUS`),
  KEY `DATUM_VERSENDET` (`DATUM_VERSENDET`),
  KEY `PROBENENTNAHME_DATUM` (`PROBENENTNAHME_DATUM`),
  KEY `ETIKETTEN_ZAEHLER` (`ETIKETTEN_ZAEHLER`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `LABORAUFTRAGWERT`
--

DROP TABLE IF EXISTS `LABORAUFTRAGWERT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LABORAUFTRAGWERT` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LABORAUFTRAGDOKU_ID` int(11) DEFAULT '-1',
  `CODE_IM_LABOR` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LABORAUFTRAG_ZUWEISER_ID` int(11) DEFAULT NULL,
  `REFERENZ_DOKUID` int(11) DEFAULT NULL,
  `NACHFORDERUNG` tinyint(4) DEFAULT NULL,
  `KUERZEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `LABORBEFUND_ID` (`LABORAUFTRAGDOKU_ID`),
  KEY `LABOR_ID` (`CODE_IM_LABOR`),
  KEY `REFERENZ_DOKUID` (`REFERENZ_DOKUID`),
  KEY `LABOR_NAME` (`LABORAUFTRAG_ZUWEISER_ID`) USING BTREE,
  KEY `NACHFORDERUNG` (`NACHFORDERUNG`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `LABORAUFTRAGWERTSTAMM`
--

DROP TABLE IF EXISTS `LABORAUFTRAGWERTSTAMM`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LABORAUFTRAGWERTSTAMM` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE_IM_LABOR` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KUERZEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEZEICHNUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TYP` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'NULL',
  `SPEZIMEN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GEB_KUERZEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEREICH` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NACHFORDERUNG` int(11) DEFAULT NULL,
  `KASSE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRIVATTARIF` double(11,2) DEFAULT NULL,
  `FLAGS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MANDANTEN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEARBEITER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LETZTE_AENDERUNG` datetime DEFAULT NULL,
  `GUELTIG_AB` date DEFAULT NULL,
  `GUELTIG_BIS` date DEFAULT NULL,
  `LABORAUFTRAG_ZUWEISER_ID` int(11) DEFAULT NULL,
  `PROFIL_PARAMETER_IDS` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GEB_BEZEICHNUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERRECH_DIAGNOSEN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROB_INF` text COLLATE utf8_unicode_ci,
  `MIC_BIOL` int(11) DEFAULT NULL,
  `ZWINGEND_PARAM` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EXCLUDE_PARAM` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `APP_GEM` int(11) DEFAULT NULL,
  `METHOD_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `z` (`CODE_IM_LABOR`,`LABORAUFTRAG_ZUWEISER_ID`),
  UNIQUE KEY `SCHLUESSEL` (`CODE_IM_LABOR`,`LABORAUFTRAG_ZUWEISER_ID`),
  KEY `ZEILE_LOCK` (`ZEILE_LOCK`),
  KEY `KUERZEL` (`KUERZEL`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `LABORAUFTRAG_NUMMERN`
--

DROP TABLE IF EXISTS `LABORAUFTRAG_NUMMERN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LABORAUFTRAG_NUMMERN` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `AUFTRAGSNUMMER` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LABOR` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MANDANT_ID` int(11) DEFAULT NULL,
  `GUELTIG_BIS` date DEFAULT NULL,
  `DOKU_ID` int(11) DEFAULT '-1',
  `STATUS` int(11) NOT NULL DEFAULT '0',
  `JAHR` int(11) DEFAULT NULL,
  `NUMMERN_TYP` int(1) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `DOKU_ID` (`DOKU_ID`),
  UNIQUE KEY `AUFTRAGSNUMMER` (`AUFTRAGSNUMMER`,`LABOR`,`JAHR`) USING BTREE,
  KEY `STATUS` (`STATUS`),
  KEY `NUMMERN_TYP` (`NUMMERN_TYP`),
  KEY `MANDANT_ID` (`MANDANT_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `LABORBEFUND`
--

DROP TABLE IF EXISTS `LABORBEFUND`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LABORBEFUND` (
  `DOKU_ID` int(11) NOT NULL DEFAULT '0',
  `SENDEN` int(11) DEFAULT NULL,
  `SENDE_STATUS` int(11) NOT NULL DEFAULT '-1',
  `SENDE_ZEIT` time DEFAULT NULL,
  `SENDE_EMPFAENGER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SENDEN_AUFTRAGSNUMMER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EMPFANGEN` int(11) DEFAULT NULL,
  `EMPFANGEN_STATUS` int(11) DEFAULT NULL,
  `EMPFANGEN_ZEIT` datetime DEFAULT NULL,
  `EMPFANGEN_ABSENDER` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EMPFANGEN_AUFTRAGSNUMMER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AUFTRAGSINFORMATION` text COLLATE utf8_unicode_ci,
  `LABORAUFTRAG_DOKUID` int(11) DEFAULT NULL,
  `LABORBEFUND_TYP` int(11) DEFAULT NULL,
  `LABORBEFUND_ART` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  UNIQUE KEY `DOKU_ID` (`DOKU_ID`),
  UNIQUE KEY `IMPORT_KENNUNG` (`EMPFANGEN_ABSENDER`,`EMPFANGEN_AUFTRAGSNUMMER`,`LABORBEFUND_TYP`,`LABORBEFUND_ART`),
  KEY `STATUS_QUITTUNG` (`SENDE_STATUS`),
  KEY `EMPFANGEN_ABSENDER` (`EMPFANGEN_ABSENDER`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `LABORNORM`
--

DROP TABLE IF EXISTS `LABORNORM`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LABORNORM` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LABORSTAMM_ID` int(11) DEFAULT NULL,
  `STATUS` int(11) DEFAULT NULL,
  `GESCHLECHT` int(3) DEFAULT NULL,
  `ALTER_VON` int(11) DEFAULT NULL,
  `ALTER_BIS` int(11) DEFAULT NULL,
  `ALTER_EINHEIT` int(11) NOT NULL DEFAULT '-1',
  `NORMBEREICH_UNTEN` double DEFAULT NULL,
  `NORMBEREICH_OBEN` double DEFAULT NULL,
  `NORMBEREICH` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEARBEITER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `LABORSTAMM_ID` (`LABORSTAMM_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=1755 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `LABORSTAMM`
--

DROP TABLE IF EXISTS `LABORSTAMM`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LABORSTAMM` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EINHEIT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KUERZEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEZEICHNUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GRUPPE` int(11) DEFAULT NULL,
  `ERMAECHTIGUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRIVAT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KASSE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEARBEITER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LETZTE_AENDERUNG_DATUM` date DEFAULT NULL,
  `LETZTE_AENDERUNG_UHRZEIT` time DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `KUERZEL` (`KUERZEL`),
  KEY `ZEILE_LOCK` (`ZEILE_LOCK`)
) ENGINE=MyISAM AUTO_INCREMENT=2178 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `LABORWERTE`
--

DROP TABLE IF EXISTS `LABORWERTE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LABORWERTE` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LABORBEFUND_ID` int(11) DEFAULT NULL,
  `UNTERSUCHUNG_ID` int(11) DEFAULT NULL,
  `SORTIERUNG` int(11) NOT NULL DEFAULT '0',
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STATUS` int(11) DEFAULT NULL,
  `LETZTE_AENDERUNG` datetime DEFAULT NULL,
  `BEZEICHNUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WARNUNG` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ABWEICHUNG` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ERGEBNISWERT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EINHEIT` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NORMBEREICH` mediumtext COLLATE utf8_unicode_ci,
  `BEARBEITER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GRUPPE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROTOKOLL` mediumtext COLLATE utf8_unicode_ci,
  `TESTBEZEICHNUNG` mediumtext COLLATE utf8_unicode_ci,
  `ERGEBNISTEXT` mediumtext COLLATE utf8_unicode_ci,
  `PROBENIDENT` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROBENINDEX` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROBENBEZEICHNUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROBENSPEZIFIKATION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NORM_UNTEN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NORM_OBEN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ENTNAHMEDATUM` date DEFAULT NULL,
  `ENTNAHMEUHRZEIT` time DEFAULT NULL,
  `HINWEISTEXT` text COLLATE utf8_unicode_ci,
  `LABORSTAMM_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `LABORBEFUND_ID` (`LABORBEFUND_ID`),
  KEY `ABNAHMEDATUM` (`ENTNAHMEDATUM`),
  KEY `BEZEICHNUNG` (`BEZEICHNUNG`),
  KEY `LABORSTAMM_ID` (`LABORSTAMM_ID`),
  KEY `ENTNAHMEDATUM` (`ENTNAHMEDATUM`)
) ENGINE=MyISAM AUTO_INCREMENT=37648 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `LABORWERTSTAMM_LABORKUERZEL_ZUORDNUNG`
--

DROP TABLE IF EXISTS `LABORWERTSTAMM_LABORKUERZEL_ZUORDNUNG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LABORWERTSTAMM_LABORKUERZEL_ZUORDNUNG` (
  `LABORWERTSTAMM_ID` int(11) DEFAULT NULL,
  `ANFORDERUNGSLABOR_ID` int(11) DEFAULT NULL,
  `ANFORDERUNGSLABOR_KUERZEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `LABORID_KUERZEL` (`ANFORDERUNGSLABOR_ID`,`ANFORDERUNGSLABOR_KUERZEL`),
  KEY `LABORWERTSTAMM_ID` (`LABORWERTSTAMM_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `LAGERBEWEGUNG`
--

DROP TABLE IF EXISTS `LAGERBEWEGUNG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LAGERBEWEGUNG` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYP` int(11) NOT NULL DEFAULT '-1',
  `DATUM` date DEFAULT NULL,
  `UHRZEIT` time DEFAULT NULL,
  `ARTIKEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEWEGUNG_TEXT` text COLLATE utf8_unicode_ci,
  `MENGE` double NOT NULL DEFAULT '0',
  `CHARGEN_NR` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HALTBAR` date DEFAULT NULL,
  `STANDORT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EK_PREIS` double NOT NULL DEFAULT '0',
  `VK_PREIS` double NOT NULL DEFAULT '0',
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEARBEITER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SYSTEM_ZEIT` datetime DEFAULT NULL,
  `ZIFFER_ID` int(11) DEFAULT '-1',
  `EK_WAEHRUNG` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `VK_WAEHRUNG` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `ZAHLUNG_DATUM` (`DATUM`),
  KEY `ZEILE_LOCK` (`ZEILE_LOCK`),
  KEY `TYP` (`TYP`),
  KEY `ZIFFER_ID` (`ZIFFER_ID`),
  KEY `DATUM` (`DATUM`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `LAGERSTAMM`
--

DROP TABLE IF EXISTS `LAGERSTAMM`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LAGERSTAMM` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `KUERZEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEZEICHNUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GRUPPE` int(11) DEFAULT NULL,
  `ERMAECHTIGUNGEN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRIVAT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KASSE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEARBEITER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LETZTE_AENDERUNG` datetime DEFAULT NULL,
  `ARTIKEL_NR` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EINHEIT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MINDESTMENGE` double NOT NULL DEFAULT '0',
  `LIEFERANT` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `KUERZEL` (`KUERZEL`),
  KEY `ZEILE_LOCK` (`ZEILE_LOCK`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `LEISTUNG`
--

DROP TABLE IF EXISTS `LEISTUNG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LEISTUNG` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LEISTUNGSDOKU_ID` int(11) NOT NULL DEFAULT '0',
  `AUFTRAGSTAMM_ID` int(11) NOT NULL DEFAULT '0',
  `ANZAHL` int(11) NOT NULL DEFAULT '1',
  `DATUM` date DEFAULT NULL,
  `UHRZEIT` time DEFAULT NULL,
  `VERTRAGSART` int(11) DEFAULT NULL,
  `PRIVATPREIS` double NOT NULL DEFAULT '0',
  `AUFTRAG_EINZELLEISTUNGEN_ID` int(11) DEFAULT NULL,
  `BENUTZER_ID` int(11) DEFAULT NULL,
  `STATUS` int(11) NOT NULL DEFAULT '0',
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEZEICHNUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEGRUENDUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `LEISTUNGSDOKU_ID` (`LEISTUNGSDOKU_ID`),
  KEY `VERTRAGSART` (`VERTRAGSART`),
  KEY `BENUTZER_ID` (`BENUTZER_ID`),
  KEY `AUFTRAGSTAMM_ID` (`AUFTRAGSTAMM_ID`),
  KEY `AUFTRAG_EINZELLEISTUNGEN_ID` (`AUFTRAG_EINZELLEISTUNGEN_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MAGISTRALITER_STAMM`
--

DROP TABLE IF EXISTS `MAGISTRALITER_STAMM`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MAGISTRALITER_STAMM` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `POSITION` int(5) NOT NULL DEFAULT '-1',
  `TYP` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KUERZEL` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `BEZEICHNUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `REZEPTUR` mediumtext COLLATE utf8_unicode_ci,
  `STATUS` int(11) DEFAULT NULL,
  `LETZTE_AENDERUNG_DATUM` date DEFAULT NULL,
  `LETZTE_AENDERUNG_UHRZEIT` time DEFAULT NULL,
  `BEARBEITER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROTOKOLL` mediumtext COLLATE utf8_unicode_ci,
  `DOSISSCHEMA` text COLLATE utf8_unicode_ci,
  `MANDANTEN` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KASSE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRIVAT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EINSTELLUNG` text COLLATE utf8_unicode_ci,
  `MEDIKAMENT_BOX` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERTRAGSART` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `KUERZEL` (`KUERZEL`)
) ENGINE=MyISAM AUTO_INCREMENT=143 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MAHNUNG`
--

DROP TABLE IF EXISTS `MAHNUNG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MAHNUNG` (
  `MAHNUNGDOKU_ID` int(11) NOT NULL DEFAULT '-1',
  `UNTERSUCHUNG_ID` int(11) NOT NULL DEFAULT '-1',
  `ANSCHRIFT_XML` text COLLATE utf8_unicode_ci,
  `ABSENDE_ANSCHRIFT` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`MAHNUNGDOKU_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MEDIKAMENT`
--

DROP TABLE IF EXISTS `MEDIKAMENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MEDIKAMENT` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PATIENT_ID` int(11) NOT NULL DEFAULT '-1',
  `DOKU_ID` int(11) NOT NULL DEFAULT '-1',
  `UNTERSUCHUNG_ID` int(11) NOT NULL DEFAULT '-1',
  `TYP` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STATUS` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATUM` date DEFAULT NULL,
  `UHRZEIT` time DEFAULT NULL,
  `MEDIKAMENT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MENGE` double DEFAULT NULL,
  `MENGENART` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ANZAHL` double DEFAULT NULL,
  `DOSISSCHEMA` text COLLATE utf8_unicode_ci,
  `MEDIKAMENT_CA_KZ` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `REZEPTUR` text COLLATE utf8_unicode_ci,
  `RESERVE2` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RESERVE3` text COLLATE utf8_unicode_ci,
  `BEARBEITER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROTOKOLL` text COLLATE utf8_unicode_ci,
  `REZEPT_DATUM` date DEFAULT NULL,
  `KUERZEL` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHARMA_NR` int(11) DEFAULT NULL,
  `KATALOG` int(5) DEFAULT NULL,
  `BOX` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `END_DATUM` date DEFAULT NULL,
  `ABSETZGRUND` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ANMERKUNG` text COLLATE utf8_unicode_ci,
  `RHYTHMUS` int(2) NOT NULL DEFAULT '1',
  `ANZEIGE` int(2) DEFAULT NULL,
  `NACHTS` double NOT NULL DEFAULT '0',
  `FRUEHNACHTS` double NOT NULL DEFAULT '0',
  `MORGENS` double NOT NULL DEFAULT '0',
  `MITTAGS` double NOT NULL DEFAULT '0',
  `NACHMITTAGS` double NOT NULL DEFAULT '0',
  `ABENDS` double NOT NULL DEFAULT '0',
  `VERSCHREIBUNGSGRUND` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FREMDARZT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DM_END_DATUM` date DEFAULT NULL,
  `REZEPTUR_REZEPT_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ELGA` tinyint(4) DEFAULT NULL,
  `VERORDNUNG_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERORDNUNG_AENDERUNG` datetime DEFAULT NULL,
  `INTERAKTION_CHECK` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `DOKU_ID` (`DOKU_ID`),
  KEY `PATIENT_ID` (`PATIENT_ID`),
  KEY `UNTERSUCHUNG_ID` (`UNTERSUCHUNG_ID`),
  KEY `ZEILE_LOCK` (`ZEILE_LOCK`),
  KEY `PHARMA_NR` (`PHARMA_NR`),
  KEY `STATUS` (`STATUS`),
  KEY `MEDIKAMENT` (`MEDIKAMENT`),
  KEY `REZEPT_DATUM` (`REZEPT_DATUM`),
  KEY `DATUM` (`DATUM`),
  KEY `TYP` (`TYP`),
  KEY `END_DATUM` (`END_DATUM`)
) ENGINE=MyISAM AUTO_INCREMENT=12144 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MEDIKAMENT_MELDUNG`
--

DROP TABLE IF EXISTS `MEDIKAMENT_MELDUNG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MEDIKAMENT_MELDUNG` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BENUTZER` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `PZN` int(11) NOT NULL DEFAULT '-1',
  `WERT` int(11) NOT NULL DEFAULT '-1',
  `LETZTE_AENDERUNG` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `BENUTZER_PZN` (`BENUTZER`,`PZN`),
  KEY `BENUTZER` (`BENUTZER`),
  KEY `PZN` (`PZN`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MEDIZINFRAGEBOGEN_FRAGEN_STAMM`
--

DROP TABLE IF EXISTS `MEDIZINFRAGEBOGEN_FRAGEN_STAMM`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MEDIZINFRAGEBOGEN_FRAGEN_STAMM` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FRAGEBOGEN_ID` int(11) DEFAULT NULL,
  `FRAGEN_KUERZEL` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `FRAGEN_SPRACHE` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `REIHENFOLGE_NUMMER` int(11) DEFAULT NULL,
  `AKTIV` int(11) DEFAULT NULL,
  `TRENNLINIE` int(11) DEFAULT NULL,
  `NUMMERIERUNG` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `FRAGE` (`FRAGEBOGEN_ID`,`FRAGEN_KUERZEL`,`FRAGEN_SPRACHE`),
  KEY `FRAGEBOGEN_ID` (`FRAGEBOGEN_ID`),
  KEY `AKTIV` (`AKTIV`),
  KEY `REIHENFOLGE_NUMMER` (`REIHENFOLGE_NUMMER`),
  KEY `FRAGEN_KUERZEL` (`FRAGEN_KUERZEL`),
  KEY `FRAGEN_SPRACHE` (`FRAGEN_SPRACHE`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MEDIZINFRAGEBOGEN_STAMM`
--

DROP TABLE IF EXISTS `MEDIZINFRAGEBOGEN_STAMM`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MEDIZINFRAGEBOGEN_STAMM` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BEZEICHNUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MANDANTEN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EINSTELLUNGEN` text COLLATE utf8_unicode_ci,
  `WEBSTATUS` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `BEZEICHNUNG` (`BEZEICHNUNG`),
  KEY `ERMAECHTIGUNG` (`MANDANTEN`),
  KEY `MANDANTEN` (`MANDANTEN`),
  KEY `WEBSTATUS` (`WEBSTATUS`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MEDIZINFRAGE_ANTWORTEN`
--

DROP TABLE IF EXISTS `MEDIZINFRAGE_ANTWORTEN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MEDIZINFRAGE_ANTWORTEN` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PATIENT_ID` int(11) DEFAULT NULL,
  `MEDIZINFRAGEBOGEN_FRAGEN_ID` int(11) DEFAULT NULL,
  `FRAGEN_KUERZEL` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FRAGEN_SPRACHE` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FRAGEN_ANTWORT` int(11) DEFAULT NULL,
  `FRAGEN_BEMERKUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FRAGEN_STATUS` int(11) DEFAULT NULL,
  `FRAGEN_AENDERUNG` int(11) DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DOKU_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `FRAGE` (`PATIENT_ID`,`FRAGEN_KUERZEL`,`FRAGEN_SPRACHE`),
  UNIQUE KEY `FRAGE_PAT` (`FRAGEN_KUERZEL`,`FRAGEN_SPRACHE`,`PATIENT_ID`),
  UNIQUE KEY `FRAGE_DOKU` (`FRAGEN_KUERZEL`,`FRAGEN_SPRACHE`,`DOKU_ID`),
  KEY `PATIENT_ID` (`PATIENT_ID`),
  KEY `FRAGEN_NUMMER` (`MEDIZINFRAGEBOGEN_FRAGEN_ID`),
  KEY `FRAGEN_STATUS` (`FRAGEN_STATUS`),
  KEY `DOKU_ID` (`DOKU_ID`),
  KEY `ZEILE_LOCK` (`ZEILE_LOCK`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MEDIZINFRAGE_STAMM`
--

DROP TABLE IF EXISTS `MEDIZINFRAGE_STAMM`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MEDIZINFRAGE_STAMM` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `KUERZEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FRAGE_TEXT` text COLLATE utf8_unicode_ci,
  `SPRACHE` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TYP` int(11) DEFAULT NULL,
  `KATEGORIE` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `KUERZEL_SPRACHE` (`KUERZEL`,`SPRACHE`),
  KEY `TYP` (`TYP`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MEDIZINSTATUS`
--

DROP TABLE IF EXISTS `MEDIZINSTATUS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MEDIZINSTATUS` (
  `DOKU_ID` int(11) NOT NULL DEFAULT '0',
  `MEDIZINSTATUS_TYP` int(11) DEFAULT NULL,
  `MEDIZINSTATUS_STATUS` int(11) DEFAULT NULL,
  `SPRACHE` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  UNIQUE KEY `DOKU_ID` (`DOKU_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MEDIZINSTATUS_ANTWORTEN`
--

DROP TABLE IF EXISTS `MEDIZINSTATUS_ANTWORTEN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MEDIZINSTATUS_ANTWORTEN` (
  `MedizinStatusDokuID` int(11) NOT NULL DEFAULT '0',
  `MedizinFrageStammID` int(11) DEFAULT NULL,
  `MedizinFrageStammKuerzel` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `MedizinFrageStammSprache` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `Antwort` int(11) DEFAULT NULL,
  `AntwortBemerkung` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`MedizinStatusDokuID`,`MedizinFrageStammKuerzel`,`MedizinFrageStammSprache`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MKPBUCH`
--

DROP TABLE IF EXISTS `MKPBUCH`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MKPBUCH` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MANDANT_ID` int(11) DEFAULT NULL,
  `PATIENT_ID` int(11) DEFAULT NULL,
  `UNTERSUCHUNG_ID` int(11) DEFAULT NULL,
  `TYP` int(11) DEFAULT NULL,
  `SUBTYP` int(11) DEFAULT NULL,
  `TERMIN_VORLAGE_KENNUNG` int(11) DEFAULT NULL,
  `DATUM` date DEFAULT NULL,
  `BEZEICHNUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ERGEBNIS` int(11) DEFAULT NULL,
  `BEMERKUNG` mediumtext COLLATE utf8_unicode_ci,
  `FREITEXT1` mediumtext COLLATE utf8_unicode_ci,
  `FREITEXT2` mediumtext COLLATE utf8_unicode_ci,
  `STATUS` int(11) DEFAULT NULL,
  `SYSTEM_DATUM` date DEFAULT NULL,
  `SYSTEM_UHRZEIT` time DEFAULT NULL,
  `BEARBEITER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROTOKOLL` mediumtext COLLATE utf8_unicode_ci,
  `MKP_KENNUNG` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `MKP_KENNUNG` (`MKP_KENNUNG`),
  KEY `PATIENT_ID` (`PATIENT_ID`),
  KEY `TYP` (`TYP`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `NACHRICHT_VERSAND`
--

DROP TABLE IF EXISTS `NACHRICHT_VERSAND`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `NACHRICHT_VERSAND` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NACHRICHT_ART` int(11) DEFAULT NULL,
  `LOKALE_DATEN_ID` int(11) DEFAULT NULL,
  `NACHRICHT_TYP` int(11) DEFAULT NULL,
  `STATUS` tinyint(4) DEFAULT NULL,
  `EMPFAENGER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BETREFF` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NACHRICHT` text COLLATE utf8_unicode_ci,
  `VERSAND_DATUM` date DEFAULT NULL,
  `VERSAND_ABLAUFDATUM` date DEFAULT NULL,
  `WEB_STATUS` tinyint(4) DEFAULT NULL,
  `WEB_ID` int(11) DEFAULT NULL,
  `WEB_ANGELEGT` datetime DEFAULT NULL,
  `WEB_VERSAND` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NACHRICHT_ART_ID_TYP` (`NACHRICHT_ART`,`LOKALE_DATEN_ID`,`NACHRICHT_TYP`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `NLG`
--

DROP TABLE IF EXISTS `NLG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `NLG` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PATIENT_ID` int(11) NOT NULL DEFAULT '-1',
  `DOKU_ID` int(11) NOT NULL DEFAULT '-1',
  `NUMMER` int(11) DEFAULT NULL,
  `GESCHWINDIGKEIT_RE` double DEFAULT NULL,
  `STRECKE_RE` double DEFAULT NULL,
  `GESCHWINDIGKEIT_LI` double DEFAULT NULL,
  `STRECKE_LI` double DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `OPERATION`
--

DROP TABLE IF EXISTS `OPERATION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OPERATION` (
  `OPERATION_DOKUID` int(11) NOT NULL DEFAULT '0',
  `OP_TEAM` text COLLATE utf8_unicode_ci,
  `OP_BEZEICHNUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OP_STARTUHRZEIT` time DEFAULT NULL,
  `OP_ENDUHRZEIT` time DEFAULT NULL,
  `KOMPLIKATIONEN` mediumtext COLLATE utf8_unicode_ci,
  `LAGERUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NARKOSE_ART` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NARKOSE_MITTEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NARKOSE_ZUSATZTEXT` text COLLATE utf8_unicode_ci,
  `OP_VERLAUF` longtext COLLATE utf8_unicode_ci,
  `DIAGNOSE` text COLLATE utf8_unicode_ci,
  UNIQUE KEY `OPERATION_DOKUID` (`OPERATION_DOKUID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `OPS`
--

DROP TABLE IF EXISTS `OPS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OPS` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PATIENT_ID` int(11) NOT NULL DEFAULT '-1',
  `OPDOKU_ID` int(11) NOT NULL DEFAULT '-1',
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STATUS` int(11) NOT NULL DEFAULT '-1',
  `DATUM` date DEFAULT NULL,
  `UHRZEIT` time DEFAULT NULL,
  `SCHLUESSEL` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SEITENLOKALISATION` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `OPS_TEXT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OPS_DATUM` date DEFAULT NULL,
  `OPS_UHRZEIT` time DEFAULT NULL,
  `BEARBEITER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROTOKOLL` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `DIAGNOSEDOKU_ID` (`OPDOKU_ID`),
  KEY `PATIENT_ID` (`PATIENT_ID`),
  KEY `ZEILE_LOCK` (`ZEILE_LOCK`),
  KEY `DATUM` (`DATUM`),
  KEY `STATUS` (`STATUS`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ORGANISATION`
--

DROP TABLE IF EXISTS `ORGANISATION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ORGANISATION` (
  `ID` int(11) NOT NULL DEFAULT '0',
  `TYP` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROTOKOLL` mediumtext COLLATE utf8_unicode_ci,
  `NOTIZ` mediumtext COLLATE utf8_unicode_ci,
  `STATUS` int(11) DEFAULT NULL,
  `KENNWORT` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PATIENT`
--

DROP TABLE IF EXISTS `PATIENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PATIENT` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `KUE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ANREDE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TITEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VORNAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WVORNAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GEBNAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAMENSZUSATZ` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAMENSVORSATZ` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GEBDATUM` date DEFAULT NULL,
  `BILD` blob,
  `NOTIZEN` text COLLATE utf8_unicode_ci,
  `PROTOKOLL` text COLLATE utf8_unicode_ci,
  `ADRESSE_TYP` int(1) DEFAULT NULL,
  `STRASSE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HAUSNUMMER` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ADRESSZUSATZ` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PLZ` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ORT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAND` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ANAMNESE1` mediumtext COLLATE utf8_unicode_ci,
  `ANAMNESE2` mediumtext COLLATE utf8_unicode_ci,
  `ANAMNESE3` mediumtext COLLATE utf8_unicode_ci,
  `TELEFON` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TELEFON_2` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FAX` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MOBIL` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EMAIL` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BANK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BANK_PLZ` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BANK_ORT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BANK_LAND` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KTO_NR` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BLZ` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KTO_INHABER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STATUS` int(11) NOT NULL DEFAULT '0',
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERSICHERUNGSNR` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CAVE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `INFO` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LETZTE_AENDERUNG_DATUM` date DEFAULT NULL,
  `LETZTE_AENDERUNG_UHRZEIT` time DEFAULT NULL,
  `FREITEXT1` text COLLATE utf8_unicode_ci,
  `FREITEXT2` text COLLATE utf8_unicode_ci,
  `STERBEDATUM` date DEFAULT NULL,
  `ENTBINDUNGSDATUM` date DEFAULT NULL,
  `NATIONALITAET` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FAMILIENSTAND` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KENNZEICHEN1` bigint(20) NOT NULL DEFAULT '-1',
  `KENNZEICHEN2` int(4) NOT NULL DEFAULT '-1',
  `ZUSATZ` mediumtext COLLATE utf8_unicode_ci,
  `EINSTELLUNGEN` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `BRIEFDATEN` mediumtext COLLATE utf8_unicode_ci,
  `ALTSYSTEM_ID` bigint(20) DEFAULT NULL,
  `KIS_ID` bigint(20) DEFAULT NULL,
  `KENNZEICHEN3` int(11) DEFAULT NULL,
  `HAUSARZT_VPNR` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ABWEICHENDE_ANSCHRIFT` mediumtext COLLATE utf8_unicode_ci,
  `BRIEF_VERSAND` text COLLATE utf8_unicode_ci,
  `BANK_IBAN` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BANK_BIC` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AUFNAHME_ZEIT` datetime DEFAULT NULL,
  `GESCHLECHT` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ALTSYSTEM_ID` (`ALTSYSTEM_ID`) USING BTREE,
  UNIQUE KEY `KIS_ID` (`KIS_ID`) USING BTREE,
  KEY `VERSICHERUNGSNR` (`VERSICHERUNGSNR`),
  KEY `NAME` (`NAME`),
  KEY `VORNAME` (`VORNAME`),
  KEY `GEBDATUM` (`GEBDATUM`),
  KEY `ZEILE_LOCK` (`ZEILE_LOCK`)
) ENGINE=MyISAM AUTO_INCREMENT=1347 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PATIENTMEDIZIN`
--

DROP TABLE IF EXISTS `PATIENTMEDIZIN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PATIENTMEDIZIN` (
  `PATIENT_ID` int(11) NOT NULL DEFAULT '0',
  `ANZSCHWANGERSCHAFTEN` int(2) DEFAULT NULL,
  `ANZGEBURTEN` int(2) DEFAULT NULL,
  `ANZABORTE` int(2) DEFAULT NULL,
  `ANZKINDER` int(2) DEFAULT NULL,
  `BLUTGRUPPE` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ANAMNESE1` mediumtext COLLATE utf8_unicode_ci,
  `ANAM1_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ANAMNESE2` mediumtext COLLATE utf8_unicode_ci,
  `ANAM2_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ANAMNESE3` mediumtext COLLATE utf8_unicode_ci,
  `ANAM3_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CAVE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `INFO` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BERUF` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FAMILIENSTAND` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RISIKOFAKTOREN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EINSTELLUNGEN` mediumtext COLLATE utf8_unicode_ci,
  `LETZTE_REGEL` date DEFAULT NULL,
  PRIMARY KEY (`PATIENT_ID`),
  KEY `ZEILE_LOCK` (`ZEILE_LOCK`),
  KEY `ANAM1_LOCK` (`ANAM1_LOCK`),
  KEY `ANAM2_LOCK` (`ANAM2_LOCK`),
  KEY `ANAM3_LOCK` (`ANAM3_LOCK`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PATIENTZUSATZ`
--

DROP TABLE IF EXISTS `PATIENTZUSATZ`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PATIENTZUSATZ` (
  `PATIENT_ID` int(11) NOT NULL DEFAULT '-1',
  `NSF` int(5) DEFAULT NULL,
  `TH_Stop` int(5) DEFAULT NULL,
  `ANAM1_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ANAM2_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ANAM3_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SPRACHE` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HAUSARZT` text COLLATE utf8_unicode_ci,
  `ANZSCHWANGERSCHAFTEN` int(2) DEFAULT '-1',
  `ANZGEBURTEN` int(2) DEFAULT '-1',
  `ANZKINDER` int(2) DEFAULT '-1',
  `KENNZEICHEN1` bigint(20) DEFAULT '-1',
  `KENNZEICHEN2` int(11) DEFAULT '-1',
  `KENNZEICHEN3` int(11) DEFAULT '-1',
  `KENNZEICHEN4` int(5) DEFAULT '-1',
  `KENNZEICHEN5` int(5) DEFAULT '-1',
  `KENNZEICHEN6` int(5) DEFAULT '-1',
  `KENNZEICHEN7` int(5) DEFAULT '-1',
  `KENNZEICHEN8` int(5) DEFAULT '-1',
  `KENNZEICHEN9` int(5) DEFAULT '-1',
  `KENNZEICHEN10` int(5) DEFAULT '-1',
  `NATIONALITAET` varchar(5) COLLATE utf8_unicode_ci DEFAULT '-1',
  `BLUTGRUPPE` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `QUITTUNG` int(1) NOT NULL DEFAULT '0',
  `SUCHNAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SERIENBRIEF_VERSANDARTEN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SERIENBRIEF_STDVERSANDART` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SERIENBRIEF_TYPEN` text COLLATE utf8_unicode_ci,
  `PARTNER_PATIENT_ID` int(11) DEFAULT NULL,
  `WEB_STATUS` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`PATIENT_ID`),
  KEY `KENNZEICHEN1` (`KENNZEICHEN1`),
  KEY `KENNZEICHEN2` (`KENNZEICHEN2`),
  KEY `QUITTUNG` (`QUITTUNG`),
  KEY `SUCHNAME` (`SUCHNAME`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PERSON`
--

DROP TABLE IF EXISTS `PERSON`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PERSON` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ANREDE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TITEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VORNAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WVORNAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GEBURTSNAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAME_VORSATZ` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAME_ZUSATZ` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KUERZEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GEBURTSDATUM` date DEFAULT NULL,
  `STERBEDATUM` date DEFAULT NULL,
  `ADRESSE` int(11) DEFAULT NULL,
  `POST_ADRESSE` int(11) DEFAULT NULL,
  `LIEFER_ADRESSE` int(11) DEFAULT NULL,
  `RECHNUNG_ADRESSE` int(11) DEFAULT NULL,
  `KOMMUNIKATION` int(11) DEFAULT NULL,
  `KONTO` int(11) DEFAULT NULL,
  `BILD` blob,
  `NOTIZEN` text COLLATE utf8_unicode_ci,
  `PROTOKOLL` text COLLATE utf8_unicode_ci,
  `LETZTE_AENDERUNG` datetime DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STATUS` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `NAME` (`NAME`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PHLEBOSTATUS`
--

DROP TABLE IF EXISTS `PHLEBOSTATUS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PHLEBOSTATUS` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PATIENT_ID` int(11) DEFAULT NULL,
  `DOKU_ID` int(11) DEFAULT NULL,
  `TYP` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATUM` date DEFAULT NULL,
  `PROPERTIES` mediumtext COLLATE utf8_unicode_ci,
  `BEMERKUNG` mediumtext COLLATE utf8_unicode_ci,
  `STATUS` int(11) DEFAULT NULL,
  `SYSTEM_DATUM` date DEFAULT NULL,
  `SYSTEM_UHRZEIT` time DEFAULT NULL,
  `BEARBEITER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROTOKOLL` mediumtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PHYSIS`
--

DROP TABLE IF EXISTS `PHYSIS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PHYSIS` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PATIENT_ID` int(11) NOT NULL DEFAULT '0',
  `TYP` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `DATUM` date DEFAULT NULL,
  `MONAT` double(3,0) NOT NULL DEFAULT '0',
  `GROESSE` double DEFAULT NULL,
  `KOPFUMFANG` double DEFAULT NULL,
  `GEWICHT` double DEFAULT NULL,
  `PERCENTILE` int(3) DEFAULT '0',
  `REFERENZ` int(3) DEFAULT '0',
  `BEMERKUNG` mediumtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=383 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PHYS_THERAPIE`
--

DROP TABLE IF EXISTS `PHYS_THERAPIE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PHYS_THERAPIE` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TH_KUERZEL` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `TH_BEZEICHNUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TH_KKBEZEICH` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TH_TYP` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TH_MAX` int(11) NOT NULL DEFAULT '10',
  `TH_DEF_VERRECHNUNGSART` int(11) DEFAULT NULL,
  `TH_CHAR` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TH_TM` double NOT NULL DEFAULT '2',
  `TH_KM` double NOT NULL DEFAULT '10',
  `TH_EINZELTERMIN` tinyint(4) NOT NULL DEFAULT '0',
  `GT_TYP` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TH_VF` tinyint(4) NOT NULL DEFAULT '0',
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GRUPPE` int(11) NOT NULL DEFAULT '0',
  `EINSTELLUNG` text COLLATE utf8_unicode_ci,
  `BENUTZER_ID` int(11) DEFAULT NULL,
  `PROTOKOLL` text COLLATE utf8_unicode_ci,
  `LETZTE_AENDERUNG` datetime DEFAULT NULL,
  `PRIVAT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KASSE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ERMAECHTIGUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FEIERTAGSTERMIN` tinyint(4) NOT NULL DEFAULT '0',
  `KALENDER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WARTELISTE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WEB_STATUS` tinyint(4) DEFAULT NULL,
  `WEB_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WEB_BESCHREIBUNG` text COLLATE utf8_unicode_ci,
  `WEB_HINWEISTEXT` text COLLATE utf8_unicode_ci,
  `WEB_PROPERTIES` text COLLATE utf8_unicode_ci,
  `STATUS` int(11) DEFAULT NULL,
  `GERAET` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MODALITAET` int(11) DEFAULT NULL,
  `TYP_PROFIL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROFIL_PARAMETER_IDS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FAVORIT_POSITION` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `TH_KUERZEL` (`TH_KUERZEL`),
  KEY `WEB_STATUS` (`WEB_STATUS`)
) ENGINE=MyISAM AUTO_INCREMENT=437 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PHYS_THERAPIE_REGION`
--

DROP TABLE IF EXISTS `PHYS_THERAPIE_REGION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PHYS_THERAPIE_REGION` (
  `THERAPIE_ID` int(11) NOT NULL DEFAULT '-1',
  `REGION_ID` int(11) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`THERAPIE_ID`,`REGION_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PLZ_AUT`
--

DROP TABLE IF EXISTS `PLZ_AUT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PLZ_AUT` (
  `PLZ` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ORT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BUNDESLAND` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZUSATZ` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ORT2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `PLZ` (`PLZ`),
  KEY `ORT` (`ORT`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PLZ_DE`
--

DROP TABLE IF EXISTS `PLZ_DE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PLZ_DE` (
  `PLZ` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ORT` varchar(31) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BUNDESLAND` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZUSATZ` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ORT2` varchar(24) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `PLZ` (`PLZ`),
  KEY `ORT` (`ORT`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PRAXIS_DATEN`
--

DROP TABLE IF EXISTS `PRAXIS_DATEN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PRAXIS_DATEN` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `KUERZEL` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `EINRICHTUNG_TYP` int(11) DEFAULT NULL,
  `HV_NUMMER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DV_NUMMER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ADRESS_CODE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FACHGEBIET` int(5) NOT NULL DEFAULT '-1',
  `KOM_ANREDE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ADRESSE` text COLLATE utf8_unicode_ci,
  `KOMMUNIKATION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KONTO` text COLLATE utf8_unicode_ci,
  `DATUM` date DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STATUS` int(11) NOT NULL DEFAULT '0',
  `TYP` int(11) NOT NULL DEFAULT '0',
  `GUELTIGKEIT` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NOTIZEN` text COLLATE utf8_unicode_ci,
  `PROTOKOLL` text COLLATE utf8_unicode_ci,
  `BUNDESLAND_KASSE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BRIEFDATEN` text COLLATE utf8_unicode_ci,
  `LETZTE_AENDERUNG_DATUM` date DEFAULT NULL,
  `LETZTE_AENDERUNG_UHRZEIT` time DEFAULT NULL,
  `EINSTELLUNGEN` mediumtext COLLATE utf8_unicode_ci,
  `GNW_EINSTELLUNGEN` mediumtext COLLATE utf8_unicode_ci,
  `PVS_EINSTELLUNGEN` mediumtext COLLATE utf8_unicode_ci,
  `ANSCHRIFT_ZEILE1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ANSCHRIFT_ZEILE2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `KUERZEL` (`KUERZEL`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PRODUKT`
--

DROP TABLE IF EXISTS `PRODUKT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PRODUKT` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MODUL` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EINSTELLUNG` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WERT` text COLLATE utf8_unicode_ci,
  `TYP` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEZEICHNUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BESCHREIBUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRODUKT` int(11) DEFAULT NULL,
  `LAND` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LIZENZ` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `TYP_2` (`TYP`,`BEZEICHNUNG`),
  KEY `TYP` (`TYP`),
  KEY `BEZEICHNUNG` (`BEZEICHNUNG`)
) ENGINE=MyISAM AUTO_INCREMENT=334 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PROTOKOLL_ABRECHNUNG`
--

DROP TABLE IF EXISTS `PROTOKOLL_ABRECHNUNG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PROTOKOLL_ABRECHNUNG` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYP` int(11) NOT NULL DEFAULT '-1',
  `MANDANT_ID` int(11) NOT NULL DEFAULT '-1',
  `SITZUNGS_NR` bigint(11) DEFAULT NULL,
  `DATUM` date DEFAULT NULL,
  `UHRZEIT` time DEFAULT NULL,
  `ABR_DATEI` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROTOKOLL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `QUARTAL` int(11) DEFAULT NULL,
  `JAHR` int(11) DEFAULT NULL,
  `KASSE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STATUS` int(11) NOT NULL DEFAULT '-1',
  `UUID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `SITZUNGS_NR` (`SITZUNGS_NR`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PROTOKOLL_DATEN`
--

DROP TABLE IF EXISTS `PROTOKOLL_DATEN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PROTOKOLL_DATEN` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATEN_ID` int(11) NOT NULL DEFAULT '-1',
  `DATEN_TYP` int(11) NOT NULL DEFAULT '0',
  `TYP` int(11) NOT NULL DEFAULT '-1',
  `DATUM` date DEFAULT NULL,
  `UHRZEIT` time DEFAULT NULL,
  `PROTOKOLL` longtext COLLATE utf8_unicode_ci,
  `PROTOKOLL_KURZTEXT` mediumtext COLLATE utf8_unicode_ci,
  `SITZUNGS_NR` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `DATEN` (`DATEN_ID`,`DATEN_TYP`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PROTOKOLL_DATENSICHERUNG`
--

DROP TABLE IF EXISTS `PROTOKOLL_DATENSICHERUNG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PROTOKOLL_DATENSICHERUNG` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYP` tinyint(4) DEFAULT NULL,
  `STATUS` tinyint(4) DEFAULT NULL,
  `SITZUNGS_NR` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATUM` date DEFAULT NULL,
  `UHRZEIT` time DEFAULT NULL,
  `PROTOKOLL_DATEI_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `AUSWERTUNG_LISTE_ID` (`STATUS`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PROTOKOLL_DOKU`
--

DROP TABLE IF EXISTS `PROTOKOLL_DOKU`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PROTOKOLL_DOKU` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DOKU_ID` int(11) NOT NULL DEFAULT '-1',
  `TYP` int(11) NOT NULL DEFAULT '-1',
  `DATUM` date DEFAULT NULL,
  `UHRZEIT` time DEFAULT NULL,
  `PROTOKOLL` longtext COLLATE utf8_unicode_ci,
  `PROTOKOLL_KURZTEXT` mediumtext COLLATE utf8_unicode_ci,
  `SITZUNGS_NR` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `DOKU_ID` (`DOKU_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=176242 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PROTOKOLL_EINTRAG_SERIENBRIEF`
--

DROP TABLE IF EXISTS `PROTOKOLL_EINTRAG_SERIENBRIEF`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PROTOKOLL_EINTRAG_SERIENBRIEF` (
  `PROTOKOLL_SERIENBRIEF_ID` int(11) NOT NULL DEFAULT '0',
  `ADRESSAT_ID` int(11) DEFAULT NULL,
  `VERSANDART` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STATUS_TEXT` text COLLATE utf8_unicode_ci,
  `ADRESSAT_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `PROTOKOLL_SERIENBRIEF_ID` (`PROTOKOLL_SERIENBRIEF_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PROTOKOLL_IMPORT`
--

DROP TABLE IF EXISTS `PROTOKOLL_IMPORT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PROTOKOLL_IMPORT` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYP` int(11) NOT NULL DEFAULT '-1',
  `ERMAECHTIGUNG_ID` int(11) NOT NULL DEFAULT '-1',
  `BENUTZER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATUM` date DEFAULT NULL,
  `UHRZEIT` time DEFAULT NULL,
  `SITZUNGS_NR` bigint(20) NOT NULL DEFAULT '-1',
  `PROTOKOLL` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ERMAECHTIGUNG_ID` (`ERMAECHTIGUNG_ID`),
  KEY `TYP` (`TYP`),
  KEY `DATUM` (`DATUM`)
) ENGINE=MyISAM AUTO_INCREMENT=2152 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PROTOKOLL_RECHNER`
--

DROP TABLE IF EXISTS `PROTOKOLL_RECHNER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PROTOKOLL_RECHNER` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RECHNER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HASH` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DETAILS` text COLLATE utf8_unicode_ci,
  `AKTIV` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PROTOKOLL_SENDEN`
--

DROP TABLE IF EXISTS `PROTOKOLL_SENDEN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PROTOKOLL_SENDEN` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYP` int(11) NOT NULL DEFAULT '-1',
  `ERMAECHTIGUNG_ID` int(11) NOT NULL DEFAULT '-1',
  `SITZUNGS_NR` bigint(11) DEFAULT NULL,
  `DATUM` date DEFAULT NULL,
  `UHRZEIT` time DEFAULT NULL,
  `SENDE_ALB` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `INFO_TEXT` text COLLATE utf8_unicode_ci,
  `STATUS` int(11) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`ID`),
  KEY `SITZUNGS_NR` (`SITZUNGS_NR`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PROTOKOLL_SERIENBRIEF`
--

DROP TABLE IF EXISTS `PROTOKOLL_SERIENBRIEF`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PROTOKOLL_SERIENBRIEF` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `AUSWERTUNG_LISTE_ID` int(11) DEFAULT NULL,
  `TYP` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATUM` date DEFAULT NULL,
  `UHRZEIT` time DEFAULT NULL,
  `BENUTZER_ID` int(11) DEFAULT NULL,
  `MANDANT_ID` int(11) DEFAULT NULL,
  `DRUCKVORLAGE_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `AUSWERTUNG_LISTE_ID` (`AUSWERTUNG_LISTE_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PROTOKOLL_SITZUNG`
--

DROP TABLE IF EXISTS `PROTOKOLL_SITZUNG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PROTOKOLL_SITZUNG` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYP` int(11) DEFAULT NULL,
  `RECHNER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OS` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `JAVA` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ACETO_VERSION` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BENUTZER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATUM_AN` date DEFAULT NULL,
  `UHRZEIT_AN` time DEFAULT NULL,
  `UHRZEIT_AKTION` time DEFAULT NULL,
  `DATUM_AB` date DEFAULT NULL,
  `UHRZEIT_AB` time DEFAULT NULL,
  `DAUER` bigint(20) DEFAULT NULL,
  `SITZUNGS_NR` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `RECHNER` (`RECHNER`),
  KEY `SITZUNGS_NR` (`SITZUNGS_NR`),
  KEY `DATUM_AB` (`DATUM_AB`),
  KEY `DATUM_AN` (`DATUM_AN`)
) ENGINE=MyISAM AUTO_INCREMENT=2158 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PROTOKOLL_SPVM`
--

DROP TABLE IF EXISTS `PROTOKOLL_SPVM`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PROTOKOLL_SPVM` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SITZUNGS_NR` bigint(11) DEFAULT NULL,
  `MAXIMAL` int(11) DEFAULT NULL,
  `FREI` int(11) DEFAULT NULL,
  `TOTAL` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `SITZUNGS_NR` (`SITZUNGS_NR`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PROTOKOLL_STAMM`
--

DROP TABLE IF EXISTS `PROTOKOLL_STAMM`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PROTOKOLL_STAMM` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `STAMM_ID` int(11) NOT NULL DEFAULT '-1',
  `STAMM_TYP` int(11) NOT NULL DEFAULT '0',
  `TYP` int(11) NOT NULL DEFAULT '-1',
  `DATUM` date DEFAULT NULL,
  `UHRZEIT` time DEFAULT NULL,
  `PROTOKOLL` longtext COLLATE utf8_unicode_ci,
  `PROTOKOLL_KURZTEXT` mediumtext COLLATE utf8_unicode_ci,
  `SITZUNGS_NR` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `STAMM` (`STAMM_ID`,`STAMM_TYP`)
) ENGINE=MyISAM AUTO_INCREMENT=1036 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PROTOKOLL_VERWALTUNG`
--

DROP TABLE IF EXISTS `PROTOKOLL_VERWALTUNG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PROTOKOLL_VERWALTUNG` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `VERWALTUNG_ID` int(11) NOT NULL DEFAULT '-1',
  `VERWALTUNG_TYP` int(11) NOT NULL DEFAULT '0',
  `TYP` int(11) NOT NULL DEFAULT '-1',
  `DATUM` date DEFAULT NULL,
  `UHRZEIT` time DEFAULT NULL,
  `PROTOKOLL` longtext COLLATE utf8_unicode_ci,
  `PROTOKOLL_KURZTEXT` mediumtext COLLATE utf8_unicode_ci,
  `SITZUNGS_NR` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `VERWALTUNG` (`VERWALTUNG_ID`,`VERWALTUNG_TYP`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PROTOKOLL_WEBTERMINE`
--

DROP TABLE IF EXISTS `PROTOKOLL_WEBTERMINE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PROTOKOLL_WEBTERMINE` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SITZUNGS_NR` bigint(20) DEFAULT NULL,
  `TYP` int(11) NOT NULL DEFAULT '-1',
  `ZEIT` datetime DEFAULT NULL,
  `PROTOKOLL` longtext COLLATE utf8_unicode_ci,
  `TERMIN_ID_LISTE` mediumtext COLLATE utf8_unicode_ci,
  `START_DATUM` date DEFAULT NULL,
  `END_DATUM` date DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RECHNUNG`
--

DROP TABLE IF EXISTS `RECHNUNG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RECHNUNG` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RECHNUNGDOKU_ID` int(11) NOT NULL DEFAULT '-1',
  `UNTERSUCHUNG_ID` int(11) NOT NULL DEFAULT '-1',
  `RE_NR` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RE_TEXT` text COLLATE utf8_unicode_ci,
  `RE_ANSCHRIFT` text COLLATE utf8_unicode_ci,
  `RE_ANSCHRIFT_XML` text COLLATE utf8_unicode_ci,
  `RE_ABSENDE_ANSCHRIFT` text COLLATE utf8_unicode_ci,
  `RE_VERSANDART` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RE_POSITIONEN` text COLLATE utf8_unicode_ci,
  `RE_ANZAHL_POS` int(11) NOT NULL DEFAULT '-1',
  `RE_SUMME_NETTO` double DEFAULT NULL,
  `RE_SUMME_NETTO_1` double DEFAULT NULL,
  `RE_SUMME_NETTO_2` double DEFAULT NULL,
  `RE_SUMME_BRUTTO` double DEFAULT NULL,
  `RE_SUMME_BRUTTO_1` double DEFAULT NULL,
  `RE_SUMME_BRUTTO_2` double DEFAULT NULL,
  `RE_MWST_1` double DEFAULT NULL,
  `RE_MWST_2` double DEFAULT NULL,
  `RE_SUMME_MWST` double DEFAULT NULL,
  `RE_SUMME_MWST_1` double DEFAULT NULL,
  `RE_SUMME_MWST_2` double DEFAULT NULL,
  `RE_WAEHRUNG` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RE_UNTERSCHREIBER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RE_SIGNED` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RE_OFFENE_SUMME_BRUTTO` double NOT NULL DEFAULT '0',
  `RE_ZAEHLER` int(10) DEFAULT NULL,
  `DATUM_VERSENDET` date DEFAULT NULL,
  `DATUM_STORNIERT` date DEFAULT NULL,
  `STATUS_ZUSATZ` mediumtext COLLATE utf8_unicode_ci,
  `MAHN_STUFE` int(11) NOT NULL DEFAULT '0',
  `RE_JAHR` int(11) DEFAULT NULL,
  `RE_MANDANT_ID` int(11) DEFAULT NULL,
  `ABRECHNUNGSSCHEIN_ID` int(11) DEFAULT NULL,
  `DIAGNOSEN_XML` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `DOKU_ID` (`RECHNUNGDOKU_ID`),
  UNIQUE KEY `RECHNUNGDOKU_ID` (`RECHNUNGDOKU_ID`),
  UNIQUE KEY `RE_NR` (`RE_NR`),
  UNIQUE KEY `RE_ZAEHLER2` (`RE_ZAEHLER`,`RE_JAHR`,`RE_MANDANT_ID`) USING BTREE,
  KEY `RE_ZAEHLER` (`RE_ZAEHLER`),
  KEY `MAHN_STUFE` (`MAHN_STUFE`)
) ENGINE=MyISAM AUTO_INCREMENT=683 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RECHNUNGPOSITION`
--

DROP TABLE IF EXISTS `RECHNUNGPOSITION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RECHNUNGPOSITION` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RECHNUNGDOKU_ID` int(11) DEFAULT NULL,
  `POS_NR` int(11) DEFAULT NULL,
  `ANZAHL` double DEFAULT NULL,
  `POSITION_DATUM` date DEFAULT NULL,
  `EINZELPREIS` double DEFAULT NULL,
  `MWST_SATZ` int(11) DEFAULT NULL,
  `KONTIERUNG` int(11) DEFAULT NULL,
  `LEISTUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RECHNUNG_TEXT` text COLLATE utf8_unicode_ci,
  `BEGRUENDUNG` text COLLATE utf8_unicode_ci,
  `MWST_PREIS` double DEFAULT NULL,
  `BRUTTO_PREIS` double DEFAULT NULL,
  `BEARBEITER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SYSTEM_DATUM` date DEFAULT NULL,
  `SYSTEM_UHRZEIT` time DEFAULT NULL,
  `FAKTOR` double NOT NULL DEFAULT '1',
  `ABZUG` double NOT NULL DEFAULT '0',
  `TYP_KOSTEN` int(11) NOT NULL DEFAULT '0',
  `ZIFFER_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `RECHNUNGDOKU_ID` (`RECHNUNGDOKU_ID`),
  KEY `TYP_KOSTEN` (`TYP_KOSTEN`),
  KEY `POSITION_DATUM` (`POSITION_DATUM`)
) ENGINE=MyISAM AUTO_INCREMENT=1013 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `REGION`
--

DROP TABLE IF EXISTS `REGION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `REGION` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RE_KUERZEL` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `RE_BEZEICHNUNG` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LETZTE_AENDERUNG` datetime DEFAULT NULL,
  `BEARBEITER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RE_KUERZEL` (`RE_KUERZEL`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `REZEPT`
--

DROP TABLE IF EXISTS `REZEPT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `REZEPT` (
  `REZEPTDOKU_ID` int(11) NOT NULL DEFAULT '-1',
  `STATUS_REZEPT` int(3) DEFAULT NULL,
  `GEBUEHR_BEFREIT` int(3) DEFAULT NULL,
  `REZEPT_TYP` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HAUSAPOTHEKE` int(11) DEFAULT NULL,
  `ELGA` tinyint(4) DEFAULT NULL,
  `EMED_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EMED_DATAMATRIX` text COLLATE utf8_unicode_ci,
  `DATUM_GEDRUCKT` date DEFAULT NULL,
  `DATUM_STORNIERT` date DEFAULT NULL,
  `BEMERKUNG_STORNIERUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  UNIQUE KEY `REZEPTDOKU_ID` (`REZEPTDOKU_ID`),
  KEY `STATUS_REZEPT` (`STATUS_REZEPT`),
  KEY `GEBUEHR_BEFREIT` (`GEBUEHR_BEFREIT`),
  KEY `REZEPT_TYP` (`REZEPT_TYP`),
  KEY `HAUSAPOTHEKE` (`HAUSAPOTHEKE`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ROHDROGEN`
--

DROP TABLE IF EXISTS `ROHDROGEN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ROHDROGEN` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYP` int(11) DEFAULT NULL,
  `LATEIN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PINYIN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PINYIN_LAUTSCHRIFT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WARNHINWEIS` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BENSKYKATEGORIE` int(11) DEFAULT NULL,
  `CHINAZEICHEN_TRADITION` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CHINAZEICHEN_KURZFORM` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NOTIZ` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SCHWANGERSCHAFT`
--

DROP TABLE IF EXISTS `SCHWANGERSCHAFT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SCHWANGERSCHAFT` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NR` int(11) NOT NULL DEFAULT '-1',
  `LETZTE_REGEL_DATUM` date DEFAULT NULL,
  `GEPLANT_GEBURT_DATUM` date DEFAULT NULL,
  `HERZTAETIGKEIT_DATUM` date DEFAULT NULL,
  `GEBURT_ABORT_DATUM` date DEFAULT NULL,
  `GEBURT_ABORT_SSW` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GEBURT_TYP` int(2) DEFAULT NULL,
  `GEBURT_ART` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ABORT_GENETIK` text COLLATE utf8_unicode_ci,
  `UNTERSUCHUNG_TEXT` longtext COLLATE utf8_unicode_ci,
  `SAEUGLINGE` longtext COLLATE utf8_unicode_ci,
  `GEBURT_ABORT_BEMERKUNG` longtext COLLATE utf8_unicode_ci,
  `SCHWANGERSCHAFTS_DOKUID` int(11) DEFAULT NULL,
  `ZYKLUS_TAGE` int(2) DEFAULT NULL,
  `SCHWANGERSCHAFTSTEST` int(2) DEFAULT NULL,
  `ERRECHNETER_GEBTERMIN` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `INVITRO_ZYKLUS_ID` int(11) DEFAULT NULL,
  `MUTTERSCHUTZ_BEGINN_DATUM` date DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `NR` (`NR`),
  KEY `SCHWANGERSCHAFTS_DOKUID` (`SCHWANGERSCHAFTS_DOKUID`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SENDELISTE`
--

DROP TABLE IF EXISTS `SENDELISTE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SENDELISTE` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DOKU_ID` int(11) NOT NULL DEFAULT '-1',
  `SENDETYP` int(11) NOT NULL DEFAULT '-1',
  `SENDESTATUS` int(11) NOT NULL DEFAULT '-1',
  `WEBSTATUS` int(11) NOT NULL DEFAULT '-1',
  `ABSENDER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EMPFAENGER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BETREFF` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TEXT` longtext COLLATE utf8_unicode_ci,
  `DATEINAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BENUTZER_ID` int(11) NOT NULL DEFAULT '0',
  `DATUM_ERSTELLT` datetime DEFAULT NULL,
  `DATUM_UEBERTRAGEN` datetime DEFAULT NULL,
  `DATUM_GESENDET` datetime DEFAULT NULL,
  `RECHNER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `DOKU_ID` (`DOKU_ID`),
  KEY `SENDETYP` (`SENDETYP`),
  KEY `SENDESTATUS` (`SENDESTATUS`),
  KEY `WEBSTATUS` (`WEBSTATUS`),
  KEY `EMPFAENGER` (`EMPFAENGER`),
  KEY `DATUM_ERSTELLT` (`DATUM_ERSTELLT`),
  KEY `BENUTZER_ID` (`BENUTZER_ID`),
  KEY `DATUM_UEBERTRAGEN` (`DATUM_UEBERTRAGEN`),
  KEY `DATUM_GESENDET` (`DATUM_GESENDET`)
) ENGINE=MyISAM AUTO_INCREMENT=17305 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SERIENBRIEF`
--

DROP TABLE IF EXISTS `SERIENBRIEF`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SERIENBRIEF` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ACETO_NR` int(11) NOT NULL DEFAULT '-1',
  `POSITION` int(11) DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `REPORT` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LOGO` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EINSTELLUNGEN` mediumtext COLLATE utf8_unicode_ci,
  `PRIVAT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KASSE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ERMAECHTIGUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT '-1',
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEARBEITER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROTOKOLL` text COLLATE utf8_unicode_ci,
  `LETZTE_AENDERUNG_DATUM` date DEFAULT NULL,
  `LETZTE_AENDERUNG_UHRZEIT` time DEFAULT NULL,
  `TYP` int(11) DEFAULT NULL,
  `BEGRUESSUNG` text COLLATE utf8_unicode_ci,
  `EINLEITUNG` text COLLATE utf8_unicode_ci,
  `ABSATZ1` text COLLATE utf8_unicode_ci,
  `ABSATZ2` text COLLATE utf8_unicode_ci,
  `ABSATZ3` text COLLATE utf8_unicode_ci,
  `SCHLUSSFORMEL` text COLLATE utf8_unicode_ci,
  `UNTERSCHRIFT` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SIS_DOK`
--

DROP TABLE IF EXISTS `SIS_DOK`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SIS_DOK` (
  `DOKNUMMER` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DOKTYP` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DOKREDDAT` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DOKTITEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DOKURL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DOKTXT` longtext COLLATE utf8_unicode_ci,
  `DOKTXK` longtext COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SIS_DOK_ID`
--

DROP TABLE IF EXISTS `SIS_DOK_ID`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SIS_DOK_ID` (
  `INDNR` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DOKNUMMER` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SIS_PICTO`
--

DROP TABLE IF EXISTS `SIS_PICTO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SIS_PICTO` (
  `ZLNUMM` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ZNUMM` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ZBEZ` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PICTO` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SST` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SST2` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ZNUMM`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SIS_ZARZST`
--

DROP TABLE IF EXISTS `SIS_ZARZST`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SIS_ZARZST` (
  `YHNUMMER` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `YKZ` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `YTXT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `YHNUMMER1` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `YDH` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `YDU` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `YDRUCK` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`YHNUMMER`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SIS_ZATC`
--

DROP TABLE IF EXISTS `SIS_ZATC`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SIS_ZATC` (
  `ZATC` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ZATCBEZ` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZATCBEZE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZDRUCK` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ZATC`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SIS_ZATCL`
--

DROP TABLE IF EXISTS `SIS_ZATCL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SIS_ZATCL` (
  `ZNUMM` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ZATC` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ZSORT` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZDRUCK` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZKZ1` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ZNUMM`,`ZATC`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SIS_ZDAR`
--

DROP TABLE IF EXISTS `SIS_ZDAR`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SIS_ZDAR` (
  `YDAR` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `YBEZ` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `YHGRUPPE` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `YUGRUPPE` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`YDAR`,`YHGRUPPE`,`YUGRUPPE`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SIS_ZHERST`
--

DROP TABLE IF EXISTS `SIS_ZHERST`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SIS_ZHERST` (
  `YCODE` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `YNAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `YTEL` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `YPLZ` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `YORT` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `YSTRASSE` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `YADR2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `YDRUCK` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `YFAX` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `YMAIL` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`YCODE`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SIS_ZINDGRST`
--

DROP TABLE IF EXISTS `SIS_ZINDGRST`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SIS_ZINDGRST` (
  `ZINDNR` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ZINDBEZ` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZINDNR2` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZDRUCK` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ZINDNR`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SIS_ZINDL`
--

DROP TABLE IF EXISTS `SIS_ZINDL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SIS_ZINDL` (
  `ZNUMM` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ZINDNR` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ZSORT` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZDRUCK` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZKZ1` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ZNUMM`,`ZINDNR`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SIS_ZINTERL`
--

DROP TABLE IF EXISTS `SIS_ZINTERL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SIS_ZINTERL` (
  `ZINTERNR` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ZINTERGR` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ZINTERART` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `YHNUMMER` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `YHNUMMER1` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZSORT` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZDRUCK` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `YHNUMMER3` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ZINTERNR`,`ZINTERGR`,`ZINTERART`,`YHNUMMER`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SIS_ZINTERST`
--

DROP TABLE IF EXISTS `SIS_ZINTERST`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SIS_ZINTERST` (
  `ZINTERNR` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ZINTERNR2` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZINTERGR1` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZINTERGR2` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZGRUPPE` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZKURZ` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZART` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZDRUCK` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZTEXT` mediumtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ZINTERNR`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SIS_ZSPEZ`
--

DROP TABLE IF EXISTS `SIS_ZSPEZ`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SIS_ZSPEZ` (
  `ZNUMM` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ZLNUMM` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZBEZ` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZLAND` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZMONO` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZANZAHL` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZHERST` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZVERTR` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZEDAT` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZBDAT` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZBEZGR` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZVERFALL` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZVERFALL2` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZLAGER` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZLOESEN` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZWARTEZ` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZABGABE` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZNA` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZRP` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZWARN` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZSUCHT` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZDRUCK` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZDRUCK2` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZANWEND` mediumtext COLLATE utf8_unicode_ci,
  `ZHINW` mediumtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ZNUMM`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SIS_ZSTOFFL`
--

DROP TABLE IF EXISTS `SIS_ZSTOFFL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SIS_ZSTOFFL` (
  `ZNUMM` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `YHNUMMER` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `YHNUMMER1` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZKZ` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZMENGE` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZMENGEINH` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZTEXT` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZSORT` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZNR` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ZDRUCK` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZKZ1` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZKZ2` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `YHNUMMER3` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ZNUMM`,`ZNR`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SIS_ZSUBSTAN`
--

DROP TABLE IF EXISTS `SIS_ZSUBSTAN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SIS_ZSUBSTAN` (
  `ZINTERGR` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ZGRBEZ` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZART` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZINTERGR1` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZDRUCK` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ZINTERGR`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `STDDIAG`
--

DROP TABLE IF EXISTS `STDDIAG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `STDDIAG` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `POSITION` int(11) DEFAULT NULL,
  `KUERZEL` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DIAGNOSEN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ICD_KODES` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MANDANTEN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEARBEITER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROTOKOLL` mediumtext COLLATE utf8_unicode_ci,
  `LETZTE_AENDERUNG_DATUM` date DEFAULT NULL,
  `LETZTE_AENDERUNG_UHRZEIT` time DEFAULT NULL,
  `WAHLARZT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KASSE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TYP` int(11) DEFAULT NULL,
  `BG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERTRAGSART` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `KASSE` (`KASSE`),
  KEY `BG` (`BG`),
  KEY `WAHLARZT` (`WAHLARZT`),
  KEY `VERTRAGSART` (`VERTRAGSART`),
  KEY `MANDANTEN` (`MANDANTEN`)
) ENGINE=MyISAM AUTO_INCREMENT=75 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `STDDIAG_GYN`
--

DROP TABLE IF EXISTS `STDDIAG_GYN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `STDDIAG_GYN` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `POSITION` int(11) DEFAULT NULL,
  `KUERZEL` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DIAGNOSEN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ICD_KODES` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ERMAECHTIGUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEARBEITER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROTOKOLL` mediumtext COLLATE utf8_unicode_ci,
  `LETZTE_AENDERUNG_DATUM` date DEFAULT NULL,
  `LETZTE_AENDERUNG_UHRZEIT` time DEFAULT NULL,
  `PRIVAT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KASSE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TYP` int(11) DEFAULT NULL,
  `BG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=69 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `STDLEIST`
--

DROP TABLE IF EXISTS `STDLEIST`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `STDLEIST` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYP` tinyint(4) DEFAULT NULL,
  `POSITION_GRUPPE` int(11) DEFAULT NULL,
  `GRUPPE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `POSITION` int(11) DEFAULT NULL,
  `KASSE_ZIFFERN_KETTE` text COLLATE utf8_unicode_ci,
  `PRIVAT_ZIFFERN_KETTE` text COLLATE utf8_unicode_ci,
  `LEISTUNG_KETTE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BG_ZIFFERN_KETTE` text COLLATE utf8_unicode_ci,
  `BEZEICHNUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MANDANTEN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SCHEIN_KENNZEICHEN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SCHEIN_ART` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEARBEITER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROTOKOLL` mediumtext COLLATE utf8_unicode_ci,
  `LETZTE_AENDERUNG` datetime DEFAULT NULL,
  `KASSE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MODALITAET` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EINSTELLUNG` text COLLATE utf8_unicode_ci,
  `WAHLARZT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERTRAGSART` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `VERTRAGSART` (`VERTRAGSART`),
  KEY `POSITION` (`POSITION`),
  KEY `BG` (`BG`),
  KEY `SCHEIN_ART` (`SCHEIN_ART`),
  KEY `SCHEIN_KENNZEICHEN` (`SCHEIN_KENNZEICHEN`),
  KEY `KASSE` (`KASSE`),
  KEY `MODALITAET` (`MODALITAET`),
  KEY `WAHLARZT` (`WAHLARZT`),
  KEY `MANDANTEN` (`MANDANTEN`),
  KEY `BEZEICHNUNG` (`BEZEICHNUNG`),
  KEY `PRIVAT` (`WAHLARZT`),
  KEY `GRUPPE` (`GRUPPE`)
) ENGINE=MyISAM AUTO_INCREMENT=577 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `STDLEIST_GYN`
--

DROP TABLE IF EXISTS `STDLEIST_GYN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `STDLEIST_GYN` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `POSITION` int(11) DEFAULT NULL,
  `KASSE_ZIFFERN_KETTE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRIVAT_ZIFFERN_KETTE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LEISTUNG_KETTE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BG_ZIFFERN_KETTE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEZEICHNUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ERMAECHTIGUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SCHEIN_KENNZEICHEN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SCHEIN_ART` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEARBEITER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROTOKOLL` mediumtext COLLATE utf8_unicode_ci,
  `LETZTE_AENDERUNG_DATUM` date DEFAULT NULL,
  `LETZTE_AENDERUNG_UHRZEIT` time DEFAULT NULL,
  `KASSE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MODALITAET` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EINSTELLUNG` text COLLATE utf8_unicode_ci,
  `PRIVAT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERTRAGSART` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `VERTRAGSART` (`VERTRAGSART`),
  KEY `POSITION` (`POSITION`),
  KEY `PRIVAT` (`PRIVAT`),
  KEY `BG` (`BG`),
  KEY `SCHEIN_ART` (`SCHEIN_ART`),
  KEY `SCHEIN_KENNZEICHEN` (`SCHEIN_KENNZEICHEN`),
  KEY `ERMAECHTIGUNG` (`ERMAECHTIGUNG`),
  KEY `KASSE` (`KASSE`),
  KEY `MODALITAET` (`MODALITAET`)
) ENGINE=MyISAM AUTO_INCREMENT=94 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `STDSIG`
--

DROP TABLE IF EXISTS `STDSIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `STDSIG` (
  `PHARMANUMMER` int(11) NOT NULL DEFAULT '-1',
  `DOSISSCHEMA` text COLLATE utf8_unicode_ci,
  `ERMAECHTIGUNG_ID` int(11) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`PHARMANUMMER`,`ERMAECHTIGUNG_ID`),
  KEY `PHARMANUMMER` (`PHARMANUMMER`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `STDSIG_GYN`
--

DROP TABLE IF EXISTS `STDSIG_GYN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `STDSIG_GYN` (
  `PHARMANUMMER` int(11) NOT NULL DEFAULT '-1',
  `DOSISSCHEMA` text COLLATE utf8_unicode_ci,
  `ERMAECHTIGUNG_ID` int(11) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`PHARMANUMMER`,`ERMAECHTIGUNG_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `TAGESINFO`
--

DROP TABLE IF EXISTS `TAGESINFO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TAGESINFO` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `KALENDER_NR` int(11) NOT NULL DEFAULT '-1',
  `TYP` int(11) DEFAULT NULL,
  `ERMAECHTIGUNG_ID` int(11) NOT NULL DEFAULT '-1',
  `INFO_DATUM` date DEFAULT NULL,
  `INFO_TEXT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LETZTE_AENDERUNG_DATUM` date DEFAULT NULL,
  `LETZTE_AENDERUNG_UHRZEIT` time DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEARBEITER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROTOKOLL` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=565 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `TERMINRESERVIERUNG`
--

DROP TABLE IF EXISTS `TERMINRESERVIERUNG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TERMINRESERVIERUNG` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `KALENDERTERMIN_ID` int(11) DEFAULT NULL,
  `WEB_RESERVIERUNGSDATEN_ID` int(11) DEFAULT NULL,
  `RESERVIERT_AM` datetime DEFAULT NULL,
  `STORNIERT_AM` datetime DEFAULT NULL,
  `PATIENT_ID` int(11) DEFAULT NULL,
  `AUFTRAG_ID` int(11) DEFAULT NULL,
  `ANREDE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GESCHLECHT` int(4) DEFAULT NULL,
  `TITEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VORNAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GEBURTSDATUM` date DEFAULT NULL,
  `VERSICHERTENNUMMER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERSICHERUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IST_NEUPATIENT` tinyint(4) DEFAULT NULL,
  `STRASSE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PLZ` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ORT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TELEFON` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MOBIL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FAX` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EMAIL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MITTEILUNG` text COLLATE utf8_unicode_ci,
  `SMS_ERINNERUNG` tinyint(4) DEFAULT NULL,
  `EMAIL_ERINNERUNG` tinyint(4) DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `WEB_RESERVIERUNGSDATEN_ID` (`WEB_RESERVIERUNGSDATEN_ID`),
  KEY `KALENDERTERMIN_ID` (`KALENDERTERMIN_ID`),
  KEY `PATIENT_ID` (`PATIENT_ID`),
  KEY `AUFTRAG_ID` (`AUFTRAG_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UNTERSUCHUNG`
--

DROP TABLE IF EXISTS `UNTERSUCHUNG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UNTERSUCHUNG` (
  `UNTERSUCHUNG_ID` int(11) NOT NULL DEFAULT '-1',
  `STATUS_UNTERSUCHUNG` int(3) DEFAULT NULL,
  `ARZTBRIEF_ID` int(11) DEFAULT NULL,
  `AUFTRAG_ID` int(11) DEFAULT NULL,
  `AUFTRAG_STAMM_ID` int(11) DEFAULT NULL,
  UNIQUE KEY `UNTERSUCHUNG_ID` (`UNTERSUCHUNG_ID`),
  KEY `ARZTBRIEF_ID` (`ARZTBRIEF_ID`),
  KEY `STATUS_UNTERSUCHUNG` (`STATUS_UNTERSUCHUNG`),
  KEY `AUFTRAG_ID` (`AUFTRAG_ID`),
  KEY `AUFTRAG_STAMM_ID` (`AUFTRAG_STAMM_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UNTERSUCHUNG_TYP`
--

DROP TABLE IF EXISTS `UNTERSUCHUNG_TYP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UNTERSUCHUNG_TYP` (
  `ID` int(11) NOT NULL DEFAULT '0',
  `TYP` int(11) DEFAULT NULL,
  `U_TEXT` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `U_SORTIERUNG` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `VERTRAGSART`
--

DROP TABLE IF EXISTS `VERTRAGSART`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VERTRAGSART` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ELTER_ID` int(11) NOT NULL DEFAULT '-1',
  `TYP` int(11) NOT NULL DEFAULT '-1',
  `POSITION` int(11) NOT NULL DEFAULT '0',
  `KUERZEL` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KENNUNG` int(11) NOT NULL DEFAULT '-1',
  `LANGTEXT` mediumtext COLLATE utf8_unicode_ci,
  `ADRESSTEXT` mediumtext COLLATE utf8_unicode_ci,
  `ADRESSE` mediumtext COLLATE utf8_unicode_ci,
  `RE_ANSCHRIFT_XML` text COLLATE utf8_unicode_ci,
  `MANDANT` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '-1',
  `BEARBEITER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LETZTE_AENDERUNG` timestamp NULL DEFAULT NULL,
  `ZIFFER_TYP` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FAKTOR` double DEFAULT NULL,
  `FAKTOR_TECH` double DEFAULT NULL,
  `FAKTOR_LAB` double DEFAULT NULL,
  `EINSTELLUNGEN` text COLLATE utf8_unicode_ci,
  `KURZKUERZEL` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `KENNUNG` (`KENNUNG`),
  UNIQUE KEY `KURZKUERZEL` (`KURZKUERZEL`),
  UNIQUE KEY `KUERZEL` (`KUERZEL`),
  KEY `TYP` (`TYP`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `VIDAL_DATEN`
--

DROP TABLE IF EXISTS `VIDAL_DATEN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VIDAL_DATEN` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CHECKSUM` int(11) DEFAULT NULL,
  `TYPE` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GUELTIG` date DEFAULT NULL,
  `VERFUEGBAR` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHARMA_NR` int(11) DEFAULT NULL,
  `ZULASSUNG_NR_STRING` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZULASSUNG_NR` int(11) DEFAULT NULL,
  `BEZEICHNUNG_KURZ` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEZEICHNUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LETZTE_AENDERUNG` date DEFAULT NULL,
  `RSIGNS` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STORAGE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `REMB` int(11) DEFAULT NULL,
  `BOX` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SSIGNS` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `INDTEXT` text COLLATE utf8_unicode_ci,
  `RULETEXT` text COLLATE utf8_unicode_ci,
  `REMARKTEXT` text COLLATE utf8_unicode_ci,
  `QUANTITY` int(11) DEFAULT NULL,
  `UNIT` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ENHUNIT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KVP` double DEFAULT NULL,
  `AVP` double DEFAULT NULL,
  `ZINH` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `PHARMA_NR` (`PHARMA_NR`),
  KEY `ZINH` (`ZINH`)
) ENGINE=MyISAM AUTO_INCREMENT=165931 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `VIDAL_REFERENZEN`
--

DROP TABLE IF EXISTS `VIDAL_REFERENZEN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VIDAL_REFERENZEN` (
  `ZINH` int(11) NOT NULL DEFAULT '0',
  `BEZEICHNUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ZINH`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `WARTELISTE`
--

DROP TABLE IF EXISTS `WARTELISTE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WARTELISTE` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PATIENT_ID` int(11) DEFAULT NULL,
  `TERMIN_START_DATUM` date DEFAULT NULL,
  `TERMIN_START_UHRZEIT` time DEFAULT NULL,
  `TERMIN_TEXT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WARTE_LISTE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WARTE_DATUM` date DEFAULT NULL,
  `WARTE_UHRZEIT` time DEFAULT NULL,
  `LETZTE_AENDERUNG_DATUM` date DEFAULT NULL,
  `LETZTE_AENDERUNG_UHRZEIT` time DEFAULT NULL,
  `TERMIN_ENDE_DATUM` date DEFAULT NULL,
  `TERMIN_ENDE_UHRZEIT` time DEFAULT NULL,
  `TYP` int(11) DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEARBEITER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROTOKOLL` text COLLATE utf8_unicode_ci,
  `PATIENT_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MANDANT_ID` int(11) DEFAULT '-1',
  `KALENDER_NR` int(11) DEFAULT NULL,
  `POSITION` bigint(11) DEFAULT NULL,
  `ERLEDIGT` int(11) NOT NULL DEFAULT '0',
  `KENNZEICHEN` int(11) DEFAULT NULL,
  `EINSTELLUNGEN` mediumtext COLLATE utf8_unicode_ci,
  `TERMIN_ANLAGE` datetime DEFAULT NULL,
  `WARTE_ENTLASSEN` datetime DEFAULT NULL,
  `WEBSTATUS` int(11) DEFAULT NULL,
  `WEBVERSION` int(11) DEFAULT NULL,
  `WEBANLAGE` datetime DEFAULT NULL,
  `WEBRESERVIERUNG_ID` int(11) DEFAULT NULL,
  `AUFTRAGLISTE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WEGZUSENDEN` tinyint(4) NOT NULL DEFAULT '0',
  `GEWAEHLTERAUFTRAG` int(11) DEFAULT '0',
  `STATUS` int(11) DEFAULT NULL,
  `EMAIL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MOBIL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IST_WEBTERMIN` tinyint(4) DEFAULT NULL,
  `WEBTERMIN_TITEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WILL_EMAIL_ERINNERUNG` tinyint(4) DEFAULT NULL,
  `WILL_SMS_ERINNERUNG` tinyint(4) DEFAULT NULL,
  `NOTIZEN` text COLLATE utf8_unicode_ci,
  `IST_WEBERINNERUNG` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `TERMIN_START` (`TERMIN_START_DATUM`,`TERMIN_START_UHRZEIT`,`KALENDER_NR`),
  UNIQUE KEY `TERMIN_ENDE` (`TERMIN_ENDE_DATUM`,`TERMIN_ENDE_UHRZEIT`,`KALENDER_NR`),
  KEY `PATIENT_ID` (`PATIENT_ID`),
  KEY `TERMIN_START_DATUM` (`TERMIN_START_DATUM`),
  KEY `WARTE_DATUM` (`WARTE_DATUM`),
  KEY `WARTE_LISTE` (`WARTE_LISTE`),
  KEY `TYP` (`TYP`),
  KEY `POSITION` (`POSITION`),
  KEY `ZEILE_LOCK` (`ZEILE_LOCK`),
  KEY `TERMIN_START_UHRZEIT` (`TERMIN_START_UHRZEIT`),
  KEY `WARTE_UHRZEIT` (`WARTE_UHRZEIT`),
  KEY `WEBSTATUS` (`WEBSTATUS`),
  KEY `WEGZUSENDEN` (`WEGZUSENDEN`),
  KEY `STATUS` (`STATUS`),
  KEY `IST_WEBTERMIN` (`IST_WEBTERMIN`),
  KEY `IST_WEBERINNERUNG` (`IST_WEBERINNERUNG`)
) ENGINE=MyISAM AUTO_INCREMENT=8793 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `WOERTERBUCH`
--

DROP TABLE IF EXISTS `WOERTERBUCH`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WOERTERBUCH` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SPRACHE` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAME` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TEXT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Sprache_Name` (`SPRACHE`,`NAME`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `XADRESSE`
--

DROP TABLE IF EXISTS `XADRESSE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `XADRESSE` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYP` int(11) NOT NULL DEFAULT '1',
  `STRASSE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PLZ` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ORT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BUNDESLAND` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAND_CODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `POSTFACH` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `POSTFACH_PLZ` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `POSTFACH_ORT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NOTIZEN` text COLLATE utf8_unicode_ci,
  `PROTOKOLL` text COLLATE utf8_unicode_ci,
  `LETZTE_AENDERUNG` datetime DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STATUS` int(11) DEFAULT NULL,
  `HAUSNUMMER` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `TABELLE` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `TABELLE` (`TABELLE`),
  KEY `TYP` (`TYP`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ZAHLUNGEN`
--

DROP TABLE IF EXISTS `ZAHLUNGEN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ZAHLUNGEN` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RECHNUNGDOKU_ID` int(11) DEFAULT NULL,
  `ZAHLUNG_ZEIT` datetime DEFAULT NULL,
  `BETRAG` double NOT NULL DEFAULT '0',
  `ZAHLUNG_TEXT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZAHLUNG_ART` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEARBEITER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SYSTEM_ZEIT` datetime DEFAULT NULL,
  `MANDANT_ID` int(11) NOT NULL DEFAULT '-1',
  `WAEHRUNG` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SCHEINDOKU_ID` int(11) DEFAULT '-1',
  `ZAEHLER` int(11) DEFAULT NULL,
  `JAHR` int(11) DEFAULT NULL,
  `UMSATZZAEHLER_BAR` double DEFAULT NULL,
  `STORNO` int(11) DEFAULT NULL,
  `BELEGART` tinyint(4) DEFAULT NULL,
  `SIGNATUR` text COLLATE utf8_unicode_ci,
  `BETRAG_MWST` double NOT NULL DEFAULT '0',
  `BETRAG_MWST_REDUZIERT` double NOT NULL DEFAULT '0',
  `JOURNAL` text COLLATE utf8_unicode_ci,
  `SERIENNUMMER_SIGZERT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SIGZERT_AUSGEFALLEN` tinyint(4) DEFAULT NULL,
  `REGISTRIERKASSE_NR` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LETZTES_KETTENGLIED_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ZAEHLER` (`ZAEHLER`,`MANDANT_ID`,`JAHR`),
  UNIQUE KEY `STORNO_BELEGART` (`STORNO`,`BELEGART`),
  UNIQUE KEY `LETZTES_KETTENGLIED_ID` (`LETZTES_KETTENGLIED_ID`),
  KEY `ZEILE_LOCK` (`ZEILE_LOCK`),
  KEY `RECHNUNGDOKU_ID` (`RECHNUNGDOKU_ID`),
  KEY `SCHEINDOKU_ID` (`SCHEINDOKU_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=409 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ZAHLUNGSPLAN`
--

DROP TABLE IF EXISTS `ZAHLUNGSPLAN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ZAHLUNGSPLAN` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SCHEINDOKU_ID` int(11) NOT NULL DEFAULT '-1',
  `ZAHLUNG_DATUM` date DEFAULT NULL,
  `BETRAG` double NOT NULL DEFAULT '0',
  `ZAHLUNG_TEXT` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ZAHLUNG_ART` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEARBEITER` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SYSTEM_DATUM` date DEFAULT NULL,
  `SYSTEM_UHRZEIT` time DEFAULT NULL,
  `STATUS` int(11) NOT NULL DEFAULT '-1',
  `RE_NR` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RE_NR` (`RE_NR`),
  KEY `ZAHLUNG_DATUM` (`ZAHLUNG_DATUM`),
  KEY `ZEILE_LOCK` (`ZEILE_LOCK`),
  KEY `SCHEINDOKU_ID` (`SCHEINDOKU_ID`),
  KEY `STATUS` (`STATUS`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ZIFFER`
--

DROP TABLE IF EXISTS `ZIFFER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ZIFFER` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LEISTUNGSDOKU_ID` int(11) DEFAULT NULL,
  `UNTERSUCHUNG_ID` int(11) DEFAULT NULL,
  `TYP` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STATUS` int(11) DEFAULT NULL,
  `SYSTEM_DATUM` date DEFAULT NULL,
  `SYSTEM_UHRZEIT` time DEFAULT NULL,
  `ZIFFER_NR` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZIFFER_ANZAHL` double DEFAULT NULL,
  `ZIFFER_BETRAG` double DEFAULT NULL,
  `ZIFFER_WAEHRUNG` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZIFFER_PUNKTE` double DEFAULT NULL,
  `ZIFFER_KM` double DEFAULT NULL,
  `ZIFFER_KURZTEXT` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZIFFER_TEXT` mediumtext COLLATE utf8_unicode_ci,
  `ZIFFER_BEGR` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZIFFER_DATUM` date DEFAULT NULL,
  `ZIFFER_UHRZEIT` time DEFAULT NULL,
  `ZIFFER_CA_KENNZEICHEN` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZUSATZKENNZEICHEN` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RESERVE1` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MWST_SATZ` int(4) DEFAULT NULL,
  `KOSTENTYP` int(4) DEFAULT NULL,
  `KONTIERUNG` int(4) DEFAULT NULL,
  `BEARBEITER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROTOKOLL` text COLLATE utf8_unicode_ci,
  `VISITENADRESSE` mediumtext COLLATE utf8_unicode_ci,
  `SACHKOSTEN` text COLLATE utf8_unicode_ci,
  `ZUSATZ` text COLLATE utf8_unicode_ci,
  `ORGAN` text COLLATE utf8_unicode_ci,
  `BEGRUENDUNG_ARZT` text COLLATE utf8_unicode_ci,
  `BEGRUENDUNG_GNR` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STATIONAER_VON` date DEFAULT NULL,
  `STATIONAER_BIS` date DEFAULT NULL,
  `OP_DATUM` date DEFAULT NULL,
  `OPS` text COLLATE utf8_unicode_ci,
  `KOMPLIKATION` text COLLATE utf8_unicode_ci,
  `PRIVAT_FAKTOR` double NOT NULL DEFAULT '1',
  `RABATT` double DEFAULT NULL,
  `OP_DAUER` double DEFAULT NULL,
  `LEISTUNG_ID` int(11) DEFAULT NULL,
  `VERTRAGSART` int(11) DEFAULT NULL,
  `DOKUMENTATION_ID` int(11) DEFAULT NULL,
  `SORTIERUNG` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `LEISTUNGSDOKU_ID` (`LEISTUNGSDOKU_ID`),
  KEY `UNTERSUCHUNG_ID` (`UNTERSUCHUNG_ID`),
  KEY `STATUS` (`STATUS`),
  KEY `ZIFFER_DATUM` (`ZIFFER_DATUM`),
  KEY `ZEILE_LOCK` (`ZEILE_LOCK`),
  KEY `LEISTUNG_ID` (`LEISTUNG_ID`),
  KEY `VERTRAGSART` (`VERTRAGSART`) USING BTREE,
  KEY `TYP` (`TYP`),
  KEY `ZIFFER_NR` (`ZIFFER_NR`)
) ENGINE=MyISAM AUTO_INCREMENT=17958 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ZIFFERN_PRIVAT`
--

DROP TABLE IF EXISTS `ZIFFERN_PRIVAT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ZIFFERN_PRIVAT` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYP` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SUBTYP` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZIFFER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZIFFER_TEXT` text COLLATE utf8_unicode_ci,
  `ZIFFER_TEXT_KURZ` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PUNKTE` double NOT NULL DEFAULT '0',
  `GRUPPE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KUERZEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NOTIZEN` text COLLATE utf8_unicode_ci,
  `PROTOKOLL` text COLLATE utf8_unicode_ci,
  `STATUS` int(11) DEFAULT NULL,
  `ZIFFER_VON` date DEFAULT NULL,
  `ZIFFER_BIS` date DEFAULT NULL,
  `BETRAG` double NOT NULL DEFAULT '0',
  `WAEHRUNG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERSION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZEITSTEMPEL` text COLLATE utf8_unicode_ci,
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `REGEL_L` text COLLATE utf8_unicode_ci,
  `MWST_SATZ` int(4) DEFAULT NULL,
  `KOSTENTYP` int(4) DEFAULT NULL,
  `KONTIERUNG` int(4) DEFAULT NULL,
  `LAGER_KUERZEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAGER_MENGE` double NOT NULL DEFAULT '0',
  `WA_TEXTE` text COLLATE utf8_unicode_ci,
  `ANGEBOT_GRUPPE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ANGEBOT_UEBERSCHRIFT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ANGEBOT_TEXT` text COLLATE utf8_unicode_ci,
  `ANGEBOT_BILD` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ANGEBOT_DETAILS` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `ZIFFER` (`ZIFFER`),
  KEY `TYP` (`TYP`)
) ENGINE=MyISAM AUTO_INCREMENT=474 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ZIFFER_GRUPPE`
--

DROP TABLE IF EXISTS `ZIFFER_GRUPPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ZIFFER_GRUPPE` (
  `ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `GRUPPE` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ZIFFER_TYP`
--

DROP TABLE IF EXISTS `ZIFFER_TYP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ZIFFER_TYP` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYP` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ZIFFER_SORTIERUNG` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ZUWEISER`
--

DROP TABLE IF EXISTS `ZUWEISER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ZUWEISER` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `KUE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ANREDE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TITEL` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VORNAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WVORNAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GEBNAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAMENSZUSATZ` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAMENSVORSATZ` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GEBDATUM` date DEFAULT NULL,
  `BILD` mediumblob,
  `NOTIZEN` mediumtext COLLATE utf8_unicode_ci,
  `PROTOKOLL` mediumtext COLLATE utf8_unicode_ci,
  `STRASSE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PLZ` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ORT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAND` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Adresse_ID1` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Adresse_ID2` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Adresse_ID3` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Adresse_ID4` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TELEFON` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TELEFON_2` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FAX` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MOBIL` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EMAIL` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BANK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BANK_PLZ` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BANK_ORT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BANK_LAND` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KTO_NR` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BLZ` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KTO_INHABER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STATUS` int(11) NOT NULL DEFAULT '-1',
  `ZEILE_LOCK` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERTRAGSPARTNERNR` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FACHGEBIET` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LETZTE_AENDERUNG_DATUM` date DEFAULT NULL,
  `LETZTE_AENDERUNG_UHRZEIT` time DEFAULT NULL,
  `BRIEFDATEN` mediumtext COLLATE utf8_unicode_ci,
  `ANSCHRIFT_ZEILE1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ANSCHRIFT_ZEILE2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEFUNDKOMM_BETRKEY` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEFUNDKOMM_EMAIL` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEFUNDKOMM_MENR` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRAXIS_TYP` int(11) NOT NULL DEFAULT '-1',
  `BSNR` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEFUNDKOMM_BEFTYPEN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRAXISZEITEN` text COLLATE utf8_unicode_ci,
  `LANR` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ANFORDERUNGSLABOR` int(2) DEFAULT NULL,
  `KOMM_VERSANDTYP` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KOMM_TECHNIK` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERRECHNUNGSSTELLE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEFUNDKOMM_TEILNEHMER` int(2) DEFAULT NULL,
  `BEFUNDE_IMMER_ERHALTEN` int(2) DEFAULT NULL,
  `KOMM_DATENFORMAT_LABORAUFTRAG` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KOMM_TECHNIK_LABORAUFTRAG` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SPRACHE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SERIENBRIEF_VERSANDARTEN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SERIENBRIEF_STDVERSANDART` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HAUSNUMMER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SERIENBRIEF_TYPEN` text COLLATE utf8_unicode_ci,
  `EINSTELLUNGEN` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `BSNR_LANR` (`VERTRAGSPARTNERNR`,`LANR`),
  UNIQUE KEY `KUE` (`KUE`) USING BTREE,
  KEY `NAME` (`NAME`),
  KEY `VERTRAGSPARTNERNR` (`VERTRAGSPARTNERNR`),
  KEY `LANR` (`LANR`)
) ENGINE=MyISAM AUTO_INCREMENT=7867 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ZYTO_BEFUNDE`
--

DROP TABLE IF EXISTS `ZYTO_BEFUNDE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ZYTO_BEFUNDE` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PATIENT_ID` int(11) NOT NULL DEFAULT '-1',
  `UNTERSUCHUNG_ID` int(11) NOT NULL DEFAULT '-1',
  `TYP` int(20) NOT NULL DEFAULT '-1',
  `DATUM` date DEFAULT NULL,
  `ABSTRICH_FREITEXT` text COLLATE utf8_unicode_ci,
  `BEFUND_ERGEBNIS` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BEFUND_FREITEXT` text COLLATE utf8_unicode_ci,
  `BEMERKUNG` text COLLATE utf8_unicode_ci,
  `STATUS` int(11) NOT NULL DEFAULT '-1',
  `SYSTEM_DATUM` date DEFAULT NULL,
  `SYSTEM_UHRZEIT` time DEFAULT NULL,
  `BEARBEITER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROTOKOLL` text COLLATE utf8_unicode_ci,
  `ERMAECHTIGUNG_ID` int(11) NOT NULL DEFAULT '-1',
  `BILD` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `POSITION` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `PATIENT_ID` (`PATIENT_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-09 21:47:40
