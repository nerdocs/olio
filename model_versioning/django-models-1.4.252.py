# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class AbrechnungBuffer(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)  # Field name made lowercase.
    mandand_id = models.IntegerField(db_column='MANDAND_ID', blank=True, null=True)  # Field name made lowercase.
    abrechnungsstellekennung = models.CharField(db_column='AbrechnungsstelleKennung', max_length=255, blank=True, null=True)  # Field name made lowercase.
    abrechnungskassenkennungen = models.CharField(db_column='AbrechnungsKassenKennungen', max_length=255, blank=True, null=True)  # Field name made lowercase.
    abrechnungskassentypen = models.CharField(db_column='AbrechnungsKassenTypen', max_length=255, blank=True, null=True)  # Field name made lowercase.
    erstellung = models.DateTimeField(db_column='Erstellung', blank=True, null=True)  # Field name made lowercase.
    zeitraumjahr = models.IntegerField(db_column='ZeitraumJahr', blank=True, null=True)  # Field name made lowercase.
    zeitraumkennung = models.IntegerField(db_column='ZeitraumKennung', blank=True, null=True)  # Field name made lowercase.
    datum_start = models.DateField(db_column='DATUM_START', blank=True, null=True)  # Field name made lowercase.
    datum_ende = models.DateField(db_column='DATUM_ENDE', blank=True, null=True)  # Field name made lowercase.
    text = models.TextField(db_column='Text', blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ABRECHNUNG_BUFFER'


class AbrechnungHl7Zuordnung(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    hl7_segment = models.CharField(db_column='HL7_SEGMENT', max_length=50, blank=True, null=True)  # Field name made lowercase.
    hl7_ergebnis_typ = models.CharField(db_column='HL7_ERGEBNIS_TYP', max_length=50, blank=True, null=True)  # Field name made lowercase.
    hl7_ergebnis_beschreibung = models.CharField(db_column='HL7_ERGEBNIS_BESCHREIBUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    hl7_ergebnis_identifier = models.CharField(db_column='HL7_ERGEBNIS_IDENTIFIER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    kasse = models.CharField(db_column='KASSE', max_length=50, blank=True, null=True)  # Field name made lowercase.
    ziffer = models.CharField(db_column='ZIFFER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    ziffer_anzahl_faktor = models.FloatField(db_column='ZIFFER_ANZAHL_FAKTOR')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ABRECHNUNG_HL7_ZUORDNUNG'


class AcetoEinstellungen(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    name = models.CharField(db_column='NAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    wert = models.TextField(db_column='WERT', blank=True, null=True)  # Field name made lowercase.
    werteliste = models.CharField(db_column='WERTELISTE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS')  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP')  # Field name made lowercase.
    bereich = models.CharField(db_column='BEREICH', max_length=255, blank=True, null=True)  # Field name made lowercase.
    kennung_einstellungsatz = models.IntegerField(db_column='KENNUNG_EINSTELLUNGSATZ')  # Field name made lowercase.
    mandant_id = models.IntegerField(db_column='MANDANT_ID', blank=True, null=True)  # Field name made lowercase.
    benutzer_id = models.IntegerField(db_column='BENUTZER_ID', blank=True, null=True)  # Field name made lowercase.
    rechner_name = models.CharField(db_column='RECHNER_NAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    patient_id = models.IntegerField(db_column='PATIENT_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ACETO_EINSTELLUNGEN'


class AcetoKontakt(models.Model):
    web_id = models.IntegerField(db_column='WEB_ID', primary_key=True)  # Field name made lowercase.
    datum = models.DateTimeField(db_column='DATUM', blank=True, null=True)  # Field name made lowercase.
    xml = models.TextField(db_column='XML')  # Field name made lowercase.
    patient_id = models.IntegerField(db_column='PATIENT_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ACETO_KONTAKT'


class Adresse(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP')  # Field name made lowercase.
    strasse = models.CharField(db_column='STRASSE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    hausnummer = models.CharField(db_column='HAUSNUMMER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    adresszusatz = models.CharField(db_column='ADRESSZUSATZ', max_length=255, blank=True, null=True)  # Field name made lowercase.
    plz = models.CharField(db_column='PLZ', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ort = models.CharField(db_column='ORT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bundesland = models.CharField(db_column='BUNDESLAND', max_length=255, blank=True, null=True)  # Field name made lowercase.
    land_code = models.CharField(db_column='LAND_CODE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    postfach = models.CharField(db_column='POSTFACH', max_length=255, blank=True, null=True)  # Field name made lowercase.
    postfach_plz = models.CharField(db_column='POSTFACH_PLZ', max_length=255, blank=True, null=True)  # Field name made lowercase.
    postfach_ort = models.CharField(db_column='POSTFACH_ORT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    postfach_land_code = models.CharField(db_column='POSTFACH_LAND_CODE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)  # Field name made lowercase.
    tabelle = models.IntegerField(db_column='TABELLE')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ADRESSE'


class Allergene(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)  # Field name made lowercase.
    kuerzel = models.CharField(db_column='KUERZEL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    beschreibung = models.TextField(db_column='BESCHREIBUNG', blank=True, null=True)  # Field name made lowercase.
    gruppe = models.IntegerField(db_column='GRUPPE', blank=True, null=True)  # Field name made lowercase.
    mandanten = models.CharField(db_column='MANDANTEN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    prick = models.IntegerField(db_column='PRICK', blank=True, null=True)  # Field name made lowercase.
    prick_prick = models.IntegerField(db_column='PRICK_PRICK', blank=True, null=True)  # Field name made lowercase.
    epicutan = models.IntegerField(db_column='EPICUTAN', blank=True, null=True)  # Field name made lowercase.
    intracutan = models.IntegerField(db_column='INTRACUTAN', blank=True, null=True)  # Field name made lowercase.
    scratch = models.IntegerField(db_column='SCRATCH', blank=True, null=True)  # Field name made lowercase.
    bluttest_intern = models.IntegerField(db_column='BLUTTEST_INTERN', blank=True, null=True)  # Field name made lowercase.
    bluttest_extern = models.IntegerField(db_column='BLUTTEST_EXTERN', blank=True, null=True)  # Field name made lowercase.
    impfung = models.IntegerField(db_column='IMPFUNG', blank=True, null=True)  # Field name made lowercase.
    typ_profil = models.CharField(db_column='TYP_PROFIL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    profil_parameter_ids = models.CharField(db_column='PROFIL_PARAMETER_IDS', max_length=255, blank=True, null=True)  # Field name made lowercase.
    favorit = models.IntegerField(db_column='FAVORIT', blank=True, null=True)  # Field name made lowercase.
    favorit_position = models.IntegerField(db_column='FAVORIT_POSITION', blank=True, null=True)  # Field name made lowercase.
    individuell = models.IntegerField(db_column='INDIVIDUELL', blank=True, null=True)  # Field name made lowercase.
    labor_auftragparameter_codeimlabor = models.CharField(db_column='LABOR_AUFTRAGPARAMETER_CODEIMLABOR', max_length=255, blank=True, null=True)  # Field name made lowercase.
    laborauftrag_zuweiser_id = models.IntegerField(db_column='LABORAUFTRAG_ZUWEISER_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ALLERGENE'
        unique_together = (('kuerzel', 'gruppe'),)


class Allergentest(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    allergentestdokuid = models.IntegerField(db_column='ALLERGENTESTDOKUID', unique=True, blank=True, null=True)  # Field name made lowercase.
    gesamtige = models.IntegerField(db_column='GESAMTIGE', blank=True, null=True)  # Field name made lowercase.
    start_zeit = models.DateTimeField(db_column='START_ZEIT', blank=True, null=True)  # Field name made lowercase.
    end_zeit = models.DateTimeField(db_column='END_ZEIT', blank=True, null=True)  # Field name made lowercase.
    timer_typ = models.IntegerField(db_column='TIMER_TYP', blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)  # Field name made lowercase.
    bemerkung = models.CharField(db_column='BEMERKUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ALLERGENTEST'


class Allergentestwert(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    allergentestdokuid = models.IntegerField(db_column='ALLERGENTESTDOKUID', blank=True, null=True)  # Field name made lowercase.
    allergen_id = models.IntegerField(db_column='ALLERGEN_ID', blank=True, null=True)  # Field name made lowercase.
    ergebniswert_1 = models.CharField(db_column='ERGEBNISWERT_1', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ergebniswert_2 = models.CharField(db_column='ERGEBNISWERT_2', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bluttest = models.IntegerField(db_column='BLUTTEST', blank=True, null=True)  # Field name made lowercase.
    testtyp = models.IntegerField(db_column='TESTTYP', blank=True, null=True)  # Field name made lowercase.
    anamnese = models.IntegerField(db_column='ANAMNESE', blank=True, null=True)  # Field name made lowercase.
    beschreibung = models.CharField(db_column='BESCHREIBUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    gruppe = models.IntegerField(db_column='GRUPPE', blank=True, null=True)  # Field name made lowercase.
    position = models.IntegerField(db_column='POSITION', blank=True, null=True)  # Field name made lowercase.
    beauftragt = models.IntegerField(db_column='BEAUFTRAGT', blank=True, null=True)  # Field name made lowercase.
    referenz = models.CharField(db_column='REFERENZ', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ALLERGENTESTWERT'


class Anamnesebogenstamm(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ermaechtigung = models.CharField(db_column='ERMAECHTIGUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ANAMNESEBOGENSTAMM'


class Angebot(models.Model):
    angebotdoku_id = models.IntegerField(db_column='ANGEBOTDOKU_ID', primary_key=True)  # Field name made lowercase.
    untersuchung_id = models.IntegerField(db_column='UNTERSUCHUNG_ID')  # Field name made lowercase.
    angebot_nr = models.CharField(db_column='ANGEBOT_NR', unique=True, max_length=50, blank=True, null=True)  # Field name made lowercase.
    angebot_text = models.TextField(db_column='ANGEBOT_TEXT', blank=True, null=True)  # Field name made lowercase.
    anschrift = models.TextField(db_column='ANSCHRIFT', blank=True, null=True)  # Field name made lowercase.
    anschrift_xml = models.TextField(db_column='ANSCHRIFT_XML', blank=True, null=True)  # Field name made lowercase.
    absende_anschrift = models.TextField(db_column='ABSENDE_ANSCHRIFT', blank=True, null=True)  # Field name made lowercase.
    positionen = models.TextField(db_column='POSITIONEN', blank=True, null=True)  # Field name made lowercase.
    anzahl_pos = models.IntegerField(db_column='ANZAHL_POS')  # Field name made lowercase.
    summe_netto = models.FloatField(db_column='SUMME_NETTO', blank=True, null=True)  # Field name made lowercase.
    summe_netto_1 = models.FloatField(db_column='SUMME_NETTO_1', blank=True, null=True)  # Field name made lowercase.
    summe_netto_2 = models.FloatField(db_column='SUMME_NETTO_2', blank=True, null=True)  # Field name made lowercase.
    summe_brutto = models.FloatField(db_column='SUMME_BRUTTO', blank=True, null=True)  # Field name made lowercase.
    summe_brutto_1 = models.FloatField(db_column='SUMME_BRUTTO_1', blank=True, null=True)  # Field name made lowercase.
    summe_brutto_2 = models.FloatField(db_column='SUMME_BRUTTO_2', blank=True, null=True)  # Field name made lowercase.
    mwst_1 = models.FloatField(db_column='MWST_1', blank=True, null=True)  # Field name made lowercase.
    mwst_2 = models.FloatField(db_column='MWST_2', blank=True, null=True)  # Field name made lowercase.
    summe_mwst = models.FloatField(db_column='SUMME_MWST', blank=True, null=True)  # Field name made lowercase.
    summe_mwst_1 = models.FloatField(db_column='SUMME_MWST_1', blank=True, null=True)  # Field name made lowercase.
    summe_mwst_2 = models.FloatField(db_column='SUMME_MWST_2', blank=True, null=True)  # Field name made lowercase.
    waehrung = models.CharField(db_column='WAEHRUNG', max_length=50, blank=True, null=True)  # Field name made lowercase.
    unterschreiber = models.CharField(db_column='UNTERSCHREIBER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    signed = models.CharField(db_column='SIGNED', max_length=255, blank=True, null=True)  # Field name made lowercase.
    offene_summe_brutto = models.FloatField(db_column='OFFENE_SUMME_BRUTTO')  # Field name made lowercase.
    zaehler = models.IntegerField(db_column='ZAEHLER')  # Field name made lowercase.
    datum_versendet = models.DateField(db_column='DATUM_VERSENDET', blank=True, null=True)  # Field name made lowercase.
    datum_storniert = models.DateField(db_column='DATUM_STORNIERT', blank=True, null=True)  # Field name made lowercase.
    status_zusatz = models.TextField(db_column='STATUS_ZUSATZ', blank=True, null=True)  # Field name made lowercase.
    mahn_stufe = models.IntegerField(db_column='MAHN_STUFE')  # Field name made lowercase.
    datum_abgelehnt = models.DateField(db_column='DATUM_ABGELEHNT', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ANGEBOT'


class Angebotposition(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    angebotsdoku_id = models.IntegerField(db_column='ANGEBOTSDOKU_ID', blank=True, null=True)  # Field name made lowercase.
    pos_nr = models.IntegerField(db_column='POS_NR', blank=True, null=True)  # Field name made lowercase.
    anzahl = models.FloatField(db_column='ANZAHL', blank=True, null=True)  # Field name made lowercase.
    position_datum = models.DateField(db_column='POSITION_DATUM', blank=True, null=True)  # Field name made lowercase.
    einzelpreis = models.FloatField(db_column='EINZELPREIS', blank=True, null=True)  # Field name made lowercase.
    mwst_satz = models.IntegerField(db_column='MWST_SATZ', blank=True, null=True)  # Field name made lowercase.
    kontierung = models.IntegerField(db_column='KONTIERUNG', blank=True, null=True)  # Field name made lowercase.
    pos_text = models.CharField(db_column='POS_TEXT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    pos_zusatz_text = models.TextField(db_column='POS_ZUSATZ_TEXT', blank=True, null=True)  # Field name made lowercase.
    mwst_preis = models.FloatField(db_column='MWST_PREIS', blank=True, null=True)  # Field name made lowercase.
    brutto_preis = models.FloatField(db_column='BRUTTO_PREIS', blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    system_datum = models.DateField(db_column='SYSTEM_DATUM', blank=True, null=True)  # Field name made lowercase.
    system_uhrzeit = models.TimeField(db_column='SYSTEM_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    faktor = models.FloatField(db_column='FAKTOR')  # Field name made lowercase.
    abzug = models.FloatField(db_column='ABZUG')  # Field name made lowercase.
    typ_kosten = models.IntegerField(db_column='TYP_KOSTEN')  # Field name made lowercase.
    angebotsgruppe = models.CharField(db_column='ANGEBOTSGRUPPE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    angebotbild = models.CharField(db_column='ANGEBOTBILD', max_length=255, blank=True, null=True)  # Field name made lowercase.
    properties = models.TextField(db_column='PROPERTIES', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ANGEBOTPOSITION'


class Arbeitsliste(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    patient_id = models.IntegerField(db_column='PATIENT_ID')  # Field name made lowercase.
    ermaechtigung_id = models.IntegerField(db_column='ERMAECHTIGUNG_ID')  # Field name made lowercase.
    patient_name = models.CharField(db_column='PATIENT_NAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    doku_id = models.IntegerField(db_column='DOKU_ID', blank=True, null=True)  # Field name made lowercase.
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    arbeitsliste_nr = models.IntegerField(db_column='ARBEITSLISTE_NR', blank=True, null=True)  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS')  # Field name made lowercase.
    einstellungen = models.TextField(db_column='EINSTELLUNGEN', blank=True, null=True)  # Field name made lowercase.
    plan_start_datum = models.DateField(db_column='PLAN_START_DATUM', blank=True, null=True)  # Field name made lowercase.
    plan_start_uhrzeit = models.TimeField(db_column='PLAN_START_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    plan_ende_datum = models.DateField(db_column='PLAN_ENDE_DATUM', blank=True, null=True)  # Field name made lowercase.
    plan_ende_uhrzeit = models.TimeField(db_column='PLAN_ENDE_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung_datum = models.DateField(db_column='LETZTE_AENDERUNG_DATUM', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung_uhrzeit = models.TimeField(db_column='LETZTE_AENDERUNG_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    erledigt = models.IntegerField(db_column='ERLEDIGT')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ARBEITSLISTE'


class Arztbrief(models.Model):
    arztbrief_id = models.IntegerField(db_column='ARZTBRIEF_ID', unique=True)  # Field name made lowercase.
    status_diktat = models.IntegerField(db_column='STATUS_DIKTAT', blank=True, null=True)  # Field name made lowercase.
    anschrift = models.TextField(db_column='ANSCHRIFT', blank=True, null=True)  # Field name made lowercase.
    kontrolle_abschnitt = models.TextField(db_column='KONTROLLE_ABSCHNITT', blank=True, null=True)  # Field name made lowercase.
    datum_versendet = models.DateField(db_column='DATUM_VERSENDET', blank=True, null=True)  # Field name made lowercase.
    status_arbeitsfluss = models.IntegerField(db_column='STATUS_ARBEITSFLUSS', blank=True, null=True)  # Field name made lowercase.
    fachbefund_abschnitt = models.TextField(db_column='FACHBEFUND_ABSCHNITT', blank=True, null=True)  # Field name made lowercase.
    operation_abschnitt = models.TextField(db_column='OPERATION_ABSCHNITT', blank=True, null=True)  # Field name made lowercase.
    ab_unterschreiber = models.CharField(db_column='AB_UNTERSCHREIBER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    ab_signed = models.CharField(db_column='AB_SIGNED', max_length=255, blank=True, null=True)  # Field name made lowercase.
    datum_storniert = models.DateField(db_column='DATUM_STORNIERT', blank=True, null=True)  # Field name made lowercase.
    bemerkung_stornierung = models.CharField(db_column='BEMERKUNG_STORNIERUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    fremdbefund_abschnitt = models.TextField(db_column='FREMDBEFUND_ABSCHNITT', blank=True, null=True)  # Field name made lowercase.
    invitro_abschnitt = models.TextField(db_column='INVITRO_ABSCHNITT', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ARZTBRIEF'


class Arztbriefliste(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    patient_id = models.IntegerField(db_column='PATIENT_ID')  # Field name made lowercase.
    ermaechtigung_id = models.IntegerField(db_column='ERMAECHTIGUNG_ID')  # Field name made lowercase.
    patient_name = models.CharField(db_column='PATIENT_NAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    doku_id = models.IntegerField(db_column='DOKU_ID', blank=True, null=True)  # Field name made lowercase.
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    arbeitsliste_nr = models.IntegerField(db_column='ARBEITSLISTE_NR', blank=True, null=True)  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS')  # Field name made lowercase.
    einstellungen = models.TextField(db_column='EINSTELLUNGEN', blank=True, null=True)  # Field name made lowercase.
    plan_start_datum = models.DateField(db_column='PLAN_START_DATUM', blank=True, null=True)  # Field name made lowercase.
    plan_start_uhrzeit = models.TimeField(db_column='PLAN_START_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    plan_ende_datum = models.DateField(db_column='PLAN_ENDE_DATUM', blank=True, null=True)  # Field name made lowercase.
    plan_ende_uhrzeit = models.TimeField(db_column='PLAN_ENDE_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung_datum = models.DateField(db_column='LETZTE_AENDERUNG_DATUM', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung_uhrzeit = models.TimeField(db_column='LETZTE_AENDERUNG_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    erledigt = models.IntegerField(db_column='ERLEDIGT')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ARZTBRIEFLISTE'


class AtElgadokument(models.Model):
    elgadokument_id = models.AutoField(db_column='ELGADOKUMENT_ID', primary_key=True)  # Field name made lowercase.
    datum = models.DateTimeField(db_column='DATUM', blank=True, null=True)  # Field name made lowercase.
    format = models.CharField(db_column='FORMAT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='STATUS', max_length=255, blank=True, null=True)  # Field name made lowercase.
    patient_id = models.IntegerField(db_column='PATIENT_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AT_ELGADOKUMENT'


class AtPunktwerte(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    typ = models.CharField(db_column='TYP', max_length=20, blank=True, null=True)  # Field name made lowercase.
    bundesland = models.CharField(db_column='BUNDESLAND', max_length=50, blank=True, null=True)  # Field name made lowercase.
    mandant_id = models.IntegerField(db_column='MANDANT_ID', blank=True, null=True)  # Field name made lowercase.
    kuerzel = models.CharField(db_column='KUERZEL', max_length=50, blank=True, null=True)  # Field name made lowercase.
    beschreibung = models.TextField(db_column='BESCHREIBUNG', blank=True, null=True)  # Field name made lowercase.
    punktwert = models.FloatField(db_column='PUNKTWERT')  # Field name made lowercase.
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)  # Field name made lowercase.
    stand = models.DateField(db_column='STAND', blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    gueltig_von_pktstand = models.FloatField(db_column='GUELTIG_VON_PKTSTAND')  # Field name made lowercase.
    gueltig_bis_pktstand = models.FloatField(db_column='GUELTIG_BIS_PKTSTAND')  # Field name made lowercase.
    fachgebiet = models.IntegerField(db_column='FACHGEBIET', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AT_PUNKTWERTE'
        unique_together = (('mandant_id', 'kuerzel', 'typ', 'fachgebiet'),)


class AtSisxAtc(models.Model):
    atc = models.CharField(db_column='ATC', primary_key=True, max_length=10)  # Field name made lowercase.
    atcbez = models.CharField(db_column='ATCBEZ', max_length=255, blank=True, null=True)  # Field name made lowercase.
    atcbeze = models.CharField(db_column='ATCBEZE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    dt = models.DateTimeField(db_column='DT', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AT_SISX_ATC'


class AtSisxBrevier(models.Model):
    mono = models.IntegerField(db_column='MONO', primary_key=True)  # Field name made lowercase.
    ewd = models.TextField(db_column='EWD', blank=True, null=True)  # Field name made lowercase.
    agd = models.TextField(db_column='AGD', blank=True, null=True)  # Field name made lowercase.
    aad = models.TextField(db_column='AAD', blank=True, null=True)  # Field name made lowercase.
    ahd = models.TextField(db_column='AHD', blank=True, null=True)  # Field name made lowercase.
    dod = models.TextField(db_column='DOD', blank=True, null=True)  # Field name made lowercase.
    gad = models.TextField(db_column='GAD', blank=True, null=True)  # Field name made lowercase.
    sstd = models.TextField(db_column='SSTD', blank=True, null=True)  # Field name made lowercase.
    nwd = models.TextField(db_column='NWD', blank=True, null=True)  # Field name made lowercase.
    wwd = models.TextField(db_column='WWD', blank=True, null=True)  # Field name made lowercase.
    ged = models.TextField(db_column='GED', blank=True, null=True)  # Field name made lowercase.
    whd = models.TextField(db_column='WHD', blank=True, null=True)  # Field name made lowercase.
    vtwd = models.TextField(db_column='VTWD', blank=True, null=True)  # Field name made lowercase.
    lhd = models.TextField(db_column='LHD', blank=True, null=True)  # Field name made lowercase.
    hkd = models.TextField(db_column='HKD', blank=True, null=True)  # Field name made lowercase.
    dt = models.DateTimeField(db_column='DT', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AT_SISX_BREVIER'


class AtSisxCode(models.Model):
    cdtyp = models.CharField(db_column='CDTYP', primary_key=True, max_length=20)  # Field name made lowercase.
    cdval = models.CharField(db_column='CDVAL', max_length=50)  # Field name made lowercase.
    dscrsd = models.CharField(db_column='DSCRSD', max_length=255, blank=True, null=True)  # Field name made lowercase.
    dscrmd = models.TextField(db_column='DSCRMD', blank=True, null=True)  # Field name made lowercase.
    dscrd = models.TextField(db_column='DSCRD', blank=True, null=True)  # Field name made lowercase.
    dt = models.DateTimeField(db_column='DT', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AT_SISX_CODE'
        unique_together = (('cdtyp', 'cdval'),)


class AtSisxCompany(models.Model):
    prtno = models.CharField(db_column='PRTNO', primary_key=True, max_length=10)  # Field name made lowercase.
    naml = models.CharField(db_column='NAML', max_length=45, blank=True, null=True)  # Field name made lowercase.
    adnam = models.CharField(db_column='ADNAM', max_length=70, blank=True, null=True)  # Field name made lowercase.
    strt = models.CharField(db_column='STRT', max_length=30, blank=True, null=True)  # Field name made lowercase.
    zip = models.CharField(db_column='ZIP', max_length=6, blank=True, null=True)  # Field name made lowercase.
    city = models.CharField(db_column='CITY', max_length=30, blank=True, null=True)  # Field name made lowercase.
    cntry = models.CharField(db_column='CNTRY', max_length=5, blank=True, null=True)  # Field name made lowercase.
    pbox = models.CharField(db_column='PBOX', max_length=8, blank=True, null=True)  # Field name made lowercase.
    tel = models.CharField(db_column='TEL', max_length=20, blank=True, null=True)  # Field name made lowercase.
    fax = models.CharField(db_column='FAX', max_length=20, blank=True, null=True)  # Field name made lowercase.
    cntct = models.CharField(db_column='CNTCT', max_length=60, blank=True, null=True)  # Field name made lowercase.
    email = models.CharField(db_column='EMAIL', max_length=60, blank=True, null=True)  # Field name made lowercase.
    www = models.CharField(db_column='WWW', max_length=60, blank=True, null=True)  # Field name made lowercase.
    ean = models.CharField(db_column='EAN', max_length=20, blank=True, null=True)  # Field name made lowercase.
    dt = models.DateTimeField(db_column='DT', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AT_SISX_COMPANY'


class AtSisxDar(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    referenz = models.IntegerField(db_column='REFERENZ', blank=True, null=True)  # Field name made lowercase.
    typ = models.CharField(db_column='TYP', max_length=10)  # Field name made lowercase.
    darreichung_code = models.CharField(db_column='DARREICHUNG_CODE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    dt = models.DateTimeField(db_column='DT', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AT_SISX_DAR'


class AtSisxDok(models.Model):
    doknummer = models.IntegerField(db_column='DOKNUMMER', primary_key=True)  # Field name made lowercase.
    doktyp = models.IntegerField(db_column='DOKTYP')  # Field name made lowercase.
    doktitel = models.CharField(db_column='DOKTITEL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    doktxt = models.TextField(db_column='DOKTXT', blank=True, null=True)  # Field name made lowercase.
    doktxk = models.TextField(db_column='DOKTXK', blank=True, null=True)  # Field name made lowercase.
    dt = models.DateTimeField(db_column='DT', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AT_SISX_DOK'


class AtSisxDokInd(models.Model):
    indnr = models.CharField(db_column='INDNR', primary_key=True, max_length=10)  # Field name made lowercase.
    doknummer = models.CharField(db_column='DOKNUMMER', max_length=10)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AT_SISX_DOK_IND'
        unique_together = (('indnr', 'doknummer'),)


class AtSisxIndgrst(models.Model):
    indnr = models.CharField(db_column='INDNR', primary_key=True, max_length=10)  # Field name made lowercase.
    indbez = models.CharField(db_column='INDBEZ', max_length=100, blank=True, null=True)  # Field name made lowercase.
    indnr2 = models.CharField(db_column='INDNR2', max_length=10, blank=True, null=True)  # Field name made lowercase.
    art = models.CharField(db_column='ART', max_length=100, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AT_SISX_INDGRST'


class AtSisxInter(models.Model):
    ixno = models.IntegerField(db_column='IXNO', primary_key=True)  # Field name made lowercase.
    grp1id = models.IntegerField(db_column='GRP1ID', blank=True, null=True)  # Field name made lowercase.
    grp2id = models.IntegerField(db_column='GRP2ID', blank=True, null=True)  # Field name made lowercase.
    internr2 = models.IntegerField(db_column='INTERNR2', blank=True, null=True)  # Field name made lowercase.
    effd = models.TextField(db_column='EFFD', blank=True, null=True)  # Field name made lowercase.
    rlv = models.IntegerField(db_column='RLV', blank=True, null=True)  # Field name made lowercase.
    rlvd = models.CharField(db_column='RLVD', max_length=255, blank=True, null=True)  # Field name made lowercase.
    efftxtd = models.TextField(db_column='EFFTXTD', blank=True, null=True)  # Field name made lowercase.
    mechd = models.TextField(db_column='MECHD', blank=True, null=True)  # Field name made lowercase.
    measd = models.TextField(db_column='MEASD', blank=True, null=True)  # Field name made lowercase.
    remd = models.TextField(db_column='REMD', blank=True, null=True)  # Field name made lowercase.
    lit = models.TextField(db_column='LIT', blank=True, null=True)  # Field name made lowercase.
    ixmch_typ = models.IntegerField(db_column='IXMCH_TYP', blank=True, null=True)  # Field name made lowercase.
    dt = models.DateTimeField(db_column='DT', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AT_SISX_INTER'


class AtSisxIxcpt(models.Model):
    ixno = models.IntegerField(db_column='IXNO', primary_key=True)  # Field name made lowercase.
    prtno = models.IntegerField(db_column='PRTNO')  # Field name made lowercase.
    cptno = models.IntegerField(db_column='CPTNO')  # Field name made lowercase.
    grp = models.IntegerField(db_column='GRP')  # Field name made lowercase.
    art = models.IntegerField(db_column='ART')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AT_SISX_IXCPT'
        unique_together = (('ixno', 'prtno', 'grp', 'art'),)


class AtSisxIxsub(models.Model):
    ixno = models.IntegerField(db_column='IXNO', primary_key=True)  # Field name made lowercase.
    subno = models.IntegerField(db_column='SUBNO')  # Field name made lowercase.
    subno1 = models.IntegerField(db_column='SUBNO1', blank=True, null=True)  # Field name made lowercase.
    grp = models.IntegerField(db_column='GRP', blank=True, null=True)  # Field name made lowercase.
    art = models.IntegerField(db_column='ART', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AT_SISX_IXSUB'
        unique_together = (('ixno', 'subno'),)


class AtSisxLim(models.Model):
    limcd = models.CharField(db_column='LIMCD', primary_key=True, max_length=20)  # Field name made lowercase.
    limtyp = models.CharField(db_column='LIMTYP', max_length=50)  # Field name made lowercase.
    dscrd = models.TextField(db_column='DSCRD', blank=True, null=True)  # Field name made lowercase.
    dt = models.DateTimeField(db_column='DT', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AT_SISX_LIM'
        unique_together = (('limcd', 'limtyp'),)


class AtSisxProduct(models.Model):
    prdno = models.IntegerField(db_column='PRDNO', primary_key=True)  # Field name made lowercase.
    dscrd = models.CharField(db_column='DSCRD', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bnamd = models.CharField(db_column='BNAMD', max_length=255, blank=True, null=True)  # Field name made lowercase.
    atc = models.CharField(db_column='ATC', max_length=100, blank=True, null=True)  # Field name made lowercase.
    it = models.CharField(db_column='IT', max_length=5, blank=True, null=True)  # Field name made lowercase.
    a_trade = models.CharField(db_column='A_TRADE', max_length=5, blank=True, null=True)  # Field name made lowercase.
    prtno = models.CharField(db_column='PRTNO', max_length=5, blank=True, null=True)  # Field name made lowercase.
    a_vertr = models.CharField(db_column='A_VERTR', max_length=40, blank=True, null=True)  # Field name made lowercase.
    a_zlinh = models.CharField(db_column='A_ZLINH', max_length=10, blank=True, null=True)  # Field name made lowercase.
    a_mono = models.CharField(db_column='A_MONO', max_length=5, blank=True, null=True)  # Field name made lowercase.
    a_anzahl = models.CharField(db_column='A_ANZAHL', max_length=5, blank=True, null=True)  # Field name made lowercase.
    a_bdat = models.DateField(db_column='A_BDAT', blank=True, null=True)  # Field name made lowercase.
    a_verfall = models.CharField(db_column='A_VERFALL', max_length=5, blank=True, null=True)  # Field name made lowercase.
    a_verfall2 = models.CharField(db_column='A_VERFALL2', max_length=5, blank=True, null=True)  # Field name made lowercase.
    a_lager = models.CharField(db_column='A_LAGER', max_length=5, blank=True, null=True)  # Field name made lowercase.
    a_sucht = models.CharField(db_column='A_SUCHT', max_length=5, blank=True, null=True)  # Field name made lowercase.
    a_wartez = models.CharField(db_column='A_WARTEZ', max_length=100, blank=True, null=True)  # Field name made lowercase.
    mono = models.IntegerField(db_column='MONO', blank=True, null=True)  # Field name made lowercase.
    cdgald = models.CharField(db_column='CDGALD', max_length=5, blank=True, null=True)  # Field name made lowercase.
    a_zloesen = models.CharField(db_column='A_ZLOESEN', max_length=5, blank=True, null=True)  # Field name made lowercase.
    a_zabgabe = models.CharField(db_column='A_ZABGABE', max_length=5, blank=True, null=True)  # Field name made lowercase.
    a_zlnumm = models.CharField(db_column='A_ZLNUMM', max_length=20, blank=True, null=True)  # Field name made lowercase.
    a_zrp = models.CharField(db_column='A_ZRP', max_length=5, blank=True, null=True)  # Field name made lowercase.
    a_warn = models.CharField(db_column='A_WARN', max_length=10, blank=True, null=True)  # Field name made lowercase.
    dt = models.DateTimeField(db_column='DT', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AT_SISX_PRODUCT'


class AtSisxProCpt(models.Model):
    prdno = models.IntegerField(db_column='PRDNO', primary_key=True)  # Field name made lowercase.
    cptlno = models.IntegerField(db_column='CPTLNO')  # Field name made lowercase.
    galf = models.CharField(db_column='GALF', max_length=50, blank=True, null=True)  # Field name made lowercase.
    a_zedat5 = models.CharField(db_column='A_ZEDAT5', max_length=255, blank=True, null=True)  # Field name made lowercase.
    excipq = models.FloatField(db_column='EXCIPQ', blank=True, null=True)  # Field name made lowercase.
    excipu = models.CharField(db_column='EXCIPU', max_length=10, blank=True, null=True)  # Field name made lowercase.
    excipcd = models.CharField(db_column='EXCIPCD', max_length=255, blank=True, null=True)  # Field name made lowercase.
    excipq2 = models.FloatField(db_column='EXCIPQ2', blank=True, null=True)  # Field name made lowercase.
    excipu2 = models.CharField(db_column='EXCIPU2', max_length=10, blank=True, null=True)  # Field name made lowercase.
    excipcd2 = models.CharField(db_column='EXCIPCD2', max_length=255, blank=True, null=True)  # Field name made lowercase.
    pqty = models.FloatField(db_column='PQTY', blank=True, null=True)  # Field name made lowercase.
    pqtyu = models.CharField(db_column='PQTYU', max_length=10, blank=True, null=True)  # Field name made lowercase.
    bezgr2 = models.CharField(db_column='BEZGR2', max_length=255, blank=True, null=True)  # Field name made lowercase.
    dt = models.DateTimeField(db_column='DT', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AT_SISX_PRO_CPT'
        unique_together = (('prdno', 'cptlno'),)


class AtSisxProCptapp(models.Model):
    prdno = models.CharField(db_column='PRDNO', primary_key=True, max_length=20)  # Field name made lowercase.
    cptlno = models.CharField(db_column='CPTLNO', max_length=50)  # Field name made lowercase.
    appno = models.CharField(db_column='APPNO', max_length=50)  # Field name made lowercase.
    meth = models.IntegerField(db_column='METH', blank=True, null=True)  # Field name made lowercase.
    mode = models.IntegerField(db_column='MODE', blank=True, null=True)  # Field name made lowercase.
    state = models.IntegerField(db_column='STATE', blank=True, null=True)  # Field name made lowercase.
    lct = models.IntegerField(db_column='LCT', blank=True, null=True)  # Field name made lowercase.
    prp = models.IntegerField(db_column='PRP', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AT_SISX_PRO_CPTAPP'
        unique_together = (('prdno', 'cptlno', 'appno'),)


class AtSisxProCptcmp(models.Model):
    prdno = models.IntegerField(db_column='PRDNO', primary_key=True)  # Field name made lowercase.
    cptlno = models.IntegerField(db_column='CPTLNO')  # Field name made lowercase.
    line = models.IntegerField(db_column='LINE')  # Field name made lowercase.
    subno = models.IntegerField(db_column='SUBNO', blank=True, null=True)  # Field name made lowercase.
    qty = models.FloatField(db_column='QTY', blank=True, null=True)  # Field name made lowercase.
    qtyu = models.CharField(db_column='QTYU', max_length=255, blank=True, null=True)  # Field name made lowercase.
    suffd = models.CharField(db_column='SUFFD', max_length=255, blank=True, null=True)  # Field name made lowercase.
    praed = models.CharField(db_column='PRAED', max_length=255, blank=True, null=True)  # Field name made lowercase.
    whk = models.CharField(db_column='WHK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ixrel = models.CharField(db_column='IXREL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    subno1 = models.IntegerField(db_column='SUBNO1', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AT_SISX_PRO_CPTCMP'
        unique_together = (('prdno', 'cptlno', 'line'),)


class AtSisxProCpttb(models.Model):
    prdno = models.IntegerField(db_column='PRDNO')  # Field name made lowercase.
    cptlno = models.IntegerField(db_column='CPTLNO')  # Field name made lowercase.
    tb_ee = models.CharField(db_column='TB_EE', max_length=5, blank=True, null=True)  # Field name made lowercase.
    tb_ee_zus = models.TextField(db_column='TB_EE_ZUS', blank=True, null=True)  # Field name made lowercase.
    tb_gd = models.CharField(db_column='TB_GD', max_length=5, blank=True, null=True)  # Field name made lowercase.
    tb_gd_zus = models.TextField(db_column='TB_GD_ZUS', blank=True, null=True)  # Field name made lowercase.
    ka_oef = models.CharField(db_column='KA_OEF', max_length=5, blank=True, null=True)  # Field name made lowercase.
    ka_oef_zus = models.TextField(db_column='KA_OEF_ZUS', blank=True, null=True)  # Field name made lowercase.
    al_sus = models.CharField(db_column='AL_SUS', max_length=5, blank=True, null=True)  # Field name made lowercase.
    al_sus_zus = models.TextField(db_column='AL_SUS_ZUS', blank=True, null=True)  # Field name made lowercase.
    zmoe = models.CharField(db_column='ZMOE', max_length=5, blank=True, null=True)  # Field name made lowercase.
    zmoe_zus = models.TextField(db_column='ZMOE_ZUS', blank=True, null=True)  # Field name made lowercase.
    sonde = models.CharField(db_column='SONDE', max_length=5, blank=True, null=True)  # Field name made lowercase.
    sonde_zus = models.TextField(db_column='SONDE_ZUS', blank=True, null=True)  # Field name made lowercase.
    cmr = models.CharField(db_column='CMR', max_length=5, blank=True, null=True)  # Field name made lowercase.
    cmr_zus = models.TextField(db_column='CMR_ZUS', blank=True, null=True)  # Field name made lowercase.
    licht = models.CharField(db_column='LICHT', max_length=5, blank=True, null=True)  # Field name made lowercase.
    licht_zus = models.TextField(db_column='LICHT_ZUS', blank=True, null=True)  # Field name made lowercase.
    tb = models.CharField(db_column='TB', max_length=5, blank=True, null=True)  # Field name made lowercase.
    tb_zus = models.TextField(db_column='TB_ZUS', blank=True, null=True)  # Field name made lowercase.
    tb_br = models.CharField(db_column='TB_BR', max_length=5, blank=True, null=True)  # Field name made lowercase.
    tb_br_zus = models.TextField(db_column='TB_BR_ZUS', blank=True, null=True)  # Field name made lowercase.
    azp = models.TextField(db_column='AZP', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AT_SISX_PRO_CPTTB'


class AtSisxSub(models.Model):
    subno = models.IntegerField(db_column='SUBNO', primary_key=True)  # Field name made lowercase.
    namd = models.CharField(db_column='NAMD', max_length=255, blank=True, null=True)  # Field name made lowercase.
    anamd = models.TextField(db_column='ANAMD', blank=True, null=True)  # Field name made lowercase.
    abdano = models.IntegerField(db_column='ABDANO', blank=True, null=True)  # Field name made lowercase.
    wsthst = models.CharField(db_column='WSTHST', max_length=20, blank=True, null=True)  # Field name made lowercase.
    subno2 = models.IntegerField(db_column='SUBNO2', blank=True, null=True)  # Field name made lowercase.
    dt = models.DateTimeField(db_column='DT', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AT_SISX_SUB'


class AtSisxSubst(models.Model):
    intergr = models.IntegerField(db_column='INTERGR', primary_key=True)  # Field name made lowercase.
    intergr1 = models.IntegerField(db_column='INTERGR1', blank=True, null=True)  # Field name made lowercase.
    grbez = models.CharField(db_column='GRBEZ', max_length=255, blank=True, null=True)  # Field name made lowercase.
    art = models.CharField(db_column='ART', max_length=20, blank=True, null=True)  # Field name made lowercase.
    dt = models.DateTimeField(db_column='DT', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AT_SISX_SUBST'


class Auftrag(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    fragestellung = models.CharField(db_column='FRAGESTELLUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    symptomatik = models.CharField(db_column='SYMPTOMATIK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    zuweiser_id = models.IntegerField(db_column='ZUWEISER_ID', blank=True, null=True)  # Field name made lowercase.
    region_id = models.IntegerField(db_column='REGION_ID', blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AUFTRAG'


class AuftragGruppe(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    fragestellung = models.CharField(db_column='FRAGESTELLUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    symptomatik = models.CharField(db_column='SYMPTOMATIK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    zuweiser_id = models.IntegerField(db_column='ZUWEISER_ID', blank=True, null=True)  # Field name made lowercase.
    region_id = models.IntegerField(db_column='REGION_ID', blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AUFTRAG_GRUPPE'


class AugeBrille(models.Model):
    doku_id = models.IntegerField(db_column='DOKU_ID', primary_key=True)  # Field name made lowercase.
    l_sphaere_fern = models.FloatField(db_column='L_SPHAERE_FERN', blank=True, null=True)  # Field name made lowercase.
    l_zylinder_fern = models.FloatField(db_column='L_ZYLINDER_FERN', blank=True, null=True)  # Field name made lowercase.
    l_achse_fern = models.FloatField(db_column='L_ACHSE_FERN', blank=True, null=True)  # Field name made lowercase.
    l_addition = models.FloatField(db_column='L_ADDITION', blank=True, null=True)  # Field name made lowercase.
    l_prisma = models.FloatField(db_column='L_PRISMA', blank=True, null=True)  # Field name made lowercase.
    l_basis = models.FloatField(db_column='L_BASIS', blank=True, null=True)  # Field name made lowercase.
    pupillardistanz = models.FloatField(db_column='PUPILLARDISTANZ', blank=True, null=True)  # Field name made lowercase.
    l_hornhautscheitelabstand = models.FloatField(db_column='L_HORNHAUTSCHEITELABSTAND', blank=True, null=True)  # Field name made lowercase.
    r_sphaere_fern = models.FloatField(db_column='R_SPHAERE_FERN', blank=True, null=True)  # Field name made lowercase.
    r_zylinder_fern = models.FloatField(db_column='R_ZYLINDER_FERN', blank=True, null=True)  # Field name made lowercase.
    r_achse_fern = models.FloatField(db_column='R_ACHSE_FERN', blank=True, null=True)  # Field name made lowercase.
    r_addition = models.FloatField(db_column='R_ADDITION', blank=True, null=True)  # Field name made lowercase.
    r_prisma = models.FloatField(db_column='R_PRISMA', blank=True, null=True)  # Field name made lowercase.
    r_basis = models.FloatField(db_column='R_BASIS', blank=True, null=True)  # Field name made lowercase.
    r_hornhautscheitelabstand = models.FloatField(db_column='R_HORNHAUTSCHEITELABSTAND', blank=True, null=True)  # Field name made lowercase.
    produktionsjahr = models.IntegerField(db_column='PRODUKTIONSJAHR', blank=True, null=True)  # Field name made lowercase.
    l_sphaere_nah = models.FloatField(db_column='L_SPHAERE_NAH', blank=True, null=True)  # Field name made lowercase.
    l_zylinder_nah = models.FloatField(db_column='L_ZYLINDER_NAH', blank=True, null=True)  # Field name made lowercase.
    l_achse_nah = models.FloatField(db_column='L_ACHSE_NAH', blank=True, null=True)  # Field name made lowercase.
    r_sphaere_nah = models.FloatField(db_column='R_SPHAERE_NAH', blank=True, null=True)  # Field name made lowercase.
    r_zylinder_nah = models.FloatField(db_column='R_ZYLINDER_NAH', blank=True, null=True)  # Field name made lowercase.
    r_achse_nah = models.FloatField(db_column='R_ACHSE_NAH', blank=True, null=True)  # Field name made lowercase.
    bemerkung_messung = models.TextField(db_column='BEMERKUNG_MESSUNG', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AUGE_BRILLE'


class AugeDruck(models.Model):
    doku_id = models.IntegerField(db_column='DOKU_ID', primary_key=True)  # Field name made lowercase.
    l_augendruck = models.FloatField(db_column='L_AUGENDRUCK', blank=True, null=True)  # Field name made lowercase.
    r_augendruck = models.FloatField(db_column='R_AUGENDRUCK', blank=True, null=True)  # Field name made lowercase.
    bemerkung_messung = models.TextField(db_column='BEMERKUNG_MESSUNG', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AUGE_DRUCK'


class AugeKeratometrie(models.Model):
    doku_id = models.IntegerField(db_column='DOKU_ID', primary_key=True)  # Field name made lowercase.
    l_r1_hornhautkruemmung = models.FloatField(db_column='L_R1_HORNHAUTKRUEMMUNG', blank=True, null=True)  # Field name made lowercase.
    l_r2_hornhautkruemmung = models.FloatField(db_column='L_R2_HORNHAUTKRUEMMUNG', blank=True, null=True)  # Field name made lowercase.
    l_ave_hornhautkruemmung = models.FloatField(db_column='L_AVE_HORNHAUTKRUEMMUNG', blank=True, null=True)  # Field name made lowercase.
    l_r1_brechwert = models.FloatField(db_column='L_R1_BRECHWERT', blank=True, null=True)  # Field name made lowercase.
    l_r2_brechwert = models.FloatField(db_column='L_R2_BRECHWERT', blank=True, null=True)  # Field name made lowercase.
    l_ave_brechwert = models.FloatField(db_column='L_AVE_BRECHWERT', blank=True, null=True)  # Field name made lowercase.
    l_r1_zylinderachse = models.FloatField(db_column='L_R1_ZYLINDERACHSE', blank=True, null=True)  # Field name made lowercase.
    l_r2_zylinderachse = models.FloatField(db_column='L_R2_ZYLINDERACHSE', blank=True, null=True)  # Field name made lowercase.
    l_cyl_zylinderwert = models.FloatField(db_column='L_CYL_ZYLINDERWERT', blank=True, null=True)  # Field name made lowercase.
    l_cyl_zylinderachse = models.FloatField(db_column='L_CYL_ZYLINDERACHSE', blank=True, null=True)  # Field name made lowercase.
    r_r1_hornhautkruemmung = models.FloatField(db_column='R_R1_HORNHAUTKRUEMMUNG', blank=True, null=True)  # Field name made lowercase.
    r_r2_hornhautkruemmung = models.FloatField(db_column='R_R2_HORNHAUTKRUEMMUNG', blank=True, null=True)  # Field name made lowercase.
    r_ave_hornhautkruemmung = models.FloatField(db_column='R_AVE_HORNHAUTKRUEMMUNG', blank=True, null=True)  # Field name made lowercase.
    r_r1_brechwert = models.FloatField(db_column='R_R1_BRECHWERT', blank=True, null=True)  # Field name made lowercase.
    r_r2_brechwert = models.FloatField(db_column='R_R2_BRECHWERT', blank=True, null=True)  # Field name made lowercase.
    r_ave_brechwert = models.FloatField(db_column='R_AVE_BRECHWERT', blank=True, null=True)  # Field name made lowercase.
    r_r1_zylinderachse = models.FloatField(db_column='R_R1_ZYLINDERACHSE', blank=True, null=True)  # Field name made lowercase.
    r_r2_zylinderachse = models.FloatField(db_column='R_R2_ZYLINDERACHSE', blank=True, null=True)  # Field name made lowercase.
    r_cyl_zylinderwert = models.FloatField(db_column='R_CYL_ZYLINDERWERT', blank=True, null=True)  # Field name made lowercase.
    r_cyl_zylinderachse = models.FloatField(db_column='R_CYL_ZYLINDERACHSE', blank=True, null=True)  # Field name made lowercase.
    bemerkung_messung = models.TextField(db_column='BEMERKUNG_MESSUNG', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AUGE_KERATOMETRIE'


class AugeRefraktion(models.Model):
    doku_id = models.IntegerField(db_column='DOKU_ID', unique=True, blank=True, null=True)  # Field name made lowercase.
    typ = models.CharField(db_column='TYP', max_length=255, blank=True, null=True)  # Field name made lowercase.
    l_sphaere_fern = models.FloatField(db_column='L_SPHAERE_FERN', blank=True, null=True)  # Field name made lowercase.
    l_zylinder_fern = models.FloatField(db_column='L_ZYLINDER_FERN', blank=True, null=True)  # Field name made lowercase.
    l_achse_fern = models.FloatField(db_column='L_ACHSE_FERN', blank=True, null=True)  # Field name made lowercase.
    l_addition = models.FloatField(db_column='L_ADDITION', blank=True, null=True)  # Field name made lowercase.
    l_prisma = models.FloatField(db_column='L_PRISMA', blank=True, null=True)  # Field name made lowercase.
    l_basis = models.FloatField(db_column='L_BASIS', blank=True, null=True)  # Field name made lowercase.
    pupillardistanz = models.FloatField(db_column='PUPILLARDISTANZ', blank=True, null=True)  # Field name made lowercase.
    r_sphaere_fern = models.FloatField(db_column='R_SPHAERE_FERN', blank=True, null=True)  # Field name made lowercase.
    r_zylinder_fern = models.FloatField(db_column='R_ZYLINDER_FERN', blank=True, null=True)  # Field name made lowercase.
    r_achse_fern = models.FloatField(db_column='R_ACHSE_FERN', blank=True, null=True)  # Field name made lowercase.
    r_addition = models.FloatField(db_column='R_ADDITION', blank=True, null=True)  # Field name made lowercase.
    r_prisma = models.FloatField(db_column='R_PRISMA', blank=True, null=True)  # Field name made lowercase.
    r_basis = models.FloatField(db_column='R_BASIS', blank=True, null=True)  # Field name made lowercase.
    cycloplegie = models.IntegerField(db_column='CYCLOPLEGIE', blank=True, null=True)  # Field name made lowercase.
    cycloplegie_tropfen = models.CharField(db_column='CYCLOPLEGIE_TROPFEN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    l_visus_sc = models.FloatField(db_column='L_VISUS_SC', blank=True, null=True)  # Field name made lowercase.
    l_visus_cc = models.FloatField(db_column='L_VISUS_CC', blank=True, null=True)  # Field name made lowercase.
    l_sphaere_nah = models.FloatField(db_column='L_SPHAERE_NAH', blank=True, null=True)  # Field name made lowercase.
    l_zylinder_nah = models.FloatField(db_column='L_ZYLINDER_NAH', blank=True, null=True)  # Field name made lowercase.
    l_achse_nah = models.FloatField(db_column='L_ACHSE_NAH', blank=True, null=True)  # Field name made lowercase.
    r_visus_sc = models.FloatField(db_column='R_VISUS_SC', blank=True, null=True)  # Field name made lowercase.
    r_visus_cc = models.FloatField(db_column='R_VISUS_CC', blank=True, null=True)  # Field name made lowercase.
    r_sphaere_nah = models.FloatField(db_column='R_SPHAERE_NAH', blank=True, null=True)  # Field name made lowercase.
    r_zylinder_nah = models.FloatField(db_column='R_ZYLINDER_NAH', blank=True, null=True)  # Field name made lowercase.
    r_achse_nah = models.FloatField(db_column='R_ACHSE_NAH', blank=True, null=True)  # Field name made lowercase.
    dominanz = models.CharField(db_column='DOMINANZ', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bemerkung_messung = models.TextField(db_column='BEMERKUNG_MESSUNG', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AUGE_REFRAKTION'


class AuswertungErgebnislisten(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    name = models.CharField(db_column='NAME', max_length=255)  # Field name made lowercase.
    beschreibung = models.CharField(db_column='BESCHREIBUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    auswertung_name = models.CharField(db_column='AUSWERTUNG_NAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    typ = models.CharField(db_column='TYP', max_length=255)  # Field name made lowercase.
    id_liste = models.TextField(db_column='ID_LISTE', blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    datum_erstellt = models.DateField(db_column='DATUM_ERSTELLT', blank=True, null=True)  # Field name made lowercase.
    aktiv = models.IntegerField(db_column='AKTIV', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AUSWERTUNG_ERGEBNISLISTEN'


class AuswertungKriterien(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    name = models.CharField(db_column='NAME', max_length=255)  # Field name made lowercase.
    beschreibung = models.CharField(db_column='BESCHREIBUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    auswertung_name = models.CharField(db_column='AUSWERTUNG_NAME', max_length=255)  # Field name made lowercase.
    suchfilter_xml = models.TextField(db_column='SUCHFILTER_XML', blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    nummer = models.IntegerField(db_column='NUMMER', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AUSWERTUNG_KRITERIEN'
        unique_together = (('auswertung_name', 'nummer'),)


class AutAbrechnungsstelle(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    mandant_id = models.IntegerField(db_column='MANDANT_ID', blank=True, null=True)  # Field name made lowercase.
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=100, blank=True, null=True)  # Field name made lowercase.
    kennung = models.CharField(db_column='KENNUNG', max_length=5, blank=True, null=True)  # Field name made lowercase.
    bundesland = models.IntegerField(db_column='BUNDESLAND', blank=True, null=True)  # Field name made lowercase.
    kassen_typ_liste = models.CharField(db_column='KASSEN_TYP_LISTE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    kassen_kennung_liste = models.CharField(db_column='KASSEN_KENNUNG_LISTE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)  # Field name made lowercase.
    einstellungen = models.TextField(db_column='EINSTELLUNGEN', blank=True, null=True)  # Field name made lowercase.
    elektronische_adresse = models.CharField(db_column='ELEKTRONISCHE_ADRESSE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    dienstleister_nr = models.CharField(db_column='DIENSTLEISTER_NR', max_length=20, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AUT_ABRECHNUNGSSTELLE'
        unique_together = (('bezeichnung', 'mandant_id'),)


class AutAbrechnungStatistik(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)  # Field name made lowercase.
    mandand_id = models.IntegerField(db_column='MANDAND_ID', blank=True, null=True)  # Field name made lowercase.
    abrechnungsstellekennung = models.CharField(db_column='AbrechnungsstelleKennung', max_length=255, blank=True, null=True)  # Field name made lowercase.
    abrechnungskassenkennungen = models.CharField(db_column='AbrechnungsKassenKennungen', max_length=255, blank=True, null=True)  # Field name made lowercase.
    abrechnungskassentypen = models.CharField(db_column='AbrechnungsKassenTypen', max_length=255, blank=True, null=True)  # Field name made lowercase.
    erstellung = models.DateTimeField(db_column='Erstellung', blank=True, null=True)  # Field name made lowercase.
    zeitraumjahr = models.IntegerField(db_column='ZeitraumJahr', blank=True, null=True)  # Field name made lowercase.
    zeitraumkennung = models.IntegerField(db_column='ZeitraumKennung', blank=True, null=True)  # Field name made lowercase.
    datum_start = models.DateField(db_column='DATUM_START', blank=True, null=True)  # Field name made lowercase.
    datum_ende = models.DateField(db_column='DATUM_ENDE', blank=True, null=True)  # Field name made lowercase.
    anzahlscheine = models.IntegerField(db_column='AnzahlScheine', blank=True, null=True)  # Field name made lowercase.
    gesamtsumme = models.FloatField(db_column='Gesamtsumme', blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AUT_ABRECHNUNG_STATISTIK'
        unique_together = (('mandand_id', 'abrechnungsstellekennung', 'datum_start', 'datum_ende'),)


class AutAbsmedikament(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    patient_id = models.IntegerField(db_column='PATIENT_ID')  # Field name made lowercase.
    absdoku_id = models.IntegerField(db_column='ABSDOKU_ID')  # Field name made lowercase.
    medikament_id = models.IntegerField(db_column='MEDIKAMENT_ID', blank=True, null=True)  # Field name made lowercase.
    begruendung = models.TextField(db_column='BEGRUENDUNG', blank=True, null=True)  # Field name made lowercase.
    diagnose = models.TextField(db_column='DIAGNOSE', blank=True, null=True)  # Field name made lowercase.
    langzeitverordnung = models.CharField(db_column='LANGZEITVERORDNUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    antrag_medikament = models.CharField(db_column='ANTRAG_MEDIKAMENT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    pharmanummer = models.CharField(db_column='PHARMANUMMER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    magistralrezeptur = models.TextField(db_column='MAGISTRALREZEPTUR', blank=True, null=True)  # Field name made lowercase.
    antrag_anzahl = models.FloatField(db_column='ANTRAG_ANZAHL', blank=True, null=True)  # Field name made lowercase.
    dosisschema = models.TextField(db_column='DOSISSCHEMA', blank=True, null=True)  # Field name made lowercase.
    bewilligt_anzahl = models.FloatField(db_column='BEWILLIGT_ANZAHL', blank=True, null=True)  # Field name made lowercase.
    bewilligt_medikament = models.CharField(db_column='BEWILLIGT_MEDIKAMENT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bewilligt_lzv_monate = models.IntegerField(db_column='BEWILLIGT_LZV_MONATE', blank=True, null=True)  # Field name made lowercase.
    bewilligt_lzv_anzahl = models.IntegerField(db_column='BEWILLIGT_LZV_ANZAHL', blank=True, null=True)  # Field name made lowercase.
    entscheidung = models.TextField(db_column='ENTSCHEIDUNG', blank=True, null=True)  # Field name made lowercase.
    info_text = models.TextField(db_column='INFO_TEXT', blank=True, null=True)  # Field name made lowercase.
    abs_anfrage_id = models.CharField(db_column='ABS_ANFRAGE_ID', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bewilligt_lzv_offen = models.IntegerField(db_column='BEWILLIGT_LZV_OFFEN', blank=True, null=True)  # Field name made lowercase.
    bewilligt_lzv_bis = models.DateField(db_column='BEWILLIGT_LZV_BIS', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AUT_ABSMEDIKAMENT'


class AutApWarenverzeichnis(models.Model):
    pharma_nr = models.IntegerField(db_column='PHARMA_NR', primary_key=True)  # Field name made lowercase.
    artikel_nr = models.IntegerField(db_column='ARTIKEL_NR', blank=True, null=True)  # Field name made lowercase.
    aenderung_code = models.CharField(db_column='AENDERUNG_CODE', max_length=5, blank=True, null=True)  # Field name made lowercase.
    gueltig_ab = models.DateField(db_column='GUELTIG_AB', blank=True, null=True)  # Field name made lowercase.
    hersteller_code = models.CharField(db_column='HERSTELLER_CODE', max_length=5, blank=True, null=True)  # Field name made lowercase.
    ap_ek_preis = models.FloatField(db_column='AP_EK_PREIS', blank=True, null=True)  # Field name made lowercase.
    kassen_preis = models.FloatField(db_column='KASSEN_PREIS', blank=True, null=True)  # Field name made lowercase.
    verkaufs_preis = models.FloatField(db_column='VERKAUFS_PREIS', blank=True, null=True)  # Field name made lowercase.
    box = models.CharField(db_column='BOX', max_length=5, blank=True, null=True)  # Field name made lowercase.
    artikel_kennzeichen = models.CharField(db_column='ARTIKEL_KENNZEICHEN', max_length=5, blank=True, null=True)  # Field name made lowercase.
    lagervorschrift = models.CharField(db_column='LAGERVORSCHRIFT', max_length=5, blank=True, null=True)  # Field name made lowercase.
    rezeptzeichen = models.CharField(db_column='REZEPTZEICHEN', max_length=5, blank=True, null=True)  # Field name made lowercase.
    rzpt_gebuehr = models.CharField(db_column='RZPT_GEBUEHR', max_length=5, blank=True, null=True)  # Field name made lowercase.
    kurztext = models.CharField(db_column='KURZTEXT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    menge = models.CharField(db_column='MENGE', max_length=10, blank=True, null=True)  # Field name made lowercase.
    mengenart = models.CharField(db_column='MENGENART', max_length=5, blank=True, null=True)  # Field name made lowercase.
    kassenzeichen = models.CharField(db_column='KASSENZEICHEN', max_length=5, blank=True, null=True)  # Field name made lowercase.
    mwst_satz = models.IntegerField(db_column='MWST_SATZ', blank=True, null=True)  # Field name made lowercase.
    liefer_code = models.CharField(db_column='LIEFER_CODE', max_length=5, blank=True, null=True)  # Field name made lowercase.
    getraenke_steuer = models.CharField(db_column='GETRAENKE_STEUER', max_length=5, blank=True, null=True)  # Field name made lowercase.
    warengruppe = models.CharField(db_column='WARENGRUPPE', max_length=5, blank=True, null=True)  # Field name made lowercase.
    kassenzeichen_zusatz = models.CharField(db_column='KASSENZEICHEN_ZUSATZ', max_length=5, blank=True, null=True)  # Field name made lowercase.
    registernummer = models.CharField(db_column='REGISTERNUMMER', max_length=6, blank=True, null=True)  # Field name made lowercase.
    preiskennzeichen = models.IntegerField(db_column='PREISKENNZEICHEN', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AUT_AP_WARENVERZEICHNIS'


class AutAuMeldungen(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    doku_id = models.IntegerField(db_column='DOKU_ID')  # Field name made lowercase.
    meldungsart = models.CharField(db_column='MELDUNGSART', max_length=50, blank=True, null=True)  # Field name made lowercase.
    ausstellungsdatum = models.DateField(db_column='AUSSTELLUNGSDATUM', blank=True, null=True)  # Field name made lowercase.
    quittung_id = models.IntegerField(db_column='QUITTUNG_ID', unique=True, blank=True, null=True)  # Field name made lowercase.
    quittung_version = models.CharField(db_column='QUITTUNG_VERSION', max_length=50, blank=True, null=True)  # Field name made lowercase.
    meldungsdaten = models.TextField(db_column='MELDUNGSDATEN', blank=True, null=True)  # Field name made lowercase.
    personendaten = models.TextField(db_column='PERSONENDATEN', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    au_von = models.DateField(db_column='AU_VON', blank=True, null=True)  # Field name made lowercase.
    au_bis = models.DateField(db_column='AU_BIS', blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)  # Field name made lowercase.
    versicherungsnummer = models.CharField(db_column='VERSICHERUNGSNUMMER', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AUT_AU_MELDUNGEN'


class AutEcardMeldungen(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)  # Field name made lowercase.
    datum = models.DateField(db_column='DATUM', blank=True, null=True)  # Field name made lowercase.
    uhrzeit = models.TimeField(db_column='UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    meldung = models.TextField(db_column='MELDUNG', blank=True, null=True)  # Field name made lowercase.
    category = models.CharField(db_column='CATEGORY', max_length=255, blank=True, null=True)  # Field name made lowercase.
    daten = models.TextField(db_column='DATEN', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AUT_ECARD_MELDUNGEN'


class AutEcardQuittungen(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    formular_doku_id = models.IntegerField(db_column='FORMULAR_DOKU_ID')  # Field name made lowercase.
    annahmezeitpunkt = models.CharField(db_column='ANNAHMEZEITPUNKT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    datenblatt_typ = models.CharField(db_column='DATENBLATT_TYP', max_length=255, blank=True, null=True)  # Field name made lowercase.
    signatur = models.CharField(db_column='SIGNATUR', max_length=255, blank=True, null=True)  # Field name made lowercase.
    sv_nummer = models.CharField(db_column='SV_NUMMER', max_length=255)  # Field name made lowercase.
    svt = models.CharField(db_column='SVT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    untersuchungsdatum = models.CharField(db_column='UNTERSUCHUNGSDATUM', max_length=255, blank=True, null=True)  # Field name made lowercase.
    vp_nummer = models.CharField(db_column='VP_NUMMER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    system_zeit = models.DateTimeField(db_column='SYSTEM_ZEIT')  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AUT_ECARD_QUITTUNGEN'


class AutGnwAufenthalt(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    patient_id = models.IntegerField(db_column='PATIENT_ID', blank=True, null=True)  # Field name made lowercase.
    patient_name_aufenthalt = models.CharField(db_column='PATIENT_NAME_AUFENTHALT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    patient_vorname_aufenthalt = models.CharField(db_column='PATIENT_VORNAME_AUFENTHALT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    patient_gebdatum_aufenthalt = models.DateField(db_column='PATIENT_GEBDATUM_AUFENTHALT', blank=True, null=True)  # Field name made lowercase.
    patient_versichnr_aufenthalt = models.CharField(db_column='PATIENT_VERSICHNR_AUFENTHALT', max_length=50, blank=True, null=True)  # Field name made lowercase.
    aufnahmezeit = models.DateTimeField(db_column='AUFNAHMEZEIT', blank=True, null=True)  # Field name made lowercase.
    entlassungszeit = models.DateTimeField(db_column='ENTLASSUNGSZEIT', blank=True, null=True)  # Field name made lowercase.
    betriebsstellekey = models.CharField(db_column='BETRIEBSSTELLEKEY', max_length=100, blank=True, null=True)  # Field name made lowercase.
    betriebsstelletext = models.CharField(db_column='BETRIEBSSTELLETEXT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    aufenthaltskey = models.CharField(db_column='AUFENTHALTSKEY', max_length=100, blank=True, null=True)  # Field name made lowercase.
    aufenthaltsart = models.CharField(db_column='AUFENTHALTSART', max_length=255, blank=True, null=True)  # Field name made lowercase.
    aufenthaltspfad = models.CharField(db_column='AUFENTHALTSPFAD', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AUT_GNW_AUFENTHALT'
        unique_together = (('betriebsstellekey', 'aufenthaltskey'),)


class AutGnwAufenthaltabfrage(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP')  # Field name made lowercase.
    befundabfrage_id = models.IntegerField(db_column='BEFUNDABFRAGE_ID')  # Field name made lowercase.
    betriebsstellekey = models.CharField(db_column='BETRIEBSSTELLEKEY', max_length=100, blank=True, null=True)  # Field name made lowercase.
    aufenthaltskey = models.CharField(db_column='AUFENTHALTSKEY', max_length=100, blank=True, null=True)  # Field name made lowercase.
    masterserver_betrkey = models.CharField(db_column='MASTERSERVER_BETRKEY', max_length=100, blank=True, null=True)  # Field name made lowercase.
    masterserver_email = models.CharField(db_column='MASTERSERVER_EMAIL', max_length=100, blank=True, null=True)  # Field name made lowercase.
    anfrage_kennung = models.CharField(db_column='ANFRAGE_KENNUNG', unique=True, max_length=100, blank=True, null=True)  # Field name made lowercase.
    anfrage_status = models.IntegerField(db_column='ANFRAGE_STATUS')  # Field name made lowercase.
    anfrage_zeit = models.DateTimeField(db_column='ANFRAGE_ZEIT', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AUT_GNW_AUFENTHALTABFRAGE'


class AutKonsultation(models.Model):
    id = models.BigIntegerField(db_column='ID', primary_key=True)  # Field name made lowercase.
    kschein_id = models.IntegerField(db_column='KSCHEIN_ID')  # Field name made lowercase.
    abrechnungsperiode = models.CharField(db_column='ABRECHNUNGSPERIODE', max_length=50, blank=True, null=True)  # Field name made lowercase.
    abstimmungsbeleg = models.TextField(db_column='ABSTIMMUNGSBELEG', blank=True, null=True)  # Field name made lowercase.
    anspruchsart = models.CharField(db_column='ANSPRUCHSART', max_length=50, blank=True, null=True)  # Field name made lowercase.
    bearbeitungsdatum = models.DateField(db_column='BEARBEITUNGSDATUM', blank=True, null=True)  # Field name made lowercase.
    behandlungsdatum = models.DateField(db_column='BEHANDLUNGSDATUM', blank=True, null=True)  # Field name made lowercase.
    behandlungsfallcode = models.CharField(db_column='BEHANDLUNGSFALLCODE', max_length=50, blank=True, null=True)  # Field name made lowercase.
    bezugsbereich = models.CharField(db_column='BEZUGSBEREICH', max_length=50, blank=True, null=True)  # Field name made lowercase.
    fachgebietscode = models.CharField(db_column='FACHGEBIETSCODE', max_length=50, blank=True, null=True)  # Field name made lowercase.
    konsultationsartcode = models.CharField(db_column='KONSULTATIONSARTCODE', max_length=50, blank=True, null=True)  # Field name made lowercase.
    limitgeprueft = models.CharField(db_column='LIMITGEPRUEFT', max_length=50, blank=True, null=True)  # Field name made lowercase.
    zustaendigesvt = models.CharField(db_column='ZUSTAENDIGESVT', max_length=50, blank=True, null=True)  # Field name made lowercase.
    ordinationsnr = models.IntegerField(db_column='ORDINATIONSNR', blank=True, null=True)  # Field name made lowercase.
    originalsvtcode = models.CharField(db_column='ORIGINALSVTCODE', max_length=50, blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='STATUS', max_length=50, blank=True, null=True)  # Field name made lowercase.
    svnummer = models.CharField(db_column='SVNUMMER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    abgeleitetesvnr = models.CharField(db_column='ABGELEITETESVNR', max_length=50, blank=True, null=True)  # Field name made lowercase.
    txnnummer = models.BigIntegerField(db_column='TXNNUMMER', blank=True, null=True)  # Field name made lowercase.
    verrechnungssvtnr = models.CharField(db_column='VERRECHNUNGSSVTNR', max_length=50, blank=True, null=True)  # Field name made lowercase.
    versichertenartcode = models.CharField(db_column='VERSICHERTENARTCODE', max_length=50, blank=True, null=True)  # Field name made lowercase.
    versichertenkategorie = models.CharField(db_column='VERSICHERTENKATEGORIE', max_length=50, blank=True, null=True)  # Field name made lowercase.
    version = models.IntegerField(db_column='VERSION', blank=True, null=True)  # Field name made lowercase.
    vertragspartnernr = models.CharField(db_column='VERTRAGSPARTNERNR', max_length=20, blank=True, null=True)  # Field name made lowercase.
    kstanteilbefreit = models.CharField(db_column='KSTANTEILBEFREIT', max_length=10, blank=True, null=True)  # Field name made lowercase.
    overlimit = models.CharField(db_column='OVERLIMIT', max_length=10, blank=True, null=True)  # Field name made lowercase.
    rzptgebfrei = models.CharField(db_column='RZPTGEBFREI', max_length=10, blank=True, null=True)  # Field name made lowercase.
    storno = models.IntegerField(db_column='STORNO', blank=True, null=True)  # Field name made lowercase.
    bearbeitungsuhrzeit = models.TimeField(db_column='BEARBEITUNGSUHRZEIT', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AUT_KONSULTATION'


class AutKostentraeger(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    typ = models.CharField(db_column='TYP', max_length=50, blank=True, null=True)  # Field name made lowercase.
    kennung = models.IntegerField(db_column='KENNUNG', blank=True, null=True)  # Field name made lowercase.
    kennung_s = models.CharField(db_column='KENNUNG_S', unique=True, max_length=50, blank=True, null=True)  # Field name made lowercase.
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=100, blank=True, null=True)  # Field name made lowercase.
    gruppe = models.CharField(db_column='GRUPPE', max_length=3, blank=True, null=True)  # Field name made lowercase.
    kuerzel = models.CharField(db_column='KUERZEL', max_length=50, blank=True, null=True)  # Field name made lowercase.
    notizen = models.TextField(db_column='NOTIZEN', blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='STATUS', max_length=50, blank=True, null=True)  # Field name made lowercase.
    gueltig_von = models.DateField(db_column='GUELTIG_VON', blank=True, null=True)  # Field name made lowercase.
    gueltig_bis = models.DateField(db_column='GUELTIG_BIS', blank=True, null=True)  # Field name made lowercase.
    version = models.CharField(db_column='VERSION', max_length=5, blank=True, null=True)  # Field name made lowercase.
    niederlassung = models.CharField(db_column='NIEDERLASSUNG', max_length=30, blank=True, null=True)  # Field name made lowercase.
    bundesland = models.CharField(db_column='BUNDESLAND', max_length=50, blank=True, null=True)  # Field name made lowercase.
    sortierung = models.IntegerField(db_column='SORTIERUNG', blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung_datum = models.DateField(db_column='LETZTE_AENDERUNG_DATUM', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung_uhrzeit = models.TimeField(db_column='LETZTE_AENDERUNG_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    strasse = models.CharField(db_column='STRASSE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    plz = models.CharField(db_column='PLZ', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ort = models.CharField(db_column='ORT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ustid = models.CharField(db_column='USTID', max_length=255, blank=True, null=True)  # Field name made lowercase.
    fallpauschale = models.FloatField(db_column='FALLPAUSCHALE', blank=True, null=True)  # Field name made lowercase.
    leererschein = models.IntegerField(db_column='LEERERSCHEIN', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AUT_KOSTENTRAEGER'


class AutKostentraegerBewilligungsstellen(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    bewilligungsstellen_id = models.IntegerField(db_column='BEWILLIGUNGSSTELLEN_ID')  # Field name made lowercase.
    aut_kostentraeger_id = models.IntegerField(db_column='AUT_KOSTENTRAEGER_ID')  # Field name made lowercase.
    aut_kschein_versich_kategorie_von = models.IntegerField(db_column='AUT_KSCHEIN_VERSICH_KATEGORIE_VON')  # Field name made lowercase.
    aut_kschein_versich_kategorie_bis = models.IntegerField(db_column='AUT_KSCHEIN_VERSICH_KATEGORIE_BIS')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AUT_KOSTENTRAEGER_BEWILLIGUNGSSTELLEN'


class AutKschein(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    patient_id = models.IntegerField(db_column='PATIENT_ID')  # Field name made lowercase.
    orgeinheit_id = models.IntegerField(db_column='ORGEINHEIT_ID')  # Field name made lowercase.
    ermaechtigung_id = models.IntegerField(db_column='ERMAECHTIGUNG_ID')  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS')  # Field name made lowercase.
    kschein_nr = models.IntegerField(db_column='KSCHEIN_NR')  # Field name made lowercase.
    abgabe_datum = models.DateField(db_column='ABGABE_DATUM', blank=True, null=True)  # Field name made lowercase.
    gueltigkeit = models.CharField(db_column='GUELTIGKEIT', max_length=10, blank=True, null=True)  # Field name made lowercase.
    gueltigkeit_jahr = models.CharField(db_column='GUELTIGKEIT_JAHR', max_length=10, blank=True, null=True)  # Field name made lowercase.
    kschein_art = models.IntegerField(db_column='KSCHEIN_ART', blank=True, null=True)  # Field name made lowercase.
    versich_kategorie = models.IntegerField(db_column='VERSICH_KATEGORIE', blank=True, null=True)  # Field name made lowercase.
    kennzeichen = models.CharField(db_column='KENNZEICHEN', max_length=10, blank=True, null=True)  # Field name made lowercase.
    zuw_nr = models.CharField(db_column='ZUW_NR', max_length=50, blank=True, null=True)  # Field name made lowercase.
    zuw_kom_anrede = models.CharField(db_column='ZUW_KOM_ANREDE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    zuw_adresse = models.CharField(db_column='ZUW_ADRESSE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    zuw_kommunikation = models.CharField(db_column='ZUW_KOMMUNIKATION', max_length=255, blank=True, null=True)  # Field name made lowercase.
    zuw_fachgebiet = models.CharField(db_column='ZUW_FACHGEBIET', max_length=50, blank=True, null=True)  # Field name made lowercase.
    ueberw_datum = models.DateField(db_column='UEBERW_DATUM', blank=True, null=True)  # Field name made lowercase.
    gruvu = models.IntegerField(db_column='GRUVU', blank=True, null=True)  # Field name made lowercase.
    kaution = models.FloatField(db_column='KAUTION', blank=True, null=True)  # Field name made lowercase.
    kaution_waehrung = models.CharField(db_column='KAUTION_WAEHRUNG', max_length=50, blank=True, null=True)  # Field name made lowercase.
    kst_code = models.CharField(db_column='KST_CODE', max_length=50, blank=True, null=True)  # Field name made lowercase.
    kasse_id = models.IntegerField(db_column='KASSE_ID', blank=True, null=True)  # Field name made lowercase.
    kasse_typ = models.CharField(db_column='KASSE_TYP', max_length=10, blank=True, null=True)  # Field name made lowercase.
    kasse_bland = models.CharField(db_column='KASSE_BLAND', max_length=10, blank=True, null=True)  # Field name made lowercase.
    rzptgeb_bis = models.DateField(db_column='RZPTGEB_BIS', blank=True, null=True)  # Field name made lowercase.
    vers_nehm_vers_nr = models.CharField(db_column='VERS_NEHM_VERS_NR', max_length=50, blank=True, null=True)  # Field name made lowercase.
    vers_nehm_gebdatum = models.DateField(db_column='VERS_NEHM_GEBDATUM', blank=True, null=True)  # Field name made lowercase.
    vers_nehm_kom_anrede = models.CharField(db_column='VERS_NEHM_KOM_ANREDE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    vers_nehm_adresse = models.CharField(db_column='VERS_NEHM_ADRESSE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    vers_nehm_kommunikation = models.CharField(db_column='VERS_NEHM_KOMMUNIKATION', max_length=255, blank=True, null=True)  # Field name made lowercase.
    verw_verh = models.CharField(db_column='VERW_VERH', max_length=50, blank=True, null=True)  # Field name made lowercase.
    zus_vers_id = models.CharField(db_column='ZUS_VERS_ID', max_length=50, blank=True, null=True)  # Field name made lowercase.
    dienst_kom_anrede = models.CharField(db_column='DIENST_KOM_ANREDE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    dienst_adresse = models.TextField(db_column='DIENST_ADRESSE', blank=True, null=True)  # Field name made lowercase.
    dienst_kommunikation = models.CharField(db_column='DIENST_KOMMUNIKATION', max_length=255, blank=True, null=True)  # Field name made lowercase.
    notizen = models.TextField(db_column='NOTIZEN', blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    patienten_adresse = models.CharField(db_column='PATIENTEN_ADRESSE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)  # Field name made lowercase.
    properties = models.TextField(db_column='PROPERTIES', blank=True, null=True)  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)  # Field name made lowercase.
    abr_monat = models.IntegerField(db_column='ABR_MONAT', blank=True, null=True)  # Field name made lowercase.
    abr_jahr = models.IntegerField(db_column='ABR_JAHR', blank=True, null=True)  # Field name made lowercase.
    vertragsart = models.IntegerField(db_column='VERTRAGSART', blank=True, null=True)  # Field name made lowercase.
    zuweiser_id = models.IntegerField(db_column='ZUWEISER_ID', blank=True, null=True)  # Field name made lowercase.
    zuweisungstext = models.TextField(db_column='ZUWEISUNGSTEXT', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AUT_KSCHEIN'


class AutLaborstammZifferZuordnung(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    stammdaten_id = models.IntegerField(db_column='STAMMDATEN_ID')  # Field name made lowercase.
    anzahl = models.IntegerField(db_column='ANZAHL')  # Field name made lowercase.
    typ = models.CharField(db_column='TYP', max_length=50, blank=True, null=True)  # Field name made lowercase.
    ziffern_kasse_id = models.IntegerField(db_column='ZIFFERN_KASSE_ID', blank=True, null=True)  # Field name made lowercase.
    ziffern_privat_id = models.IntegerField(db_column='ZIFFERN_PRIVAT_ID', blank=True, null=True)  # Field name made lowercase.
    status = models.FloatField(db_column='STATUS', blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    vertragsart_kennung = models.IntegerField(db_column='VERTRAGSART_KENNUNG', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AUT_LABORSTAMM_ZIFFER_ZUORDNUNG'


class AutTherapieZuordnung(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    phys_therapie_id = models.IntegerField(db_column='PHYS_THERAPIE_ID')  # Field name made lowercase.
    anzahl = models.IntegerField(db_column='ANZAHL')  # Field name made lowercase.
    typ = models.CharField(db_column='TYP', max_length=50, blank=True, null=True)  # Field name made lowercase.
    aut_ziffern_kasse_id = models.IntegerField(db_column='AUT_ZIFFERN_KASSE_ID', blank=True, null=True)  # Field name made lowercase.
    ziffern_privat_id = models.IntegerField(db_column='ZIFFERN_PRIVAT_ID', blank=True, null=True)  # Field name made lowercase.
    status = models.FloatField(db_column='STATUS', blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    vertragsart_kennung = models.IntegerField(db_column='VERTRAGSART_KENNUNG', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AUT_THERAPIE_ZUORDNUNG'


class AutZiffernKasse(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    typ = models.CharField(db_column='TYP', max_length=20, blank=True, null=True)  # Field name made lowercase.
    subtyp = models.CharField(db_column='SUBTYP', max_length=20, blank=True, null=True)  # Field name made lowercase.
    ziffer = models.CharField(db_column='ZIFFER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ziffer_text = models.TextField(db_column='ZIFFER_TEXT', blank=True, null=True)  # Field name made lowercase.
    ziffer_text_kurz = models.CharField(db_column='ZIFFER_TEXT_KURZ', max_length=255, blank=True, null=True)  # Field name made lowercase.
    punkte = models.FloatField(db_column='PUNKTE')  # Field name made lowercase.
    gruppe = models.CharField(db_column='GRUPPE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    kuerzel = models.CharField(db_column='KUERZEL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    notizen = models.TextField(db_column='NOTIZEN', blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    status = models.FloatField(db_column='STATUS', blank=True, null=True)  # Field name made lowercase.
    ziffer_von = models.DateField(db_column='ZIFFER_VON', blank=True, null=True)  # Field name made lowercase.
    ziffer_bis = models.DateField(db_column='ZIFFER_BIS', blank=True, null=True)  # Field name made lowercase.
    betrag = models.FloatField(db_column='BETRAG')  # Field name made lowercase.
    waehrung = models.CharField(db_column='WAEHRUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    version = models.CharField(db_column='VERSION', max_length=255, blank=True, null=True)  # Field name made lowercase.
    zeitstempel = models.DateTimeField(db_column='ZEITSTEMPEL', blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    regel_l = models.TextField(db_column='REGEL_L', blank=True, null=True)  # Field name made lowercase.
    erstattung_1 = models.FloatField(db_column='ERSTATTUNG_1', blank=True, null=True)  # Field name made lowercase.
    erstattung_2 = models.FloatField(db_column='ERSTATTUNG_2', blank=True, null=True)  # Field name made lowercase.
    fachgebiete = models.CharField(db_column='FACHGEBIETE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    betrag2 = models.FloatField(db_column='BETRAG2', blank=True, null=True)  # Field name made lowercase.
    punktwert_kuerzel = models.CharField(db_column='PUNKTWERT_KUERZEL', max_length=20, blank=True, null=True)  # Field name made lowercase.
    einstellungen = models.TextField(db_column='EINSTELLUNGEN', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AUT_ZIFFERN_KASSE'
        unique_together = (('ziffer', 'typ', 'subtyp', 'ziffer_von'), ('ziffer', 'typ', 'subtyp', 'ziffer_bis'),)


class BausteineText(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP')  # Field name made lowercase.
    position = models.IntegerField(db_column='POSITION')  # Field name made lowercase.
    kuerzel = models.CharField(db_column='KUERZEL', max_length=50, blank=True, null=True)  # Field name made lowercase.
    langtext = models.TextField(db_column='LANGTEXT', blank=True, null=True)  # Field name made lowercase.
    tb_kategorie = models.CharField(db_column='TB_KATEGORIE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    wahlarzt = models.CharField(db_column='WAHLARZT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    kasse = models.CharField(db_column='KASSE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    mandanten = models.CharField(db_column='MANDANTEN', max_length=255)  # Field name made lowercase.
    bg = models.CharField(db_column='BG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    vertragsart = models.CharField(db_column='VERTRAGSART', max_length=255, blank=True, null=True)  # Field name made lowercase.
    system_zeit = models.DateTimeField(db_column='SYSTEM_ZEIT', blank=True, null=True)  # Field name made lowercase.
    liste = models.TextField(db_column='LISTE', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'BAUSTEINE_TEXT'


class BausteineUntersuchung(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    elter_id = models.CharField(db_column='ELTER_ID', max_length=50, blank=True, null=True)  # Field name made lowercase.
    kuerzel = models.CharField(db_column='KUERZEL', max_length=50, blank=True, null=True)  # Field name made lowercase.
    langtext = models.TextField(db_column='LANGTEXT', blank=True, null=True)  # Field name made lowercase.
    typ = models.CharField(db_column='TYP', max_length=50, blank=True, null=True)  # Field name made lowercase.
    wahlarzt = models.CharField(db_column='WAHLARZT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    kasse = models.CharField(db_column='KASSE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ziffer = models.CharField(db_column='ZIFFER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    diagnose = models.CharField(db_column='DIAGNOSE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    therapie = models.TextField(db_column='THERAPIE', blank=True, null=True)  # Field name made lowercase.
    befund = models.TextField(db_column='BEFUND', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    mandanten = models.CharField(db_column='MANDANTEN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    untersuchung = models.TextField(db_column='UNTERSUCHUNG', blank=True, null=True)  # Field name made lowercase.
    anamnese = models.TextField(db_column='ANAMNESE', blank=True, null=True)  # Field name made lowercase.
    formular_1_id = models.IntegerField(db_column='FORMULAR_1_ID', blank=True, null=True)  # Field name made lowercase.
    einstellungen = models.TextField(db_column='EINSTELLUNGEN', blank=True, null=True)  # Field name made lowercase.
    roentgen = models.TextField(db_column='ROENTGEN', blank=True, null=True)  # Field name made lowercase.
    sono = models.TextField(db_column='SONO', blank=True, null=True)  # Field name made lowercase.
    formular_2_id = models.IntegerField(db_column='FORMULAR_2_ID', blank=True, null=True)  # Field name made lowercase.
    bg = models.CharField(db_column='BG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    vertragsart = models.CharField(db_column='VERTRAGSART', max_length=255, blank=True, null=True)  # Field name made lowercase.
    rezept = models.TextField(db_column='REZEPT', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'BAUSTEINE_UNTERSUCHUNG'


class BehandlungenEinzelleistungen(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    behandlungen_gruppen_id = models.IntegerField(db_column='BEHANDLUNGEN_GRUPPEN_ID')  # Field name made lowercase.
    therapie_id = models.IntegerField(db_column='THERAPIE_ID')  # Field name made lowercase.
    region_id = models.IntegerField(db_column='REGION_ID', blank=True, null=True)  # Field name made lowercase.
    be_bemerkung = models.CharField(db_column='BE_BEMERKUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    vertragsart = models.IntegerField(db_column='VERTRAGSART', blank=True, null=True)  # Field name made lowercase.
    be_reihe = models.SmallIntegerField(db_column='BE_REIHE')  # Field name made lowercase.
    be_privatpreis = models.FloatField(db_column='BE_PRIVATPREIS', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'BEHANDLUNGEN_EINZELLEISTUNGEN'


class BehandlungenGruppen(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    doku_id = models.IntegerField(db_column='DOKU_ID')  # Field name made lowercase.
    untersuchung_id = models.IntegerField(db_column='UNTERSUCHUNG_ID')  # Field name made lowercase.
    bg_anzahl = models.SmallIntegerField(db_column='BG_ANZAHL')  # Field name made lowercase.
    bg_erhalten = models.SmallIntegerField(db_column='BG_ERHALTEN')  # Field name made lowercase.
    bg_status = models.CharField(db_column='BG_STATUS', max_length=2)  # Field name made lowercase.
    bg_vorige_id = models.IntegerField(db_column='BG_VORIGE_ID', blank=True, null=True)  # Field name made lowercase.
    benutzer_id = models.IntegerField(db_column='BENUTZER_ID', blank=True, null=True)  # Field name made lowercase.
    bg_aktivierbar_bis = models.DateTimeField(db_column='BG_AKTIVIERBAR_BIS', blank=True, null=True)  # Field name made lowercase.
    bg_aenr = models.SmallIntegerField(db_column='BG_AENR')  # Field name made lowercase.
    bg_nr = models.SmallIntegerField(db_column='BG_NR')  # Field name made lowercase.
    bg_name = models.CharField(db_column='BG_NAME', max_length=30, blank=True, null=True)  # Field name made lowercase.
    faxstatus = models.CharField(db_column='FAXSTATUS', max_length=1, blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bearbeitungs_status = models.IntegerField(db_column='BEARBEITUNGS_STATUS')  # Field name made lowercase.
    bearbeitungs_instanz = models.IntegerField(db_column='BEARBEITUNGS_INSTANZ')  # Field name made lowercase.
    bewilligungs_status = models.IntegerField(db_column='BEWILLIGUNGS_STATUS')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'BEHANDLUNGEN_GRUPPEN'


class Behandlungsplaneinheit(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    behandlungsplandoku_id = models.IntegerField(db_column='BEHANDLUNGSPLANDOKU_ID', blank=True, null=True)  # Field name made lowercase.
    daten = models.TextField(db_column='DATEN', blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'BEHANDLUNGSPLANEINHEIT'


class BehandlungsplaneinheitStamm(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    behandlungsplan_id = models.IntegerField(db_column='BEHANDLUNGSPLAN_ID', blank=True, null=True)  # Field name made lowercase.
    kuerzel = models.CharField(db_column='KUERZEL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)  # Field name made lowercase.
    sortierung = models.IntegerField(db_column='SORTIERUNG', blank=True, null=True)  # Field name made lowercase.
    beschreibung = models.TextField(db_column='BESCHREIBUNG', blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'BEHANDLUNGSPLANEINHEIT_STAMM'
        unique_together = (('kuerzel', 'behandlungsplan_id'),)


class BehandlungsplanStamm(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    mandant = models.CharField(db_column='MANDANT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'BEHANDLUNGSPLAN_STAMM'


class Benutzer(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    kue = models.CharField(db_column='KUE', unique=True, max_length=255, blank=True, null=True)  # Field name made lowercase.
    anrede = models.CharField(db_column='ANREDE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    titel = models.CharField(db_column='TITEL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    vorname = models.CharField(db_column='VORNAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    wvorname = models.CharField(db_column='WVORNAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    name = models.CharField(db_column='NAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    gebname = models.CharField(db_column='GEBNAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    namenszusatz = models.CharField(db_column='NAMENSZUSATZ', max_length=255, blank=True, null=True)  # Field name made lowercase.
    namensvorsatz = models.CharField(db_column='NAMENSVORSATZ', max_length=255, blank=True, null=True)  # Field name made lowercase.
    gebdatum = models.DateField(db_column='GEBDATUM', blank=True, null=True)  # Field name made lowercase.
    bild = models.TextField(db_column='BILD', blank=True, null=True)  # Field name made lowercase.
    notizen = models.TextField(db_column='NOTIZEN', blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    strasse = models.CharField(db_column='STRASSE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    plz = models.CharField(db_column='PLZ', max_length=50, blank=True, null=True)  # Field name made lowercase.
    ort = models.CharField(db_column='ORT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    land = models.CharField(db_column='LAND', max_length=255, blank=True, null=True)  # Field name made lowercase.
    adresse_id1 = models.CharField(db_column='Adresse_ID1', max_length=50, blank=True, null=True)  # Field name made lowercase.
    adresse_id2 = models.CharField(db_column='Adresse_ID2', max_length=50, blank=True, null=True)  # Field name made lowercase.
    adresse_id3 = models.CharField(db_column='Adresse_ID3', max_length=50, blank=True, null=True)  # Field name made lowercase.
    adresse_id4 = models.CharField(db_column='Adresse_ID4', max_length=50, blank=True, null=True)  # Field name made lowercase.
    telefon = models.CharField(db_column='TELEFON', max_length=50, blank=True, null=True)  # Field name made lowercase.
    telefon_2 = models.CharField(db_column='TELEFON_2', max_length=50, blank=True, null=True)  # Field name made lowercase.
    fax = models.CharField(db_column='FAX', max_length=50, blank=True, null=True)  # Field name made lowercase.
    mobil = models.CharField(db_column='MOBIL', max_length=50, blank=True, null=True)  # Field name made lowercase.
    email = models.CharField(db_column='EMAIL', max_length=100, blank=True, null=True)  # Field name made lowercase.
    bank = models.CharField(db_column='BANK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bank_plz = models.CharField(db_column='BANK_PLZ', max_length=50, blank=True, null=True)  # Field name made lowercase.
    bank_ort = models.CharField(db_column='BANK_ORT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bank_land = models.CharField(db_column='BANK_LAND', max_length=255, blank=True, null=True)  # Field name made lowercase.
    kto_nr = models.CharField(db_column='KTO_NR', max_length=50, blank=True, null=True)  # Field name made lowercase.
    blz = models.CharField(db_column='BLZ', max_length=50, blank=True, null=True)  # Field name made lowercase.
    kto_inhaber = models.CharField(db_column='KTO_INHABER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS')  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    kennwort = models.CharField(db_column='KENNWORT', max_length=50, blank=True, null=True)  # Field name made lowercase.
    letzte_kennwoerter = models.CharField(db_column='LETZTE_KENNWOERTER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    gueltig_ab = models.DateField(db_column='GUELTIG_AB', blank=True, null=True)  # Field name made lowercase.
    gueltig_bis = models.DateField(db_column='GUELTIG_BIS', blank=True, null=True)  # Field name made lowercase.
    organisation = models.CharField(db_column='ORGANISATION', max_length=255, blank=True, null=True)  # Field name made lowercase.
    mandant = models.CharField(db_column='MANDANT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    gruppe = models.CharField(db_column='GRUPPE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    einstellung = models.TextField(db_column='EINSTELLUNG', blank=True, null=True)  # Field name made lowercase.
    sprache = models.CharField(db_column='SPRACHE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    werkzeugleiste = models.TextField(db_column='WERKZEUGLEISTE', blank=True, null=True)  # Field name made lowercase.
    arztnummer = models.CharField(db_column='ARZTNUMMER', max_length=25, blank=True, null=True)  # Field name made lowercase.
    unterschrift_bildpfad = models.CharField(db_column='UNTERSCHRIFT_BILDPFAD', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'BENUTZER'


class Berechtigung(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    role = models.IntegerField(db_column='ROLE')  # Field name made lowercase.
    benutzer_id = models.IntegerField(db_column='BENUTZER_ID')  # Field name made lowercase.
    ermaechtigung_id = models.IntegerField(db_column='ERMAECHTIGUNG_ID')  # Field name made lowercase.
    organisation_id = models.IntegerField(db_column='ORGANISATION_ID')  # Field name made lowercase.
    aktion = models.IntegerField(db_column='AKTION')  # Field name made lowercase.
    berechtigung = models.IntegerField(db_column='BERECHTIGUNG')  # Field name made lowercase.
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'BERECHTIGUNG'


class BewilligungslisteFaxBarcodeZaehler(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    fax_barcode = models.IntegerField(db_column='FAX_BARCODE', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'BEWILLIGUNGSLISTE_FAX_BARCODE_ZAEHLER'


class Bewilligungsstellen(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    kuerzel = models.CharField(db_column='KUERZEL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    faxnummer = models.CharField(db_column='FAXNUMMER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    formulare_id = models.IntegerField(db_column='FORMULARE_ID', blank=True, null=True)  # Field name made lowercase.
    kostentraeger_positionsnummern_ermitteln = models.IntegerField(db_column='KOSTENTRAEGER_POSITIONSNUMMERN_ERMITTELN')  # Field name made lowercase.
    uhrzeit_erstes_fax = models.DateTimeField(db_column='UHRZEIT_ERSTES_FAX', blank=True, null=True)  # Field name made lowercase.
    uhrzeit_letztes_fax = models.DateTimeField(db_column='UHRZEIT_LETZTES_FAX', blank=True, null=True)  # Field name made lowercase.
    wiederholung_intervall = models.IntegerField(db_column='WIEDERHOLUNG_INTERVALL', blank=True, null=True)  # Field name made lowercase.
    auto_vor_erster_leistung = models.IntegerField(db_column='AUTO_VOR_ERSTER_LEISTUNG')  # Field name made lowercase.
    auto_bundesland_nicht = models.IntegerField(db_column='AUTO_BUNDESLAND_NICHT')  # Field name made lowercase.
    auto_min_behandlungen_kalenderjahr = models.IntegerField(db_column='AUTO_MIN_BEHANDLUNGEN_KALENDERJAHR')  # Field name made lowercase.
    auto_einzelleistungen_pro_behandlung_kalenderjahr = models.IntegerField(db_column='AUTO_EINZELLEISTUNGEN_PRO_BEHANDLUNG_KALENDERJAHR')  # Field name made lowercase.
    automatisch_faxen = models.IntegerField(db_column='AUTOMATISCH_FAXEN')  # Field name made lowercase.
    sammelsicht = models.IntegerField(db_column='SAMMELSICHT')  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS')  # Field name made lowercase.
    benutzer_id = models.IntegerField(db_column='BENUTZER_ID', blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'BEWILLIGUNGSSTELLEN'


class BlzAut(models.Model):
    lfd_nr = models.AutoField(db_column='Lfd_Nr', primary_key=True)  # Field name made lowercase.
    blz = models.FloatField(db_column='BLZ', blank=True, null=True)  # Field name made lowercase.
    eigen_blz = models.CharField(db_column='EIGEN_BLZ', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bisherige_blz = models.CharField(db_column='BISHERIGE_BLZ', max_length=255, blank=True, null=True)  # Field name made lowercase.
    lzb_girokonto = models.CharField(db_column='LZB_GIROKONTO', max_length=255, blank=True, null=True)  # Field name made lowercase.
    loeschungsdatum = models.CharField(db_column='LOESCHUNGSDATUM', max_length=255, blank=True, null=True)  # Field name made lowercase.
    hinweis = models.CharField(db_column='HINWEIS', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    kurzbezeichnung = models.CharField(db_column='KURZBEZEICHNUNG', max_length=50, blank=True, null=True)  # Field name made lowercase.
    strasse = models.CharField(db_column='STRASSE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    plz = models.CharField(db_column='PLZ', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ort = models.CharField(db_column='ORT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bundesland = models.CharField(db_column='BUNDESLAND', max_length=255, blank=True, null=True)  # Field name made lowercase.
    telefon = models.CharField(db_column='TELEFON', max_length=255, blank=True, null=True)  # Field name made lowercase.
    fax = models.CharField(db_column='FAX', max_length=255, blank=True, null=True)  # Field name made lowercase.
    btx_bezeichnung = models.CharField(db_column='BTX_BEZEICHNUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    pan = models.CharField(db_column='PAN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    veroef = models.CharField(db_column='VEROEF', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bic = models.CharField(db_column='BIC', max_length=255, blank=True, null=True)  # Field name made lowercase.
    pruefziffer = models.CharField(db_column='PRUEFZIFFER', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'BLZ_AUT'


class BlzDe(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    blz = models.IntegerField(db_column='BLZ', blank=True, null=True)  # Field name made lowercase.
    merkmal = models.IntegerField(db_column='MERKMAL', blank=True, null=True)  # Field name made lowercase.
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    plz = models.CharField(db_column='PLZ', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ort = models.CharField(db_column='ORT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    kurzbezeichnung = models.CharField(db_column='KURZBEZEICHNUNG', max_length=50, blank=True, null=True)  # Field name made lowercase.
    pan = models.CharField(db_column='PAN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bic = models.CharField(db_column='BIC', max_length=255, blank=True, null=True)  # Field name made lowercase.
    pruefziffer = models.CharField(db_column='PRUEFZIFFER', max_length=10, blank=True, null=True)  # Field name made lowercase.
    datensatz_id = models.CharField(db_column='DATENSATZ_ID', max_length=20, blank=True, null=True)  # Field name made lowercase.
    aenderung_kz = models.CharField(db_column='AENDERUNG_KZ', max_length=10, blank=True, null=True)  # Field name made lowercase.
    loeschung = models.CharField(db_column='LOESCHUNG', max_length=50, blank=True, null=True)  # Field name made lowercase.
    nachfolge_blz = models.CharField(db_column='NACHFOLGE_BLZ', max_length=10, blank=True, null=True)  # Field name made lowercase.
    hinweis = models.CharField(db_column='HINWEIS', max_length=255, blank=True, null=True)  # Field name made lowercase.
    telefon = models.CharField(db_column='TELEFON', max_length=255, blank=True, null=True)  # Field name made lowercase.
    fax = models.CharField(db_column='FAX', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'BLZ_DE'


class Combo(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    name = models.CharField(db_column='NAME', max_length=50, blank=True, null=True)  # Field name made lowercase.
    text_kurz_de = models.CharField(db_column='TEXT_KURZ_DE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    text_kurz_en = models.CharField(db_column='TEXT_KURZ_EN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    text_kurz_fr = models.CharField(db_column='TEXT_KURZ_FR', max_length=255, blank=True, null=True)  # Field name made lowercase.
    text_kurz_sp = models.CharField(db_column='TEXT_KURZ_SP', max_length=255, blank=True, null=True)  # Field name made lowercase.
    text_de = models.CharField(db_column='TEXT_DE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    text_en = models.CharField(db_column='TEXT_EN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    text_fr = models.CharField(db_column='TEXT_FR', max_length=255, blank=True, null=True)  # Field name made lowercase.
    text_sp = models.CharField(db_column='TEXT_SP', max_length=255, blank=True, null=True)  # Field name made lowercase.
    kennung = models.IntegerField(db_column='KENNUNG', blank=True, null=True)  # Field name made lowercase.
    zusatz_1 = models.TextField(db_column='ZUSATZ_1', blank=True, null=True)  # Field name made lowercase.
    zusatz_2 = models.TextField(db_column='ZUSATZ_2', blank=True, null=True)  # Field name made lowercase.
    zusatz_3 = models.CharField(db_column='ZUSATZ_3', max_length=50, blank=True, null=True)  # Field name made lowercase.
    zusatz_4 = models.CharField(db_column='ZUSATZ_4', max_length=50, blank=True, null=True)  # Field name made lowercase.
    kennung_s = models.CharField(db_column='KENNUNG_S', max_length=255, blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'COMBO'
        unique_together = (('name', 'kennung_s'), ('name', 'kennung_s'), ('name', 'kennung'),)


class Dermatest(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    patient_id = models.IntegerField(db_column='PATIENT_ID', blank=True, null=True)  # Field name made lowercase.
    untersuchung_id = models.IntegerField(db_column='UNTERSUCHUNG_ID', blank=True, null=True)  # Field name made lowercase.
    typ = models.CharField(db_column='TYP', max_length=50, blank=True, null=True)  # Field name made lowercase.
    datum = models.DateField(db_column='DATUM', blank=True, null=True)  # Field name made lowercase.
    test = models.CharField(db_column='TEST', max_length=50, blank=True, null=True)  # Field name made lowercase.
    testergebnis = models.IntegerField(db_column='TESTERGEBNIS', blank=True, null=True)  # Field name made lowercase.
    bemerkung = models.TextField(db_column='BEMERKUNG', blank=True, null=True)  # Field name made lowercase.
    freitext1 = models.TextField(db_column='FREITEXT1', blank=True, null=True)  # Field name made lowercase.
    freitext2 = models.TextField(db_column='FREITEXT2', blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)  # Field name made lowercase.
    system_datum = models.DateField(db_column='SYSTEM_DATUM', blank=True, null=True)  # Field name made lowercase.
    system_uhrzeit = models.TimeField(db_column='SYSTEM_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'DERMATEST'


class Diagnose(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    patient_id = models.IntegerField(db_column='PATIENT_ID')  # Field name made lowercase.
    diagnosedoku_id = models.IntegerField(db_column='DIAGNOSEDOKU_ID')  # Field name made lowercase.
    untersuchung_id = models.IntegerField(db_column='UNTERSUCHUNG_ID')  # Field name made lowercase.
    typ = models.CharField(db_column='TYP', max_length=10)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS')  # Field name made lowercase.
    datum = models.DateField(db_column='DATUM', blank=True, null=True)  # Field name made lowercase.
    uhrzeit = models.TimeField(db_column='UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    schluessel = models.CharField(db_column='SCHLUESSEL', max_length=50, blank=True, null=True)  # Field name made lowercase.
    diagnose_text = models.CharField(db_column='DIAGNOSE_TEXT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    diagnose_datum = models.DateField(db_column='DIAGNOSE_DATUM', blank=True, null=True)  # Field name made lowercase.
    diagnose_uhrzeit = models.TimeField(db_column='DIAGNOSE_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    notizen = models.TextField(db_column='NOTIZEN', blank=True, null=True)  # Field name made lowercase.
    kuerzel = models.CharField(db_column='KUERZEL', max_length=50, blank=True, null=True)  # Field name made lowercase.
    zusatz = models.TextField(db_column='ZUSATZ', blank=True, null=True)  # Field name made lowercase.
    sicherheit = models.CharField(db_column='SICHERHEIT', max_length=10, blank=True, null=True)  # Field name made lowercase.
    lokalisation = models.CharField(db_column='LOKALISATION', max_length=10, blank=True, null=True)  # Field name made lowercase.
    erlauterung = models.CharField(db_column='ERLAUTERUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    schluessel_typ = models.IntegerField(db_column='SCHLUESSEL_TYP')  # Field name made lowercase.
    sort = models.IntegerField(db_column='SORT')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'DIAGNOSE'


class Dokumentation(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    typ = models.CharField(db_column='TYP', max_length=50, blank=True, null=True)  # Field name made lowercase.
    fremd_id = models.IntegerField(db_column='FREMD_ID')  # Field name made lowercase.
    patient_id = models.IntegerField(db_column='PATIENT_ID')  # Field name made lowercase.
    fall_nr = models.IntegerField(db_column='FALL_NR')  # Field name made lowercase.
    untersuchung_nr = models.IntegerField(db_column='UNTERSUCHUNG_NR')  # Field name made lowercase.
    orgeinheit_id = models.IntegerField(db_column='ORGEINHEIT_ID')  # Field name made lowercase.
    ermaechtigung_id = models.IntegerField(db_column='ERMAECHTIGUNG_ID')  # Field name made lowercase.
    kschein_id = models.IntegerField(db_column='KSCHEIN_ID')  # Field name made lowercase.
    datum = models.DateField(db_column='DATUM', blank=True, null=True)  # Field name made lowercase.
    doku_text = models.CharField(db_column='DOKU_TEXT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    notizen = models.TextField(db_column='NOTIZEN', blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS')  # Field name made lowercase.
    kennwort = models.CharField(db_column='KENNWORT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    fall_zaehler = models.CharField(db_column='FALL_ZAEHLER', unique=True, max_length=50, blank=True, null=True)  # Field name made lowercase.
    unters_zaehler = models.CharField(db_column='UNTERS_ZAEHLER', unique=True, max_length=50, blank=True, null=True)  # Field name made lowercase.
    freitext1 = models.TextField(db_column='FREITEXT1', blank=True, null=True)  # Field name made lowercase.
    freitext2 = models.TextField(db_column='FREITEXT2', blank=True, null=True)  # Field name made lowercase.
    freitext3 = models.TextField(db_column='FREITEXT3', blank=True, null=True)  # Field name made lowercase.
    freitext4 = models.TextField(db_column='FREITEXT4', blank=True, null=True)  # Field name made lowercase.
    doku_properties = models.TextField(db_column='DOKU_PROPERTIES', blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    arzt1 = models.CharField(db_column='ARZT1', max_length=50, blank=True, null=True)  # Field name made lowercase.
    arzt2 = models.CharField(db_column='ARZT2', max_length=50, blank=True, null=True)  # Field name made lowercase.
    subtyp = models.IntegerField(db_column='SUBTYP')  # Field name made lowercase.
    letzte_aenderung_datum = models.DateField(db_column='LETZTE_AENDERUNG_DATUM', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung_uhrzeit = models.TimeField(db_column='LETZTE_AENDERUNG_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    abrechnung = models.CharField(db_column='ABRECHNUNG', max_length=50, blank=True, null=True)  # Field name made lowercase.
    doku_uhrzeit = models.TimeField(db_column='DOKU_UHRZEIT', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'DOKUMENTATION'


class DokuTyp(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    doku_typ = models.CharField(db_column='DOKU_TYP', max_length=50, blank=True, null=True)  # Field name made lowercase.
    doku_sortierung = models.IntegerField(db_column='DOKU_SORTIERUNG')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'DOKU_TYP'


class Druckereinstellung(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP')  # Field name made lowercase.
    referenz_id = models.IntegerField(db_column='REFERENZ_ID')  # Field name made lowercase.
    rechner_name = models.CharField(db_column='RECHNER_NAME', max_length=255)  # Field name made lowercase.
    druckereinstellung = models.TextField(db_column='DRUCKEREINSTELLUNG', blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=255)  # Field name made lowercase.
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG')  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'DRUCKEREINSTELLUNG'


class EhmvAtc(models.Model):
    atc = models.CharField(db_column='ATC', primary_key=True, max_length=7)  # Field name made lowercase.
    bedeutung = models.CharField(db_column='Bedeutung', max_length=200, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'EHMV_ATC'


class EhmvHinweis(models.Model):
    pharmanummer = models.IntegerField(db_column='Pharmanummer', primary_key=True)  # Field name made lowercase.
    hinweise = models.TextField(db_column='Hinweise', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'EHMV_HINWEIS'


class EhmvIndtext(models.Model):
    pharmanummer = models.IntegerField(db_column='Pharmanummer', primary_key=True)  # Field name made lowercase.
    kriterien = models.TextField(db_column='Kriterien', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'EHMV_INDTEXT'


class EhmvMedikament(models.Model):
    pharmanummer = models.IntegerField(db_column='Pharmanummer')  # Field name made lowercase.
    regnrpre = models.CharField(db_column='RegNrPre', max_length=2, blank=True, null=True)  # Field name made lowercase.
    regnr = models.CharField(db_column='RegNr', max_length=6, blank=True, null=True)  # Field name made lowercase.
    regnreu = models.CharField(db_column='RegNrEU', max_length=3, blank=True, null=True)  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=100, blank=True, null=True)  # Field name made lowercase.
    box = models.CharField(db_column='Box', max_length=3, blank=True, null=True)  # Field name made lowercase.
    kassenzeichen = models.CharField(db_column='Kassenzeichen', max_length=3, blank=True, null=True)  # Field name made lowercase.
    menge = models.CharField(db_column='Menge', max_length=6, blank=True, null=True)  # Field name made lowercase.
    mengenart = models.CharField(db_column='Mengenart', max_length=2, blank=True, null=True)  # Field name made lowercase.
    kvp = models.CharField(db_column='KVP', max_length=9, blank=True, null=True)  # Field name made lowercase.
    kvp_einheit = models.CharField(db_column='KVP_Einheit', max_length=9, blank=True, null=True)  # Field name made lowercase.
    darreichung = models.CharField(db_column='Darreichung', max_length=9, blank=True, null=True)  # Field name made lowercase.
    teilbarkeit = models.CharField(db_column='Teilbarkeit', max_length=3, blank=True, null=True)  # Field name made lowercase.
    refaktie = models.CharField(db_column='Refaktie', max_length=3, blank=True, null=True)  # Field name made lowercase.
    abgabeanzahl = models.IntegerField(db_column='Abgabeanzahl', blank=True, null=True)  # Field name made lowercase.
    hinweis = models.CharField(db_column='Hinweis', max_length=28, blank=True, null=True)  # Field name made lowercase.
    langzeitbewilligung = models.CharField(db_column='Langzeitbewilligung', max_length=5, blank=True, null=True)  # Field name made lowercase.
    suchtgift = models.CharField(db_column='Suchtgift', max_length=5, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'EHMV_MEDIKAMENT'


class EhmvPharma(models.Model):
    pharmanummer = models.IntegerField(db_column='Pharmanummer', blank=True, null=True)  # Field name made lowercase.
    position = models.IntegerField(db_column='Position', blank=True, null=True)  # Field name made lowercase.
    pharmanummervgl = models.IntegerField(db_column='PharmanummerVgl', blank=True, null=True)  # Field name made lowercase.
    positionvgl = models.IntegerField(db_column='PositionVgl', blank=True, null=True)  # Field name made lowercase.
    kennzeichenvgl = models.IntegerField(db_column='KennzeichenVgl', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'EHMV_PHARMA'


class EhmvRegeltexte(models.Model):
    pharmanummer = models.IntegerField(db_column='Pharmanummer', primary_key=True)  # Field name made lowercase.
    hinweis = models.TextField(db_column='Hinweis', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'EHMV_REGELTEXTE'


class EhmvWirkstoff(models.Model):
    pharmanummer = models.IntegerField(db_column='Pharmanummer')  # Field name made lowercase.
    atc_who = models.CharField(db_column='ATC_WHO', max_length=7, blank=True, null=True)  # Field name made lowercase.
    laufnummer = models.IntegerField(db_column='Laufnummer', blank=True, null=True)  # Field name made lowercase.
    atc_erg = models.CharField(db_column='ATC_ERG', max_length=7, blank=True, null=True)  # Field name made lowercase.
    staerke = models.FloatField(db_column='Staerke', blank=True, null=True)  # Field name made lowercase.
    dimension = models.CharField(db_column='Dimension', max_length=9, blank=True, null=True)  # Field name made lowercase.
    eigenschaft = models.CharField(db_column='Eigenschaft', max_length=20, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'EHMV_WIRKSTOFF'


class Email(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    emaildoku_id = models.IntegerField(db_column='EMAILDOKU_ID', unique=True, blank=True, null=True)  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)  # Field name made lowercase.
    zeit = models.DateTimeField(db_column='ZEIT', blank=True, null=True)  # Field name made lowercase.
    content_type = models.CharField(db_column='CONTENT_TYPE', max_length=255)  # Field name made lowercase.
    mime_version = models.CharField(db_column='MIME_VERSION', max_length=255, blank=True, null=True)  # Field name made lowercase.
    absender = models.CharField(db_column='ABSENDER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    empfaenger = models.CharField(db_column='EMPFAENGER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    message_id = models.CharField(db_column='MESSAGE_ID', max_length=255, blank=True, null=True)  # Field name made lowercase.
    betreff = models.TextField(db_column='BETREFF', blank=True, null=True)  # Field name made lowercase.
    nachricht_text = models.TextField(db_column='NACHRICHT_TEXT', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'EMAIL'


class ErinnerungTermin(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    mandant_id = models.IntegerField(db_column='MANDANT_ID', blank=True, null=True)  # Field name made lowercase.
    patient_id = models.IntegerField(db_column='PATIENT_ID', blank=True, null=True)  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)  # Field name made lowercase.
    datum = models.DateField(db_column='DATUM', blank=True, null=True)  # Field name made lowercase.
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bemerkung = models.TextField(db_column='BEMERKUNG', blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)  # Field name made lowercase.
    system_zeit = models.DateTimeField(db_column='SYSTEM_ZEIT', blank=True, null=True)  # Field name made lowercase.
    anlage_zeit = models.DateTimeField(db_column='ANLAGE_ZEIT', blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    erinnerung_kennung = models.IntegerField(db_column='ERINNERUNG_KENNUNG', blank=True, null=True)  # Field name made lowercase.
    referenzdoku_id = models.IntegerField(db_column='REFERENZDOKU_ID', blank=True, null=True)  # Field name made lowercase.
    will_email_erinnerung = models.IntegerField(db_column='WILL_EMAIL_ERINNERUNG', blank=True, null=True)  # Field name made lowercase.
    will_sms_erinnerung = models.IntegerField(db_column='WILL_SMS_ERINNERUNG', blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ERINNERUNG_TERMIN'


class Fachbefund(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    patient_id = models.IntegerField(db_column='PATIENT_ID')  # Field name made lowercase.
    doku_id = models.IntegerField(db_column='DOKU_ID')  # Field name made lowercase.
    untersuchung_id = models.IntegerField(db_column='UNTERSUCHUNG_ID')  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)  # Field name made lowercase.
    anschrift = models.TextField(db_column='ANSCHRIFT', blank=True, null=True)  # Field name made lowercase.
    email = models.CharField(db_column='EMAIL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    begruessung = models.TextField(db_column='BEGRUESSUNG', blank=True, null=True)  # Field name made lowercase.
    einleitung = models.TextField(db_column='EINLEITUNG', blank=True, null=True)  # Field name made lowercase.
    anamnese = models.TextField(db_column='ANAMNESE', blank=True, null=True)  # Field name made lowercase.
    text_1 = models.TextField(db_column='TEXT_1', blank=True, null=True)  # Field name made lowercase.
    text_2 = models.TextField(db_column='TEXT_2', blank=True, null=True)  # Field name made lowercase.
    text_3 = models.TextField(db_column='TEXT_3', blank=True, null=True)  # Field name made lowercase.
    text_4 = models.TextField(db_column='TEXT_4', blank=True, null=True)  # Field name made lowercase.
    text_5 = models.TextField(db_column='TEXT_5', blank=True, null=True)  # Field name made lowercase.
    text_6 = models.TextField(db_column='TEXT_6', blank=True, null=True)  # Field name made lowercase.
    text_7 = models.TextField(db_column='TEXT_7', blank=True, null=True)  # Field name made lowercase.
    text_8 = models.TextField(db_column='TEXT_8', blank=True, null=True)  # Field name made lowercase.
    text_9 = models.TextField(db_column='TEXT_9', blank=True, null=True)  # Field name made lowercase.
    text_10 = models.TextField(db_column='TEXT_10', blank=True, null=True)  # Field name made lowercase.
    text_11 = models.TextField(db_column='TEXT_11', blank=True, null=True)  # Field name made lowercase.
    text_12 = models.TextField(db_column='TEXT_12', blank=True, null=True)  # Field name made lowercase.
    text_13 = models.TextField(db_column='TEXT_13', blank=True, null=True)  # Field name made lowercase.
    text_14 = models.TextField(db_column='TEXT_14', blank=True, null=True)  # Field name made lowercase.
    text_15 = models.TextField(db_column='TEXT_15', blank=True, null=True)  # Field name made lowercase.
    text_16 = models.TextField(db_column='TEXT_16', blank=True, null=True)  # Field name made lowercase.
    schlussformel = models.TextField(db_column='SCHLUSSFORMEL', blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    notizen = models.TextField(db_column='NOTIZEN', blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung_datum = models.DateField(db_column='LETZTE_AENDERUNG_DATUM', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung_uhrzeit = models.TimeField(db_column='LETZTE_AENDERUNG_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    zusatz = models.TextField(db_column='ZUSATZ', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'FACHBEFUND'


class Fallnummer(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    fallnummer = models.CharField(db_column='FALLNUMMER', max_length=50)  # Field name made lowercase.
    patient_id = models.IntegerField(db_column='PATIENT_ID')  # Field name made lowercase.
    aufnahme_zeit = models.DateTimeField(db_column='AUFNAHME_ZEIT', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'FALLNUMMER'
        unique_together = (('fallnummer', 'patient_id'),)


class Firma(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    name = models.CharField(db_column='NAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    abteilung = models.CharField(db_column='ABTEILUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    kurzname = models.CharField(db_column='KURZNAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    kuerzel = models.CharField(db_column='KUERZEL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    adresse = models.IntegerField(db_column='ADRESSE')  # Field name made lowercase.
    post_adresse = models.IntegerField(db_column='POST_ADRESSE')  # Field name made lowercase.
    liefer_adresse = models.IntegerField(db_column='LIEFER_ADRESSE')  # Field name made lowercase.
    rechnung_adresse = models.IntegerField(db_column='RECHNUNG_ADRESSE')  # Field name made lowercase.
    kommunikation = models.IntegerField(db_column='KOMMUNIKATION')  # Field name made lowercase.
    konto = models.IntegerField(db_column='KONTO')  # Field name made lowercase.
    ansprechpartner = models.IntegerField(db_column='ANSPRECHPARTNER')  # Field name made lowercase.
    notizen = models.TextField(db_column='NOTIZEN', blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'FIRMA'


class Formulare(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    aceto_nr = models.IntegerField(db_column='ACETO_NR', unique=True, blank=True, null=True)  # Field name made lowercase.
    position = models.IntegerField(db_column='POSITION', blank=True, null=True)  # Field name made lowercase.
    name = models.CharField(db_column='NAME', max_length=100, blank=True, null=True)  # Field name made lowercase.
    beschreibung = models.CharField(db_column='BESCHREIBUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    report = models.CharField(db_column='REPORT', max_length=50, blank=True, null=True)  # Field name made lowercase.
    farbe = models.CharField(db_column='FARBE', max_length=50, blank=True, null=True)  # Field name made lowercase.
    logo = models.CharField(db_column='LOGO', max_length=50, blank=True, null=True)  # Field name made lowercase.
    einstellungen = models.TextField(db_column='EINSTELLUNGEN', blank=True, null=True)  # Field name made lowercase.
    felder = models.TextField(db_column='FELDER', blank=True, null=True)  # Field name made lowercase.
    wahlarzt = models.CharField(db_column='WAHLARZT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    kasse = models.CharField(db_column='KASSE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    mandanten = models.CharField(db_column='MANDANTEN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)  # Field name made lowercase.
    schnell_knopf = models.IntegerField(db_column='SCHNELL_KNOPF', unique=True, blank=True, null=True)  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)  # Field name made lowercase.
    version = models.FloatField(db_column='VERSION', blank=True, null=True)  # Field name made lowercase.
    pdftitel = models.CharField(db_column='PDFTITEL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    pdfdokumentformat = models.CharField(db_column='PDFDOKUMENTFORMAT', max_length=50, blank=True, null=True)  # Field name made lowercase.
    pdfcssexpert = models.TextField(db_column='PDFCSSEXPERT', blank=True, null=True)  # Field name made lowercase.
    htmldokument = models.TextField(db_column='HTMLDOKUMENT', blank=True, null=True)  # Field name made lowercase.
    logo_seite2 = models.CharField(db_column='LOGO_SEITE2', max_length=50, blank=True, null=True)  # Field name made lowercase.
    notizen = models.TextField(db_column='NOTIZEN', blank=True, null=True)  # Field name made lowercase.
    druck_css = models.CharField(db_column='DRUCK_CSS', max_length=50, blank=True, null=True)  # Field name made lowercase.
    email_css = models.CharField(db_column='EMAIL_CSS', max_length=50, blank=True, null=True)  # Field name made lowercase.
    fax_css = models.CharField(db_column='FAX_CSS', max_length=50, blank=True, null=True)  # Field name made lowercase.
    serienbrief_typen = models.TextField(db_column='SERIENBRIEF_TYPEN', blank=True, null=True)  # Field name made lowercase.
    serienbrief_adressat_typ = models.CharField(db_column='SERIENBRIEF_ADRESSAT_TYP', max_length=50, blank=True, null=True)  # Field name made lowercase.
    vertragsart = models.CharField(db_column='VERTRAGSART', max_length=255, blank=True, null=True)  # Field name made lowercase.
    sprache = models.CharField(db_column='SPRACHE', max_length=10, blank=True, null=True)  # Field name made lowercase.
    gruppe = models.IntegerField(db_column='GRUPPE', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'FORMULARE'


class FtpArchiv(models.Model):
    ftp_archiv_id = models.AutoField(db_column='FTP_ARCHIV_ID', primary_key=True)  # Field name made lowercase.
    datei_name = models.CharField(db_column='DATEI_NAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    pfad = models.CharField(db_column='PFAD', max_length=255, blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    zugriff_von = models.CharField(db_column='ZUGRIFF_VON', max_length=255, blank=True, null=True)  # Field name made lowercase.
    datum_zugriff = models.DateField(db_column='DATUM_ZUGRIFF', blank=True, null=True)  # Field name made lowercase.
    uhrzeit_zugriff = models.TimeField(db_column='UHRZEIT_ZUGRIFF', blank=True, null=True)  # Field name made lowercase.
    zugriff_computername = models.CharField(db_column='ZUGRIFF_COMPUTERNAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    aktion = models.IntegerField(db_column='AKTION', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'FTP_ARCHIV'


class Geraete(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    gt_typ = models.CharField(db_column='GT_TYP', max_length=50, blank=True, null=True)  # Field name made lowercase.
    gr_kuerzel = models.CharField(db_column='GR_KUERZEL', unique=True, max_length=255, blank=True, null=True)  # Field name made lowercase.
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    gr_beschreibung = models.TextField(db_column='GR_BESCHREIBUNG', blank=True, null=True)  # Field name made lowercase.
    gruppe = models.IntegerField(db_column='GRUPPE', blank=True, null=True)  # Field name made lowercase.
    ermaechtigungen = models.CharField(db_column='ERMAECHTIGUNGEN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)  # Field name made lowercase.
    gr_timestart = models.DateTimeField(db_column='GR_TIMESTART')  # Field name made lowercase.
    gr_rechenraster = models.IntegerField(db_column='GR_RECHENRASTER')  # Field name made lowercase.
    gr_gm = models.IntegerField(db_column='GR_GM')  # Field name made lowercase.
    modalitaet = models.IntegerField(db_column='MODALITAET', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'GERAETE'


class GeraeteCode(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    geraet_id = models.IntegerField(db_column='GERAET_ID', blank=True, null=True)  # Field name made lowercase.
    procedure_code = models.CharField(db_column='PROCEDURE_CODE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    beschreibung = models.TextField(db_column='BESCHREIBUNG', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'GERAETE_CODE'
        unique_together = (('geraet_id', 'procedure_code'),)


class GeraeteTypen(models.Model):
    gt_typ = models.CharField(db_column='GT_TYP', primary_key=True, max_length=50)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'GERAETE_TYPEN'


class Grafik(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    patient_id = models.IntegerField(db_column='PATIENT_ID', blank=True, null=True)  # Field name made lowercase.
    typ = models.CharField(db_column='TYP', max_length=50, blank=True, null=True)  # Field name made lowercase.
    datum = models.DateField(db_column='DATUM', blank=True, null=True)  # Field name made lowercase.
    bild = models.CharField(db_column='BILD', max_length=50, blank=True, null=True)  # Field name made lowercase.
    position = models.CharField(db_column='POSITION', max_length=50, blank=True, null=True)  # Field name made lowercase.
    markierung_text = models.TextField(db_column='MARKIERUNG_TEXT', blank=True, null=True)  # Field name made lowercase.
    freitext1 = models.TextField(db_column='FREITEXT1', blank=True, null=True)  # Field name made lowercase.
    freitext2 = models.TextField(db_column='FREITEXT2', blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)  # Field name made lowercase.
    system_datum = models.DateField(db_column='SYSTEM_DATUM', blank=True, null=True)  # Field name made lowercase.
    system_uhrzeit = models.TimeField(db_column='SYSTEM_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'GRAFIK'


class Gruppen(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    typ = models.CharField(db_column='TYP', max_length=11, blank=True, null=True)  # Field name made lowercase.
    name = models.CharField(db_column='NAME', max_length=50, blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)  # Field name made lowercase.
    kennung = models.IntegerField(db_column='KENNUNG', blank=True, null=True)  # Field name made lowercase.
    beschreibung = models.CharField(db_column='BESCHREIBUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'GRUPPEN'


class HeilmittelStamm(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    typ = models.CharField(db_column='TYP', max_length=50, blank=True, null=True)  # Field name made lowercase.
    position = models.IntegerField(db_column='POSITION', blank=True, null=True)  # Field name made lowercase.
    kuerzel = models.CharField(db_column='KUERZEL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    gruppentherapie = models.IntegerField(db_column='GRUPPENTHERAPIE', blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    wahlarzt = models.CharField(db_column='WAHLARZT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    vertragsart = models.CharField(db_column='VERTRAGSART', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bg = models.CharField(db_column='BG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    kasse = models.CharField(db_column='KASSE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    mandanten = models.CharField(db_column='MANDANTEN', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'HEILMITTEL_STAMM'
        unique_together = (('kuerzel', 'typ'),)


class HilfsmittelStamm(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    position = models.IntegerField(db_column='POSITION')  # Field name made lowercase.
    typ = models.CharField(db_column='TYP', max_length=50, blank=True, null=True)  # Field name made lowercase.
    kuerzel = models.CharField(db_column='KUERZEL', unique=True, max_length=255)  # Field name made lowercase.
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    rezeptur = models.TextField(db_column='REZEPTUR', blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung_datum = models.DateField(db_column='LETZTE_AENDERUNG_DATUM', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung_uhrzeit = models.TimeField(db_column='LETZTE_AENDERUNG_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    dosisschema = models.TextField(db_column='DOSISSCHEMA', blank=True, null=True)  # Field name made lowercase.
    mandanten = models.CharField(db_column='MANDANTEN', max_length=255)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    kasse = models.CharField(db_column='KASSE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    privat = models.CharField(db_column='PRIVAT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    einstellung = models.TextField(db_column='EINSTELLUNG', blank=True, null=True)  # Field name made lowercase.
    medikament_box = models.CharField(db_column='MEDIKAMENT_BOX', max_length=5, blank=True, null=True)  # Field name made lowercase.
    vertragsart = models.CharField(db_column='VERTRAGSART', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'HILFSMITTEL_STAMM'


class Historie(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    doku_typ = models.CharField(db_column='DOKU_TYP', max_length=255, blank=True, null=True)  # Field name made lowercase.
    doku_id = models.IntegerField(db_column='DOKU_ID', blank=True, null=True)  # Field name made lowercase.
    zeit = models.DateTimeField(db_column='ZEIT', blank=True, null=True)  # Field name made lowercase.
    benutzer = models.CharField(db_column='BENUTZER', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'HISTORIE'


class HistoBefunde(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    patient_id = models.IntegerField(db_column='PATIENT_ID', blank=True, null=True)  # Field name made lowercase.
    untersuchung_id = models.IntegerField(db_column='UNTERSUCHUNG_ID', blank=True, null=True)  # Field name made lowercase.
    typ = models.CharField(db_column='TYP', max_length=50, blank=True, null=True)  # Field name made lowercase.
    datum = models.DateField(db_column='DATUM', blank=True, null=True)  # Field name made lowercase.
    abstrich_freitext = models.TextField(db_column='ABSTRICH_FREITEXT', blank=True, null=True)  # Field name made lowercase.
    befund_ergebnis = models.CharField(db_column='BEFUND_ERGEBNIS', max_length=50, blank=True, null=True)  # Field name made lowercase.
    befund_freitext = models.TextField(db_column='BEFUND_FREITEXT', blank=True, null=True)  # Field name made lowercase.
    bemerkung = models.TextField(db_column='BEMERKUNG', blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)  # Field name made lowercase.
    system_datum = models.DateField(db_column='SYSTEM_DATUM', blank=True, null=True)  # Field name made lowercase.
    system_uhrzeit = models.TimeField(db_column='SYSTEM_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    ermaechtigung_id = models.IntegerField(db_column='ERMAECHTIGUNG_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'HISTO_BEFUNDE'


class Hl7Auftragsdaten(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    typ = models.CharField(db_column='TYP', max_length=20, blank=True, null=True)  # Field name made lowercase.
    trigger_event = models.CharField(db_column='TRIGGER_EVENT', max_length=20, blank=True, null=True)  # Field name made lowercase.
    message_id = models.CharField(db_column='MESSAGE_ID', max_length=255, blank=True, null=True)  # Field name made lowercase.
    dateiname = models.CharField(db_column='DATEINAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    patient_id = models.IntegerField(db_column='PATIENT_ID', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)  # Field name made lowercase.
    message = models.TextField(db_column='MESSAGE', blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'HL7_AUFTRAGSDATEN'


class Hl7Leistungsdaten(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    typ = models.CharField(db_column='TYP', max_length=20, blank=True, null=True)  # Field name made lowercase.
    trigger_event = models.CharField(db_column='TRIGGER_EVENT', max_length=20, blank=True, null=True)  # Field name made lowercase.
    segment = models.CharField(db_column='SEGMENT', max_length=20, blank=True, null=True)  # Field name made lowercase.
    patient_id = models.IntegerField(db_column='PATIENT_ID')  # Field name made lowercase.
    datum = models.DateField(db_column='DATUM', blank=True, null=True)  # Field name made lowercase.
    producer_id = models.CharField(db_column='PRODUCER_ID', unique=True, max_length=255, blank=True, null=True)  # Field name made lowercase.
    ergebnis = models.CharField(db_column='ERGEBNIS', max_length=50, blank=True, null=True)  # Field name made lowercase.
    ergebnis_typ = models.CharField(db_column='ERGEBNIS_TYP', max_length=10, blank=True, null=True)  # Field name made lowercase.
    ergebnis_identifier = models.CharField(db_column='ERGEBNIS_IDENTIFIER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ergebnis_status = models.CharField(db_column='ERGEBNIS_STATUS', max_length=10, blank=True, null=True)  # Field name made lowercase.
    ergebnis_einheit = models.CharField(db_column='ERGEBNIS_EINHEIT', max_length=50, blank=True, null=True)  # Field name made lowercase.
    beschreibung = models.CharField(db_column='BESCHREIBUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS')  # Field name made lowercase.
    ermaechtigung_id = models.IntegerField(db_column='ERMAECHTIGUNG_ID')  # Field name made lowercase.
    dateiname = models.CharField(db_column='DATEINAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'HL7_LEISTUNGSDATEN'


class Hl7Sendeprotokoll(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    typ = models.CharField(db_column='TYP', max_length=20, blank=True, null=True)  # Field name made lowercase.
    trigger_event = models.CharField(db_column='TRIGGER_EVENT', max_length=20, blank=True, null=True)  # Field name made lowercase.
    dateiname = models.CharField(db_column='DATEINAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    patient_id = models.IntegerField(db_column='PATIENT_ID', blank=True, null=True)  # Field name made lowercase.
    doku_id = models.IntegerField(db_column='DOKU_ID', blank=True, null=True)  # Field name made lowercase.
    zeit = models.DateTimeField(db_column='ZEIT', blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    nachricht = models.TextField(db_column='NACHRICHT', blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'HL7_SENDEPROTOKOLL'


class IcdKapitel(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    hierarchie = models.IntegerField(db_column='HIERARCHIE')  # Field name made lowercase.
    nummer = models.CharField(db_column='NUMMER', max_length=20, blank=True, null=True)  # Field name made lowercase.
    von_icd = models.CharField(db_column='VON_ICD', max_length=20, blank=True, null=True)  # Field name made lowercase.
    bis_icd = models.CharField(db_column='BIS_ICD', max_length=20, blank=True, null=True)  # Field name made lowercase.
    bezeichnung = models.TextField(db_column='BEZEICHNUNG', blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ICD_KAPITEL'


class IcdKatalog(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    typ = models.CharField(db_column='TYP', max_length=10, blank=True, null=True)  # Field name made lowercase.
    icd_kode = models.CharField(db_column='ICD_KODE', max_length=6, blank=True, null=True)  # Field name made lowercase.
    icd_text = models.TextField(db_column='ICD_TEXT', blank=True, null=True)  # Field name made lowercase.
    icd_typ = models.IntegerField(db_column='ICD_TYP', blank=True, null=True)  # Field name made lowercase.
    notation = models.CharField(db_column='NOTATION', max_length=1, blank=True, null=True)  # Field name made lowercase.
    geschlechtsbezug = models.CharField(db_column='GESCHLECHTSBEZUG', max_length=2, blank=True, null=True)  # Field name made lowercase.
    alter_unten = models.CharField(db_column='ALTER_UNTEN', max_length=10, blank=True, null=True)  # Field name made lowercase.
    alter_oben = models.CharField(db_column='ALTER_OBEN', max_length=10, blank=True, null=True)  # Field name made lowercase.
    alter_fehler = models.CharField(db_column='ALTER_FEHLER', max_length=1, blank=True, null=True)  # Field name made lowercase.
    exotik = models.CharField(db_column='EXOTIK', max_length=1, blank=True, null=True)  # Field name made lowercase.
    kode_belegt = models.CharField(db_column='KODE_BELEGT', max_length=1, blank=True, null=True)  # Field name made lowercase.
    meldepflicht = models.CharField(db_column='MELDEPFLICHT', max_length=1, blank=True, null=True)  # Field name made lowercase.
    ebm_kombi = models.CharField(db_column='EBM_KOMBI', max_length=1, blank=True, null=True)  # Field name made lowercase.
    gruppe = models.CharField(db_column='GRUPPE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    kuerzel = models.CharField(db_column='KUERZEL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    notizen = models.CharField(db_column='NOTIZEN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    protokoll = models.CharField(db_column='PROTOKOLL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='STATUS', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ziffer_von = models.CharField(db_column='ZIFFER_VON', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ziffer_bis = models.CharField(db_column='ZIFFER_BIS', max_length=255, blank=True, null=True)  # Field name made lowercase.
    version = models.CharField(db_column='VERSION', max_length=255, blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)  # Field name made lowercase.
    geschlecht_fehler = models.CharField(db_column='GESCHLECHT_FEHLER', max_length=10)  # Field name made lowercase.
    akr_ref = models.CharField(db_column='AKR_REF', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ICD_KATALOG'


class IcdThesaurus(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    icd_kode = models.CharField(db_column='ICD_KODE', max_length=6, blank=True, null=True)  # Field name made lowercase.
    thesaurus = models.CharField(db_column='THESAURUS', max_length=255, blank=True, null=True)  # Field name made lowercase.
    hausarztkodierung = models.CharField(db_column='HAUSARZTKODIERUNG', max_length=5, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ICD_THESAURUS'


class Impfbuch(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    ermaechtigung_id = models.IntegerField(db_column='ERMAECHTIGUNG_ID')  # Field name made lowercase.
    patient_id = models.IntegerField(db_column='PATIENT_ID')  # Field name made lowercase.
    untersuchung_id = models.IntegerField(db_column='UNTERSUCHUNG_ID', blank=True, null=True)  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP')  # Field name made lowercase.
    subtyp = models.IntegerField(db_column='SUBTYP')  # Field name made lowercase.
    datum = models.DateField(db_column='DATUM', blank=True, null=True)  # Field name made lowercase.
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ergebnis = models.IntegerField(db_column='ERGEBNIS')  # Field name made lowercase.
    bemerkung = models.TextField(db_column='BEMERKUNG', blank=True, null=True)  # Field name made lowercase.
    freitext1 = models.TextField(db_column='FREITEXT1', blank=True, null=True)  # Field name made lowercase.
    freitext2 = models.TextField(db_column='FREITEXT2', blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS')  # Field name made lowercase.
    system_datum = models.DateField(db_column='SYSTEM_DATUM', blank=True, null=True)  # Field name made lowercase.
    system_uhrzeit = models.TimeField(db_column='SYSTEM_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    chargennr = models.CharField(db_column='CHARGENNR', max_length=255, blank=True, null=True)  # Field name made lowercase.
    impfart = models.IntegerField(db_column='IMPFART')  # Field name made lowercase.
    impf_kennung = models.IntegerField(db_column='IMPF_KENNUNG')  # Field name made lowercase.
    impftyp = models.IntegerField(db_column='IMPFTYP')  # Field name made lowercase.
    impfstoff = models.CharField(db_column='IMPFSTOFF', max_length=255, blank=True, null=True)  # Field name made lowercase.
    hersteller = models.CharField(db_column='HERSTELLER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    dosierung = models.CharField(db_column='DOSIERUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    dosierung2 = models.CharField(db_column='DOSIERUNG2', max_length=255, blank=True, null=True)  # Field name made lowercase.
    impf_dokuid = models.IntegerField(db_column='IMPF_DOKUID', blank=True, null=True)  # Field name made lowercase.
    impfer = models.CharField(db_column='IMPFER', max_length=100, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'IMPFBUCH'


class Import(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    altsystem_id = models.BigIntegerField(db_column='ALTSYSTEM_ID', blank=True, null=True)  # Field name made lowercase.
    text = models.TextField(db_column='TEXT', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'IMPORT'


class Import2(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    altsystem_id = models.BigIntegerField(db_column='ALTSYSTEM_ID', blank=True, null=True)  # Field name made lowercase.
    datum = models.DateField(db_column='DATUM', blank=True, null=True)  # Field name made lowercase.
    text = models.TextField(db_column='TEXT', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'IMPORT2'


class IvfEmbryokryo(models.Model):
    embryokryo_dokuid = models.IntegerField(db_column='EMBRYOKRYO_DOKUID', unique=True, blank=True, null=True)  # Field name made lowercase.
    kommentar = models.TextField(db_column='KOMMENTAR', blank=True, null=True)  # Field name made lowercase.
    labor = models.CharField(db_column='LABOR', max_length=255, blank=True, null=True)  # Field name made lowercase.
    externekryo = models.IntegerField(db_column='EXTERNEKRYO', blank=True, null=True)  # Field name made lowercase.
    invitro_zyklus_id = models.IntegerField(db_column='INVITRO_ZYKLUS_ID', blank=True, null=True)  # Field name made lowercase.
    embryonen_anzahl = models.IntegerField(db_column='EMBRYONEN_ANZAHL', blank=True, null=True)  # Field name made lowercase.
    embryonen_qualitaet = models.CharField(db_column='EMBRYONEN_QUALITAET', max_length=255, blank=True, null=True)  # Field name made lowercase.
    befund = models.CharField(db_column='BEFUND', max_length=255, blank=True, null=True)  # Field name made lowercase.
    befund_sonstiges = models.CharField(db_column='BEFUND_SONSTIGES', max_length=255, blank=True, null=True)  # Field name made lowercase.
    lager_typ = models.IntegerField(db_column='LAGER_TYP', blank=True, null=True)  # Field name made lowercase.
    lager_id = models.CharField(db_column='LAGER_ID', max_length=255, blank=True, null=True)  # Field name made lowercase.
    lager_code = models.CharField(db_column='LAGER_CODE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    lager_behaelter = models.IntegerField(db_column='LAGER_BEHAELTER', blank=True, null=True)  # Field name made lowercase.
    lager_position = models.CharField(db_column='LAGER_POSITION', max_length=255, blank=True, null=True)  # Field name made lowercase.
    tag = models.IntegerField(db_column='TAG', blank=True, null=True)  # Field name made lowercase.
    lager_anzahl = models.CharField(db_column='LAGER_ANZAHL', max_length=100, blank=True, null=True)  # Field name made lowercase.
    lager_typfarbe = models.CharField(db_column='LAGER_TYPFARBE', max_length=100, blank=True, null=True)  # Field name made lowercase.
    lager_positionfarbe = models.CharField(db_column='LAGER_POSITIONFARBE', max_length=100, blank=True, null=True)  # Field name made lowercase.
    methode_gefrieren_pbs_min = models.IntegerField(db_column='METHODE_GEFRIEREN_PBS_MIN', blank=True, null=True)  # Field name made lowercase.
    methode_gefrieren_pbs_sek = models.IntegerField(db_column='METHODE_GEFRIEREN_PBS_SEK', blank=True, null=True)  # Field name made lowercase.
    methode_gefrieren_55_min = models.IntegerField(db_column='METHODE_GEFRIEREN_55_MIN', blank=True, null=True)  # Field name made lowercase.
    methode_gefrieren_55_sek = models.IntegerField(db_column='METHODE_GEFRIEREN_55_SEK', blank=True, null=True)  # Field name made lowercase.
    methode_gefrieren_1010_min = models.IntegerField(db_column='METHODE_GEFRIEREN_1010_MIN', blank=True, null=True)  # Field name made lowercase.
    methode_gefrieren_1010_sek = models.IntegerField(db_column='METHODE_GEFRIEREN_1010_SEK', blank=True, null=True)  # Field name made lowercase.
    methode_gefrieren_1515_min = models.IntegerField(db_column='METHODE_GEFRIEREN_1515_MIN', blank=True, null=True)  # Field name made lowercase.
    methode_gefrieren_1515_sek = models.IntegerField(db_column='METHODE_GEFRIEREN_1515_SEK', blank=True, null=True)  # Field name made lowercase.
    methode_gefrieren_2020_min = models.IntegerField(db_column='METHODE_GEFRIEREN_2020_MIN', blank=True, null=True)  # Field name made lowercase.
    methode_gefrieren_2020_sek = models.IntegerField(db_column='METHODE_GEFRIEREN_2020_SEK', blank=True, null=True)  # Field name made lowercase.
    methode_auftauen_suc1m_min = models.IntegerField(db_column='METHODE_AUFTAUEN_SUC1M_MIN', blank=True, null=True)  # Field name made lowercase.
    methode_auftauen_suc1m_sek = models.IntegerField(db_column='METHODE_AUFTAUEN_SUC1M_SEK', blank=True, null=True)  # Field name made lowercase.
    methode_auftauen_suc05m_min = models.IntegerField(db_column='METHODE_AUFTAUEN_SUC05M_MIN', blank=True, null=True)  # Field name made lowercase.
    methode_auftauen_suc05m_sek = models.IntegerField(db_column='METHODE_AUFTAUEN_SUC05M_SEK', blank=True, null=True)  # Field name made lowercase.
    methode_auftauen_suc025m_min = models.IntegerField(db_column='METHODE_AUFTAUEN_SUC025M_MIN', blank=True, null=True)  # Field name made lowercase.
    methode_auftauen_suc025m_sek = models.IntegerField(db_column='METHODE_AUFTAUEN_SUC025M_SEK', blank=True, null=True)  # Field name made lowercase.
    methode_auftauen_pbs_min = models.IntegerField(db_column='METHODE_AUFTAUEN_PBS_MIN', blank=True, null=True)  # Field name made lowercase.
    methode_auftauen_pbs_sek = models.IntegerField(db_column='METHODE_AUFTAUEN_PBS_SEK', blank=True, null=True)  # Field name made lowercase.
    methode_auftauen_temperatur = models.IntegerField(db_column='METHODE_AUFTAUEN_TEMPERATUR', blank=True, null=True)  # Field name made lowercase.
    methode_auftauen_suc0125m_min = models.IntegerField(db_column='METHODE_AUFTAUEN_SUC0125M_MIN', blank=True, null=True)  # Field name made lowercase.
    methode_auftauen_suc0125m_sek = models.IntegerField(db_column='METHODE_AUFTAUEN_SUC0125M_SEK', blank=True, null=True)  # Field name made lowercase.
    methode_auftauen_laserhatching = models.IntegerField(db_column='METHODE_AUFTAUEN_LASERHATCHING', blank=True, null=True)  # Field name made lowercase.
    methode_gefrieren_temperatur = models.IntegerField(db_column='METHODE_GEFRIEREN_TEMPERATUR', blank=True, null=True)  # Field name made lowercase.
    kryo_abgeschlossen = models.IntegerField(db_column='KRYO_ABGESCHLOSSEN', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'IVF_EMBRYOKRYO'


class IvfEmbryotransfer(models.Model):
    embryotransfer_dokuid = models.IntegerField(db_column='EMBRYOTRANSFER_DOKUID', unique=True, blank=True, null=True)  # Field name made lowercase.
    kommentar = models.TextField(db_column='KOMMENTAR', blank=True, null=True)  # Field name made lowercase.
    invitro_zyklus_id = models.IntegerField(db_column='INVITRO_ZYKLUS_ID', blank=True, null=True)  # Field name made lowercase.
    tag = models.IntegerField(db_column='TAG', blank=True, null=True)  # Field name made lowercase.
    labor = models.CharField(db_column='LABOR', max_length=255, blank=True, null=True)  # Field name made lowercase.
    embryonen_anz = models.IntegerField(db_column='EMBRYONEN_ANZ', blank=True, null=True)  # Field name made lowercase.
    embryonen_typ = models.IntegerField(db_column='EMBRYONEN_TYP', blank=True, null=True)  # Field name made lowercase.
    embryonen_qualitaet = models.CharField(db_column='EMBRYONEN_QUALITAET', max_length=255, blank=True, null=True)  # Field name made lowercase.
    intern = models.TextField(db_column='INTERN', blank=True, null=True)  # Field name made lowercase.
    sondenlaenge = models.CharField(db_column='SONDENLAENGE', max_length=50, blank=True, null=True)  # Field name made lowercase.
    position_uterus = models.IntegerField(db_column='POSITION_UTERUS', blank=True, null=True)  # Field name made lowercase.
    schwierigkeit = models.IntegerField(db_column='SCHWIERIGKEIT', blank=True, null=True)  # Field name made lowercase.
    transferset = models.IntegerField(db_column='TRANSFERSET', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'IVF_EMBRYOTRANSFER'


class IvfInvitroZyklus(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    patient_id = models.IntegerField(db_column='PATIENT_ID', blank=True, null=True)  # Field name made lowercase.
    datum = models.DateField(db_column='DATUM', blank=True, null=True)  # Field name made lowercase.
    mandant_id = models.IntegerField(db_column='MANDANT_ID', blank=True, null=True)  # Field name made lowercase.
    bearbeiter_id = models.IntegerField(db_column='BEARBEITER_ID', blank=True, null=True)  # Field name made lowercase.
    arzt_id = models.IntegerField(db_column='ARZT_ID', blank=True, null=True)  # Field name made lowercase.
    start_down_reg = models.DateField(db_column='START_DOWN_REG', blank=True, null=True)  # Field name made lowercase.
    down_reg_bemerkung = models.CharField(db_column='DOWN_REG_BEMERKUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    letzte_menstruation = models.DateField(db_column='LETZTE_MENSTRUATION', blank=True, null=True)  # Field name made lowercase.
    start_stimulation = models.DateField(db_column='START_STIMULATION', blank=True, null=True)  # Field name made lowercase.
    ende_stimulation = models.DateField(db_column='ENDE_STIMULATION', blank=True, null=True)  # Field name made lowercase.
    ultraschall_1 = models.DateField(db_column='ULTRASCHALL_1', blank=True, null=True)  # Field name made lowercase.
    ultraschall_imhaus = models.IntegerField(db_column='ULTRASCHALL_IMHAUS', blank=True, null=True)  # Field name made lowercase.
    aktivierung_datum = models.DateField(db_column='AKTIVIERUNG_DATUM', blank=True, null=True)  # Field name made lowercase.
    aktivierung_uhrzeit = models.TimeField(db_column='AKTIVIERUNG_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    punktion_datum = models.DateField(db_column='PUNKTION_DATUM', blank=True, null=True)  # Field name made lowercase.
    punktion_uhrzeit = models.TimeField(db_column='PUNKTION_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    punktion_imhaus = models.IntegerField(db_column='PUNKTION_IMHAUS', blank=True, null=True)  # Field name made lowercase.
    behandlungen = models.CharField(db_column='BEHANDLUNGEN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bemerkungen = models.TextField(db_column='BEMERKUNGEN', blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    therapieplan_titel = models.CharField(db_column='THERAPIEPLAN_TITEL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    therapieplan_nummer = models.IntegerField(db_column='THERAPIEPLAN_NUMMER', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'IVF_INVITRO_ZYKLUS'


class IvfInvitroZyklusTabelleneintraege(models.Model):
    zyklus_id = models.IntegerField(db_column='ZYKLUS_ID', blank=True, null=True)  # Field name made lowercase.
    name = models.CharField(db_column='NAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    us_seite = models.CharField(db_column='US_SEITE', max_length=1, blank=True, null=True)  # Field name made lowercase.
    datum_string = models.CharField(db_column='DATUM_STRING', max_length=100, blank=True, null=True)  # Field name made lowercase.
    wert = models.CharField(db_column='WERT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    typ = models.CharField(db_column='TYP', max_length=3, blank=True, null=True)  # Field name made lowercase.
    zeilenposition = models.IntegerField(db_column='ZEILENPOSITION', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'IVF_INVITRO_ZYKLUS_TABELLENEINTRAEGE'


class IvfKultur(models.Model):
    kultur_dokuid = models.IntegerField(db_column='KULTUR_DOKUID', unique=True, blank=True, null=True)  # Field name made lowercase.
    kommentar = models.TextField(db_column='KOMMENTAR', blank=True, null=True)  # Field name made lowercase.
    invitro_zyklus_id = models.IntegerField(db_column='INVITRO_ZYKLUS_ID', blank=True, null=True)  # Field name made lowercase.
    punktiondoku_id = models.IntegerField(db_column='PUNKTIONDOKU_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'IVF_KULTUR'


class IvfKulturDaten(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    kultur_dokuid = models.IntegerField(db_column='KULTUR_DOKUID', blank=True, null=True)  # Field name made lowercase.
    eizelle_nummer = models.IntegerField(db_column='EIZELLE_NUMMER', blank=True, null=True)  # Field name made lowercase.
    tag0_datum = models.DateField(db_column='TAG0_DATUM', blank=True, null=True)  # Field name made lowercase.
    tag0_uhrzeit = models.TimeField(db_column='TAG0_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    tag0_daten = models.TextField(db_column='TAG0_DATEN', blank=True, null=True)  # Field name made lowercase.
    tag1_datum = models.DateField(db_column='TAG1_DATUM', blank=True, null=True)  # Field name made lowercase.
    tag1_uhrzeit = models.TimeField(db_column='TAG1_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    tag1_daten = models.TextField(db_column='TAG1_DATEN', blank=True, null=True)  # Field name made lowercase.
    tag2_datum = models.DateField(db_column='TAG2_DATUM', blank=True, null=True)  # Field name made lowercase.
    tag2_uhrzeit = models.TimeField(db_column='TAG2_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    tag2_daten = models.TextField(db_column='TAG2_DATEN', blank=True, null=True)  # Field name made lowercase.
    tag3_datum = models.DateField(db_column='TAG3_DATUM', blank=True, null=True)  # Field name made lowercase.
    tag3_uhrzeit = models.TimeField(db_column='TAG3_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    tag3_daten = models.TextField(db_column='TAG3_DATEN', blank=True, null=True)  # Field name made lowercase.
    tag4_datum = models.DateField(db_column='TAG4_DATUM', blank=True, null=True)  # Field name made lowercase.
    tag4_uhrzeit = models.TimeField(db_column='TAG4_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    tag4_daten = models.TextField(db_column='TAG4_DATEN', blank=True, null=True)  # Field name made lowercase.
    tag5_datum = models.DateField(db_column='TAG5_DATUM', blank=True, null=True)  # Field name made lowercase.
    tag5_uhrzeit = models.TimeField(db_column='TAG5_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    tag5_daten = models.TextField(db_column='TAG5_DATEN', blank=True, null=True)  # Field name made lowercase.
    tag6_datum = models.DateField(db_column='TAG6_DATUM', blank=True, null=True)  # Field name made lowercase.
    tag6_uhrzeit = models.TimeField(db_column='TAG6_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    tag6_daten = models.TextField(db_column='TAG6_DATEN', blank=True, null=True)  # Field name made lowercase.
    endablauf_transfer = models.IntegerField(db_column='ENDABLAUF_TRANSFER', blank=True, null=True)  # Field name made lowercase.
    endablauf_hatch = models.IntegerField(db_column='ENDABLAUF_HATCH', blank=True, null=True)  # Field name made lowercase.
    endablauf_kryo = models.IntegerField(db_column='ENDABLAUF_KRYO', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'IVF_KULTUR_DATEN'
        unique_together = (('kultur_dokuid', 'eizelle_nummer'),)


class IvfPunktion(models.Model):
    punktion_dokuid = models.IntegerField(db_column='PUNKTION_DOKUID', unique=True)  # Field name made lowercase.
    kommentar = models.TextField(db_column='KOMMENTAR', blank=True, null=True)  # Field name made lowercase.
    labor = models.CharField(db_column='LABOR', max_length=255, blank=True, null=True)  # Field name made lowercase.
    punktionen = models.TextField(db_column='PUNKTIONEN', blank=True, null=True)  # Field name made lowercase.
    invitro_zyklus_id = models.IntegerField(db_column='INVITRO_ZYKLUS_ID', blank=True, null=True)  # Field name made lowercase.
    hyaluronidase = models.IntegerField(db_column='HYALURONIDASE', blank=True, null=True)  # Field name made lowercase.
    uhrzeit = models.TimeField(db_column='UHRZEIT', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'IVF_PUNKTION'


class IvfSamenkryo(models.Model):
    samenkryo_dokuid = models.IntegerField(db_column='SAMENKRYO_DOKUID', unique=True, blank=True, null=True)  # Field name made lowercase.
    kommentar = models.TextField(db_column='KOMMENTAR', blank=True, null=True)  # Field name made lowercase.
    labor = models.CharField(db_column='LABOR', max_length=255, blank=True, null=True)  # Field name made lowercase.
    externekryo = models.IntegerField(db_column='EXTERNEKRYO', blank=True, null=True)  # Field name made lowercase.
    invitro_zyklus_id = models.IntegerField(db_column='INVITRO_ZYKLUS_ID', blank=True, null=True)  # Field name made lowercase.
    befund = models.CharField(db_column='BEFUND', max_length=255, blank=True, null=True)  # Field name made lowercase.
    befund_sonstiges = models.CharField(db_column='BEFUND_SONSTIGES', max_length=255, blank=True, null=True)  # Field name made lowercase.
    lager_typ = models.IntegerField(db_column='LAGER_TYP', blank=True, null=True)  # Field name made lowercase.
    lager_id = models.CharField(db_column='LAGER_ID', max_length=255, blank=True, null=True)  # Field name made lowercase.
    lager_code = models.CharField(db_column='LAGER_CODE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    lager_behaelter = models.IntegerField(db_column='LAGER_BEHAELTER', blank=True, null=True)  # Field name made lowercase.
    lager_position = models.CharField(db_column='LAGER_POSITION', max_length=255, blank=True, null=True)  # Field name made lowercase.
    lager_anzahl = models.CharField(db_column='LAGER_ANZAHL', max_length=100, blank=True, null=True)  # Field name made lowercase.
    lager_typfarbe = models.CharField(db_column='LAGER_TYPFARBE', max_length=100, blank=True, null=True)  # Field name made lowercase.
    lager_positionfarbe = models.CharField(db_column='LAGER_POSITIONFARBE', max_length=100, blank=True, null=True)  # Field name made lowercase.
    methode = models.CharField(db_column='METHODE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    methode_kommentar = models.TextField(db_column='METHODE_KOMMENTAR', blank=True, null=True)  # Field name made lowercase.
    spermiogramm_id = models.IntegerField(db_column='SPERMIOGRAMM_ID', blank=True, null=True)  # Field name made lowercase.
    tese = models.IntegerField(db_column='TESE', blank=True, null=True)  # Field name made lowercase.
    tese_50er = models.CharField(db_column='TESE_50ER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    tese_90er = models.CharField(db_column='TESE_90ER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    tese_70er = models.CharField(db_column='TESE_70ER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    tese_ergebnisse = models.TextField(db_column='TESE_ERGEBNISSE', blank=True, null=True)  # Field name made lowercase.
    tese_ueberstand = models.CharField(db_column='TESE_UEBERSTAND', max_length=255, blank=True, null=True)  # Field name made lowercase.
    kryo_abgeschlossen = models.IntegerField(db_column='KRYO_ABGESCHLOSSEN', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'IVF_SAMENKRYO'


class IvfSpermiogramm(models.Model):
    spermiogramm_dokuid = models.IntegerField(db_column='SPERMIOGRAMM_DOKUID', unique=True, blank=True, null=True)  # Field name made lowercase.
    kommentar = models.TextField(db_column='KOMMENTAR', blank=True, null=True)  # Field name made lowercase.
    invitro_zyklus_id = models.IntegerField(db_column='INVITRO_ZYKLUS_ID', blank=True, null=True)  # Field name made lowercase.
    labor = models.CharField(db_column='LABOR', max_length=255, blank=True, null=True)  # Field name made lowercase.
    spermiengewinnung = models.IntegerField(db_column='SPERMIENGEWINNUNG', blank=True, null=True)  # Field name made lowercase.
    spermiengewinnung_sontiges = models.TextField(db_column='SPERMIENGEWINNUNG_SONTIGES', blank=True, null=True)  # Field name made lowercase.
    samenpraeparationfuer = models.IntegerField(db_column='SAMENPRAEPARATIONFUER', blank=True, null=True)  # Field name made lowercase.
    number_3gradientensperma = models.CharField(db_column='3GRADIENTENSPERMA', max_length=100, blank=True, null=True)  # Field name made lowercase. Field renamed because it wasn't a valid Python identifier.
    generation = models.CharField(db_column='GENERATION', max_length=255, blank=True, null=True)  # Field name made lowercase.
    tese_obstruktion = models.IntegerField(db_column='TESE_OBSTRUKTION', blank=True, null=True)  # Field name made lowercase.
    tese_ergebnis = models.TextField(db_column='TESE_ERGEBNIS', blank=True, null=True)  # Field name made lowercase.
    samen_volumen = models.CharField(db_column='SAMEN_VOLUMEN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    samen_viskositaet = models.IntegerField(db_column='SAMEN_VISKOSITAET', blank=True, null=True)  # Field name made lowercase.
    samen_verfluessigen = models.CharField(db_column='SAMEN_VERFLUESSIGEN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    samen_konzentration = models.CharField(db_column='SAMEN_KONZENTRATION', max_length=255, blank=True, null=True)  # Field name made lowercase.
    samen_konzentration_einheit = models.CharField(db_column='SAMEN_KONZENTRATION_EINHEIT', max_length=100, blank=True, null=True)  # Field name made lowercase.
    samen_totalemotilitaetprozent = models.CharField(db_column='SAMEN_TOTALEMOTILITAETPROZENT', max_length=50, blank=True, null=True)  # Field name made lowercase.
    samen_schnellprogressivprozent = models.CharField(db_column='SAMEN_SCHNELLPROGRESSIVPROZENT', max_length=50, blank=True, null=True)  # Field name made lowercase.
    samen_langsamprogressivprozent = models.CharField(db_column='SAMEN_LANGSAMPROGRESSIVPROZENT', max_length=50, blank=True, null=True)  # Field name made lowercase.
    samen_beweglichprozent = models.CharField(db_column='SAMEN_BEWEGLICHPROZENT', max_length=50, blank=True, null=True)  # Field name made lowercase.
    samen_unbeweglichprozent = models.CharField(db_column='SAMEN_UNBEWEGLICHPROZENT', max_length=50, blank=True, null=True)  # Field name made lowercase.
    samen_pathologischprozent = models.CharField(db_column='SAMEN_PATHOLOGISCHPROZENT', max_length=50, blank=True, null=True)  # Field name made lowercase.
    samen_rundzellenleukosanzahl = models.CharField(db_column='SAMEN_RUNDZELLENLEUKOSANZAHL', max_length=100, blank=True, null=True)  # Field name made lowercase.
    samen_agglutination = models.IntegerField(db_column='SAMEN_AGGLUTINATION', blank=True, null=True)  # Field name made lowercase.
    tese_seite = models.CharField(db_column='TESE_SEITE', max_length=50, blank=True, null=True)  # Field name made lowercase.
    imsi_klasse1prozent = models.CharField(db_column='IMSI_KLASSE1PROZENT', max_length=50, blank=True, null=True)  # Field name made lowercase.
    imsi_klasse2prozent = models.CharField(db_column='IMSI_KLASSE2PROZENT', max_length=50, blank=True, null=True)  # Field name made lowercase.
    imsi_klasse3prozent = models.CharField(db_column='IMSI_KLASSE3PROZENT', max_length=50, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'IVF_SPERMIOGRAMM'


class Kalenderstamm(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    name = models.CharField(db_column='NAME', max_length=50, blank=True, null=True)  # Field name made lowercase.
    beschreibung = models.CharField(db_column='BESCHREIBUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    kennung = models.IntegerField(db_column='KENNUNG', unique=True, blank=True, null=True)  # Field name made lowercase.
    zusatz_1 = models.TextField(db_column='ZUSATZ_1', blank=True, null=True)  # Field name made lowercase.
    zusatz_2 = models.TextField(db_column='ZUSATZ_2', blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'KALENDERSTAMM'


class Kalendervorlage(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    kalender_kennung = models.IntegerField(db_column='KALENDER_KENNUNG')  # Field name made lowercase.
    vorlagen_kennung = models.IntegerField(db_column='VORLAGEN_KENNUNG')  # Field name made lowercase.
    gueltig_ab = models.DateField(db_column='GUELTIG_AB', blank=True, null=True)  # Field name made lowercase.
    gueltig_bis = models.DateField(db_column='GUELTIG_BIS', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'KALENDERVORLAGE'


class KatalogIcd(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    typ = models.CharField(db_column='TYP', max_length=255, blank=True, null=True)  # Field name made lowercase.
    typ_1 = models.CharField(db_column='TYP_1', max_length=255, blank=True, null=True)  # Field name made lowercase.
    typ_2 = models.CharField(db_column='TYP_2', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ziffer = models.CharField(db_column='ZIFFER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ziffer_text_1 = models.CharField(db_column='ZIFFER_TEXT_1', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ziffer_text_2 = models.CharField(db_column='ZIFFER_TEXT_2', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ziffer_text_3 = models.CharField(db_column='ZIFFER_TEXT_3', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ziffer_text = models.CharField(db_column='ZIFFER_TEXT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ziffer_text_kurz = models.CharField(db_column='ZIFFER_TEXT_KURZ', max_length=255, blank=True, null=True)  # Field name made lowercase.
    punkte = models.CharField(db_column='PUNKTE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    gruppe = models.CharField(db_column='GRUPPE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    kuerzel = models.CharField(db_column='KUERZEL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    notizen = models.CharField(db_column='NOTIZEN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    protokoll = models.CharField(db_column='PROTOKOLL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='STATUS', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ziffer_von = models.CharField(db_column='ZIFFER_VON', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ziffer_bis = models.CharField(db_column='ZIFFER_BIS', max_length=255, blank=True, null=True)  # Field name made lowercase.
    betrag = models.CharField(db_column='BETRAG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    waehrung = models.CharField(db_column='WAEHRUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    version = models.CharField(db_column='VERSION', max_length=255, blank=True, null=True)  # Field name made lowercase.
    zeitstempel = models.CharField(db_column='ZEITSTEMPEL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    lock = models.CharField(db_column='LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'KATALOG_ICD'


class KatalogKapitel(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP')  # Field name made lowercase.
    index = models.CharField(db_column='INDEX', max_length=15, blank=True, null=True)  # Field name made lowercase.
    bereich = models.CharField(db_column='BEREICH', max_length=15, blank=True, null=True)  # Field name made lowercase.
    kapitel = models.CharField(db_column='KAPITEL', max_length=15, blank=True, null=True)  # Field name made lowercase.
    abschnitt = models.CharField(db_column='ABSCHNITT', max_length=15, blank=True, null=True)  # Field name made lowercase.
    uabschnitt = models.CharField(db_column='UABSCHNITT', max_length=15, blank=True, null=True)  # Field name made lowercase.
    block = models.CharField(db_column='BLOCK', max_length=15, blank=True, null=True)  # Field name made lowercase.
    titel = models.CharField(db_column='TITEL', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'KATALOG_KAPITEL'
        unique_together = (('index', 'typ'),)


class Kontraindikationenauftrag(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    anamnesefragenstamm_id = models.IntegerField(db_column='ANAMNESEFRAGENSTAMM_ID')  # Field name made lowercase.
    auftragstamm_id = models.IntegerField(db_column='AUFTRAGSTAMM_ID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'KONTRAINDIKATIONENAUFTRAG'
        unique_together = (('anamnesefragenstamm_id', 'auftragstamm_id'),)


class Kreislauf(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    patient_id = models.IntegerField(db_column='PATIENT_ID')  # Field name made lowercase.
    typ = models.CharField(db_column='TYP', max_length=50)  # Field name made lowercase.
    datum = models.DateField(db_column='DATUM', blank=True, null=True)  # Field name made lowercase.
    systole = models.FloatField(db_column='SYSTOLE', blank=True, null=True)  # Field name made lowercase.
    diastole = models.FloatField(db_column='DIASTOLE', blank=True, null=True)  # Field name made lowercase.
    puls = models.FloatField(db_column='PULS', blank=True, null=True)  # Field name made lowercase.
    bemerkung = models.TextField(db_column='BEMERKUNG', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'KREISLAUF'


class Kultur(models.Model):
    kultur_dokuid = models.IntegerField(db_column='KULTUR_DOKUID', unique=True, blank=True, null=True)  # Field name made lowercase.
    kommentar = models.TextField(db_column='KOMMENTAR', blank=True, null=True)  # Field name made lowercase.
    invitro_zyklus_id = models.IntegerField(db_column='INVITRO_ZYKLUS_ID', blank=True, null=True)  # Field name made lowercase.
    tag0_datum = models.DateField(db_column='TAG0_DATUM', blank=True, null=True)  # Field name made lowercase.
    tag0_uhrzeit = models.TimeField(db_column='TAG0_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    tag0_daten = models.TextField(db_column='TAG0_DATEN', blank=True, null=True)  # Field name made lowercase.
    tag1_datum = models.DateField(db_column='TAG1_DATUM', blank=True, null=True)  # Field name made lowercase.
    tag1_uhrzeit = models.TimeField(db_column='TAG1_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    tag1_daten = models.TextField(db_column='TAG1_DATEN', blank=True, null=True)  # Field name made lowercase.
    tag2_datum = models.DateField(db_column='TAG2_DATUM', blank=True, null=True)  # Field name made lowercase.
    tag2_uhrzeit = models.TimeField(db_column='TAG2_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    tag2_daten = models.TextField(db_column='TAG2_DATEN', blank=True, null=True)  # Field name made lowercase.
    tag3_datum = models.DateField(db_column='TAG3_DATUM', blank=True, null=True)  # Field name made lowercase.
    tag3_uhrzeit = models.TimeField(db_column='TAG3_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    tag3_daten = models.TextField(db_column='TAG3_DATEN', blank=True, null=True)  # Field name made lowercase.
    tag4_datum = models.DateField(db_column='TAG4_DATUM', blank=True, null=True)  # Field name made lowercase.
    tag4_uhrzeit = models.TimeField(db_column='TAG4_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    tag4_daten = models.TextField(db_column='TAG4_DATEN', blank=True, null=True)  # Field name made lowercase.
    tag5_datum = models.DateField(db_column='TAG5_DATUM', blank=True, null=True)  # Field name made lowercase.
    tag5_uhrzeit = models.TimeField(db_column='TAG5_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    tag5_daten = models.TextField(db_column='TAG5_DATEN', blank=True, null=True)  # Field name made lowercase.
    tag6_datum = models.DateField(db_column='TAG6_DATUM', blank=True, null=True)  # Field name made lowercase.
    tag6_uhrzeit = models.TimeField(db_column='TAG6_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    tag6_daten = models.TextField(db_column='TAG6_DATEN', blank=True, null=True)  # Field name made lowercase.
    endablauf_transfer = models.IntegerField(db_column='ENDABLAUF_TRANSFER', blank=True, null=True)  # Field name made lowercase.
    endablauf_hatch = models.IntegerField(db_column='ENDABLAUF_HATCH', blank=True, null=True)  # Field name made lowercase.
    endablauf_kryo = models.IntegerField(db_column='ENDABLAUF_KRYO', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'KULTUR'


class Laborauftrag(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    laborauftragdoku_id = models.IntegerField(db_column='LABORAUFTRAGDOKU_ID', unique=True)  # Field name made lowercase.
    datum_versendet = models.DateField(db_column='DATUM_VERSENDET', blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS')  # Field name made lowercase.
    diagnose = models.CharField(db_column='DIAGNOSE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    kommentar = models.CharField(db_column='KOMMENTAR', max_length=255, blank=True, null=True)  # Field name made lowercase.
    auftragsnummer = models.IntegerField(db_column='AUFTRAGSNUMMER', unique=True, blank=True, null=True)  # Field name made lowercase.
    probenentnahme_datum = models.DateField(db_column='PROBENENTNAHME_DATUM', blank=True, null=True)  # Field name made lowercase.
    etiketten_zaehler = models.IntegerField(db_column='ETIKETTEN_ZAEHLER', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'LABORAUFTRAG'


class Laborauftragwert(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    laborauftragdoku_id = models.IntegerField(db_column='LABORAUFTRAGDOKU_ID', blank=True, null=True)  # Field name made lowercase.
    code_im_labor = models.CharField(db_column='CODE_IM_LABOR', max_length=255, blank=True, null=True)  # Field name made lowercase.
    laborauftrag_zuweiser_id = models.IntegerField(db_column='LABORAUFTRAG_ZUWEISER_ID', blank=True, null=True)  # Field name made lowercase.
    referenz_dokuid = models.IntegerField(db_column='REFERENZ_DOKUID', blank=True, null=True)  # Field name made lowercase.
    nachforderung = models.IntegerField(db_column='NACHFORDERUNG', blank=True, null=True)  # Field name made lowercase.
    kuerzel = models.CharField(db_column='KUERZEL', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'LABORAUFTRAGWERT'


class Laborauftragwertstamm(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    code_im_labor = models.CharField(db_column='CODE_IM_LABOR', max_length=255, blank=True, null=True)  # Field name made lowercase.
    kuerzel = models.CharField(db_column='KUERZEL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    typ = models.CharField(db_column='TYP', max_length=255, blank=True, null=True)  # Field name made lowercase.
    spezimen = models.CharField(db_column='SPEZIMEN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    geb_kuerzel = models.CharField(db_column='GEB_KUERZEL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bereich = models.CharField(db_column='BEREICH', max_length=255, blank=True, null=True)  # Field name made lowercase.
    nachforderung = models.IntegerField(db_column='NACHFORDERUNG', blank=True, null=True)  # Field name made lowercase.
    kasse = models.CharField(db_column='KASSE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    privattarif = models.FloatField(db_column='PRIVATTARIF', blank=True, null=True)  # Field name made lowercase.
    flags = models.CharField(db_column='FLAGS', max_length=255, blank=True, null=True)  # Field name made lowercase.
    mandanten = models.CharField(db_column='MANDANTEN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)  # Field name made lowercase.
    gueltig_ab = models.DateField(db_column='GUELTIG_AB', blank=True, null=True)  # Field name made lowercase.
    gueltig_bis = models.DateField(db_column='GUELTIG_BIS', blank=True, null=True)  # Field name made lowercase.
    laborauftrag_zuweiser_id = models.IntegerField(db_column='LABORAUFTRAG_ZUWEISER_ID', blank=True, null=True)  # Field name made lowercase.
    profil_parameter_ids = models.CharField(db_column='PROFIL_PARAMETER_IDS', max_length=500, blank=True, null=True)  # Field name made lowercase.
    geb_bezeichnung = models.CharField(db_column='GEB_BEZEICHNUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    verrech_diagnosen = models.CharField(db_column='VERRECH_DIAGNOSEN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    prob_inf = models.TextField(db_column='PROB_INF', blank=True, null=True)  # Field name made lowercase.
    mic_biol = models.IntegerField(db_column='MIC_BIOL', blank=True, null=True)  # Field name made lowercase.
    zwingend_param = models.CharField(db_column='ZWINGEND_PARAM', max_length=255, blank=True, null=True)  # Field name made lowercase.
    exclude_param = models.CharField(db_column='EXCLUDE_PARAM', max_length=255, blank=True, null=True)  # Field name made lowercase.
    app_gem = models.IntegerField(db_column='APP_GEM', blank=True, null=True)  # Field name made lowercase.
    method_id = models.CharField(db_column='METHOD_ID', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'LABORAUFTRAGWERTSTAMM'
        unique_together = (('code_im_labor', 'laborauftrag_zuweiser_id'), ('code_im_labor', 'laborauftrag_zuweiser_id'),)


class LaborauftragNummern(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    auftragsnummer = models.CharField(db_column='AUFTRAGSNUMMER', max_length=20, blank=True, null=True)  # Field name made lowercase.
    labor = models.CharField(db_column='LABOR', max_length=100, blank=True, null=True)  # Field name made lowercase.
    mandant_id = models.IntegerField(db_column='MANDANT_ID', blank=True, null=True)  # Field name made lowercase.
    gueltig_bis = models.DateField(db_column='GUELTIG_BIS', blank=True, null=True)  # Field name made lowercase.
    doku_id = models.IntegerField(db_column='DOKU_ID', unique=True, blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS')  # Field name made lowercase.
    jahr = models.IntegerField(db_column='JAHR', blank=True, null=True)  # Field name made lowercase.
    nummern_typ = models.IntegerField(db_column='NUMMERN_TYP', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'LABORAUFTRAG_NUMMERN'
        unique_together = (('auftragsnummer', 'labor', 'jahr'),)


class Laborbefund(models.Model):
    doku_id = models.IntegerField(db_column='DOKU_ID', unique=True)  # Field name made lowercase.
    senden = models.IntegerField(db_column='SENDEN', blank=True, null=True)  # Field name made lowercase.
    sende_status = models.IntegerField(db_column='SENDE_STATUS')  # Field name made lowercase.
    sende_zeit = models.TimeField(db_column='SENDE_ZEIT', blank=True, null=True)  # Field name made lowercase.
    sende_empfaenger = models.CharField(db_column='SENDE_EMPFAENGER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    senden_auftragsnummer = models.CharField(db_column='SENDEN_AUFTRAGSNUMMER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    empfangen = models.IntegerField(db_column='EMPFANGEN', blank=True, null=True)  # Field name made lowercase.
    empfangen_status = models.IntegerField(db_column='EMPFANGEN_STATUS', blank=True, null=True)  # Field name made lowercase.
    empfangen_zeit = models.DateTimeField(db_column='EMPFANGEN_ZEIT', blank=True, null=True)  # Field name made lowercase.
    empfangen_absender = models.CharField(db_column='EMPFANGEN_ABSENDER', max_length=100, blank=True, null=True)  # Field name made lowercase.
    empfangen_auftragsnummer = models.CharField(db_column='EMPFANGEN_AUFTRAGSNUMMER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    auftragsinformation = models.TextField(db_column='AUFTRAGSINFORMATION', blank=True, null=True)  # Field name made lowercase.
    laborauftrag_dokuid = models.IntegerField(db_column='LABORAUFTRAG_DOKUID', blank=True, null=True)  # Field name made lowercase.
    laborbefund_typ = models.IntegerField(db_column='LABORBEFUND_TYP', blank=True, null=True)  # Field name made lowercase.
    laborbefund_art = models.CharField(db_column='LABORBEFUND_ART', max_length=2, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'LABORBEFUND'
        unique_together = (('empfangen_absender', 'empfangen_auftragsnummer', 'laborbefund_typ', 'laborbefund_art'),)


class Labornorm(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    laborstamm_id = models.IntegerField(db_column='LABORSTAMM_ID', blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)  # Field name made lowercase.
    geschlecht = models.IntegerField(db_column='GESCHLECHT', blank=True, null=True)  # Field name made lowercase.
    alter_von = models.IntegerField(db_column='ALTER_VON', blank=True, null=True)  # Field name made lowercase.
    alter_bis = models.IntegerField(db_column='ALTER_BIS', blank=True, null=True)  # Field name made lowercase.
    alter_einheit = models.IntegerField(db_column='ALTER_EINHEIT')  # Field name made lowercase.
    normbereich_unten = models.FloatField(db_column='NORMBEREICH_UNTEN', blank=True, null=True)  # Field name made lowercase.
    normbereich_oben = models.FloatField(db_column='NORMBEREICH_OBEN', blank=True, null=True)  # Field name made lowercase.
    normbereich = models.CharField(db_column='NORMBEREICH', max_length=50, blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'LABORNORM'


class Laborstamm(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    einheit = models.CharField(db_column='EINHEIT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    kuerzel = models.CharField(db_column='KUERZEL', unique=True, max_length=255, blank=True, null=True)  # Field name made lowercase.
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    gruppe = models.IntegerField(db_column='GRUPPE', blank=True, null=True)  # Field name made lowercase.
    ermaechtigung = models.CharField(db_column='ERMAECHTIGUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    privat = models.CharField(db_column='PRIVAT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    kasse = models.CharField(db_column='KASSE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung_datum = models.DateField(db_column='LETZTE_AENDERUNG_DATUM', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung_uhrzeit = models.TimeField(db_column='LETZTE_AENDERUNG_UHRZEIT', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'LABORSTAMM'


class Laborwerte(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    laborbefund_id = models.IntegerField(db_column='LABORBEFUND_ID', blank=True, null=True)  # Field name made lowercase.
    untersuchung_id = models.IntegerField(db_column='UNTERSUCHUNG_ID', blank=True, null=True)  # Field name made lowercase.
    sortierung = models.IntegerField(db_column='SORTIERUNG')  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)  # Field name made lowercase.
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    warnung = models.CharField(db_column='WARNUNG', max_length=50, blank=True, null=True)  # Field name made lowercase.
    abweichung = models.CharField(db_column='ABWEICHUNG', max_length=50, blank=True, null=True)  # Field name made lowercase.
    ergebniswert = models.CharField(db_column='ERGEBNISWERT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    einheit = models.CharField(db_column='EINHEIT', max_length=50, blank=True, null=True)  # Field name made lowercase.
    normbereich = models.TextField(db_column='NORMBEREICH', blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    gruppe = models.CharField(db_column='GRUPPE', max_length=50, blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    testbezeichnung = models.TextField(db_column='TESTBEZEICHNUNG', blank=True, null=True)  # Field name made lowercase.
    ergebnistext = models.TextField(db_column='ERGEBNISTEXT', blank=True, null=True)  # Field name made lowercase.
    probenident = models.CharField(db_column='PROBENIDENT', max_length=30, blank=True, null=True)  # Field name made lowercase.
    probenindex = models.CharField(db_column='PROBENINDEX', max_length=10, blank=True, null=True)  # Field name made lowercase.
    probenbezeichnung = models.CharField(db_column='PROBENBEZEICHNUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    probenspezifikation = models.CharField(db_column='PROBENSPEZIFIKATION', max_length=255, blank=True, null=True)  # Field name made lowercase.
    norm_unten = models.CharField(db_column='NORM_UNTEN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    norm_oben = models.CharField(db_column='NORM_OBEN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    entnahmedatum = models.DateField(db_column='ENTNAHMEDATUM', blank=True, null=True)  # Field name made lowercase.
    entnahmeuhrzeit = models.TimeField(db_column='ENTNAHMEUHRZEIT', blank=True, null=True)  # Field name made lowercase.
    hinweistext = models.TextField(db_column='HINWEISTEXT', blank=True, null=True)  # Field name made lowercase.
    laborstamm_id = models.IntegerField(db_column='LABORSTAMM_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'LABORWERTE'


class LaborwertstammLaborkuerzelZuordnung(models.Model):
    laborwertstamm_id = models.IntegerField(db_column='LABORWERTSTAMM_ID', blank=True, null=True)  # Field name made lowercase.
    anforderungslabor_id = models.IntegerField(db_column='ANFORDERUNGSLABOR_ID', blank=True, null=True)  # Field name made lowercase.
    anforderungslabor_kuerzel = models.CharField(db_column='ANFORDERUNGSLABOR_KUERZEL', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'LABORWERTSTAMM_LABORKUERZEL_ZUORDNUNG'


class Lagerbewegung(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP')  # Field name made lowercase.
    datum = models.DateField(db_column='DATUM', blank=True, null=True)  # Field name made lowercase.
    uhrzeit = models.TimeField(db_column='UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    artikel = models.CharField(db_column='ARTIKEL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bewegung_text = models.TextField(db_column='BEWEGUNG_TEXT', blank=True, null=True)  # Field name made lowercase.
    menge = models.FloatField(db_column='MENGE')  # Field name made lowercase.
    chargen_nr = models.CharField(db_column='CHARGEN_NR', max_length=255, blank=True, null=True)  # Field name made lowercase.
    haltbar = models.DateField(db_column='HALTBAR', blank=True, null=True)  # Field name made lowercase.
    standort = models.CharField(db_column='STANDORT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ek_preis = models.FloatField(db_column='EK_PREIS')  # Field name made lowercase.
    vk_preis = models.FloatField(db_column='VK_PREIS')  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    system_zeit = models.DateTimeField(db_column='SYSTEM_ZEIT', blank=True, null=True)  # Field name made lowercase.
    ziffer_id = models.IntegerField(db_column='ZIFFER_ID', blank=True, null=True)  # Field name made lowercase.
    ek_waehrung = models.CharField(db_column='EK_WAEHRUNG', max_length=255)  # Field name made lowercase.
    vk_waehrung = models.CharField(db_column='VK_WAEHRUNG', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'LAGERBEWEGUNG'


class Lagerstamm(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    kuerzel = models.CharField(db_column='KUERZEL', unique=True, max_length=255, blank=True, null=True)  # Field name made lowercase.
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    gruppe = models.IntegerField(db_column='GRUPPE', blank=True, null=True)  # Field name made lowercase.
    ermaechtigungen = models.CharField(db_column='ERMAECHTIGUNGEN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    privat = models.CharField(db_column='PRIVAT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    kasse = models.CharField(db_column='KASSE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)  # Field name made lowercase.
    artikel_nr = models.CharField(db_column='ARTIKEL_NR', max_length=50, blank=True, null=True)  # Field name made lowercase.
    einheit = models.CharField(db_column='EINHEIT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    mindestmenge = models.FloatField(db_column='MINDESTMENGE')  # Field name made lowercase.
    lieferant = models.IntegerField(db_column='LIEFERANT', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'LAGERSTAMM'


class Leistung(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    leistungsdoku_id = models.IntegerField(db_column='LEISTUNGSDOKU_ID')  # Field name made lowercase.
    auftragstamm_id = models.IntegerField(db_column='AUFTRAGSTAMM_ID')  # Field name made lowercase.
    anzahl = models.IntegerField(db_column='ANZAHL')  # Field name made lowercase.
    datum = models.DateField(db_column='DATUM', blank=True, null=True)  # Field name made lowercase.
    uhrzeit = models.TimeField(db_column='UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    vertragsart = models.IntegerField(db_column='VERTRAGSART', blank=True, null=True)  # Field name made lowercase.
    privatpreis = models.FloatField(db_column='PRIVATPREIS')  # Field name made lowercase.
    auftrag_einzelleistungen_id = models.IntegerField(db_column='AUFTRAG_EINZELLEISTUNGEN_ID', blank=True, null=True)  # Field name made lowercase.
    benutzer_id = models.IntegerField(db_column='BENUTZER_ID', blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS')  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    begruendung = models.CharField(db_column='BEGRUENDUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'LEISTUNG'


class MagistraliterStamm(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    position = models.IntegerField(db_column='POSITION')  # Field name made lowercase.
    typ = models.CharField(db_column='TYP', max_length=50, blank=True, null=True)  # Field name made lowercase.
    kuerzel = models.CharField(db_column='KUERZEL', max_length=255)  # Field name made lowercase.
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    rezeptur = models.TextField(db_column='REZEPTUR', blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung_datum = models.DateField(db_column='LETZTE_AENDERUNG_DATUM', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung_uhrzeit = models.TimeField(db_column='LETZTE_AENDERUNG_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    dosisschema = models.TextField(db_column='DOSISSCHEMA', blank=True, null=True)  # Field name made lowercase.
    mandanten = models.CharField(db_column='MANDANTEN', max_length=255)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    kasse = models.CharField(db_column='KASSE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    privat = models.CharField(db_column='PRIVAT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    einstellung = models.TextField(db_column='EINSTELLUNG', blank=True, null=True)  # Field name made lowercase.
    medikament_box = models.CharField(db_column='MEDIKAMENT_BOX', max_length=5, blank=True, null=True)  # Field name made lowercase.
    vertragsart = models.CharField(db_column='VERTRAGSART', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'MAGISTRALITER_STAMM'


class Mahnung(models.Model):
    mahnungdoku_id = models.IntegerField(db_column='MAHNUNGDOKU_ID', primary_key=True)  # Field name made lowercase.
    untersuchung_id = models.IntegerField(db_column='UNTERSUCHUNG_ID')  # Field name made lowercase.
    anschrift_xml = models.TextField(db_column='ANSCHRIFT_XML', blank=True, null=True)  # Field name made lowercase.
    absende_anschrift = models.TextField(db_column='ABSENDE_ANSCHRIFT', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'MAHNUNG'


class Medikament(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    patient_id = models.IntegerField(db_column='PATIENT_ID')  # Field name made lowercase.
    doku_id = models.IntegerField(db_column='DOKU_ID')  # Field name made lowercase.
    untersuchung_id = models.IntegerField(db_column='UNTERSUCHUNG_ID')  # Field name made lowercase.
    typ = models.CharField(db_column='TYP', max_length=10)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    status = models.CharField(db_column='STATUS', max_length=50, blank=True, null=True)  # Field name made lowercase.
    datum = models.DateField(db_column='DATUM', blank=True, null=True)  # Field name made lowercase.
    uhrzeit = models.TimeField(db_column='UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    medikament = models.CharField(db_column='MEDIKAMENT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    menge = models.FloatField(db_column='MENGE', blank=True, null=True)  # Field name made lowercase.
    mengenart = models.CharField(db_column='MENGENART', max_length=255, blank=True, null=True)  # Field name made lowercase.
    anzahl = models.FloatField(db_column='ANZAHL', blank=True, null=True)  # Field name made lowercase.
    dosisschema = models.TextField(db_column='DOSISSCHEMA', blank=True, null=True)  # Field name made lowercase.
    medikament_ca_kz = models.CharField(db_column='MEDIKAMENT_CA_KZ', max_length=50, blank=True, null=True)  # Field name made lowercase.
    rezeptur = models.TextField(db_column='REZEPTUR', blank=True, null=True)  # Field name made lowercase.
    reserve2 = models.CharField(db_column='RESERVE2', max_length=50, blank=True, null=True)  # Field name made lowercase.
    reserve3 = models.TextField(db_column='RESERVE3', blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    rezept_datum = models.DateField(db_column='REZEPT_DATUM', blank=True, null=True)  # Field name made lowercase.
    kuerzel = models.CharField(db_column='KUERZEL', max_length=50, blank=True, null=True)  # Field name made lowercase.
    pharma_nr = models.IntegerField(db_column='PHARMA_NR', blank=True, null=True)  # Field name made lowercase.
    katalog = models.IntegerField(db_column='KATALOG', blank=True, null=True)  # Field name made lowercase.
    box = models.CharField(db_column='BOX', max_length=5, blank=True, null=True)  # Field name made lowercase.
    end_datum = models.DateField(db_column='END_DATUM', blank=True, null=True)  # Field name made lowercase.
    absetzgrund = models.CharField(db_column='ABSETZGRUND', max_length=255, blank=True, null=True)  # Field name made lowercase.
    anmerkung = models.TextField(db_column='ANMERKUNG', blank=True, null=True)  # Field name made lowercase.
    rhythmus = models.IntegerField(db_column='RHYTHMUS')  # Field name made lowercase.
    anzeige = models.IntegerField(db_column='ANZEIGE', blank=True, null=True)  # Field name made lowercase.
    nachts = models.FloatField(db_column='NACHTS')  # Field name made lowercase.
    fruehnachts = models.FloatField(db_column='FRUEHNACHTS')  # Field name made lowercase.
    morgens = models.FloatField(db_column='MORGENS')  # Field name made lowercase.
    mittags = models.FloatField(db_column='MITTAGS')  # Field name made lowercase.
    nachmittags = models.FloatField(db_column='NACHMITTAGS')  # Field name made lowercase.
    abends = models.FloatField(db_column='ABENDS')  # Field name made lowercase.
    verschreibungsgrund = models.CharField(db_column='VERSCHREIBUNGSGRUND', max_length=255, blank=True, null=True)  # Field name made lowercase.
    fremdarzt = models.CharField(db_column='FREMDARZT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    dm_end_datum = models.DateField(db_column='DM_END_DATUM', blank=True, null=True)  # Field name made lowercase.
    rezeptur_rezept_name = models.CharField(db_column='REZEPTUR_REZEPT_NAME', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'MEDIKAMENT'


class MedikamentMeldung(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    benutzer = models.CharField(db_column='BENUTZER', max_length=255)  # Field name made lowercase.
    pzn = models.IntegerField(db_column='PZN')  # Field name made lowercase.
    wert = models.IntegerField(db_column='WERT')  # Field name made lowercase.
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'MEDIKAMENT_MELDUNG'
        unique_together = (('benutzer', 'pzn'),)


class MedizinfragebogenFragenStamm(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    fragebogen_id = models.IntegerField(db_column='FRAGEBOGEN_ID', blank=True, null=True)  # Field name made lowercase.
    fragen_kuerzel = models.CharField(db_column='FRAGEN_KUERZEL', max_length=50)  # Field name made lowercase.
    fragen_sprache = models.CharField(db_column='FRAGEN_SPRACHE', max_length=11)  # Field name made lowercase.
    reihenfolge_nummer = models.IntegerField(db_column='REIHENFOLGE_NUMMER', blank=True, null=True)  # Field name made lowercase.
    aktiv = models.IntegerField(db_column='AKTIV', blank=True, null=True)  # Field name made lowercase.
    trennlinie = models.IntegerField(db_column='TRENNLINIE', blank=True, null=True)  # Field name made lowercase.
    nummerierung = models.IntegerField(db_column='NUMMERIERUNG', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'MEDIZINFRAGEBOGEN_FRAGEN_STAMM'
        unique_together = (('fragebogen_id', 'fragen_kuerzel', 'fragen_sprache'),)


class MedizinfragebogenStamm(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', unique=True, max_length=255, blank=True, null=True)  # Field name made lowercase.
    mandanten = models.CharField(db_column='MANDANTEN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    einstellungen = models.TextField(db_column='EINSTELLUNGEN', blank=True, null=True)  # Field name made lowercase.
    webstatus = models.IntegerField(db_column='WEBSTATUS', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'MEDIZINFRAGEBOGEN_STAMM'


class MedizinfrageAntworten(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    patient_id = models.IntegerField(db_column='PATIENT_ID', blank=True, null=True)  # Field name made lowercase.
    medizinfragebogen_fragen_id = models.IntegerField(db_column='MEDIZINFRAGEBOGEN_FRAGEN_ID', blank=True, null=True)  # Field name made lowercase.
    fragen_kuerzel = models.CharField(db_column='FRAGEN_KUERZEL', max_length=50, blank=True, null=True)  # Field name made lowercase.
    fragen_sprache = models.CharField(db_column='FRAGEN_SPRACHE', max_length=11, blank=True, null=True)  # Field name made lowercase.
    fragen_antwort = models.IntegerField(db_column='FRAGEN_ANTWORT', blank=True, null=True)  # Field name made lowercase.
    fragen_bemerkung = models.CharField(db_column='FRAGEN_BEMERKUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    fragen_status = models.IntegerField(db_column='FRAGEN_STATUS', blank=True, null=True)  # Field name made lowercase.
    fragen_aenderung = models.IntegerField(db_column='FRAGEN_AENDERUNG', blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    doku_id = models.IntegerField(db_column='DOKU_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'MEDIZINFRAGE_ANTWORTEN'
        unique_together = (('fragen_kuerzel', 'fragen_sprache', 'patient_id'), ('fragen_kuerzel', 'fragen_sprache', 'doku_id'), ('patient_id', 'fragen_kuerzel', 'fragen_sprache'),)


class MedizinfrageStamm(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    kuerzel = models.CharField(db_column='KUERZEL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    frage_text = models.TextField(db_column='FRAGE_TEXT', blank=True, null=True)  # Field name made lowercase.
    sprache = models.CharField(db_column='SPRACHE', max_length=10, blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)  # Field name made lowercase.
    kategorie = models.IntegerField(db_column='KATEGORIE', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'MEDIZINFRAGE_STAMM'
        unique_together = (('kuerzel', 'sprache'),)


class Medizinstatus(models.Model):
    doku_id = models.IntegerField(db_column='DOKU_ID', unique=True)  # Field name made lowercase.
    medizinstatus_typ = models.IntegerField(db_column='MEDIZINSTATUS_TYP', blank=True, null=True)  # Field name made lowercase.
    medizinstatus_status = models.IntegerField(db_column='MEDIZINSTATUS_STATUS', blank=True, null=True)  # Field name made lowercase.
    sprache = models.CharField(db_column='SPRACHE', max_length=11, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'MEDIZINSTATUS'


class MedizinstatusAntworten(models.Model):
    medizinstatusdokuid = models.IntegerField(db_column='MedizinStatusDokuID', primary_key=True)  # Field name made lowercase.
    medizinfragestammid = models.IntegerField(db_column='MedizinFrageStammID', blank=True, null=True)  # Field name made lowercase.
    medizinfragestammkuerzel = models.CharField(db_column='MedizinFrageStammKuerzel', max_length=50)  # Field name made lowercase.
    medizinfragestammsprache = models.CharField(db_column='MedizinFrageStammSprache', max_length=11)  # Field name made lowercase.
    antwort = models.IntegerField(db_column='Antwort', blank=True, null=True)  # Field name made lowercase.
    antwortbemerkung = models.TextField(db_column='AntwortBemerkung', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'MEDIZINSTATUS_ANTWORTEN'
        unique_together = (('medizinstatusdokuid', 'medizinfragestammkuerzel', 'medizinfragestammsprache'),)


class Mkpbuch(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    mandant_id = models.IntegerField(db_column='MANDANT_ID', blank=True, null=True)  # Field name made lowercase.
    patient_id = models.IntegerField(db_column='PATIENT_ID', blank=True, null=True)  # Field name made lowercase.
    untersuchung_id = models.IntegerField(db_column='UNTERSUCHUNG_ID', blank=True, null=True)  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)  # Field name made lowercase.
    subtyp = models.IntegerField(db_column='SUBTYP', blank=True, null=True)  # Field name made lowercase.
    termin_vorlage_kennung = models.IntegerField(db_column='TERMIN_VORLAGE_KENNUNG', blank=True, null=True)  # Field name made lowercase.
    datum = models.DateField(db_column='DATUM', blank=True, null=True)  # Field name made lowercase.
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ergebnis = models.IntegerField(db_column='ERGEBNIS', blank=True, null=True)  # Field name made lowercase.
    bemerkung = models.TextField(db_column='BEMERKUNG', blank=True, null=True)  # Field name made lowercase.
    freitext1 = models.TextField(db_column='FREITEXT1', blank=True, null=True)  # Field name made lowercase.
    freitext2 = models.TextField(db_column='FREITEXT2', blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)  # Field name made lowercase.
    system_datum = models.DateField(db_column='SYSTEM_DATUM', blank=True, null=True)  # Field name made lowercase.
    system_uhrzeit = models.TimeField(db_column='SYSTEM_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    mkp_kennung = models.IntegerField(db_column='MKP_KENNUNG', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'MKPBUCH'


class NachrichtVersand(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    nachricht_art = models.IntegerField(db_column='NACHRICHT_ART', blank=True, null=True)  # Field name made lowercase.
    lokale_daten_id = models.IntegerField(db_column='LOKALE_DATEN_ID', blank=True, null=True)  # Field name made lowercase.
    nachricht_typ = models.IntegerField(db_column='NACHRICHT_TYP', blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)  # Field name made lowercase.
    empfaenger = models.CharField(db_column='EMPFAENGER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    betreff = models.CharField(db_column='BETREFF', max_length=255, blank=True, null=True)  # Field name made lowercase.
    nachricht = models.TextField(db_column='NACHRICHT', blank=True, null=True)  # Field name made lowercase.
    versand_datum = models.DateField(db_column='VERSAND_DATUM', blank=True, null=True)  # Field name made lowercase.
    versand_ablaufdatum = models.DateField(db_column='VERSAND_ABLAUFDATUM', blank=True, null=True)  # Field name made lowercase.
    web_status = models.IntegerField(db_column='WEB_STATUS', blank=True, null=True)  # Field name made lowercase.
    web_id = models.IntegerField(db_column='WEB_ID', blank=True, null=True)  # Field name made lowercase.
    web_angelegt = models.DateTimeField(db_column='WEB_ANGELEGT', blank=True, null=True)  # Field name made lowercase.
    web_versand = models.DateTimeField(db_column='WEB_VERSAND', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NACHRICHT_VERSAND'
        unique_together = (('nachricht_art', 'lokale_daten_id', 'nachricht_typ'),)


class Nlg(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    patient_id = models.IntegerField(db_column='PATIENT_ID')  # Field name made lowercase.
    doku_id = models.IntegerField(db_column='DOKU_ID')  # Field name made lowercase.
    nummer = models.IntegerField(db_column='NUMMER', blank=True, null=True)  # Field name made lowercase.
    geschwindigkeit_re = models.FloatField(db_column='GESCHWINDIGKEIT_RE', blank=True, null=True)  # Field name made lowercase.
    strecke_re = models.FloatField(db_column='STRECKE_RE', blank=True, null=True)  # Field name made lowercase.
    geschwindigkeit_li = models.FloatField(db_column='GESCHWINDIGKEIT_LI', blank=True, null=True)  # Field name made lowercase.
    strecke_li = models.FloatField(db_column='STRECKE_LI', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NLG'


class Operation(models.Model):
    operation_dokuid = models.IntegerField(db_column='OPERATION_DOKUID', unique=True)  # Field name made lowercase.
    op_team = models.TextField(db_column='OP_TEAM', blank=True, null=True)  # Field name made lowercase.
    op_bezeichnung = models.CharField(db_column='OP_BEZEICHNUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    op_startuhrzeit = models.TimeField(db_column='OP_STARTUHRZEIT', blank=True, null=True)  # Field name made lowercase.
    op_enduhrzeit = models.TimeField(db_column='OP_ENDUHRZEIT', blank=True, null=True)  # Field name made lowercase.
    komplikationen = models.TextField(db_column='KOMPLIKATIONEN', blank=True, null=True)  # Field name made lowercase.
    lagerung = models.CharField(db_column='LAGERUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    narkose_art = models.CharField(db_column='NARKOSE_ART', max_length=255, blank=True, null=True)  # Field name made lowercase.
    narkose_mittel = models.CharField(db_column='NARKOSE_MITTEL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    narkose_zusatztext = models.TextField(db_column='NARKOSE_ZUSATZTEXT', blank=True, null=True)  # Field name made lowercase.
    op_verlauf = models.TextField(db_column='OP_VERLAUF', blank=True, null=True)  # Field name made lowercase.
    diagnose = models.TextField(db_column='DIAGNOSE', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'OPERATION'


class Ops(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    patient_id = models.IntegerField(db_column='PATIENT_ID')  # Field name made lowercase.
    opdoku_id = models.IntegerField(db_column='OPDOKU_ID')  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS')  # Field name made lowercase.
    datum = models.DateField(db_column='DATUM', blank=True, null=True)  # Field name made lowercase.
    uhrzeit = models.TimeField(db_column='UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    schluessel = models.CharField(db_column='SCHLUESSEL', max_length=50, blank=True, null=True)  # Field name made lowercase.
    seitenlokalisation = models.CharField(db_column='SEITENLOKALISATION', max_length=5)  # Field name made lowercase.
    ops_text = models.CharField(db_column='OPS_TEXT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ops_datum = models.DateField(db_column='OPS_DATUM', blank=True, null=True)  # Field name made lowercase.
    ops_uhrzeit = models.TimeField(db_column='OPS_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'OPS'


class Organisation(models.Model):
    id = models.IntegerField(db_column='ID', primary_key=True)  # Field name made lowercase.
    typ = models.CharField(db_column='TYP', max_length=50, blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    notiz = models.TextField(db_column='NOTIZ', blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)  # Field name made lowercase.
    kennwort = models.CharField(db_column='KENNWORT', max_length=50, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ORGANISATION'


class Patient(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    kue = models.CharField(db_column='KUE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    anrede = models.CharField(db_column='ANREDE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    titel = models.CharField(db_column='TITEL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    vorname = models.CharField(db_column='VORNAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    wvorname = models.CharField(db_column='WVORNAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    name = models.CharField(db_column='NAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    gebname = models.CharField(db_column='GEBNAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    namenszusatz = models.CharField(db_column='NAMENSZUSATZ', max_length=255, blank=True, null=True)  # Field name made lowercase.
    namensvorsatz = models.CharField(db_column='NAMENSVORSATZ', max_length=255, blank=True, null=True)  # Field name made lowercase.
    gebdatum = models.DateField(db_column='GEBDATUM', blank=True, null=True)  # Field name made lowercase.
    bild = models.TextField(db_column='BILD', blank=True, null=True)  # Field name made lowercase.
    notizen = models.TextField(db_column='NOTIZEN', blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    adresse_typ = models.IntegerField(db_column='ADRESSE_TYP', blank=True, null=True)  # Field name made lowercase.
    strasse = models.CharField(db_column='STRASSE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    hausnummer = models.CharField(db_column='HAUSNUMMER', max_length=30, blank=True, null=True)  # Field name made lowercase.
    adresszusatz = models.CharField(db_column='ADRESSZUSATZ', max_length=255, blank=True, null=True)  # Field name made lowercase.
    plz = models.CharField(db_column='PLZ', max_length=50, blank=True, null=True)  # Field name made lowercase.
    ort = models.CharField(db_column='ORT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    land = models.CharField(db_column='LAND', max_length=255, blank=True, null=True)  # Field name made lowercase.
    anamnese1 = models.TextField(db_column='ANAMNESE1', blank=True, null=True)  # Field name made lowercase.
    anamnese2 = models.TextField(db_column='ANAMNESE2', blank=True, null=True)  # Field name made lowercase.
    anamnese3 = models.TextField(db_column='ANAMNESE3', blank=True, null=True)  # Field name made lowercase.
    telefon = models.CharField(db_column='TELEFON', max_length=50, blank=True, null=True)  # Field name made lowercase.
    telefon_2 = models.CharField(db_column='TELEFON_2', max_length=50, blank=True, null=True)  # Field name made lowercase.
    fax = models.CharField(db_column='FAX', max_length=50, blank=True, null=True)  # Field name made lowercase.
    mobil = models.CharField(db_column='MOBIL', max_length=50, blank=True, null=True)  # Field name made lowercase.
    email = models.CharField(db_column='EMAIL', max_length=100, blank=True, null=True)  # Field name made lowercase.
    bank = models.CharField(db_column='BANK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bank_plz = models.CharField(db_column='BANK_PLZ', max_length=50, blank=True, null=True)  # Field name made lowercase.
    bank_ort = models.CharField(db_column='BANK_ORT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bank_land = models.CharField(db_column='BANK_LAND', max_length=255, blank=True, null=True)  # Field name made lowercase.
    kto_nr = models.CharField(db_column='KTO_NR', max_length=50, blank=True, null=True)  # Field name made lowercase.
    blz = models.CharField(db_column='BLZ', max_length=50, blank=True, null=True)  # Field name made lowercase.
    kto_inhaber = models.CharField(db_column='KTO_INHABER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS')  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    versicherungsnr = models.CharField(db_column='VERSICHERUNGSNR', max_length=50, blank=True, null=True)  # Field name made lowercase.
    cave = models.CharField(db_column='CAVE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    info = models.CharField(db_column='INFO', max_length=255, blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung_datum = models.DateField(db_column='LETZTE_AENDERUNG_DATUM', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung_uhrzeit = models.TimeField(db_column='LETZTE_AENDERUNG_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    freitext1 = models.TextField(db_column='FREITEXT1', blank=True, null=True)  # Field name made lowercase.
    freitext2 = models.TextField(db_column='FREITEXT2', blank=True, null=True)  # Field name made lowercase.
    sterbedatum = models.DateField(db_column='STERBEDATUM', blank=True, null=True)  # Field name made lowercase.
    entbindungsdatum = models.DateField(db_column='ENTBINDUNGSDATUM', blank=True, null=True)  # Field name made lowercase.
    nationalitaet = models.CharField(db_column='NATIONALITAET', max_length=255, blank=True, null=True)  # Field name made lowercase.
    familienstand = models.CharField(db_column='FAMILIENSTAND', max_length=255, blank=True, null=True)  # Field name made lowercase.
    kennzeichen1 = models.BigIntegerField(db_column='KENNZEICHEN1')  # Field name made lowercase.
    kennzeichen2 = models.IntegerField(db_column='KENNZEICHEN2')  # Field name made lowercase.
    zusatz = models.TextField(db_column='ZUSATZ', blank=True, null=True)  # Field name made lowercase.
    einstellungen = models.TextField(db_column='EINSTELLUNGEN')  # Field name made lowercase.
    briefdaten = models.TextField(db_column='BRIEFDATEN', blank=True, null=True)  # Field name made lowercase.
    altsystem_id = models.BigIntegerField(db_column='ALTSYSTEM_ID', unique=True, blank=True, null=True)  # Field name made lowercase.
    kis_id = models.BigIntegerField(db_column='KIS_ID', unique=True, blank=True, null=True)  # Field name made lowercase.
    kennzeichen3 = models.IntegerField(db_column='KENNZEICHEN3', blank=True, null=True)  # Field name made lowercase.
    hausarzt_vpnr = models.CharField(db_column='HAUSARZT_VPNR', max_length=50, blank=True, null=True)  # Field name made lowercase.
    abweichende_anschrift = models.TextField(db_column='ABWEICHENDE_ANSCHRIFT', blank=True, null=True)  # Field name made lowercase.
    brief_versand = models.TextField(db_column='BRIEF_VERSAND', blank=True, null=True)  # Field name made lowercase.
    bank_iban = models.CharField(db_column='BANK_IBAN', max_length=50, blank=True, null=True)  # Field name made lowercase.
    bank_bic = models.CharField(db_column='BANK_BIC', max_length=50, blank=True, null=True)  # Field name made lowercase.
    aufnahme_zeit = models.DateTimeField(db_column='AUFNAHME_ZEIT', blank=True, null=True)  # Field name made lowercase.
    geschlecht = models.IntegerField(db_column='GESCHLECHT')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'PATIENT'


class Patientmedizin(models.Model):
    patient_id = models.IntegerField(db_column='PATIENT_ID', primary_key=True)  # Field name made lowercase.
    anzschwangerschaften = models.IntegerField(db_column='ANZSCHWANGERSCHAFTEN', blank=True, null=True)  # Field name made lowercase.
    anzgeburten = models.IntegerField(db_column='ANZGEBURTEN', blank=True, null=True)  # Field name made lowercase.
    anzaborte = models.IntegerField(db_column='ANZABORTE', blank=True, null=True)  # Field name made lowercase.
    anzkinder = models.IntegerField(db_column='ANZKINDER', blank=True, null=True)  # Field name made lowercase.
    blutgruppe = models.CharField(db_column='BLUTGRUPPE', max_length=5, blank=True, null=True)  # Field name made lowercase.
    anamnese1 = models.TextField(db_column='ANAMNESE1', blank=True, null=True)  # Field name made lowercase.
    anam1_lock = models.CharField(db_column='ANAM1_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    anamnese2 = models.TextField(db_column='ANAMNESE2', blank=True, null=True)  # Field name made lowercase.
    anam2_lock = models.CharField(db_column='ANAM2_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    anamnese3 = models.TextField(db_column='ANAMNESE3', blank=True, null=True)  # Field name made lowercase.
    anam3_lock = models.CharField(db_column='ANAM3_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    cave = models.CharField(db_column='CAVE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    info = models.CharField(db_column='INFO', max_length=255, blank=True, null=True)  # Field name made lowercase.
    beruf = models.CharField(db_column='BERUF', max_length=255, blank=True, null=True)  # Field name made lowercase.
    familienstand = models.CharField(db_column='FAMILIENSTAND', max_length=255, blank=True, null=True)  # Field name made lowercase.
    risikofaktoren = models.CharField(db_column='RISIKOFAKTOREN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    einstellungen = models.TextField(db_column='EINSTELLUNGEN', blank=True, null=True)  # Field name made lowercase.
    letzte_regel = models.DateField(db_column='LETZTE_REGEL', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'PATIENTMEDIZIN'


class Patientzusatz(models.Model):
    patient_id = models.IntegerField(db_column='PATIENT_ID', primary_key=True)  # Field name made lowercase.
    nsf = models.IntegerField(db_column='NSF', blank=True, null=True)  # Field name made lowercase.
    th_stop = models.IntegerField(db_column='TH_Stop', blank=True, null=True)  # Field name made lowercase.
    anam1_lock = models.CharField(db_column='ANAM1_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    anam2_lock = models.CharField(db_column='ANAM2_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    anam3_lock = models.CharField(db_column='ANAM3_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    sprache = models.CharField(db_column='SPRACHE', max_length=5, blank=True, null=True)  # Field name made lowercase.
    hausarzt = models.TextField(db_column='HAUSARZT', blank=True, null=True)  # Field name made lowercase.
    anzschwangerschaften = models.IntegerField(db_column='ANZSCHWANGERSCHAFTEN', blank=True, null=True)  # Field name made lowercase.
    anzgeburten = models.IntegerField(db_column='ANZGEBURTEN', blank=True, null=True)  # Field name made lowercase.
    anzkinder = models.IntegerField(db_column='ANZKINDER', blank=True, null=True)  # Field name made lowercase.
    kennzeichen1 = models.BigIntegerField(db_column='KENNZEICHEN1', blank=True, null=True)  # Field name made lowercase.
    kennzeichen2 = models.IntegerField(db_column='KENNZEICHEN2', blank=True, null=True)  # Field name made lowercase.
    kennzeichen3 = models.IntegerField(db_column='KENNZEICHEN3', blank=True, null=True)  # Field name made lowercase.
    kennzeichen4 = models.IntegerField(db_column='KENNZEICHEN4', blank=True, null=True)  # Field name made lowercase.
    kennzeichen5 = models.IntegerField(db_column='KENNZEICHEN5', blank=True, null=True)  # Field name made lowercase.
    kennzeichen6 = models.IntegerField(db_column='KENNZEICHEN6', blank=True, null=True)  # Field name made lowercase.
    kennzeichen7 = models.IntegerField(db_column='KENNZEICHEN7', blank=True, null=True)  # Field name made lowercase.
    kennzeichen8 = models.IntegerField(db_column='KENNZEICHEN8', blank=True, null=True)  # Field name made lowercase.
    kennzeichen9 = models.IntegerField(db_column='KENNZEICHEN9', blank=True, null=True)  # Field name made lowercase.
    kennzeichen10 = models.IntegerField(db_column='KENNZEICHEN10', blank=True, null=True)  # Field name made lowercase.
    nationalitaet = models.CharField(db_column='NATIONALITAET', max_length=5, blank=True, null=True)  # Field name made lowercase.
    blutgruppe = models.CharField(db_column='BLUTGRUPPE', max_length=5, blank=True, null=True)  # Field name made lowercase.
    quittung = models.IntegerField(db_column='QUITTUNG')  # Field name made lowercase.
    suchname = models.CharField(db_column='SUCHNAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    serienbrief_versandarten = models.CharField(db_column='SERIENBRIEF_VERSANDARTEN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    serienbrief_stdversandart = models.CharField(db_column='SERIENBRIEF_STDVERSANDART', max_length=50, blank=True, null=True)  # Field name made lowercase.
    serienbrief_typen = models.TextField(db_column='SERIENBRIEF_TYPEN', blank=True, null=True)  # Field name made lowercase.
    partner_patient_id = models.IntegerField(db_column='PARTNER_PATIENT_ID', blank=True, null=True)  # Field name made lowercase.
    web_status = models.IntegerField(db_column='WEB_STATUS', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'PATIENTZUSATZ'


class Person(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    anrede = models.CharField(db_column='ANREDE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    titel = models.CharField(db_column='TITEL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    name = models.CharField(db_column='NAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    vorname = models.CharField(db_column='VORNAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    wvorname = models.CharField(db_column='WVORNAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    geburtsname = models.CharField(db_column='GEBURTSNAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    name_vorsatz = models.CharField(db_column='NAME_VORSATZ', max_length=255, blank=True, null=True)  # Field name made lowercase.
    name_zusatz = models.CharField(db_column='NAME_ZUSATZ', max_length=255, blank=True, null=True)  # Field name made lowercase.
    kuerzel = models.CharField(db_column='KUERZEL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    geburtsdatum = models.DateField(db_column='GEBURTSDATUM', blank=True, null=True)  # Field name made lowercase.
    sterbedatum = models.DateField(db_column='STERBEDATUM', blank=True, null=True)  # Field name made lowercase.
    adresse = models.IntegerField(db_column='ADRESSE', blank=True, null=True)  # Field name made lowercase.
    post_adresse = models.IntegerField(db_column='POST_ADRESSE', blank=True, null=True)  # Field name made lowercase.
    liefer_adresse = models.IntegerField(db_column='LIEFER_ADRESSE', blank=True, null=True)  # Field name made lowercase.
    rechnung_adresse = models.IntegerField(db_column='RECHNUNG_ADRESSE', blank=True, null=True)  # Field name made lowercase.
    kommunikation = models.IntegerField(db_column='KOMMUNIKATION', blank=True, null=True)  # Field name made lowercase.
    konto = models.IntegerField(db_column='KONTO', blank=True, null=True)  # Field name made lowercase.
    bild = models.TextField(db_column='BILD', blank=True, null=True)  # Field name made lowercase.
    notizen = models.TextField(db_column='NOTIZEN', blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'PERSON'


class Phlebostatus(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    patient_id = models.IntegerField(db_column='PATIENT_ID', blank=True, null=True)  # Field name made lowercase.
    doku_id = models.IntegerField(db_column='DOKU_ID', blank=True, null=True)  # Field name made lowercase.
    typ = models.CharField(db_column='TYP', max_length=50, blank=True, null=True)  # Field name made lowercase.
    datum = models.DateField(db_column='DATUM', blank=True, null=True)  # Field name made lowercase.
    properties = models.TextField(db_column='PROPERTIES', blank=True, null=True)  # Field name made lowercase.
    bemerkung = models.TextField(db_column='BEMERKUNG', blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)  # Field name made lowercase.
    system_datum = models.DateField(db_column='SYSTEM_DATUM', blank=True, null=True)  # Field name made lowercase.
    system_uhrzeit = models.TimeField(db_column='SYSTEM_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'PHLEBOSTATUS'


class Physis(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    patient_id = models.IntegerField(db_column='PATIENT_ID')  # Field name made lowercase.
    typ = models.CharField(db_column='TYP', max_length=50)  # Field name made lowercase.
    datum = models.DateField(db_column='DATUM', blank=True, null=True)  # Field name made lowercase.
    monat = models.FloatField(db_column='MONAT')  # Field name made lowercase.
    groesse = models.FloatField(db_column='GROESSE', blank=True, null=True)  # Field name made lowercase.
    kopfumfang = models.FloatField(db_column='KOPFUMFANG', blank=True, null=True)  # Field name made lowercase.
    gewicht = models.FloatField(db_column='GEWICHT', blank=True, null=True)  # Field name made lowercase.
    percentile = models.IntegerField(db_column='PERCENTILE', blank=True, null=True)  # Field name made lowercase.
    referenz = models.IntegerField(db_column='REFERENZ', blank=True, null=True)  # Field name made lowercase.
    bemerkung = models.TextField(db_column='BEMERKUNG', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'PHYSIS'


class PhysTherapie(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    th_kuerzel = models.CharField(db_column='TH_KUERZEL', unique=True, max_length=30)  # Field name made lowercase.
    th_bezeichnung = models.CharField(db_column='TH_BEZEICHNUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    th_kkbezeich = models.CharField(db_column='TH_KKBEZEICH', max_length=50, blank=True, null=True)  # Field name made lowercase.
    th_typ = models.CharField(db_column='TH_TYP', max_length=1, blank=True, null=True)  # Field name made lowercase.
    th_max = models.IntegerField(db_column='TH_MAX')  # Field name made lowercase.
    th_def_verrechnungsart = models.IntegerField(db_column='TH_DEF_VERRECHNUNGSART', blank=True, null=True)  # Field name made lowercase.
    th_char = models.CharField(db_column='TH_CHAR', max_length=1, blank=True, null=True)  # Field name made lowercase.
    th_tm = models.FloatField(db_column='TH_TM')  # Field name made lowercase.
    th_km = models.FloatField(db_column='TH_KM')  # Field name made lowercase.
    th_einzeltermin = models.IntegerField(db_column='TH_EINZELTERMIN')  # Field name made lowercase.
    gt_typ = models.CharField(db_column='GT_TYP', max_length=50, blank=True, null=True)  # Field name made lowercase.
    th_vf = models.IntegerField(db_column='TH_VF')  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    gruppe = models.IntegerField(db_column='GRUPPE')  # Field name made lowercase.
    einstellung = models.TextField(db_column='EINSTELLUNG', blank=True, null=True)  # Field name made lowercase.
    benutzer_id = models.IntegerField(db_column='BENUTZER_ID', blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)  # Field name made lowercase.
    privat = models.CharField(db_column='PRIVAT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    kasse = models.CharField(db_column='KASSE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ermaechtigung = models.CharField(db_column='ERMAECHTIGUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    feiertagstermin = models.IntegerField(db_column='FEIERTAGSTERMIN')  # Field name made lowercase.
    kalender = models.CharField(db_column='KALENDER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    warteliste = models.CharField(db_column='WARTELISTE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    web_status = models.IntegerField(db_column='WEB_STATUS', blank=True, null=True)  # Field name made lowercase.
    web_name = models.CharField(db_column='WEB_NAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    web_beschreibung = models.TextField(db_column='WEB_BESCHREIBUNG', blank=True, null=True)  # Field name made lowercase.
    web_hinweistext = models.TextField(db_column='WEB_HINWEISTEXT', blank=True, null=True)  # Field name made lowercase.
    web_properties = models.TextField(db_column='WEB_PROPERTIES', blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)  # Field name made lowercase.
    geraet = models.CharField(db_column='GERAET', max_length=255, blank=True, null=True)  # Field name made lowercase.
    modalitaet = models.IntegerField(db_column='MODALITAET', blank=True, null=True)  # Field name made lowercase.
    typ_profil = models.CharField(db_column='TYP_PROFIL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    profil_parameter_ids = models.CharField(db_column='PROFIL_PARAMETER_IDS', max_length=255, blank=True, null=True)  # Field name made lowercase.
    favorit_position = models.IntegerField(db_column='FAVORIT_POSITION', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'PHYS_THERAPIE'


class PhysTherapieRegion(models.Model):
    therapie_id = models.IntegerField(db_column='THERAPIE_ID', primary_key=True)  # Field name made lowercase.
    region_id = models.IntegerField(db_column='REGION_ID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'PHYS_THERAPIE_REGION'
        unique_together = (('therapie_id', 'region_id'),)


class PlzAut(models.Model):
    plz = models.CharField(db_column='PLZ', max_length=10, blank=True, null=True)  # Field name made lowercase.
    ort = models.CharField(db_column='ORT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bundesland = models.CharField(db_column='BUNDESLAND', max_length=255, blank=True, null=True)  # Field name made lowercase.
    zusatz = models.CharField(db_column='ZUSATZ', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ort2 = models.CharField(db_column='ORT2', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'PLZ_AUT'


class PlzDe(models.Model):
    plz = models.CharField(db_column='PLZ', max_length=5, blank=True, null=True)  # Field name made lowercase.
    ort = models.CharField(db_column='ORT', max_length=31, blank=True, null=True)  # Field name made lowercase.
    bundesland = models.CharField(db_column='BUNDESLAND', max_length=50, blank=True, null=True)  # Field name made lowercase.
    zusatz = models.CharField(db_column='ZUSATZ', max_length=40, blank=True, null=True)  # Field name made lowercase.
    ort2 = models.CharField(db_column='ORT2', max_length=24, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'PLZ_DE'


class PraxisDaten(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    kuerzel = models.CharField(db_column='KUERZEL', unique=True, max_length=50)  # Field name made lowercase.
    einrichtung_typ = models.IntegerField(db_column='EINRICHTUNG_TYP', blank=True, null=True)  # Field name made lowercase.
    hv_nummer = models.CharField(db_column='HV_NUMMER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    dv_nummer = models.CharField(db_column='DV_NUMMER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    adress_code = models.CharField(db_column='ADRESS_CODE', max_length=50, blank=True, null=True)  # Field name made lowercase.
    fachgebiet = models.IntegerField(db_column='FACHGEBIET')  # Field name made lowercase.
    kom_anrede = models.CharField(db_column='KOM_ANREDE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    adresse = models.TextField(db_column='ADRESSE', blank=True, null=True)  # Field name made lowercase.
    kommunikation = models.CharField(db_column='KOMMUNIKATION', max_length=255, blank=True, null=True)  # Field name made lowercase.
    konto = models.TextField(db_column='KONTO', blank=True, null=True)  # Field name made lowercase.
    datum = models.DateField(db_column='DATUM', blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS')  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP')  # Field name made lowercase.
    gueltigkeit = models.CharField(db_column='GUELTIGKEIT', max_length=50, blank=True, null=True)  # Field name made lowercase.
    notizen = models.TextField(db_column='NOTIZEN', blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    kasse1_id = models.IntegerField(db_column='KASSE1_ID')  # Field name made lowercase.
    kasse1_typ = models.CharField(db_column='KASSE1_TYP', max_length=50, blank=True, null=True)  # Field name made lowercase.
    kasse1_bland = models.IntegerField(db_column='KASSE1_BLAND')  # Field name made lowercase.
    kasse2_id = models.IntegerField(db_column='KASSE2_ID')  # Field name made lowercase.
    kasse2_typ = models.CharField(db_column='KASSE2_TYP', max_length=50, blank=True, null=True)  # Field name made lowercase.
    kasse2_bland = models.IntegerField(db_column='KASSE2_BLAND')  # Field name made lowercase.
    kasse3_id = models.IntegerField(db_column='KASSE3_ID')  # Field name made lowercase.
    kasse3_typ = models.CharField(db_column='KASSE3_TYP', max_length=50, blank=True, null=True)  # Field name made lowercase.
    kasse3_bland = models.IntegerField(db_column='KASSE3_BLAND')  # Field name made lowercase.
    kasse4_id = models.IntegerField(db_column='KASSE4_ID')  # Field name made lowercase.
    kasse4_typ = models.CharField(db_column='KASSE4_TYP', max_length=50, blank=True, null=True)  # Field name made lowercase.
    kasse4_bland = models.IntegerField(db_column='KASSE4_BLAND')  # Field name made lowercase.
    kasse5_id = models.IntegerField(db_column='KASSE5_ID')  # Field name made lowercase.
    kasse5_typ = models.CharField(db_column='KASSE5_TYP', max_length=50, blank=True, null=True)  # Field name made lowercase.
    kasse5_bland = models.IntegerField(db_column='KASSE5_BLAND')  # Field name made lowercase.
    briefdaten = models.TextField(db_column='BRIEFDATEN', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung_datum = models.DateField(db_column='LETZTE_AENDERUNG_DATUM', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung_uhrzeit = models.TimeField(db_column='LETZTE_AENDERUNG_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    einstellungen = models.TextField(db_column='EINSTELLUNGEN', blank=True, null=True)  # Field name made lowercase.
    gnw_einstellungen = models.TextField(db_column='GNW_EINSTELLUNGEN', blank=True, null=True)  # Field name made lowercase.
    pvs_einstellungen = models.TextField(db_column='PVS_EINSTELLUNGEN', blank=True, null=True)  # Field name made lowercase.
    anschrift_zeile1 = models.CharField(db_column='ANSCHRIFT_ZEILE1', max_length=255, blank=True, null=True)  # Field name made lowercase.
    anschrift_zeile2 = models.CharField(db_column='ANSCHRIFT_ZEILE2', max_length=255, blank=True, null=True)  # Field name made lowercase.
    kasse6_id = models.IntegerField(db_column='KASSE6_ID')  # Field name made lowercase.
    kasse6_typ = models.CharField(db_column='KASSE6_TYP', max_length=50, blank=True, null=True)  # Field name made lowercase.
    kasse6_bland = models.IntegerField(db_column='KASSE6_BLAND')  # Field name made lowercase.
    kasse7_id = models.IntegerField(db_column='KASSE7_ID')  # Field name made lowercase.
    kasse7_typ = models.CharField(db_column='KASSE7_TYP', max_length=50, blank=True, null=True)  # Field name made lowercase.
    kasse7_bland = models.IntegerField(db_column='KASSE7_BLAND')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'PRAXIS_DATEN'


class Produkt(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    modul = models.CharField(db_column='MODUL', max_length=50, blank=True, null=True)  # Field name made lowercase.
    einstellung = models.CharField(db_column='EINSTELLUNG', max_length=50, blank=True, null=True)  # Field name made lowercase.
    wert = models.TextField(db_column='WERT', blank=True, null=True)  # Field name made lowercase.
    typ = models.CharField(db_column='TYP', max_length=50, blank=True, null=True)  # Field name made lowercase.
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    beschreibung = models.CharField(db_column='BESCHREIBUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    produkt = models.IntegerField(db_column='PRODUKT', blank=True, null=True)  # Field name made lowercase.
    land = models.CharField(db_column='LAND', max_length=10, blank=True, null=True)  # Field name made lowercase.
    lizenz = models.CharField(db_column='LIZENZ', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'PRODUKT'


class ProtokollAbrechnung(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP')  # Field name made lowercase.
    mandant_id = models.IntegerField(db_column='MANDANT_ID')  # Field name made lowercase.
    sitzungs_nr = models.BigIntegerField(db_column='SITZUNGS_NR', blank=True, null=True)  # Field name made lowercase.
    datum = models.DateField(db_column='DATUM', blank=True, null=True)  # Field name made lowercase.
    uhrzeit = models.TimeField(db_column='UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    abr_datei = models.CharField(db_column='ABR_DATEI', max_length=255, blank=True, null=True)  # Field name made lowercase.
    protokoll = models.CharField(db_column='PROTOKOLL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    quartal = models.IntegerField(db_column='QUARTAL', blank=True, null=True)  # Field name made lowercase.
    jahr = models.IntegerField(db_column='JAHR', blank=True, null=True)  # Field name made lowercase.
    kasse = models.CharField(db_column='KASSE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS')  # Field name made lowercase.
    uuid = models.CharField(db_column='UUID', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'PROTOKOLL_ABRECHNUNG'


class ProtokollDaten(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    daten_id = models.IntegerField(db_column='DATEN_ID')  # Field name made lowercase.
    daten_typ = models.IntegerField(db_column='DATEN_TYP')  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP')  # Field name made lowercase.
    datum = models.DateField(db_column='DATUM', blank=True, null=True)  # Field name made lowercase.
    uhrzeit = models.TimeField(db_column='UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    protokoll_kurztext = models.TextField(db_column='PROTOKOLL_KURZTEXT', blank=True, null=True)  # Field name made lowercase.
    sitzungs_nr = models.BigIntegerField(db_column='SITZUNGS_NR', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'PROTOKOLL_DATEN'


class ProtokollDatensicherung(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)  # Field name made lowercase.
    sitzungs_nr = models.CharField(db_column='SITZUNGS_NR', max_length=255, blank=True, null=True)  # Field name made lowercase.
    datum = models.DateField(db_column='DATUM', blank=True, null=True)  # Field name made lowercase.
    uhrzeit = models.TimeField(db_column='UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    protokoll_datei_name = models.CharField(db_column='PROTOKOLL_DATEI_NAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'PROTOKOLL_DATENSICHERUNG'


class ProtokollDoku(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    doku_id = models.IntegerField(db_column='DOKU_ID')  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP')  # Field name made lowercase.
    datum = models.DateField(db_column='DATUM', blank=True, null=True)  # Field name made lowercase.
    uhrzeit = models.TimeField(db_column='UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    protokoll_kurztext = models.TextField(db_column='PROTOKOLL_KURZTEXT', blank=True, null=True)  # Field name made lowercase.
    sitzungs_nr = models.BigIntegerField(db_column='SITZUNGS_NR', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'PROTOKOLL_DOKU'


class ProtokollEintragSerienbrief(models.Model):
    protokoll_serienbrief_id = models.IntegerField(db_column='PROTOKOLL_SERIENBRIEF_ID')  # Field name made lowercase.
    adressat_id = models.IntegerField(db_column='ADRESSAT_ID', blank=True, null=True)  # Field name made lowercase.
    versandart = models.CharField(db_column='VERSANDART', max_length=255, blank=True, null=True)  # Field name made lowercase.
    status_text = models.TextField(db_column='STATUS_TEXT', blank=True, null=True)  # Field name made lowercase.
    adressat_name = models.CharField(db_column='ADRESSAT_NAME', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'PROTOKOLL_EINTRAG_SERIENBRIEF'


class ProtokollImport(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP')  # Field name made lowercase.
    ermaechtigung_id = models.IntegerField(db_column='ERMAECHTIGUNG_ID')  # Field name made lowercase.
    benutzer = models.CharField(db_column='BENUTZER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    datum = models.DateField(db_column='DATUM', blank=True, null=True)  # Field name made lowercase.
    uhrzeit = models.TimeField(db_column='UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    sitzungs_nr = models.BigIntegerField(db_column='SITZUNGS_NR')  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'PROTOKOLL_IMPORT'


class ProtokollRechner(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    rechner = models.CharField(db_column='RECHNER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    hash = models.CharField(db_column='HASH', max_length=255, blank=True, null=True)  # Field name made lowercase.
    details = models.TextField(db_column='DETAILS', blank=True, null=True)  # Field name made lowercase.
    aktiv = models.IntegerField(db_column='AKTIV', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'PROTOKOLL_RECHNER'


class ProtokollSenden(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP')  # Field name made lowercase.
    ermaechtigung_id = models.IntegerField(db_column='ERMAECHTIGUNG_ID')  # Field name made lowercase.
    sitzungs_nr = models.BigIntegerField(db_column='SITZUNGS_NR', blank=True, null=True)  # Field name made lowercase.
    datum = models.DateField(db_column='DATUM', blank=True, null=True)  # Field name made lowercase.
    uhrzeit = models.TimeField(db_column='UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    sende_alb = models.CharField(db_column='SENDE_ALB', max_length=255, blank=True, null=True)  # Field name made lowercase.
    info_text = models.TextField(db_column='INFO_TEXT', blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'PROTOKOLL_SENDEN'


class ProtokollSerienbrief(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    auswertung_liste_id = models.IntegerField(db_column='AUSWERTUNG_LISTE_ID', blank=True, null=True)  # Field name made lowercase.
    typ = models.CharField(db_column='TYP', max_length=50, blank=True, null=True)  # Field name made lowercase.
    datum = models.DateField(db_column='DATUM', blank=True, null=True)  # Field name made lowercase.
    uhrzeit = models.TimeField(db_column='UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    benutzer_id = models.IntegerField(db_column='BENUTZER_ID', blank=True, null=True)  # Field name made lowercase.
    mandant_id = models.IntegerField(db_column='MANDANT_ID', blank=True, null=True)  # Field name made lowercase.
    druckvorlage_id = models.IntegerField(db_column='DRUCKVORLAGE_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'PROTOKOLL_SERIENBRIEF'


class ProtokollSitzung(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)  # Field name made lowercase.
    rechner = models.CharField(db_column='RECHNER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    os = models.CharField(db_column='OS', max_length=50, blank=True, null=True)  # Field name made lowercase.
    java = models.CharField(db_column='JAVA', max_length=50, blank=True, null=True)  # Field name made lowercase.
    aceto_version = models.CharField(db_column='ACETO_VERSION', max_length=50, blank=True, null=True)  # Field name made lowercase.
    benutzer = models.CharField(db_column='BENUTZER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    datum_an = models.DateField(db_column='DATUM_AN', blank=True, null=True)  # Field name made lowercase.
    uhrzeit_an = models.TimeField(db_column='UHRZEIT_AN', blank=True, null=True)  # Field name made lowercase.
    uhrzeit_aktion = models.TimeField(db_column='UHRZEIT_AKTION', blank=True, null=True)  # Field name made lowercase.
    datum_ab = models.DateField(db_column='DATUM_AB', blank=True, null=True)  # Field name made lowercase.
    uhrzeit_ab = models.TimeField(db_column='UHRZEIT_AB', blank=True, null=True)  # Field name made lowercase.
    dauer = models.BigIntegerField(db_column='DAUER', blank=True, null=True)  # Field name made lowercase.
    sitzungs_nr = models.BigIntegerField(db_column='SITZUNGS_NR', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'PROTOKOLL_SITZUNG'


class ProtokollSpvm(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    sitzungs_nr = models.BigIntegerField(db_column='SITZUNGS_NR', blank=True, null=True)  # Field name made lowercase.
    maximal = models.IntegerField(db_column='MAXIMAL', blank=True, null=True)  # Field name made lowercase.
    frei = models.IntegerField(db_column='FREI', blank=True, null=True)  # Field name made lowercase.
    total = models.IntegerField(db_column='TOTAL', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'PROTOKOLL_SPVM'


class ProtokollStamm(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    stamm_id = models.IntegerField(db_column='STAMM_ID')  # Field name made lowercase.
    stamm_typ = models.IntegerField(db_column='STAMM_TYP')  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP')  # Field name made lowercase.
    datum = models.DateField(db_column='DATUM', blank=True, null=True)  # Field name made lowercase.
    uhrzeit = models.TimeField(db_column='UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    protokoll_kurztext = models.TextField(db_column='PROTOKOLL_KURZTEXT', blank=True, null=True)  # Field name made lowercase.
    sitzungs_nr = models.BigIntegerField(db_column='SITZUNGS_NR', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'PROTOKOLL_STAMM'


class ProtokollVerwaltung(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    verwaltung_id = models.IntegerField(db_column='VERWALTUNG_ID')  # Field name made lowercase.
    verwaltung_typ = models.IntegerField(db_column='VERWALTUNG_TYP')  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP')  # Field name made lowercase.
    datum = models.DateField(db_column='DATUM', blank=True, null=True)  # Field name made lowercase.
    uhrzeit = models.TimeField(db_column='UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    protokoll_kurztext = models.TextField(db_column='PROTOKOLL_KURZTEXT', blank=True, null=True)  # Field name made lowercase.
    sitzungs_nr = models.BigIntegerField(db_column='SITZUNGS_NR', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'PROTOKOLL_VERWALTUNG'


class ProtokollWebtermine(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    sitzungs_nr = models.BigIntegerField(db_column='SITZUNGS_NR', blank=True, null=True)  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP')  # Field name made lowercase.
    zeit = models.DateTimeField(db_column='ZEIT', blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    termin_id_liste = models.TextField(db_column='TERMIN_ID_LISTE', blank=True, null=True)  # Field name made lowercase.
    start_datum = models.DateField(db_column='START_DATUM', blank=True, null=True)  # Field name made lowercase.
    end_datum = models.DateField(db_column='END_DATUM', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'PROTOKOLL_WEBTERMINE'


class Rechnung(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    rechnungdoku_id = models.IntegerField(db_column='RECHNUNGDOKU_ID', unique=True)  # Field name made lowercase.
    untersuchung_id = models.IntegerField(db_column='UNTERSUCHUNG_ID')  # Field name made lowercase.
    re_nr = models.CharField(db_column='RE_NR', unique=True, max_length=50, blank=True, null=True)  # Field name made lowercase.
    re_text = models.TextField(db_column='RE_TEXT', blank=True, null=True)  # Field name made lowercase.
    re_anschrift = models.TextField(db_column='RE_ANSCHRIFT', blank=True, null=True)  # Field name made lowercase.
    re_anschrift_xml = models.TextField(db_column='RE_ANSCHRIFT_XML', blank=True, null=True)  # Field name made lowercase.
    re_absende_anschrift = models.TextField(db_column='RE_ABSENDE_ANSCHRIFT', blank=True, null=True)  # Field name made lowercase.
    re_versandart = models.CharField(db_column='RE_VERSANDART', max_length=255, blank=True, null=True)  # Field name made lowercase.
    re_positionen = models.TextField(db_column='RE_POSITIONEN', blank=True, null=True)  # Field name made lowercase.
    re_anzahl_pos = models.IntegerField(db_column='RE_ANZAHL_POS')  # Field name made lowercase.
    re_summe_netto = models.FloatField(db_column='RE_SUMME_NETTO', blank=True, null=True)  # Field name made lowercase.
    re_summe_netto_1 = models.FloatField(db_column='RE_SUMME_NETTO_1', blank=True, null=True)  # Field name made lowercase.
    re_summe_netto_2 = models.FloatField(db_column='RE_SUMME_NETTO_2', blank=True, null=True)  # Field name made lowercase.
    re_summe_brutto = models.FloatField(db_column='RE_SUMME_BRUTTO', blank=True, null=True)  # Field name made lowercase.
    re_summe_brutto_1 = models.FloatField(db_column='RE_SUMME_BRUTTO_1', blank=True, null=True)  # Field name made lowercase.
    re_summe_brutto_2 = models.FloatField(db_column='RE_SUMME_BRUTTO_2', blank=True, null=True)  # Field name made lowercase.
    re_mwst_1 = models.FloatField(db_column='RE_MWST_1', blank=True, null=True)  # Field name made lowercase.
    re_mwst_2 = models.FloatField(db_column='RE_MWST_2', blank=True, null=True)  # Field name made lowercase.
    re_summe_mwst = models.FloatField(db_column='RE_SUMME_MWST', blank=True, null=True)  # Field name made lowercase.
    re_summe_mwst_1 = models.FloatField(db_column='RE_SUMME_MWST_1', blank=True, null=True)  # Field name made lowercase.
    re_summe_mwst_2 = models.FloatField(db_column='RE_SUMME_MWST_2', blank=True, null=True)  # Field name made lowercase.
    re_waehrung = models.CharField(db_column='RE_WAEHRUNG', max_length=50, blank=True, null=True)  # Field name made lowercase.
    re_unterschreiber = models.CharField(db_column='RE_UNTERSCHREIBER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    re_signed = models.CharField(db_column='RE_SIGNED', max_length=255, blank=True, null=True)  # Field name made lowercase.
    re_offene_summe_brutto = models.FloatField(db_column='RE_OFFENE_SUMME_BRUTTO')  # Field name made lowercase.
    re_zaehler = models.IntegerField(db_column='RE_ZAEHLER', blank=True, null=True)  # Field name made lowercase.
    datum_versendet = models.DateField(db_column='DATUM_VERSENDET', blank=True, null=True)  # Field name made lowercase.
    datum_storniert = models.DateField(db_column='DATUM_STORNIERT', blank=True, null=True)  # Field name made lowercase.
    status_zusatz = models.TextField(db_column='STATUS_ZUSATZ', blank=True, null=True)  # Field name made lowercase.
    mahn_stufe = models.IntegerField(db_column='MAHN_STUFE')  # Field name made lowercase.
    re_jahr = models.IntegerField(db_column='RE_JAHR', blank=True, null=True)  # Field name made lowercase.
    re_mandant_id = models.IntegerField(db_column='RE_MANDANT_ID', blank=True, null=True)  # Field name made lowercase.
    abrechnungsschein_id = models.IntegerField(db_column='ABRECHNUNGSSCHEIN_ID', blank=True, null=True)  # Field name made lowercase.
    diagnosen_xml = models.TextField(db_column='DIAGNOSEN_XML', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'RECHNUNG'
        unique_together = (('re_zaehler', 're_jahr', 're_mandant_id'),)


class Rechnungposition(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    rechnungdoku_id = models.IntegerField(db_column='RECHNUNGDOKU_ID', blank=True, null=True)  # Field name made lowercase.
    pos_nr = models.IntegerField(db_column='POS_NR', blank=True, null=True)  # Field name made lowercase.
    anzahl = models.FloatField(db_column='ANZAHL', blank=True, null=True)  # Field name made lowercase.
    position_datum = models.DateField(db_column='POSITION_DATUM', blank=True, null=True)  # Field name made lowercase.
    einzelpreis = models.FloatField(db_column='EINZELPREIS', blank=True, null=True)  # Field name made lowercase.
    mwst_satz = models.IntegerField(db_column='MWST_SATZ', blank=True, null=True)  # Field name made lowercase.
    kontierung = models.IntegerField(db_column='KONTIERUNG', blank=True, null=True)  # Field name made lowercase.
    leistung = models.CharField(db_column='LEISTUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    rechnung_text = models.TextField(db_column='RECHNUNG_TEXT', blank=True, null=True)  # Field name made lowercase.
    begruendung = models.TextField(db_column='BEGRUENDUNG', blank=True, null=True)  # Field name made lowercase.
    mwst_preis = models.FloatField(db_column='MWST_PREIS', blank=True, null=True)  # Field name made lowercase.
    brutto_preis = models.FloatField(db_column='BRUTTO_PREIS', blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    system_datum = models.DateField(db_column='SYSTEM_DATUM', blank=True, null=True)  # Field name made lowercase.
    system_uhrzeit = models.TimeField(db_column='SYSTEM_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    faktor = models.FloatField(db_column='FAKTOR')  # Field name made lowercase.
    abzug = models.FloatField(db_column='ABZUG')  # Field name made lowercase.
    typ_kosten = models.IntegerField(db_column='TYP_KOSTEN')  # Field name made lowercase.
    ziffer_id = models.IntegerField(db_column='ZIFFER_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'RECHNUNGPOSITION'


class Region(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    re_kuerzel = models.CharField(db_column='RE_KUERZEL', unique=True, max_length=20)  # Field name made lowercase.
    re_bezeichnung = models.CharField(db_column='RE_BEZEICHNUNG', max_length=35, blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REGION'


class Rezept(models.Model):
    rezeptdoku_id = models.IntegerField(db_column='REZEPTDOKU_ID', unique=True)  # Field name made lowercase.
    status_rezept = models.IntegerField(db_column='STATUS_REZEPT', blank=True, null=True)  # Field name made lowercase.
    gebuehr_befreit = models.IntegerField(db_column='GEBUEHR_BEFREIT', blank=True, null=True)  # Field name made lowercase.
    rezept_typ = models.CharField(db_column='REZEPT_TYP', max_length=50, blank=True, null=True)  # Field name made lowercase.
    hausapotheke = models.IntegerField(db_column='HAUSAPOTHEKE', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'REZEPT'


class Rohdrogen(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)  # Field name made lowercase.
    latein = models.CharField(db_column='LATEIN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    pinyin = models.CharField(db_column='PINYIN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    pinyin_lautschrift = models.CharField(db_column='PINYIN_LAUTSCHRIFT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    warnhinweis = models.CharField(db_column='WARNHINWEIS', max_length=30, blank=True, null=True)  # Field name made lowercase.
    benskykategorie = models.IntegerField(db_column='BENSKYKATEGORIE', blank=True, null=True)  # Field name made lowercase.
    chinazeichen_tradition = models.CharField(db_column='CHINAZEICHEN_TRADITION', max_length=30, blank=True, null=True)  # Field name made lowercase.
    chinazeichen_kurzform = models.CharField(db_column='CHINAZEICHEN_KURZFORM', max_length=30, blank=True, null=True)  # Field name made lowercase.
    notiz = models.TextField(db_column='NOTIZ', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ROHDROGEN'


class Schwangerschaft(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    nr = models.IntegerField(db_column='NR')  # Field name made lowercase.
    letzte_regel_datum = models.DateField(db_column='LETZTE_REGEL_DATUM', blank=True, null=True)  # Field name made lowercase.
    geplant_geburt_datum = models.DateField(db_column='GEPLANT_GEBURT_DATUM', blank=True, null=True)  # Field name made lowercase.
    herztaetigkeit_datum = models.DateField(db_column='HERZTAETIGKEIT_DATUM', blank=True, null=True)  # Field name made lowercase.
    geburt_abort_datum = models.DateField(db_column='GEBURT_ABORT_DATUM', blank=True, null=True)  # Field name made lowercase.
    geburt_abort_ssw = models.CharField(db_column='GEBURT_ABORT_SSW', max_length=255, blank=True, null=True)  # Field name made lowercase.
    geburt_typ = models.IntegerField(db_column='GEBURT_TYP', blank=True, null=True)  # Field name made lowercase.
    geburt_art = models.CharField(db_column='GEBURT_ART', max_length=255, blank=True, null=True)  # Field name made lowercase.
    abort_genetik = models.TextField(db_column='ABORT_GENETIK', blank=True, null=True)  # Field name made lowercase.
    untersuchung_text = models.TextField(db_column='UNTERSUCHUNG_TEXT', blank=True, null=True)  # Field name made lowercase.
    saeuglinge = models.TextField(db_column='SAEUGLINGE', blank=True, null=True)  # Field name made lowercase.
    geburt_abort_bemerkung = models.TextField(db_column='GEBURT_ABORT_BEMERKUNG', blank=True, null=True)  # Field name made lowercase.
    schwangerschafts_dokuid = models.IntegerField(db_column='SCHWANGERSCHAFTS_DOKUID', blank=True, null=True)  # Field name made lowercase.
    zyklus_tage = models.IntegerField(db_column='ZYKLUS_TAGE', blank=True, null=True)  # Field name made lowercase.
    schwangerschaftstest = models.IntegerField(db_column='SCHWANGERSCHAFTSTEST', blank=True, null=True)  # Field name made lowercase.
    errechneter_gebtermin = models.CharField(db_column='ERRECHNETER_GEBTERMIN', max_length=100, blank=True, null=True)  # Field name made lowercase.
    invitro_zyklus_id = models.IntegerField(db_column='INVITRO_ZYKLUS_ID', blank=True, null=True)  # Field name made lowercase.
    mutterschutz_beginn_datum = models.DateField(db_column='MUTTERSCHUTZ_BEGINN_DATUM', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'SCHWANGERSCHAFT'


class Sendeliste(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    doku_id = models.IntegerField(db_column='DOKU_ID')  # Field name made lowercase.
    sendetyp = models.IntegerField(db_column='SENDETYP')  # Field name made lowercase.
    sendestatus = models.IntegerField(db_column='SENDESTATUS')  # Field name made lowercase.
    webstatus = models.IntegerField(db_column='WEBSTATUS')  # Field name made lowercase.
    absender = models.CharField(db_column='ABSENDER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    empfaenger = models.CharField(db_column='EMPFAENGER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    betreff = models.CharField(db_column='BETREFF', max_length=255, blank=True, null=True)  # Field name made lowercase.
    text = models.TextField(db_column='TEXT', blank=True, null=True)  # Field name made lowercase.
    dateiname = models.CharField(db_column='DATEINAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    benutzer_id = models.IntegerField(db_column='BENUTZER_ID')  # Field name made lowercase.
    datum_erstellt = models.DateTimeField(db_column='DATUM_ERSTELLT', blank=True, null=True)  # Field name made lowercase.
    datum_uebertragen = models.DateTimeField(db_column='DATUM_UEBERTRAGEN', blank=True, null=True)  # Field name made lowercase.
    datum_gesendet = models.DateTimeField(db_column='DATUM_GESENDET', blank=True, null=True)  # Field name made lowercase.
    rechner = models.CharField(db_column='RECHNER', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'SENDELISTE'


class Serienbrief(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    aceto_nr = models.IntegerField(db_column='ACETO_NR')  # Field name made lowercase.
    position = models.IntegerField(db_column='POSITION', blank=True, null=True)  # Field name made lowercase.
    name = models.CharField(db_column='NAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    report = models.CharField(db_column='REPORT', max_length=50, blank=True, null=True)  # Field name made lowercase.
    logo = models.CharField(db_column='LOGO', max_length=50, blank=True, null=True)  # Field name made lowercase.
    einstellungen = models.TextField(db_column='EINSTELLUNGEN', blank=True, null=True)  # Field name made lowercase.
    privat = models.CharField(db_column='PRIVAT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    kasse = models.CharField(db_column='KASSE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ermaechtigung = models.CharField(db_column='ERMAECHTIGUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung_datum = models.DateField(db_column='LETZTE_AENDERUNG_DATUM', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung_uhrzeit = models.TimeField(db_column='LETZTE_AENDERUNG_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)  # Field name made lowercase.
    begruessung = models.TextField(db_column='BEGRUESSUNG', blank=True, null=True)  # Field name made lowercase.
    einleitung = models.TextField(db_column='EINLEITUNG', blank=True, null=True)  # Field name made lowercase.
    absatz1 = models.TextField(db_column='ABSATZ1', blank=True, null=True)  # Field name made lowercase.
    absatz2 = models.TextField(db_column='ABSATZ2', blank=True, null=True)  # Field name made lowercase.
    absatz3 = models.TextField(db_column='ABSATZ3', blank=True, null=True)  # Field name made lowercase.
    schlussformel = models.TextField(db_column='SCHLUSSFORMEL', blank=True, null=True)  # Field name made lowercase.
    unterschrift = models.TextField(db_column='UNTERSCHRIFT', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'SERIENBRIEF'


class SisDok(models.Model):
    doknummer = models.CharField(db_column='DOKNUMMER', max_length=10, blank=True, null=True)  # Field name made lowercase.
    doktyp = models.CharField(db_column='DOKTYP', max_length=10, blank=True, null=True)  # Field name made lowercase.
    dokreddat = models.CharField(db_column='DOKREDDAT', max_length=10, blank=True, null=True)  # Field name made lowercase.
    doktitel = models.CharField(db_column='DOKTITEL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    dokurl = models.CharField(db_column='DOKURL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    doktxt = models.TextField(db_column='DOKTXT', blank=True, null=True)  # Field name made lowercase.
    doktxk = models.TextField(db_column='DOKTXK', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'SIS_DOK'


class SisDokId(models.Model):
    indnr = models.CharField(db_column='INDNR', max_length=10, blank=True, null=True)  # Field name made lowercase.
    doknummer = models.CharField(db_column='DOKNUMMER', max_length=10, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'SIS_DOK_ID'


class SisPicto(models.Model):
    zlnumm = models.CharField(db_column='ZLNUMM', max_length=10)  # Field name made lowercase.
    znumm = models.CharField(db_column='ZNUMM', primary_key=True, max_length=10)  # Field name made lowercase.
    zbez = models.CharField(db_column='ZBEZ', max_length=100, blank=True, null=True)  # Field name made lowercase.
    picto = models.CharField(db_column='PICTO', max_length=20, blank=True, null=True)  # Field name made lowercase.
    sst = models.CharField(db_column='SST', max_length=10, blank=True, null=True)  # Field name made lowercase.
    sst2 = models.CharField(db_column='SST2', max_length=10, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'SIS_PICTO'


class SisZarzst(models.Model):
    yhnummer = models.CharField(db_column='YHNUMMER', primary_key=True, max_length=10)  # Field name made lowercase.
    ykz = models.CharField(db_column='YKZ', max_length=10, blank=True, null=True)  # Field name made lowercase.
    ytxt = models.CharField(db_column='YTXT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    yhnummer1 = models.CharField(db_column='YHNUMMER1', max_length=10, blank=True, null=True)  # Field name made lowercase.
    ydh = models.CharField(db_column='YDH', max_length=10, blank=True, null=True)  # Field name made lowercase.
    ydu = models.CharField(db_column='YDU', max_length=10, blank=True, null=True)  # Field name made lowercase.
    ydruck = models.CharField(db_column='YDRUCK', max_length=5, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'SIS_ZARZST'


class SisZatc(models.Model):
    zatc = models.CharField(db_column='ZATC', primary_key=True, max_length=10)  # Field name made lowercase.
    zatcbez = models.CharField(db_column='ZATCBEZ', max_length=255, blank=True, null=True)  # Field name made lowercase.
    zatcbeze = models.CharField(db_column='ZATCBEZE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    zdruck = models.CharField(db_column='ZDRUCK', max_length=5, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'SIS_ZATC'


class SisZatcl(models.Model):
    znumm = models.CharField(db_column='ZNUMM', primary_key=True, max_length=10)  # Field name made lowercase.
    zatc = models.CharField(db_column='ZATC', max_length=10)  # Field name made lowercase.
    zsort = models.CharField(db_column='ZSORT', max_length=10, blank=True, null=True)  # Field name made lowercase.
    zdruck = models.CharField(db_column='ZDRUCK', max_length=5, blank=True, null=True)  # Field name made lowercase.
    zkz1 = models.CharField(db_column='ZKZ1', max_length=5, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'SIS_ZATCL'
        unique_together = (('znumm', 'zatc'),)


class SisZdar(models.Model):
    ydar = models.CharField(db_column='YDAR', primary_key=True, max_length=10)  # Field name made lowercase.
    ybez = models.CharField(db_column='YBEZ', max_length=255, blank=True, null=True)  # Field name made lowercase.
    yhgruppe = models.CharField(db_column='YHGRUPPE', max_length=10)  # Field name made lowercase.
    yugruppe = models.CharField(db_column='YUGRUPPE', max_length=10)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'SIS_ZDAR'
        unique_together = (('ydar', 'yhgruppe', 'yugruppe'),)


class SisZherst(models.Model):
    ycode = models.CharField(db_column='YCODE', primary_key=True, max_length=10)  # Field name made lowercase.
    yname = models.CharField(db_column='YNAME', max_length=100, blank=True, null=True)  # Field name made lowercase.
    ytel = models.CharField(db_column='YTEL', max_length=100, blank=True, null=True)  # Field name made lowercase.
    yplz = models.CharField(db_column='YPLZ', max_length=10, blank=True, null=True)  # Field name made lowercase.
    yort = models.CharField(db_column='YORT', max_length=100, blank=True, null=True)  # Field name made lowercase.
    ystrasse = models.CharField(db_column='YSTRASSE', max_length=100, blank=True, null=True)  # Field name made lowercase.
    yadr2 = models.CharField(db_column='YADR2', max_length=100, blank=True, null=True)  # Field name made lowercase.
    ydruck = models.CharField(db_column='YDRUCK', max_length=5, blank=True, null=True)  # Field name made lowercase.
    yfax = models.CharField(db_column='YFAX', max_length=100, blank=True, null=True)  # Field name made lowercase.
    ymail = models.CharField(db_column='YMAIL', max_length=100, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'SIS_ZHERST'


class SisZindgrst(models.Model):
    zindnr = models.CharField(db_column='ZINDNR', primary_key=True, max_length=10)  # Field name made lowercase.
    zindbez = models.CharField(db_column='ZINDBEZ', max_length=100, blank=True, null=True)  # Field name made lowercase.
    zindnr2 = models.CharField(db_column='ZINDNR2', max_length=10, blank=True, null=True)  # Field name made lowercase.
    zdruck = models.CharField(db_column='ZDRUCK', max_length=5, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'SIS_ZINDGRST'


class SisZindl(models.Model):
    znumm = models.CharField(db_column='ZNUMM', primary_key=True, max_length=10)  # Field name made lowercase.
    zindnr = models.CharField(db_column='ZINDNR', max_length=10)  # Field name made lowercase.
    zsort = models.CharField(db_column='ZSORT', max_length=10, blank=True, null=True)  # Field name made lowercase.
    zdruck = models.CharField(db_column='ZDRUCK', max_length=5, blank=True, null=True)  # Field name made lowercase.
    zkz1 = models.CharField(db_column='ZKZ1', max_length=5, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'SIS_ZINDL'
        unique_together = (('znumm', 'zindnr'),)


class SisZinterl(models.Model):
    zinternr = models.CharField(db_column='ZINTERNR', primary_key=True, max_length=10)  # Field name made lowercase.
    zintergr = models.CharField(db_column='ZINTERGR', max_length=5)  # Field name made lowercase.
    zinterart = models.CharField(db_column='ZINTERART', max_length=5)  # Field name made lowercase.
    yhnummer = models.CharField(db_column='YHNUMMER', max_length=10)  # Field name made lowercase.
    yhnummer1 = models.CharField(db_column='YHNUMMER1', max_length=10, blank=True, null=True)  # Field name made lowercase.
    zsort = models.CharField(db_column='ZSORT', max_length=10, blank=True, null=True)  # Field name made lowercase.
    zdruck = models.CharField(db_column='ZDRUCK', max_length=5, blank=True, null=True)  # Field name made lowercase.
    yhnummer3 = models.CharField(db_column='YHNUMMER3', max_length=10, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'SIS_ZINTERL'
        unique_together = (('zinternr', 'zintergr', 'zinterart', 'yhnummer'),)


class SisZinterst(models.Model):
    zinternr = models.CharField(db_column='ZINTERNR', primary_key=True, max_length=10)  # Field name made lowercase.
    zinternr2 = models.CharField(db_column='ZINTERNR2', max_length=10, blank=True, null=True)  # Field name made lowercase.
    zintergr1 = models.CharField(db_column='ZINTERGR1', max_length=10, blank=True, null=True)  # Field name made lowercase.
    zintergr2 = models.CharField(db_column='ZINTERGR2', max_length=10, blank=True, null=True)  # Field name made lowercase.
    zgruppe = models.CharField(db_column='ZGRUPPE', max_length=10, blank=True, null=True)  # Field name made lowercase.
    zkurz = models.CharField(db_column='ZKURZ', max_length=100, blank=True, null=True)  # Field name made lowercase.
    zart = models.CharField(db_column='ZART', max_length=5, blank=True, null=True)  # Field name made lowercase.
    zdruck = models.CharField(db_column='ZDRUCK', max_length=5, blank=True, null=True)  # Field name made lowercase.
    ztext = models.TextField(db_column='ZTEXT', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'SIS_ZINTERST'


class SisZspez(models.Model):
    znumm = models.CharField(db_column='ZNUMM', primary_key=True, max_length=10)  # Field name made lowercase.
    zlnumm = models.CharField(db_column='ZLNUMM', max_length=10, blank=True, null=True)  # Field name made lowercase.
    zbez = models.CharField(db_column='ZBEZ', max_length=100, blank=True, null=True)  # Field name made lowercase.
    zland = models.CharField(db_column='ZLAND', max_length=5, blank=True, null=True)  # Field name made lowercase.
    zmono = models.CharField(db_column='ZMONO', max_length=5, blank=True, null=True)  # Field name made lowercase.
    zanzahl = models.CharField(db_column='ZANZAHL', max_length=5, blank=True, null=True)  # Field name made lowercase.
    zherst = models.CharField(db_column='ZHERST', max_length=10, blank=True, null=True)  # Field name made lowercase.
    zvertr = models.CharField(db_column='ZVERTR', max_length=10, blank=True, null=True)  # Field name made lowercase.
    zedat = models.CharField(db_column='ZEDAT', max_length=20, blank=True, null=True)  # Field name made lowercase.
    zbdat = models.CharField(db_column='ZBDAT', max_length=20, blank=True, null=True)  # Field name made lowercase.
    zbezgr = models.CharField(db_column='ZBEZGR', max_length=40, blank=True, null=True)  # Field name made lowercase.
    zverfall = models.CharField(db_column='ZVERFALL', max_length=5, blank=True, null=True)  # Field name made lowercase.
    zverfall2 = models.CharField(db_column='ZVERFALL2', max_length=10, blank=True, null=True)  # Field name made lowercase.
    zlager = models.CharField(db_column='ZLAGER', max_length=5, blank=True, null=True)  # Field name made lowercase.
    zloesen = models.CharField(db_column='ZLOESEN', max_length=5, blank=True, null=True)  # Field name made lowercase.
    zwartez = models.CharField(db_column='ZWARTEZ', max_length=5, blank=True, null=True)  # Field name made lowercase.
    zabgabe = models.CharField(db_column='ZABGABE', max_length=5, blank=True, null=True)  # Field name made lowercase.
    zna = models.CharField(db_column='ZNA', max_length=5, blank=True, null=True)  # Field name made lowercase.
    zrp = models.CharField(db_column='ZRP', max_length=10, blank=True, null=True)  # Field name made lowercase.
    zwarn = models.CharField(db_column='ZWARN', max_length=5, blank=True, null=True)  # Field name made lowercase.
    zsucht = models.CharField(db_column='ZSUCHT', max_length=10, blank=True, null=True)  # Field name made lowercase.
    zdruck = models.CharField(db_column='ZDRUCK', max_length=5, blank=True, null=True)  # Field name made lowercase.
    zdruck2 = models.CharField(db_column='ZDRUCK2', max_length=20, blank=True, null=True)  # Field name made lowercase.
    zanwend = models.TextField(db_column='ZANWEND', blank=True, null=True)  # Field name made lowercase.
    zhinw = models.TextField(db_column='ZHINW', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'SIS_ZSPEZ'


class SisZstoffl(models.Model):
    znumm = models.CharField(db_column='ZNUMM', primary_key=True, max_length=10)  # Field name made lowercase.
    yhnummer = models.CharField(db_column='YHNUMMER', max_length=10, blank=True, null=True)  # Field name made lowercase.
    yhnummer1 = models.CharField(db_column='YHNUMMER1', max_length=10, blank=True, null=True)  # Field name made lowercase.
    zkz = models.CharField(db_column='ZKZ', max_length=10, blank=True, null=True)  # Field name made lowercase.
    zmenge = models.CharField(db_column='ZMENGE', max_length=20, blank=True, null=True)  # Field name made lowercase.
    zmengeinh = models.CharField(db_column='ZMENGEINH', max_length=10, blank=True, null=True)  # Field name made lowercase.
    ztext = models.CharField(db_column='ZTEXT', max_length=20, blank=True, null=True)  # Field name made lowercase.
    zsort = models.CharField(db_column='ZSORT', max_length=10, blank=True, null=True)  # Field name made lowercase.
    znr = models.CharField(db_column='ZNR', max_length=10)  # Field name made lowercase.
    zdruck = models.CharField(db_column='ZDRUCK', max_length=5, blank=True, null=True)  # Field name made lowercase.
    zkz1 = models.CharField(db_column='ZKZ1', max_length=5, blank=True, null=True)  # Field name made lowercase.
    zkz2 = models.CharField(db_column='ZKZ2', max_length=5, blank=True, null=True)  # Field name made lowercase.
    yhnummer3 = models.CharField(db_column='YHNUMMER3', max_length=10, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'SIS_ZSTOFFL'
        unique_together = (('znumm', 'znr'),)


class SisZsubstan(models.Model):
    zintergr = models.CharField(db_column='ZINTERGR', primary_key=True, max_length=10)  # Field name made lowercase.
    zgrbez = models.CharField(db_column='ZGRBEZ', max_length=100, blank=True, null=True)  # Field name made lowercase.
    zart = models.CharField(db_column='ZART', max_length=10, blank=True, null=True)  # Field name made lowercase.
    zintergr1 = models.CharField(db_column='ZINTERGR1', max_length=10, blank=True, null=True)  # Field name made lowercase.
    zdruck = models.CharField(db_column='ZDRUCK', max_length=5, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'SIS_ZSUBSTAN'


class Stddiag(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    position = models.IntegerField(db_column='POSITION', blank=True, null=True)  # Field name made lowercase.
    kuerzel = models.CharField(db_column='KUERZEL', max_length=50, blank=True, null=True)  # Field name made lowercase.
    diagnosen = models.CharField(db_column='DIAGNOSEN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    icd_kodes = models.CharField(db_column='ICD_KODES', max_length=255, blank=True, null=True)  # Field name made lowercase.
    mandanten = models.CharField(db_column='MANDANTEN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung_datum = models.DateField(db_column='LETZTE_AENDERUNG_DATUM', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung_uhrzeit = models.TimeField(db_column='LETZTE_AENDERUNG_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    wahlarzt = models.CharField(db_column='WAHLARZT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    kasse = models.CharField(db_column='KASSE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)  # Field name made lowercase.
    bg = models.CharField(db_column='BG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    vertragsart = models.CharField(db_column='VERTRAGSART', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'STDDIAG'


class StddiagGyn(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    position = models.IntegerField(db_column='POSITION', blank=True, null=True)  # Field name made lowercase.
    kuerzel = models.CharField(db_column='KUERZEL', max_length=50, blank=True, null=True)  # Field name made lowercase.
    diagnosen = models.CharField(db_column='DIAGNOSEN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    icd_kodes = models.CharField(db_column='ICD_KODES', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ermaechtigung = models.CharField(db_column='ERMAECHTIGUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung_datum = models.DateField(db_column='LETZTE_AENDERUNG_DATUM', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung_uhrzeit = models.TimeField(db_column='LETZTE_AENDERUNG_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    privat = models.CharField(db_column='PRIVAT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    kasse = models.CharField(db_column='KASSE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)  # Field name made lowercase.
    bg = models.CharField(db_column='BG', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'STDDIAG_GYN'


class Stdleist(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)  # Field name made lowercase.
    position_gruppe = models.IntegerField(db_column='POSITION_GRUPPE', blank=True, null=True)  # Field name made lowercase.
    gruppe = models.CharField(db_column='GRUPPE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    position = models.IntegerField(db_column='POSITION', blank=True, null=True)  # Field name made lowercase.
    kasse_ziffern_kette = models.TextField(db_column='KASSE_ZIFFERN_KETTE', blank=True, null=True)  # Field name made lowercase.
    privat_ziffern_kette = models.TextField(db_column='PRIVAT_ZIFFERN_KETTE', blank=True, null=True)  # Field name made lowercase.
    leistung_kette = models.CharField(db_column='LEISTUNG_KETTE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bg_ziffern_kette = models.TextField(db_column='BG_ZIFFERN_KETTE', blank=True, null=True)  # Field name made lowercase.
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    mandanten = models.CharField(db_column='MANDANTEN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    schein_kennzeichen = models.CharField(db_column='SCHEIN_KENNZEICHEN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    schein_art = models.CharField(db_column='SCHEIN_ART', max_length=255, blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)  # Field name made lowercase.
    kasse = models.CharField(db_column='KASSE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    modalitaet = models.CharField(db_column='MODALITAET', max_length=255, blank=True, null=True)  # Field name made lowercase.
    einstellung = models.TextField(db_column='EINSTELLUNG', blank=True, null=True)  # Field name made lowercase.
    wahlarzt = models.CharField(db_column='WAHLARZT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bg = models.CharField(db_column='BG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    vertragsart = models.CharField(db_column='VERTRAGSART', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'STDLEIST'


class StdleistGyn(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    position = models.IntegerField(db_column='POSITION', blank=True, null=True)  # Field name made lowercase.
    kasse_ziffern_kette = models.CharField(db_column='KASSE_ZIFFERN_KETTE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    privat_ziffern_kette = models.CharField(db_column='PRIVAT_ZIFFERN_KETTE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    leistung_kette = models.CharField(db_column='LEISTUNG_KETTE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bg_ziffern_kette = models.CharField(db_column='BG_ZIFFERN_KETTE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ermaechtigung = models.CharField(db_column='ERMAECHTIGUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    schein_kennzeichen = models.CharField(db_column='SCHEIN_KENNZEICHEN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    schein_art = models.CharField(db_column='SCHEIN_ART', max_length=255, blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung_datum = models.DateField(db_column='LETZTE_AENDERUNG_DATUM', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung_uhrzeit = models.TimeField(db_column='LETZTE_AENDERUNG_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    kasse = models.CharField(db_column='KASSE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    modalitaet = models.CharField(db_column='MODALITAET', max_length=255, blank=True, null=True)  # Field name made lowercase.
    einstellung = models.TextField(db_column='EINSTELLUNG', blank=True, null=True)  # Field name made lowercase.
    privat = models.CharField(db_column='PRIVAT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bg = models.CharField(db_column='BG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    vertragsart = models.CharField(db_column='VERTRAGSART', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'STDLEIST_GYN'


class Stdsig(models.Model):
    pharmanummer = models.IntegerField(db_column='PHARMANUMMER', primary_key=True)  # Field name made lowercase.
    dosisschema = models.TextField(db_column='DOSISSCHEMA', blank=True, null=True)  # Field name made lowercase.
    ermaechtigung_id = models.IntegerField(db_column='ERMAECHTIGUNG_ID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'STDSIG'
        unique_together = (('pharmanummer', 'ermaechtigung_id'),)


class StdsigGyn(models.Model):
    pharmanummer = models.IntegerField(db_column='PHARMANUMMER', primary_key=True)  # Field name made lowercase.
    dosisschema = models.TextField(db_column='DOSISSCHEMA', blank=True, null=True)  # Field name made lowercase.
    ermaechtigung_id = models.IntegerField(db_column='ERMAECHTIGUNG_ID')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'STDSIG_GYN'
        unique_together = (('pharmanummer', 'ermaechtigung_id'),)


class Tagesinfo(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    kalender_nr = models.IntegerField(db_column='KALENDER_NR')  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)  # Field name made lowercase.
    ermaechtigung_id = models.IntegerField(db_column='ERMAECHTIGUNG_ID')  # Field name made lowercase.
    info_datum = models.DateField(db_column='INFO_DATUM', blank=True, null=True)  # Field name made lowercase.
    info_text = models.CharField(db_column='INFO_TEXT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung_datum = models.DateField(db_column='LETZTE_AENDERUNG_DATUM', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung_uhrzeit = models.TimeField(db_column='LETZTE_AENDERUNG_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'TAGESINFO'


class Terminreservierung(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    kalendertermin_id = models.IntegerField(db_column='KALENDERTERMIN_ID', blank=True, null=True)  # Field name made lowercase.
    web_reservierungsdaten_id = models.IntegerField(db_column='WEB_RESERVIERUNGSDATEN_ID', blank=True, null=True)  # Field name made lowercase.
    reserviert_am = models.DateTimeField(db_column='RESERVIERT_AM', blank=True, null=True)  # Field name made lowercase.
    storniert_am = models.DateTimeField(db_column='STORNIERT_AM', blank=True, null=True)  # Field name made lowercase.
    patient_id = models.IntegerField(db_column='PATIENT_ID', blank=True, null=True)  # Field name made lowercase.
    auftrag_id = models.IntegerField(db_column='AUFTRAG_ID', blank=True, null=True)  # Field name made lowercase.
    anrede = models.CharField(db_column='ANREDE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    geschlecht = models.IntegerField(db_column='GESCHLECHT', blank=True, null=True)  # Field name made lowercase.
    titel = models.CharField(db_column='TITEL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    vorname = models.CharField(db_column='VORNAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    name = models.CharField(db_column='NAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    geburtsdatum = models.DateField(db_column='GEBURTSDATUM', blank=True, null=True)  # Field name made lowercase.
    versichertennummer = models.CharField(db_column='VERSICHERTENNUMMER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    versicherung = models.CharField(db_column='VERSICHERUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ist_neupatient = models.IntegerField(db_column='IST_NEUPATIENT', blank=True, null=True)  # Field name made lowercase.
    strasse = models.CharField(db_column='STRASSE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    plz = models.CharField(db_column='PLZ', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ort = models.CharField(db_column='ORT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    telefon = models.CharField(db_column='TELEFON', max_length=255, blank=True, null=True)  # Field name made lowercase.
    mobil = models.CharField(db_column='MOBIL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    fax = models.CharField(db_column='FAX', max_length=255, blank=True, null=True)  # Field name made lowercase.
    email = models.CharField(db_column='EMAIL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    mitteilung = models.TextField(db_column='MITTEILUNG', blank=True, null=True)  # Field name made lowercase.
    sms_erinnerung = models.IntegerField(db_column='SMS_ERINNERUNG', blank=True, null=True)  # Field name made lowercase.
    email_erinnerung = models.IntegerField(db_column='EMAIL_ERINNERUNG', blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'TERMINRESERVIERUNG'


class Untersuchung(models.Model):
    untersuchung_id = models.IntegerField(db_column='UNTERSUCHUNG_ID', unique=True)  # Field name made lowercase.
    status_untersuchung = models.IntegerField(db_column='STATUS_UNTERSUCHUNG', blank=True, null=True)  # Field name made lowercase.
    arztbrief_id = models.IntegerField(db_column='ARZTBRIEF_ID', blank=True, null=True)  # Field name made lowercase.
    auftrag_id = models.IntegerField(db_column='AUFTRAG_ID', blank=True, null=True)  # Field name made lowercase.
    auftrag_stamm_id = models.IntegerField(db_column='AUFTRAG_STAMM_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'UNTERSUCHUNG'


class UntersuchungTyp(models.Model):
    id = models.IntegerField(db_column='ID', primary_key=True)  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)  # Field name made lowercase.
    u_text = models.CharField(db_column='U_TEXT', max_length=100, blank=True, null=True)  # Field name made lowercase.
    u_sortierung = models.IntegerField(db_column='U_SORTIERUNG', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'UNTERSUCHUNG_TYP'


class Vertragsart(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    elter_id = models.IntegerField(db_column='ELTER_ID')  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP')  # Field name made lowercase.
    position = models.IntegerField(db_column='POSITION')  # Field name made lowercase.
    kuerzel = models.CharField(db_column='KUERZEL', unique=True, max_length=50, blank=True, null=True)  # Field name made lowercase.
    kennung = models.IntegerField(db_column='KENNUNG', unique=True)  # Field name made lowercase.
    langtext = models.TextField(db_column='LANGTEXT', blank=True, null=True)  # Field name made lowercase.
    adresstext = models.TextField(db_column='ADRESSTEXT', blank=True, null=True)  # Field name made lowercase.
    adresse = models.TextField(db_column='ADRESSE', blank=True, null=True)  # Field name made lowercase.
    re_anschrift_xml = models.TextField(db_column='RE_ANSCHRIFT_XML', blank=True, null=True)  # Field name made lowercase.
    mandant = models.CharField(db_column='MANDANT', max_length=255)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)  # Field name made lowercase.
    ziffer_typ = models.CharField(db_column='ZIFFER_TYP', max_length=50, blank=True, null=True)  # Field name made lowercase.
    faktor = models.FloatField(db_column='FAKTOR', blank=True, null=True)  # Field name made lowercase.
    faktor_tech = models.FloatField(db_column='FAKTOR_TECH', blank=True, null=True)  # Field name made lowercase.
    faktor_lab = models.FloatField(db_column='FAKTOR_LAB', blank=True, null=True)  # Field name made lowercase.
    einstellungen = models.TextField(db_column='EINSTELLUNGEN', blank=True, null=True)  # Field name made lowercase.
    kurzkuerzel = models.CharField(db_column='KURZKUERZEL', unique=True, max_length=5, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'VERTRAGSART'


class VidalDaten(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    checksum = models.IntegerField(db_column='CHECKSUM', blank=True, null=True)  # Field name made lowercase.
    type = models.CharField(db_column='TYPE', max_length=3, blank=True, null=True)  # Field name made lowercase.
    gueltig = models.DateField(db_column='GUELTIG', blank=True, null=True)  # Field name made lowercase.
    verfuegbar = models.CharField(db_column='VERFUEGBAR', max_length=3, blank=True, null=True)  # Field name made lowercase.
    pharma_nr = models.IntegerField(db_column='PHARMA_NR', blank=True, null=True)  # Field name made lowercase.
    zulassung_nr_string = models.CharField(db_column='ZULASSUNG_NR_STRING', max_length=50, blank=True, null=True)  # Field name made lowercase.
    zulassung_nr = models.IntegerField(db_column='ZULASSUNG_NR', blank=True, null=True)  # Field name made lowercase.
    bezeichnung_kurz = models.CharField(db_column='BEZEICHNUNG_KURZ', max_length=50, blank=True, null=True)  # Field name made lowercase.
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung = models.DateField(db_column='LETZTE_AENDERUNG', blank=True, null=True)  # Field name made lowercase.
    rsigns = models.CharField(db_column='RSIGNS', max_length=50, blank=True, null=True)  # Field name made lowercase.
    storage = models.CharField(db_column='STORAGE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    remb = models.IntegerField(db_column='REMB', blank=True, null=True)  # Field name made lowercase.
    box = models.CharField(db_column='BOX', max_length=1, blank=True, null=True)  # Field name made lowercase.
    ssigns = models.CharField(db_column='SSIGNS', max_length=50, blank=True, null=True)  # Field name made lowercase.
    indtext = models.TextField(db_column='INDTEXT', blank=True, null=True)  # Field name made lowercase.
    ruletext = models.TextField(db_column='RULETEXT', blank=True, null=True)  # Field name made lowercase.
    remarktext = models.TextField(db_column='REMARKTEXT', blank=True, null=True)  # Field name made lowercase.
    quantity = models.IntegerField(db_column='QUANTITY', blank=True, null=True)  # Field name made lowercase.
    unit = models.CharField(db_column='UNIT', max_length=50, blank=True, null=True)  # Field name made lowercase.
    enhunit = models.CharField(db_column='ENHUNIT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    kvp = models.FloatField(db_column='KVP', blank=True, null=True)  # Field name made lowercase.
    avp = models.FloatField(db_column='AVP', blank=True, null=True)  # Field name made lowercase.
    zinh = models.IntegerField(db_column='ZINH', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'VIDAL_DATEN'


class VidalReferenzen(models.Model):
    zinh = models.IntegerField(db_column='ZINH', primary_key=True)  # Field name made lowercase.
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'VIDAL_REFERENZEN'


class Warteliste(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    patient_id = models.IntegerField(db_column='PATIENT_ID', blank=True, null=True)  # Field name made lowercase.
    termin_start_datum = models.DateField(db_column='TERMIN_START_DATUM', blank=True, null=True)  # Field name made lowercase.
    termin_start_uhrzeit = models.TimeField(db_column='TERMIN_START_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    termin_text = models.CharField(db_column='TERMIN_TEXT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    warte_liste = models.CharField(db_column='WARTE_LISTE', max_length=50, blank=True, null=True)  # Field name made lowercase.
    warte_datum = models.DateField(db_column='WARTE_DATUM', blank=True, null=True)  # Field name made lowercase.
    warte_uhrzeit = models.TimeField(db_column='WARTE_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung_datum = models.DateField(db_column='LETZTE_AENDERUNG_DATUM', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung_uhrzeit = models.TimeField(db_column='LETZTE_AENDERUNG_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    termin_ende_datum = models.DateField(db_column='TERMIN_ENDE_DATUM', blank=True, null=True)  # Field name made lowercase.
    termin_ende_uhrzeit = models.TimeField(db_column='TERMIN_ENDE_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    patient_name = models.CharField(db_column='PATIENT_NAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ermaechtigung_id = models.IntegerField(db_column='ERMAECHTIGUNG_ID')  # Field name made lowercase.
    kalender_nr = models.IntegerField(db_column='KALENDER_NR', blank=True, null=True)  # Field name made lowercase.
    position = models.BigIntegerField(db_column='POSITION', blank=True, null=True)  # Field name made lowercase.
    erledigt = models.IntegerField(db_column='ERLEDIGT')  # Field name made lowercase.
    kennzeichen = models.IntegerField(db_column='KENNZEICHEN', blank=True, null=True)  # Field name made lowercase.
    einstellungen = models.TextField(db_column='EINSTELLUNGEN', blank=True, null=True)  # Field name made lowercase.
    termin_anlage = models.DateTimeField(db_column='TERMIN_ANLAGE', blank=True, null=True)  # Field name made lowercase.
    warte_entlassen = models.DateTimeField(db_column='WARTE_ENTLASSEN', blank=True, null=True)  # Field name made lowercase.
    webstatus = models.IntegerField(db_column='WEBSTATUS', blank=True, null=True)  # Field name made lowercase.
    webversion = models.IntegerField(db_column='WEBVERSION', blank=True, null=True)  # Field name made lowercase.
    webanlage = models.DateTimeField(db_column='WEBANLAGE', blank=True, null=True)  # Field name made lowercase.
    webreservierung_id = models.IntegerField(db_column='WEBRESERVIERUNG_ID', blank=True, null=True)  # Field name made lowercase.
    auftragliste = models.CharField(db_column='AUFTRAGLISTE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    wegzusenden = models.IntegerField(db_column='WEGZUSENDEN')  # Field name made lowercase.
    gewaehlterauftrag = models.IntegerField(db_column='GEWAEHLTERAUFTRAG', blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)  # Field name made lowercase.
    email = models.CharField(db_column='EMAIL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    mobil = models.CharField(db_column='MOBIL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ist_webtermin = models.IntegerField(db_column='IST_WEBTERMIN', blank=True, null=True)  # Field name made lowercase.
    webtermin_titel = models.CharField(db_column='WEBTERMIN_TITEL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    will_email_erinnerung = models.IntegerField(db_column='WILL_EMAIL_ERINNERUNG', blank=True, null=True)  # Field name made lowercase.
    will_sms_erinnerung = models.IntegerField(db_column='WILL_SMS_ERINNERUNG', blank=True, null=True)  # Field name made lowercase.
    notizen = models.TextField(db_column='NOTIZEN', blank=True, null=True)  # Field name made lowercase.
    ist_weberinnerung = models.IntegerField(db_column='IST_WEBERINNERUNG', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'WARTELISTE'
        unique_together = (('termin_ende_datum', 'termin_ende_uhrzeit', 'kalender_nr'), ('termin_start_datum', 'termin_start_uhrzeit', 'kalender_nr'),)


class Woerterbuch(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    sprache = models.CharField(db_column='SPRACHE', max_length=10, blank=True, null=True)  # Field name made lowercase.
    name = models.CharField(db_column='NAME', max_length=20, blank=True, null=True)  # Field name made lowercase.
    text = models.CharField(db_column='TEXT', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'WOERTERBUCH'
        unique_together = (('sprache', 'name'),)


class Xadresse(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP')  # Field name made lowercase.
    strasse = models.CharField(db_column='STRASSE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    plz = models.CharField(db_column='PLZ', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ort = models.CharField(db_column='ORT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bundesland = models.CharField(db_column='BUNDESLAND', max_length=255, blank=True, null=True)  # Field name made lowercase.
    land_code = models.CharField(db_column='LAND_CODE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    postfach = models.CharField(db_column='POSTFACH', max_length=255, blank=True, null=True)  # Field name made lowercase.
    postfach_plz = models.CharField(db_column='POSTFACH_PLZ', max_length=255, blank=True, null=True)  # Field name made lowercase.
    postfach_ort = models.CharField(db_column='POSTFACH_ORT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    notizen = models.TextField(db_column='NOTIZEN', blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)  # Field name made lowercase.
    hausnummer = models.CharField(db_column='HAUSNUMMER', max_length=255)  # Field name made lowercase.
    tabelle = models.IntegerField(db_column='TABELLE')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'XADRESSE'


class Zahlungen(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    rechnungdoku_id = models.IntegerField(db_column='RECHNUNGDOKU_ID', blank=True, null=True)  # Field name made lowercase.
    zahlung_zeit = models.DateTimeField(db_column='ZAHLUNG_ZEIT', blank=True, null=True)  # Field name made lowercase.
    betrag = models.FloatField(db_column='BETRAG')  # Field name made lowercase.
    zahlung_text = models.CharField(db_column='ZAHLUNG_TEXT', max_length=255)  # Field name made lowercase.
    zahlung_art = models.CharField(db_column='ZAHLUNG_ART', max_length=255, blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    system_zeit = models.DateTimeField(db_column='SYSTEM_ZEIT', blank=True, null=True)  # Field name made lowercase.
    mandant_id = models.IntegerField(db_column='MANDANT_ID')  # Field name made lowercase.
    waehrung = models.CharField(db_column='WAEHRUNG', max_length=50, blank=True, null=True)  # Field name made lowercase.
    scheindoku_id = models.IntegerField(db_column='SCHEINDOKU_ID', blank=True, null=True)  # Field name made lowercase.
    zaehler = models.IntegerField(db_column='ZAEHLER', blank=True, null=True)  # Field name made lowercase.
    jahr = models.IntegerField(db_column='JAHR', blank=True, null=True)  # Field name made lowercase.
    umsatzzaehler_bar = models.FloatField(db_column='UMSATZZAEHLER_BAR', blank=True, null=True)  # Field name made lowercase.
    storno = models.IntegerField(db_column='STORNO', blank=True, null=True)  # Field name made lowercase.
    belegart = models.IntegerField(db_column='BELEGART', blank=True, null=True)  # Field name made lowercase.
    signatur = models.TextField(db_column='SIGNATUR', blank=True, null=True)  # Field name made lowercase.
    betrag_mwst = models.FloatField(db_column='BETRAG_MWST')  # Field name made lowercase.
    betrag_mwst_reduziert = models.FloatField(db_column='BETRAG_MWST_REDUZIERT')  # Field name made lowercase.
    journal = models.TextField(db_column='JOURNAL', blank=True, null=True)  # Field name made lowercase.
    seriennummer_sigzert = models.CharField(db_column='SERIENNUMMER_SIGZERT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    sigzert_ausgefallen = models.IntegerField(db_column='SIGZERT_AUSGEFALLEN', blank=True, null=True)  # Field name made lowercase.
    registrierkasse_nr = models.CharField(db_column='REGISTRIERKASSE_NR', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ZAHLUNGEN'
        unique_together = (('zaehler', 'mandant_id', 'jahr'), ('storno', 'belegart'),)


class Zahlungsplan(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    scheindoku_id = models.IntegerField(db_column='SCHEINDOKU_ID')  # Field name made lowercase.
    zahlung_datum = models.DateField(db_column='ZAHLUNG_DATUM', blank=True, null=True)  # Field name made lowercase.
    betrag = models.FloatField(db_column='BETRAG')  # Field name made lowercase.
    zahlung_text = models.CharField(db_column='ZAHLUNG_TEXT', max_length=255)  # Field name made lowercase.
    zahlung_art = models.CharField(db_column='ZAHLUNG_ART', max_length=20, blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=100, blank=True, null=True)  # Field name made lowercase.
    system_datum = models.DateField(db_column='SYSTEM_DATUM', blank=True, null=True)  # Field name made lowercase.
    system_uhrzeit = models.TimeField(db_column='SYSTEM_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS')  # Field name made lowercase.
    re_nr = models.CharField(db_column='RE_NR', unique=True, max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ZAHLUNGSPLAN'


class Ziffer(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    leistungsdoku_id = models.IntegerField(db_column='LEISTUNGSDOKU_ID', blank=True, null=True)  # Field name made lowercase.
    untersuchung_id = models.IntegerField(db_column='UNTERSUCHUNG_ID', blank=True, null=True)  # Field name made lowercase.
    typ = models.CharField(db_column='TYP', max_length=50, blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)  # Field name made lowercase.
    system_datum = models.DateField(db_column='SYSTEM_DATUM', blank=True, null=True)  # Field name made lowercase.
    system_uhrzeit = models.TimeField(db_column='SYSTEM_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    ziffer_nr = models.CharField(db_column='ZIFFER_NR', max_length=50, blank=True, null=True)  # Field name made lowercase.
    ziffer_anzahl = models.FloatField(db_column='ZIFFER_ANZAHL', blank=True, null=True)  # Field name made lowercase.
    ziffer_betrag = models.FloatField(db_column='ZIFFER_BETRAG', blank=True, null=True)  # Field name made lowercase.
    ziffer_waehrung = models.CharField(db_column='ZIFFER_WAEHRUNG', max_length=50, blank=True, null=True)  # Field name made lowercase.
    ziffer_punkte = models.FloatField(db_column='ZIFFER_PUNKTE', blank=True, null=True)  # Field name made lowercase.
    ziffer_km = models.FloatField(db_column='ZIFFER_KM', blank=True, null=True)  # Field name made lowercase.
    ziffer_kurztext = models.CharField(db_column='ZIFFER_KURZTEXT', max_length=50, blank=True, null=True)  # Field name made lowercase.
    ziffer_text = models.TextField(db_column='ZIFFER_TEXT', blank=True, null=True)  # Field name made lowercase.
    ziffer_begr = models.CharField(db_column='ZIFFER_BEGR', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ziffer_datum = models.DateField(db_column='ZIFFER_DATUM', blank=True, null=True)  # Field name made lowercase.
    ziffer_uhrzeit = models.TimeField(db_column='ZIFFER_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    ziffer_ca_kennzeichen = models.CharField(db_column='ZIFFER_CA_KENNZEICHEN', max_length=50, blank=True, null=True)  # Field name made lowercase.
    zusatzkennzeichen = models.CharField(db_column='ZUSATZKENNZEICHEN', max_length=50, blank=True, null=True)  # Field name made lowercase.
    reserve1 = models.CharField(db_column='RESERVE1', max_length=50, blank=True, null=True)  # Field name made lowercase.
    mwst_satz = models.IntegerField(db_column='MWST_SATZ', blank=True, null=True)  # Field name made lowercase.
    kostentyp = models.IntegerField(db_column='KOSTENTYP', blank=True, null=True)  # Field name made lowercase.
    kontierung = models.IntegerField(db_column='KONTIERUNG', blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    visitenadresse = models.TextField(db_column='VISITENADRESSE', blank=True, null=True)  # Field name made lowercase.
    sachkosten = models.TextField(db_column='SACHKOSTEN', blank=True, null=True)  # Field name made lowercase.
    zusatz = models.TextField(db_column='ZUSATZ', blank=True, null=True)  # Field name made lowercase.
    organ = models.TextField(db_column='ORGAN', blank=True, null=True)  # Field name made lowercase.
    begruendung_arzt = models.TextField(db_column='BEGRUENDUNG_ARZT', blank=True, null=True)  # Field name made lowercase.
    begruendung_gnr = models.CharField(db_column='BEGRUENDUNG_GNR', max_length=255, blank=True, null=True)  # Field name made lowercase.
    stationaer_von = models.DateField(db_column='STATIONAER_VON', blank=True, null=True)  # Field name made lowercase.
    stationaer_bis = models.DateField(db_column='STATIONAER_BIS', blank=True, null=True)  # Field name made lowercase.
    op_datum = models.DateField(db_column='OP_DATUM', blank=True, null=True)  # Field name made lowercase.
    ops = models.TextField(db_column='OPS', blank=True, null=True)  # Field name made lowercase.
    komplikation = models.TextField(db_column='KOMPLIKATION', blank=True, null=True)  # Field name made lowercase.
    privat_faktor = models.FloatField(db_column='PRIVAT_FAKTOR')  # Field name made lowercase.
    rabatt = models.FloatField(db_column='RABATT', blank=True, null=True)  # Field name made lowercase.
    op_dauer = models.FloatField(db_column='OP_DAUER', blank=True, null=True)  # Field name made lowercase.
    leistung_id = models.IntegerField(db_column='LEISTUNG_ID', blank=True, null=True)  # Field name made lowercase.
    vertragsart = models.IntegerField(db_column='VERTRAGSART', blank=True, null=True)  # Field name made lowercase.
    dokumentation_id = models.IntegerField(db_column='DOKUMENTATION_ID', blank=True, null=True)  # Field name made lowercase.
    sortierung = models.IntegerField(db_column='SORTIERUNG', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ZIFFER'


class ZiffernPrivat(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    typ = models.CharField(db_column='TYP', max_length=255, blank=True, null=True)  # Field name made lowercase.
    subtyp = models.CharField(db_column='SUBTYP', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ziffer = models.CharField(db_column='ZIFFER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ziffer_text = models.TextField(db_column='ZIFFER_TEXT', blank=True, null=True)  # Field name made lowercase.
    ziffer_text_kurz = models.CharField(db_column='ZIFFER_TEXT_KURZ', max_length=255, blank=True, null=True)  # Field name made lowercase.
    punkte = models.FloatField(db_column='PUNKTE')  # Field name made lowercase.
    gruppe = models.CharField(db_column='GRUPPE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    kuerzel = models.CharField(db_column='KUERZEL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    notizen = models.TextField(db_column='NOTIZEN', blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)  # Field name made lowercase.
    ziffer_von = models.DateField(db_column='ZIFFER_VON', blank=True, null=True)  # Field name made lowercase.
    ziffer_bis = models.DateField(db_column='ZIFFER_BIS', blank=True, null=True)  # Field name made lowercase.
    betrag = models.FloatField(db_column='BETRAG')  # Field name made lowercase.
    waehrung = models.CharField(db_column='WAEHRUNG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    version = models.CharField(db_column='VERSION', max_length=255, blank=True, null=True)  # Field name made lowercase.
    zeitstempel = models.TextField(db_column='ZEITSTEMPEL', blank=True, null=True)  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    regel_l = models.TextField(db_column='REGEL_L', blank=True, null=True)  # Field name made lowercase.
    mwst_satz = models.IntegerField(db_column='MWST_SATZ', blank=True, null=True)  # Field name made lowercase.
    kostentyp = models.IntegerField(db_column='KOSTENTYP', blank=True, null=True)  # Field name made lowercase.
    kontierung = models.IntegerField(db_column='KONTIERUNG', blank=True, null=True)  # Field name made lowercase.
    lager_kuerzel = models.CharField(db_column='LAGER_KUERZEL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    lager_menge = models.FloatField(db_column='LAGER_MENGE')  # Field name made lowercase.
    wa_texte = models.TextField(db_column='WA_TEXTE', blank=True, null=True)  # Field name made lowercase.
    angebot_gruppe = models.CharField(db_column='ANGEBOT_GRUPPE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    angebot_ueberschrift = models.CharField(db_column='ANGEBOT_UEBERSCHRIFT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    angebot_text = models.TextField(db_column='ANGEBOT_TEXT', blank=True, null=True)  # Field name made lowercase.
    angebot_bild = models.CharField(db_column='ANGEBOT_BILD', max_length=255, blank=True, null=True)  # Field name made lowercase.
    angebot_details = models.TextField(db_column='ANGEBOT_DETAILS', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ZIFFERN_PRIVAT'


class ZifferGruppe(models.Model):
    id = models.CharField(db_column='ID', primary_key=True, max_length=50)  # Field name made lowercase.
    gruppe = models.CharField(db_column='GRUPPE', max_length=200, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ZIFFER_GRUPPE'


class ZifferTyp(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    typ = models.CharField(db_column='TYP', max_length=50, blank=True, null=True)  # Field name made lowercase.
    ziffer_sortierung = models.IntegerField(db_column='ZIFFER_SORTIERUNG', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ZIFFER_TYP'


class Zuweiser(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    kue = models.CharField(db_column='KUE', unique=True, max_length=50, blank=True, null=True)  # Field name made lowercase.
    anrede = models.CharField(db_column='ANREDE', max_length=50, blank=True, null=True)  # Field name made lowercase.
    titel = models.CharField(db_column='TITEL', max_length=50, blank=True, null=True)  # Field name made lowercase.
    vorname = models.CharField(db_column='VORNAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    wvorname = models.CharField(db_column='WVORNAME', max_length=50, blank=True, null=True)  # Field name made lowercase.
    name = models.CharField(db_column='NAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    gebname = models.CharField(db_column='GEBNAME', max_length=50, blank=True, null=True)  # Field name made lowercase.
    namenszusatz = models.CharField(db_column='NAMENSZUSATZ', max_length=50, blank=True, null=True)  # Field name made lowercase.
    namensvorsatz = models.CharField(db_column='NAMENSVORSATZ', max_length=50, blank=True, null=True)  # Field name made lowercase.
    gebdatum = models.DateField(db_column='GEBDATUM', blank=True, null=True)  # Field name made lowercase.
    bild = models.TextField(db_column='BILD', blank=True, null=True)  # Field name made lowercase.
    notizen = models.TextField(db_column='NOTIZEN', blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    strasse = models.CharField(db_column='STRASSE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    plz = models.CharField(db_column='PLZ', max_length=50, blank=True, null=True)  # Field name made lowercase.
    ort = models.CharField(db_column='ORT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    land = models.CharField(db_column='LAND', max_length=50, blank=True, null=True)  # Field name made lowercase.
    adresse_id1 = models.CharField(db_column='Adresse_ID1', max_length=50, blank=True, null=True)  # Field name made lowercase.
    adresse_id2 = models.CharField(db_column='Adresse_ID2', max_length=50, blank=True, null=True)  # Field name made lowercase.
    adresse_id3 = models.CharField(db_column='Adresse_ID3', max_length=50, blank=True, null=True)  # Field name made lowercase.
    adresse_id4 = models.CharField(db_column='Adresse_ID4', max_length=50, blank=True, null=True)  # Field name made lowercase.
    telefon = models.CharField(db_column='TELEFON', max_length=50, blank=True, null=True)  # Field name made lowercase.
    telefon_2 = models.CharField(db_column='TELEFON_2', max_length=50, blank=True, null=True)  # Field name made lowercase.
    fax = models.CharField(db_column='FAX', max_length=50, blank=True, null=True)  # Field name made lowercase.
    mobil = models.CharField(db_column='MOBIL', max_length=50, blank=True, null=True)  # Field name made lowercase.
    email = models.CharField(db_column='EMAIL', max_length=50, blank=True, null=True)  # Field name made lowercase.
    bank = models.CharField(db_column='BANK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bank_plz = models.CharField(db_column='BANK_PLZ', max_length=50, blank=True, null=True)  # Field name made lowercase.
    bank_ort = models.CharField(db_column='BANK_ORT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    bank_land = models.CharField(db_column='BANK_LAND', max_length=50, blank=True, null=True)  # Field name made lowercase.
    kto_nr = models.CharField(db_column='KTO_NR', max_length=50, blank=True, null=True)  # Field name made lowercase.
    blz = models.CharField(db_column='BLZ', max_length=50, blank=True, null=True)  # Field name made lowercase.
    kto_inhaber = models.CharField(db_column='KTO_INHABER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS')  # Field name made lowercase.
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)  # Field name made lowercase.
    vertragspartnernr = models.CharField(db_column='VERTRAGSPARTNERNR', max_length=50, blank=True, null=True)  # Field name made lowercase.
    fachgebiet = models.CharField(db_column='FACHGEBIET', max_length=50, blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung_datum = models.DateField(db_column='LETZTE_AENDERUNG_DATUM', blank=True, null=True)  # Field name made lowercase.
    letzte_aenderung_uhrzeit = models.TimeField(db_column='LETZTE_AENDERUNG_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    briefdaten = models.TextField(db_column='BRIEFDATEN', blank=True, null=True)  # Field name made lowercase.
    anschrift_zeile1 = models.CharField(db_column='ANSCHRIFT_ZEILE1', max_length=255, blank=True, null=True)  # Field name made lowercase.
    anschrift_zeile2 = models.CharField(db_column='ANSCHRIFT_ZEILE2', max_length=255, blank=True, null=True)  # Field name made lowercase.
    befundkomm_betrkey = models.CharField(db_column='BEFUNDKOMM_BETRKEY', max_length=50, blank=True, null=True)  # Field name made lowercase.
    befundkomm_email = models.CharField(db_column='BEFUNDKOMM_EMAIL', max_length=50, blank=True, null=True)  # Field name made lowercase.
    befundkomm_menr = models.CharField(db_column='BEFUNDKOMM_MENR', max_length=50, blank=True, null=True)  # Field name made lowercase.
    praxis_typ = models.IntegerField(db_column='PRAXIS_TYP')  # Field name made lowercase.
    bsnr = models.CharField(db_column='BSNR', max_length=50, blank=True, null=True)  # Field name made lowercase.
    befundkomm_beftypen = models.CharField(db_column='BEFUNDKOMM_BEFTYPEN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    praxiszeiten = models.TextField(db_column='PRAXISZEITEN', blank=True, null=True)  # Field name made lowercase.
    lanr = models.CharField(db_column='LANR', max_length=50, blank=True, null=True)  # Field name made lowercase.
    anforderungslabor = models.IntegerField(db_column='ANFORDERUNGSLABOR', blank=True, null=True)  # Field name made lowercase.
    komm_versandtyp = models.CharField(db_column='KOMM_VERSANDTYP', max_length=50, blank=True, null=True)  # Field name made lowercase.
    komm_technik = models.CharField(db_column='KOMM_TECHNIK', max_length=50, blank=True, null=True)  # Field name made lowercase.
    verrechnungsstelle = models.CharField(db_column='VERRECHNUNGSSTELLE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    befundkomm_teilnehmer = models.IntegerField(db_column='BEFUNDKOMM_TEILNEHMER', blank=True, null=True)  # Field name made lowercase.
    befunde_immer_erhalten = models.IntegerField(db_column='BEFUNDE_IMMER_ERHALTEN', blank=True, null=True)  # Field name made lowercase.
    komm_datenformat_laborauftrag = models.CharField(db_column='KOMM_DATENFORMAT_LABORAUFTRAG', max_length=50, blank=True, null=True)  # Field name made lowercase.
    komm_technik_laborauftrag = models.CharField(db_column='KOMM_TECHNIK_LABORAUFTRAG', max_length=50, blank=True, null=True)  # Field name made lowercase.
    sprache = models.CharField(db_column='SPRACHE', max_length=50, blank=True, null=True)  # Field name made lowercase.
    serienbrief_versandarten = models.CharField(db_column='SERIENBRIEF_VERSANDARTEN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    serienbrief_stdversandart = models.CharField(db_column='SERIENBRIEF_STDVERSANDART', max_length=50, blank=True, null=True)  # Field name made lowercase.
    hausnummer = models.CharField(db_column='HAUSNUMMER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    serienbrief_typen = models.TextField(db_column='SERIENBRIEF_TYPEN', blank=True, null=True)  # Field name made lowercase.
    einstellungen = models.TextField(db_column='EINSTELLUNGEN', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ZUWEISER'
        unique_together = (('vertragspartnernr', 'lanr'),)


class ZytoBefunde(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    patient_id = models.IntegerField(db_column='PATIENT_ID')  # Field name made lowercase.
    untersuchung_id = models.IntegerField(db_column='UNTERSUCHUNG_ID')  # Field name made lowercase.
    typ = models.IntegerField(db_column='TYP')  # Field name made lowercase.
    datum = models.DateField(db_column='DATUM', blank=True, null=True)  # Field name made lowercase.
    abstrich_freitext = models.TextField(db_column='ABSTRICH_FREITEXT', blank=True, null=True)  # Field name made lowercase.
    befund_ergebnis = models.CharField(db_column='BEFUND_ERGEBNIS', max_length=50, blank=True, null=True)  # Field name made lowercase.
    befund_freitext = models.TextField(db_column='BEFUND_FREITEXT', blank=True, null=True)  # Field name made lowercase.
    bemerkung = models.TextField(db_column='BEMERKUNG', blank=True, null=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS')  # Field name made lowercase.
    system_datum = models.DateField(db_column='SYSTEM_DATUM', blank=True, null=True)  # Field name made lowercase.
    system_uhrzeit = models.TimeField(db_column='SYSTEM_UHRZEIT', blank=True, null=True)  # Field name made lowercase.
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)  # Field name made lowercase.
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)  # Field name made lowercase.
    ermaechtigung_id = models.IntegerField(db_column='ERMAECHTIGUNG_ID')  # Field name made lowercase.
    bild = models.CharField(db_column='BILD', max_length=50, blank=True, null=True)  # Field name made lowercase.
    position = models.CharField(db_column='POSITION', max_length=50, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ZYTO_BEFUNDE'
