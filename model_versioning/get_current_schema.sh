#!/bin/bash

MYSQL_HOST=kerberos

usage() {
  cat <<EOF
Usage:
get_current_schema <version_string>

  version_string       A string that is appended to the filename
                       and should refer to the current ACETO version like "1.6.47-P4".
                       Try to avoid spaces.
EOF
}


die() {
  echo "ERROR: $1"
  exit 1
}


check_prerequisites() {

  # Check if .my.cnf exists and access to DB is ok
  mysql -h $MYSQL_HOST --execute 'show databases;' -D acetoweb || die "Could not connect to database." >/dev/null
}


# fetches DB version from aceto database and saves it into $db_version variable.
get_db_version() {
  db_version=$(mysql --host $MYSQL_HOST -se "select WERT from acetoweb.PRODUKT where MODUL=\"MODUL_BASIS\" and EINSTELLUNG=\"DATENBANK_VERSION\";"|cut -f1)
  while [ "${db_version}" == "" ]; do
    echo "The DB version could not be retrieved."
    echo "Please enter a db version manually (or hit [Ctrl]+[C]). It should be like '1.4.256'."
    echo "You can find it with this SQL query:"
    echo "SELECT WERT from PRODUKT WHERE EINSTELLUNG = 'DATENBANK_VERSION';"
    read -p "DB version: " db_version
  done
  echo "Database version: ${db_version}"
}


# fetches the SQL schema from the aceto DB and saves it into a file
get_mysql_schema(){
  if [ -f ${acetoschema} ]; then
    echo "A file with the name '${acetoschema}' already exists. Skipping."
  else
    echo "Trying to create database schema from acetoweb DB: '${acetoschema}' ..."
    mysqldump acetoweb --host $MYSQL_HOST -d > ${acetoschema}
    if [ $? == 0 ]; then
      echo "Everything OK."
    else
      echo "FAILED"
      rm -rf ${acetoschema}
    fi
  fi
}

# fetches the Django models vie "inspectdb" from the aceto DB and saves it into a file
get_django_schema() {
  if [ -f ${djangoschema} ]; then
    echo "A file with the name '${djangoschema}' already exists. Skipping."
  else
    echo "Trying to create Django model schema from acetoweb DB: '${djangoschema}'..."
    (
      cd ..
      . env/bin/activate
      ./manage.py inspectdb --database acetomed
    )  > ${djangoschema}
    if [ $? == 0 ]; then
      echo "Everything OK."
    else
      echo "FAILED"
      rm -rf ${djangoschema}
    fi
  fi
}



######## MAIN ##########

if ! ping -q -c1 kerberos; then
  echo "ERROR: Kerberos is not available."
  exit 1
fi

check_prerequisites
get_db_version
acetoschema="aceto-sql-schema-${db_version}.sql"
djangoschema="django-models-${db_version}.py"

get_mysql_schema
get_django_schema
