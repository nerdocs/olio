from django.contrib import admin
from olio.acetobugs.models import Call, Problem, AcetoMandant


class CallInline(admin.TabularInline):
    model = Call.problems.through
    can_delete = False
    verbose_name = 'Anruf'
    verbose_name_plural = 'Anrufe'


@admin.register(Problem)
class ProblemAdmin(admin.ModelAdmin):
    inlines = [CallInline]
    list_display = ('created', 'title', 'description')
    list_filter = ['solved']


@admin.register(Call)
class CallAdmin(admin.ModelAdmin):
    pass


admin.site.register(AcetoMandant)
