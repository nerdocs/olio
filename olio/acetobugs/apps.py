from django.apps import AppConfig


class AcetobugsConfig(AppConfig):
    name = 'acetobugs'
