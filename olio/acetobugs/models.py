from django.db import models
from django.utils.datetime_safe import strftime


class AcetoMandant(models.Model):
    id = models.PositiveIntegerField(primary_key=True)
    firstname = models.CharField(max_length=255)
    lastname = models.CharField(max_length=255)

    def __str__(self):
        return '{}, {}'.format(self.lastname, self.firstname)

    class Meta:
        verbose_name = 'Aceto-Mandant'
        verbose_name_plural = 'Aceto-Mandanten'


class Problem(models.Model):
    aceto_mandant = models.ForeignKey(AcetoMandant,
                                     default=716,
                                     on_delete=models.PROTECT)
    aceto_ticketnumber = models.IntegerField(verbose_name='Hilfefall-Nr.')

    title = models.CharField(max_length=50, verbose_name='Titel')
    description = models.TextField(verbose_name='Beschreibung')
    created = models.DateTimeField(auto_now=True, verbose_name='Erstellt am')
    solved = models.BooleanField(verbose_name='Problem wurde gelöst')

    class Meta:
        verbose_name = 'Problem'
        verbose_name_plural = 'Probleme'

    def __str__(self):
        return self.title


class Call(models.Model):
    timestamp = models.DateTimeField(editable=True,
                                     verbose_name='Anrufzeit')
    reached = models.BooleanField(default=False,
                                  verbose_name="ACETO am Telefon erreicht")
    problems = models.ManyToManyField(Problem,
                                      verbose_name='Probleme',
                                      related_name='calls')
    description = models.TextField(blank=True,
                                   verbose_name='Zusammenfassung des Anrufs')

    def __str__(self):
        return '{}'.format(strftime(self.timestamp, '%d.%m.%Y %M:%S'))

    class Meta:
        verbose_name = 'Anruf'
        verbose_name_plural = 'Anrufe'
