from django.views.generic import TemplateView, ListView
from olio.acetobugs.models import Problem


class ProblemsView(ListView):
    model = Problem

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['problems'] = Problem.objects.all()
        return context


class ProblemChangeView(ListView):
    model = Problem

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['problem'] = Problem.objects.get(pk=kwargs['pk'])
        return context