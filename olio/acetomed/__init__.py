

class Router(object):
    """
    A Django DB router that ensures that all acetomed models go to
    the acetomed database.
    """
    def db_for_read(self, model, **hints):
        """
        Attempts to read acetomed models go to acetomed.
        """
        if model._meta.app_label == 'acetomed':
            return 'acetomed'
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write patient models go to acetomed.
        """
        # ATM don't allow writing to acetomed database

        if model._meta.app_label == 'acetomed':
            return None
            # return 'acetomed'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the acetomed app is involved.
        """
        if obj1._meta.app_label == 'acetomed' or \
           obj2._meta.app_label == 'acetomed':
            return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Make sure the acetomed app only appears in the 'acetomed'
        database. Migrations ATM aren't allowed.
        """
        if app_label == 'acetomed' or db == 'acetomed':
            return False
            # return db == 'acetomed'
        return None