from django.apps import apps
from django.contrib import admin
from django.contrib.admin.sites import AlreadyRegistered

from .models import *


@admin.register(Dokumentation)
class DokumentationAdmin(admin.ModelAdmin):
    # list_display = ("datum", "patient_id", "typ", "doku_text",)
    # list_display_links = ("doku_text",)
    list_filter = ("typ", "status", "patient", )
    # readonly_fields = ('patient',)


@admin.register(Stdleist)
class StdleistAdmin(admin.ModelAdmin):
    search_fields = ("kasse", )
    list_display = ("bezeichnung", "kasse_ziffern_kette", "kasse")

@admin.register(Laborstamm)
class LaborstammAdmin(admin.ModelAdmin):
    search_fields = ("kuerzel", "bezeichnung",)
    list_display = ("kuerzel", "bezeichnung", "einheit",)


@admin.register(Patient)
class PatientAdmin(admin.ModelAdmin):
    search_fields = ("name", "vorname")

@admin.register(Medikament)
class MedikamentAdmin(admin.ModelAdmin):
    list_display = ("medikament", "menge", "mengenart",
            "morgens", "mittags", "abends")
    search_fields = ('medikament',)
    raw_id_fields = ('untersuchung_id', 'doku_id', 'patient_id')
    list_filter = ('patient_id',)


@admin.register(ErinnerungTermin)
class ErinnerungTerminAdmin(admin.ModelAdmin):
    list_display = ('datum', 'patient', 'bemerkung')
    list_filter = ['bezeichnung']

@admin.register(Formulare)
class FormulareAdmin(admin.ModelAdmin):
    list_display = ('name', 'id')

@admin.register(Combo)
class ComboAdmin(admin.ModelAdmin):
    list_filter = ['name']
    list_display = ['kennung', 'name', 'text_de']
    ordering = ['kennung']

for app_name in ['acetomed', ]:
    app_models = apps.get_app_config(app_name).get_models()
    for model in app_models:
        try:
            if not model._meta.abstract:
                admin.site.register(model)
        except AlreadyRegistered:
            pass




# sinnlos:
#admin.site.register(Import)
#admin.site.register(Import2)
