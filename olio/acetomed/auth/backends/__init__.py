from django.contrib.auth.models import User, Permission
from olio.acetomed.models import Benutzer


class AcetoAuthBackend:

    def get_user(self, user_id):
        """
        Returns a user with the given user id from the ACETO database.

        It creates a Django User object if that doesn't exist and matches it
        with the ACETO user id.
        """
        try:
            benutzer = Benutzer.objects.get(id=user_id)
        except Benutzer.DoesNotExist:
            return None

        try:
            # try to get a user from the Django db
            user = User.objects.get(id=user_id)
        except User.DoesNotExist:
            # if there is none, create a new one and give him the same id
            # as the ACETO user
            user = User(id=benutzer.id,
                        username=benutzer.kue,
                        password=benutzer.kennwort,
                        first_name=benutzer.vorname,
                        last_name=benutzer.name,
                        )
            user.is_staff = True  # FIXME! security flaw!
            for permission_name in ["olio.add_benutzer",
                               "olio.change_benutzer",
                               "olio.add_user",
                               "olio.change_user" ]:
                try:
                    p = Permission.objects.get(codename=permission_name)
                except Permission.DoesNotExist:
                    continue
                
                user.user_permissions.add(p)
            
            user.is_superuser = True
            user.save()
        
        # link Django user to ACETO user
        user.save()
        user.profile = benutzer
        
        return user
    
    def authenticate(self, request, username=None, password=None):
        """Check the username/password and return a user."""
        try:
            benutzer = Benutzer.objects.get(kue=username)
            if benutzer.kennwort == password:
                return self.get_user(benutzer.id)
        except Benutzer.DoesNotExist:
            pass

        return None

