
import base64

from django.db import models


class Base64CharField(models.CharField):
    """Represents a char field which is saved as base64 in the database"""

    def from_db_value(self, value, expression, connection, context):
        """Returns a str from the database value, which is encoded as base64 string"""
        if value is None:
            return None
        else:
            return base64.b64decode(value).decode("utf-8")

#    def ###
#        value = base64.b64encode(value)


class Base64TextField(models.TextField):
    """Represents a text field which is saved as base64 in the database"""

    def from_db_value(self, value, expression, connection, context):
        """Returns a str from the database value, which is encoded as base64 string"""
        if value is None:
            return None
        else:
            return base64.b64decode(value).decode("utf-8")
