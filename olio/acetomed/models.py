# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.

from django.db import models
from .fields import Base64CharField, Base64TextField
from enum import Enum

__VERSION__ = (1,4,274)

Fachgebiete = {
    1: 'Allgemeinmedizin',
    # FIXME: add rest
}


class AbrechnungBuffer(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)
    mandand_id = models.IntegerField(db_column='MANDAND_ID', blank=True, null=True)
    abrechnungsstellekennung = models.CharField(db_column='AbrechnungsstelleKennung', max_length=255, blank=True, null=True)
    abrechnungskassenkennungen = models.CharField(db_column='AbrechnungsKassenKennungen', max_length=255, blank=True, null=True)
    abrechnungskassentypen = models.CharField(db_column='AbrechnungsKassenTypen', max_length=255, blank=True, null=True)
    erstellung = models.DateTimeField(db_column='Erstellung', blank=True, null=True)
    zeitraumjahr = models.IntegerField(db_column='ZeitraumJahr', blank=True, null=True)
    zeitraumkennung = models.IntegerField(db_column='ZeitraumKennung', blank=True, null=True)
    datum_start = models.DateField(db_column='DATUM_START', blank=True, null=True)
    datum_ende = models.DateField(db_column='DATUM_ENDE', blank=True, null=True)
    text = models.TextField(db_column='Text', blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ABRECHNUNG_BUFFER'


class AbrechnungHl7Zuordnung(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    hl7_segment = models.CharField(db_column='HL7_SEGMENT', max_length=50, blank=True, null=True)
    hl7_ergebnis_typ = models.CharField(db_column='HL7_ERGEBNIS_TYP', max_length=50, blank=True, null=True)
    hl7_ergebnis_beschreibung = models.CharField(db_column='HL7_ERGEBNIS_BESCHREIBUNG', max_length=255, blank=True, null=True)
    hl7_ergebnis_identifier = models.CharField(db_column='HL7_ERGEBNIS_IDENTIFIER', max_length=50, blank=True, null=True)
    kasse = models.CharField(db_column='KASSE', max_length=50, blank=True, null=True)
    ziffer = models.CharField(db_column='ZIFFER', max_length=50, blank=True, null=True)
    ziffer_anzahl_faktor = models.FloatField(db_column='ZIFFER_ANZAHL_FAKTOR')

    class Meta:
        managed = False
        db_table = 'ABRECHNUNG_HL7_ZUORDNUNG'


class AcetoEinstellungen(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    name = models.CharField(db_column='NAME', max_length=255, blank=True, null=True)
    wert = models.TextField(db_column='WERT', blank=True, null=True)
    werteliste = models.CharField(db_column='WERTELISTE', max_length=255, blank=True, null=True)
    status = models.IntegerField(db_column='STATUS')
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)
    typ = models.IntegerField(db_column='TYP')
    bereich = models.CharField(db_column='BEREICH', max_length=255, blank=True, null=True)
    fremd_id = models.IntegerField(db_column='FREMD_ID')
    mandant_id = models.IntegerField(db_column='MANDANT_ID', blank=True, null=True)
    # on_delete=models.PROTECT because of ReadOnly DB
    benutzer = models.ForeignKey("Benutzer", db_column='BENUTZER_ID', blank=True, null=True, on_delete=models.PROTECT)
    rechner_name = models.CharField(db_column='RECHNER_NAME', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ACETO_EINSTELLUNGEN'


class AcetoKontakt(models.Model):
    web_id = models.IntegerField(db_column='WEB_ID', primary_key=True)
    datum = models.DateTimeField(db_column='DATUM', blank=True, null=True)
    xml = models.TextField(db_column='XML')
    patient_id = models.IntegerField(db_column='PATIENT_ID', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ACETO_KONTAKT'


class Adresse(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    typ = models.IntegerField(db_column='TYP')
    strasse = models.CharField(db_column='STRASSE', max_length=255, blank=True, null=True)
    hausnummer = models.CharField(db_column='HAUSNUMMER', max_length=255, blank=True, null=True)
    adresszusatz = models.CharField(db_column='ADRESSZUSATZ', max_length=255, blank=True, null=True)
    plz = models.CharField(db_column='PLZ', max_length=255, blank=True, null=True)
    ort = models.CharField(db_column='ORT', max_length=255, blank=True, null=True)
    bundesland = models.CharField(db_column='BUNDESLAND', max_length=255, blank=True, null=True)
    land_code = models.CharField(db_column='LAND_CODE', max_length=255, blank=True, null=True)
    postfach = models.CharField(db_column='POSTFACH', max_length=255, blank=True, null=True)
    postfach_plz = models.CharField(db_column='POSTFACH_PLZ', max_length=255, blank=True, null=True)
    postfach_ort = models.CharField(db_column='POSTFACH_ORT', max_length=255, blank=True, null=True)
    postfach_land_code = models.CharField(db_column='POSTFACH_LAND_CODE', max_length=255, blank=True, null=True)
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)
    tabelle = models.IntegerField(db_column='TABELLE')

    class Meta:
        managed = False
        db_table = 'ADRESSE'
        verbose_name = "Adresse"
        verbose_name_plural = "Adressen"

    def __str__(self):
        return "{} {}, {} {}".format(
            self.strasse,
            self.hausnummer,
            self.plz,
            self.ort)


class Allergene(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)
    kuerzel = models.CharField(db_column='KUERZEL', max_length=255, blank=True, null=True)
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)
    beschreibung = models.TextField(db_column='BESCHREIBUNG', blank=True, null=True)
    gruppe = models.IntegerField(db_column='GRUPPE', blank=True, null=True)
    mandanten = models.CharField(db_column='MANDANTEN', max_length=255, blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)
    prick = models.IntegerField(db_column='PRICK', blank=True, null=True)
    prick_prick = models.IntegerField(db_column='PRICK_PRICK', blank=True, null=True)
    epicutan = models.IntegerField(db_column='EPICUTAN', blank=True, null=True)
    intracutan = models.IntegerField(db_column='INTRACUTAN', blank=True, null=True)
    scratch = models.IntegerField(db_column='SCRATCH', blank=True, null=True)
    bluttest_intern = models.IntegerField(db_column='BLUTTEST_INTERN', blank=True, null=True)
    bluttest_extern = models.IntegerField(db_column='BLUTTEST_EXTERN', blank=True, null=True)
    impfung = models.IntegerField(db_column='IMPFUNG', blank=True, null=True)
    typ_profil = models.CharField(db_column='TYP_PROFIL', max_length=255, blank=True, null=True)
    profil_parameter_ids = models.CharField(db_column='PROFIL_PARAMETER_IDS', max_length=255, blank=True, null=True)
    favorit = models.IntegerField(db_column='FAVORIT', blank=True, null=True)
    favorit_position = models.IntegerField(db_column='FAVORIT_POSITION', blank=True, null=True)
    individuell = models.IntegerField(db_column='INDIVIDUELL', blank=True, null=True)
    labor_auftragparameter_codeimlabor = models.CharField(db_column='LABOR_AUFTRAGPARAMETER_CODEIMLABOR', max_length=255, blank=True, null=True)
    laborauftrag_zuweiser_id = models.IntegerField(db_column='LABORAUFTRAG_ZUWEISER_ID', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ALLERGENE'
        unique_together = (('kuerzel', 'gruppe'),)
        verbose_name_plural = "Allergene"

    def __str__(self):
        return self.bezeichnung


class Allergentest(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    allergentestdokuid = models.IntegerField(db_column='ALLERGENTESTDOKUID', unique=True, blank=True, null=True)
    gesamtige = models.IntegerField(db_column='GESAMTIGE', blank=True, null=True)
    start_zeit = models.DateTimeField(db_column='START_ZEIT', blank=True, null=True)
    end_zeit = models.DateTimeField(db_column='END_ZEIT', blank=True, null=True)
    timer_typ = models.IntegerField(db_column='TIMER_TYP', blank=True, null=True)
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)
    bemerkung = models.CharField(db_column='BEMERKUNG', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ALLERGENTEST'


class Allergentestwert(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    allergentestdokuid = models.IntegerField(db_column='ALLERGENTESTDOKUID', blank=True, null=True)
    allergen_id = models.IntegerField(db_column='ALLERGEN_ID', blank=True, null=True)
    ergebniswert_1 = models.CharField(db_column='ERGEBNISWERT_1', max_length=255, blank=True, null=True)
    ergebniswert_2 = models.CharField(db_column='ERGEBNISWERT_2', max_length=255, blank=True, null=True)
    bluttest = models.IntegerField(db_column='BLUTTEST', blank=True, null=True)
    testtyp = models.IntegerField(db_column='TESTTYP', blank=True, null=True)
    anamnese = models.IntegerField(db_column='ANAMNESE', blank=True, null=True)
    beschreibung = models.CharField(db_column='BESCHREIBUNG', max_length=255, blank=True, null=True)
    gruppe = models.IntegerField(db_column='GRUPPE', blank=True, null=True)
    position = models.IntegerField(db_column='POSITION', blank=True, null=True)
    beauftragt = models.IntegerField(db_column='BEAUFTRAGT', blank=True, null=True)
    referenz = models.CharField(db_column='REFERENZ', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ALLERGENTESTWERT'


class Anamnesebogenstamm(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)
    ermaechtigung = models.CharField(db_column='ERMAECHTIGUNG', max_length=255, blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ANAMNESEBOGENSTAMM'


class Angebot(models.Model):
    angebotdoku_id = models.IntegerField(db_column='ANGEBOTDOKU_ID', primary_key=True)
    untersuchung_id = models.IntegerField(db_column='UNTERSUCHUNG_ID')
    angebot_nr = models.CharField(db_column='ANGEBOT_NR', unique=True, max_length=50, blank=True, null=True)
    angebot_text = models.TextField(db_column='ANGEBOT_TEXT', blank=True, null=True)
    anschrift = models.TextField(db_column='ANSCHRIFT', blank=True, null=True)
    anschrift_xml = models.TextField(db_column='ANSCHRIFT_XML', blank=True, null=True)
    absende_anschrift = models.TextField(db_column='ABSENDE_ANSCHRIFT', blank=True, null=True)
    positionen = models.TextField(db_column='POSITIONEN', blank=True, null=True)
    anzahl_pos = models.IntegerField(db_column='ANZAHL_POS')
    summe_netto = models.FloatField(db_column='SUMME_NETTO', blank=True, null=True)
    summe_netto_1 = models.FloatField(db_column='SUMME_NETTO_1', blank=True, null=True)
    summe_netto_2 = models.FloatField(db_column='SUMME_NETTO_2', blank=True, null=True)
    summe_brutto = models.FloatField(db_column='SUMME_BRUTTO', blank=True, null=True)
    summe_brutto_1 = models.FloatField(db_column='SUMME_BRUTTO_1', blank=True, null=True)
    summe_brutto_2 = models.FloatField(db_column='SUMME_BRUTTO_2', blank=True, null=True)
    mwst_1 = models.FloatField(db_column='MWST_1', blank=True, null=True)
    mwst_2 = models.FloatField(db_column='MWST_2', blank=True, null=True)
    summe_mwst = models.FloatField(db_column='SUMME_MWST', blank=True, null=True)
    summe_mwst_1 = models.FloatField(db_column='SUMME_MWST_1', blank=True, null=True)
    summe_mwst_2 = models.FloatField(db_column='SUMME_MWST_2', blank=True, null=True)
    waehrung = models.CharField(db_column='WAEHRUNG', max_length=50, blank=True, null=True)
    unterschreiber = models.CharField(db_column='UNTERSCHREIBER', max_length=50, blank=True, null=True)
    signed = models.CharField(db_column='SIGNED', max_length=255, blank=True, null=True)
    offene_summe_brutto = models.FloatField(db_column='OFFENE_SUMME_BRUTTO')
    zaehler = models.IntegerField(db_column='ZAEHLER')
    datum_versendet = models.DateField(db_column='DATUM_VERSENDET', blank=True, null=True)
    datum_storniert = models.DateField(db_column='DATUM_STORNIERT', blank=True, null=True)
    status_zusatz = models.TextField(db_column='STATUS_ZUSATZ', blank=True, null=True)
    mahn_stufe = models.IntegerField(db_column='MAHN_STUFE')
    datum_abgelehnt = models.DateField(db_column='DATUM_ABGELEHNT', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ANGEBOT'


class Angebotposition(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    angebotsdoku_id = models.IntegerField(db_column='ANGEBOTSDOKU_ID', blank=True, null=True)
    pos_nr = models.IntegerField(db_column='POS_NR', blank=True, null=True)
    anzahl = models.FloatField(db_column='ANZAHL', blank=True, null=True)
    position_datum = models.DateField(db_column='POSITION_DATUM', blank=True, null=True)
    einzelpreis = models.FloatField(db_column='EINZELPREIS', blank=True, null=True)
    mwst_satz = models.IntegerField(db_column='MWST_SATZ', blank=True, null=True)
    kontierung = models.IntegerField(db_column='KONTIERUNG', blank=True, null=True)
    pos_text = models.CharField(db_column='POS_TEXT', max_length=255, blank=True, null=True)
    pos_zusatz_text = models.TextField(db_column='POS_ZUSATZ_TEXT', blank=True, null=True)
    mwst_preis = models.FloatField(db_column='MWST_PREIS', blank=True, null=True)
    brutto_preis = models.FloatField(db_column='BRUTTO_PREIS', blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)
    system_datum = models.DateField(db_column='SYSTEM_DATUM', blank=True, null=True)
    system_uhrzeit = models.TimeField(db_column='SYSTEM_UHRZEIT', blank=True, null=True)
    faktor = models.FloatField(db_column='FAKTOR')
    abzug = models.FloatField(db_column='ABZUG')
    typ_kosten = models.IntegerField(db_column='TYP_KOSTEN')
    angebotsgruppe = models.CharField(db_column='ANGEBOTSGRUPPE', max_length=255, blank=True, null=True)
    angebotbild = models.CharField(db_column='ANGEBOTBILD', max_length=255, blank=True, null=True)
    properties = models.TextField(db_column='PROPERTIES', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ANGEBOTPOSITION'
        verbose_name_plural = "Angebote"


class Arbeitsliste(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    patient_id = models.IntegerField(db_column='PATIENT_ID')
    ermaechtigung_id = models.IntegerField(db_column='ERMAECHTIGUNG_ID')
    patient_name = models.CharField(db_column='PATIENT_NAME', max_length=255, blank=True, null=True)
    doku_id = models.IntegerField(db_column='DOKU_ID', blank=True, null=True)
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)
    arbeitsliste_nr = models.IntegerField(db_column='ARBEITSLISTE_NR', blank=True, null=True)
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)
    status = models.IntegerField(db_column='STATUS')
    einstellungen = models.TextField(db_column='EINSTELLUNGEN', blank=True, null=True)
    plan_start_datum = models.DateField(db_column='PLAN_START_DATUM', blank=True, null=True)
    plan_start_uhrzeit = models.TimeField(db_column='PLAN_START_UHRZEIT', blank=True, null=True)
    plan_ende_datum = models.DateField(db_column='PLAN_ENDE_DATUM', blank=True, null=True)
    plan_ende_uhrzeit = models.TimeField(db_column='PLAN_ENDE_UHRZEIT', blank=True, null=True)
    letzte_aenderung_datum = models.DateField(db_column='LETZTE_AENDERUNG_DATUM', blank=True, null=True)
    letzte_aenderung_uhrzeit = models.TimeField(db_column='LETZTE_AENDERUNG_UHRZEIT', blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)
    erledigt = models.IntegerField(db_column='ERLEDIGT')

    class Meta:
        managed = False
        db_table = 'ARBEITSLISTE'
        verbose_name_plural = "Arbeitslisten"


class Arztbrief(models.Model):
    id = models.IntegerField(db_column='ARZTBRIEF_ID', unique=True, primary_key=True)
    status_diktat = models.IntegerField(db_column='STATUS_DIKTAT', blank=True, null=True)
    anschrift = Base64TextField(db_column='ANSCHRIFT', blank=True, null=True)
    kontrolle_abschnitt = Base64TextField(db_column='KONTROLLE_ABSCHNITT', blank=True, null=True)
    datum_versendet = models.DateField(db_column='DATUM_VERSENDET', blank=True, null=True)
    status_arbeitsfluss = models.IntegerField(db_column='STATUS_ARBEITSFLUSS', blank=True, null=True)
    fachbefund_abschnitt = Base64TextField(db_column='FACHBEFUND_ABSCHNITT', blank=True, null=True)
    operation_abschnitt = Base64TextField(db_column='OPERATION_ABSCHNITT', blank=True, null=True)
    ab_unterschreiber = models.CharField(db_column='AB_UNTERSCHREIBER', max_length=50, blank=True, null=True)
    ab_signed = models.CharField(db_column='AB_SIGNED', max_length=255, blank=True, null=True)
    datum_storniert = models.DateField(db_column='DATUM_STORNIERT', blank=True, null=True)
    bemerkung_stornierung = models.CharField(db_column='BEMERKUNG_STORNIERUNG', max_length=255, blank=True, null=True)
    fremdbefund_abschnitt = Base64TextField(db_column='FREMDBEFUND_ABSCHNITT', blank=True, null=True)
    invitro_abschnitt = models.TextField(db_column='INVITRO_ABSCHNITT', blank=True, null=True)
    augen_abschnitt = models.TextField(db_column='AUGEN_ABSCHNITT', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ARZTBRIEF'
        verbose_name_plural = "Arztbriefe"

    def __str__(self):
        storniert = self.datum_storniert is not None
        val = '{} {}'.format(self.ab_unterschreiber, self.status_arbeitsfluss)
        if storniert:
            val += ' (storniert)'
        return val


class Arztbriefliste(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    patient_id = models.IntegerField(db_column='PATIENT_ID')
    ermaechtigung_id = models.IntegerField(db_column='ERMAECHTIGUNG_ID')
    patient_name = models.CharField(db_column='PATIENT_NAME', max_length=255, blank=True, null=True)
    doku_id = models.IntegerField(db_column='DOKU_ID', blank=True, null=True)
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)
    arbeitsliste_nr = models.IntegerField(db_column='ARBEITSLISTE_NR', blank=True, null=True)
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)
    status = models.IntegerField(db_column='STATUS')
    einstellungen = models.TextField(db_column='EINSTELLUNGEN', blank=True, null=True)
    plan_start_datum = models.DateField(db_column='PLAN_START_DATUM', blank=True, null=True)
    plan_start_uhrzeit = models.TimeField(db_column='PLAN_START_UHRZEIT', blank=True, null=True)
    plan_ende_datum = models.DateField(db_column='PLAN_ENDE_DATUM', blank=True, null=True)
    plan_ende_uhrzeit = models.TimeField(db_column='PLAN_ENDE_UHRZEIT', blank=True, null=True)
    letzte_aenderung_datum = models.DateField(db_column='LETZTE_AENDERUNG_DATUM', blank=True, null=True)
    letzte_aenderung_uhrzeit = models.TimeField(db_column='LETZTE_AENDERUNG_UHRZEIT', blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)
    erledigt = models.IntegerField(db_column='ERLEDIGT')

    class Meta:
        managed = False
        db_table = 'ARZTBRIEFLISTE'
        verbose_name_plural = "Arztbrieflisten"


class AtPunktwerte(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    typ = models.CharField(db_column='TYP', max_length=20, blank=True, null=True)
    bundesland = models.CharField(db_column='BUNDESLAND', max_length=50, blank=True, null=True)
    mandant_id = models.IntegerField(db_column='MANDANT_ID', blank=True, null=True)
    kuerzel = models.CharField(db_column='KUERZEL', max_length=50, blank=True, null=True)
    beschreibung = models.TextField(db_column='BESCHREIBUNG', blank=True, null=True)
    punktwert = models.FloatField(db_column='PUNKTWERT')
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)
    stand = models.DateField(db_column='STAND', blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    gueltig_von_pktstand = models.FloatField(db_column='GUELTIG_VON_PKTSTAND')
    gueltig_bis_pktstand = models.FloatField(db_column='GUELTIG_BIS_PKTSTAND')
    fachgebiet = models.IntegerField(db_column='FACHGEBIET', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AT_PUNKTWERTE'
        unique_together = (('mandant_id', 'kuerzel', 'typ', 'fachgebiet'), ('mandant_id', 'kuerzel', 'typ'),)


class AtSisxAtc(models.Model):
    atc = models.CharField(db_column='ATC', primary_key=True, max_length=10)
    atcbez = models.CharField(db_column='ATCBEZ', max_length=255, blank=True, null=True)
    atcbeze = models.CharField(db_column='ATCBEZE', max_length=255, blank=True, null=True)
    dt = models.DateTimeField(db_column='DT', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AT_SISX_ATC'


class AtSisxBrevier(models.Model):
    mono = models.IntegerField(db_column='MONO', primary_key=True)
    ewd = models.TextField(db_column='EWD', blank=True, null=True)
    agd = models.TextField(db_column='AGD', blank=True, null=True)
    aad = models.TextField(db_column='AAD', blank=True, null=True)
    ahd = models.TextField(db_column='AHD', blank=True, null=True)
    dod = models.TextField(db_column='DOD', blank=True, null=True)
    gad = models.TextField(db_column='GAD', blank=True, null=True)
    sstd = models.TextField(db_column='SSTD', blank=True, null=True)
    nwd = models.TextField(db_column='NWD', blank=True, null=True)
    wwd = models.TextField(db_column='WWD', blank=True, null=True)
    ged = models.TextField(db_column='GED', blank=True, null=True)
    whd = models.TextField(db_column='WHD', blank=True, null=True)
    vtwd = models.TextField(db_column='VTWD', blank=True, null=True)
    lhd = models.TextField(db_column='LHD', blank=True, null=True)
    hkd = models.TextField(db_column='HKD', blank=True, null=True)
    dt = models.DateTimeField(db_column='DT', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AT_SISX_BREVIER'


class AtSisxCode(models.Model):
    cdtyp = models.CharField(db_column='CDTYP', primary_key=True, max_length=20)
    cdval = models.CharField(db_column='CDVAL', max_length=50)
    dscrsd = models.CharField(db_column='DSCRSD', max_length=255, blank=True, null=True)
    dscrmd = models.TextField(db_column='DSCRMD', blank=True, null=True)
    dscrd = models.TextField(db_column='DSCRD', blank=True, null=True)
    dt = models.DateTimeField(db_column='DT', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AT_SISX_CODE'
        unique_together = (('cdtyp', 'cdval'),)


class AtSisxCompany(models.Model):
    prtno = models.CharField(db_column='PRTNO', primary_key=True, max_length=10)
    naml = models.CharField(db_column='NAML', max_length=45, blank=True, null=True)
    adnam = models.CharField(db_column='ADNAM', max_length=70, blank=True, null=True)
    strt = models.CharField(db_column='STRT', max_length=30, blank=True, null=True)
    zip = models.CharField(db_column='ZIP', max_length=6, blank=True, null=True)
    city = models.CharField(db_column='CITY', max_length=30, blank=True, null=True)
    cntry = models.CharField(db_column='CNTRY', max_length=20, blank=True, null=True)
    pbox = models.CharField(db_column='PBOX', max_length=8, blank=True, null=True)
    tel = models.CharField(db_column='TEL', max_length=100, blank=True, null=True)
    fax = models.CharField(db_column='FAX', max_length=100, blank=True, null=True)
    cntct = models.CharField(db_column='CNTCT', max_length=60, blank=True, null=True)
    email = models.CharField(db_column='EMAIL', max_length=60, blank=True, null=True)
    www = models.CharField(db_column='WWW', max_length=60, blank=True, null=True)
    ean = models.CharField(db_column='EAN', max_length=20, blank=True, null=True)
    dt = models.DateTimeField(db_column='DT', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AT_SISX_COMPANY'


class AtSisxDar(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    referenz = models.IntegerField(db_column='REFERENZ', blank=True, null=True)
    typ = models.CharField(db_column='TYP', max_length=10)
    darreichung_code = models.CharField(db_column='DARREICHUNG_CODE', max_length=255, blank=True, null=True)
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)
    dt = models.DateTimeField(db_column='DT', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AT_SISX_DAR'


class AtSisxDok(models.Model):
    doknummer = models.IntegerField(db_column='DOKNUMMER', primary_key=True)
    doktyp = models.IntegerField(db_column='DOKTYP')
    doktitel = models.CharField(db_column='DOKTITEL', max_length=255, blank=True, null=True)
    doktxt = models.TextField(db_column='DOKTXT', blank=True, null=True)
    doktxk = models.TextField(db_column='DOKTXK', blank=True, null=True)
    dt = models.DateTimeField(db_column='DT', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AT_SISX_DOK'


class AtSisxDokInd(models.Model):
    indnr = models.CharField(db_column='INDNR', primary_key=True, max_length=10)
    doknummer = models.CharField(db_column='DOKNUMMER', max_length=10)

    class Meta:
        managed = False
        db_table = 'AT_SISX_DOK_IND'
        unique_together = (('indnr', 'doknummer'),)


class AtSisxIndgrst(models.Model):
    indnr = models.CharField(db_column='INDNR', primary_key=True, max_length=10)
    indbez = models.CharField(db_column='INDBEZ', max_length=100, blank=True, null=True)
    indnr2 = models.CharField(db_column='INDNR2', max_length=10, blank=True, null=True)
    art = models.CharField(db_column='ART', max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AT_SISX_INDGRST'


class AtSisxInter(models.Model):
    ixno = models.IntegerField(db_column='IXNO', primary_key=True)
    grp1id = models.IntegerField(db_column='GRP1ID', blank=True, null=True)
    grp2id = models.IntegerField(db_column='GRP2ID', blank=True, null=True)
    internr2 = models.IntegerField(db_column='INTERNR2', blank=True, null=True)
    effd = models.TextField(db_column='EFFD', blank=True, null=True)
    rlv = models.IntegerField(db_column='RLV', blank=True, null=True)
    rlvd = models.CharField(db_column='RLVD', max_length=255, blank=True, null=True)
    efftxtd = models.TextField(db_column='EFFTXTD', blank=True, null=True)
    mechd = models.TextField(db_column='MECHD', blank=True, null=True)
    measd = models.TextField(db_column='MEASD', blank=True, null=True)
    remd = models.TextField(db_column='REMD', blank=True, null=True)
    lit = models.TextField(db_column='LIT', blank=True, null=True)
    ixmch_typ = models.IntegerField(db_column='IXMCH_TYP', blank=True, null=True)
    dt = models.DateTimeField(db_column='DT', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AT_SISX_INTER'


class AtSisxIxcpt(models.Model):
    ixno = models.IntegerField(db_column='IXNO', primary_key=True)
    prtno = models.IntegerField(db_column='PRTNO')
    cptno = models.IntegerField(db_column='CPTNO')
    grp = models.IntegerField(db_column='GRP')
    art = models.IntegerField(db_column='ART')

    class Meta:
        managed = False
        db_table = 'AT_SISX_IXCPT'
        unique_together = (('ixno', 'prtno', 'grp', 'art'),)


class AtSisxIxsub(models.Model):
    ixno = models.IntegerField(db_column='IXNO', primary_key=True)
    subno = models.IntegerField(db_column='SUBNO')
    subno1 = models.IntegerField(db_column='SUBNO1', blank=True, null=True)
    grp = models.IntegerField(db_column='GRP', blank=True, null=True)
    art = models.IntegerField(db_column='ART', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AT_SISX_IXSUB'
        unique_together = (('ixno', 'subno'),)


class AtSisxLim(models.Model):
    limcd = models.CharField(db_column='LIMCD', primary_key=True, max_length=20)
    limtyp = models.CharField(db_column='LIMTYP', max_length=50)
    dscrd = models.TextField(db_column='DSCRD', blank=True, null=True)
    dt = models.DateTimeField(db_column='DT', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AT_SISX_LIM'
        unique_together = (('limcd', 'limtyp'),)


class AtSisxProduct(models.Model):
    prdno = models.IntegerField(db_column='PRDNO', primary_key=True)
    dscrd = models.CharField(db_column='DSCRD', max_length=255, blank=True, null=True)
    bnamd = models.CharField(db_column='BNAMD', max_length=255, blank=True, null=True)
    atc = models.CharField(db_column='ATC', max_length=100, blank=True, null=True)
    it = models.CharField(db_column='IT', max_length=100, blank=True, null=True)
    a_trade = models.CharField(db_column='A_TRADE', max_length=5, blank=True, null=True)
    prtno = models.CharField(db_column='PRTNO', max_length=5, blank=True, null=True)
    a_vertr = models.CharField(db_column='A_VERTR', max_length=40, blank=True, null=True)
    a_zlinh = models.CharField(db_column='A_ZLINH', max_length=10, blank=True, null=True)
    a_mono = models.CharField(db_column='A_MONO', max_length=5, blank=True, null=True)
    a_anzahl = models.CharField(db_column='A_ANZAHL', max_length=5, blank=True, null=True)
    a_bdat = models.DateField(db_column='A_BDAT', blank=True, null=True)
    a_verfall = models.CharField(db_column='A_VERFALL', max_length=5, blank=True, null=True)
    a_verfall2 = models.CharField(db_column='A_VERFALL2', max_length=5, blank=True, null=True)
    a_lager = models.CharField(db_column='A_LAGER', max_length=5, blank=True, null=True)
    a_sucht = models.CharField(db_column='A_SUCHT', max_length=5, blank=True, null=True)
    a_wartez = models.CharField(db_column='A_WARTEZ', max_length=100, blank=True, null=True)
    mono = models.IntegerField(db_column='MONO', blank=True, null=True)
    cdgald = models.CharField(db_column='CDGALD', max_length=5, blank=True, null=True)
    a_zloesen = models.CharField(db_column='A_ZLOESEN', max_length=5, blank=True, null=True)
    a_zabgabe = models.CharField(db_column='A_ZABGABE', max_length=5, blank=True, null=True)
    a_zlnumm = models.CharField(db_column='A_ZLNUMM', max_length=20, blank=True, null=True)
    a_zrp = models.CharField(db_column='A_ZRP', max_length=5, blank=True, null=True)
    a_warn = models.CharField(db_column='A_WARN', max_length=10, blank=True, null=True)
    dt = models.DateTimeField(db_column='DT', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AT_SISX_PRODUCT'


class AtSisxProCpt(models.Model):
    prdno = models.IntegerField(db_column='PRDNO', primary_key=True)
    cptlno = models.IntegerField(db_column='CPTLNO')
    galf = models.CharField(db_column='GALF', max_length=50, blank=True, null=True)
    a_zedat5 = models.CharField(db_column='A_ZEDAT5', max_length=255, blank=True, null=True)
    excipq = models.FloatField(db_column='EXCIPQ', blank=True, null=True)
    excipu = models.CharField(db_column='EXCIPU', max_length=10, blank=True, null=True)
    excipcd = models.CharField(db_column='EXCIPCD', max_length=255, blank=True, null=True)
    excipq2 = models.FloatField(db_column='EXCIPQ2', blank=True, null=True)
    excipu2 = models.CharField(db_column='EXCIPU2', max_length=10, blank=True, null=True)
    excipcd2 = models.CharField(db_column='EXCIPCD2', max_length=255, blank=True, null=True)
    pqty = models.FloatField(db_column='PQTY', blank=True, null=True)
    pqtyu = models.CharField(db_column='PQTYU', max_length=10, blank=True, null=True)
    bezgr2 = models.CharField(db_column='BEZGR2', max_length=255, blank=True, null=True)
    dt = models.DateTimeField(db_column='DT', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AT_SISX_PRO_CPT'
        unique_together = (('prdno', 'cptlno'),)


class AtSisxProCptapp(models.Model):
    prdno = models.CharField(db_column='PRDNO', primary_key=True, max_length=20)
    cptlno = models.CharField(db_column='CPTLNO', max_length=50)
    appno = models.CharField(db_column='APPNO', max_length=50)
    meth = models.IntegerField(db_column='METH', blank=True, null=True)
    mode = models.IntegerField(db_column='MODE', blank=True, null=True)
    state = models.IntegerField(db_column='STATE', blank=True, null=True)
    lct = models.IntegerField(db_column='LCT', blank=True, null=True)
    prp = models.IntegerField(db_column='PRP', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AT_SISX_PRO_CPTAPP'
        unique_together = (('prdno', 'cptlno', 'appno'),)


class AtSisxProCptcmp(models.Model):
    prdno = models.IntegerField(db_column='PRDNO', primary_key=True)
    cptlno = models.IntegerField(db_column='CPTLNO')
    line = models.IntegerField(db_column='LINE')
    subno = models.IntegerField(db_column='SUBNO', blank=True, null=True)
    qty = models.FloatField(db_column='QTY', blank=True, null=True)
    qtyu = models.CharField(db_column='QTYU', max_length=255, blank=True, null=True)
    suffd = models.CharField(db_column='SUFFD', max_length=255, blank=True, null=True)
    praed = models.CharField(db_column='PRAED', max_length=255, blank=True, null=True)
    whk = models.CharField(db_column='WHK', max_length=255, blank=True, null=True)
    ixrel = models.CharField(db_column='IXREL', max_length=255, blank=True, null=True)
    subno1 = models.IntegerField(db_column='SUBNO1', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AT_SISX_PRO_CPTCMP'
        unique_together = (('prdno', 'cptlno', 'line'),)


class AtSisxProCpttb(models.Model):
    prdno = models.IntegerField(db_column='PRDNO')
    cptlno = models.IntegerField(db_column='CPTLNO')
    tb_ee = models.CharField(db_column='TB_EE', max_length=5, blank=True, null=True)
    tb_ee_zus = models.TextField(db_column='TB_EE_ZUS', blank=True, null=True)
    tb_gd = models.CharField(db_column='TB_GD', max_length=5, blank=True, null=True)
    tb_gd_zus = models.TextField(db_column='TB_GD_ZUS', blank=True, null=True)
    ka_oef = models.CharField(db_column='KA_OEF', max_length=5, blank=True, null=True)
    ka_oef_zus = models.TextField(db_column='KA_OEF_ZUS', blank=True, null=True)
    al_sus = models.CharField(db_column='AL_SUS', max_length=5, blank=True, null=True)
    al_sus_zus = models.TextField(db_column='AL_SUS_ZUS', blank=True, null=True)
    zmoe = models.CharField(db_column='ZMOE', max_length=5, blank=True, null=True)
    zmoe_zus = models.TextField(db_column='ZMOE_ZUS', blank=True, null=True)
    sonde = models.CharField(db_column='SONDE', max_length=5, blank=True, null=True)
    sonde_zus = models.TextField(db_column='SONDE_ZUS', blank=True, null=True)
    cmr = models.CharField(db_column='CMR', max_length=5, blank=True, null=True)
    cmr_zus = models.TextField(db_column='CMR_ZUS', blank=True, null=True)
    licht = models.CharField(db_column='LICHT', max_length=5, blank=True, null=True)
    licht_zus = models.TextField(db_column='LICHT_ZUS', blank=True, null=True)
    tb = models.CharField(db_column='TB', max_length=5, blank=True, null=True)
    tb_zus = models.TextField(db_column='TB_ZUS', blank=True, null=True)
    tb_br = models.CharField(db_column='TB_BR', max_length=5, blank=True, null=True)
    tb_br_zus = models.TextField(db_column='TB_BR_ZUS', blank=True, null=True)
    azp = models.TextField(db_column='AZP', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AT_SISX_PRO_CPTTB'


class AtSisxSub(models.Model):
    subno = models.IntegerField(db_column='SUBNO', primary_key=True)
    namd = models.CharField(db_column='NAMD', max_length=255, blank=True, null=True)
    anamd = models.TextField(db_column='ANAMD', blank=True, null=True)
    abdano = models.IntegerField(db_column='ABDANO', blank=True, null=True)
    wsthst = models.CharField(db_column='WSTHST', max_length=20, blank=True, null=True)
    subno2 = models.IntegerField(db_column='SUBNO2', blank=True, null=True)
    dt = models.DateTimeField(db_column='DT', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AT_SISX_SUB'


class AtSisxSubst(models.Model):
    intergr = models.IntegerField(db_column='INTERGR', primary_key=True)
    intergr1 = models.IntegerField(db_column='INTERGR1', blank=True, null=True)
    grbez = models.CharField(db_column='GRBEZ', max_length=255, blank=True, null=True)
    art = models.CharField(db_column='ART', max_length=20, blank=True, null=True)
    dt = models.DateTimeField(db_column='DT', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AT_SISX_SUBST'


class Auftrag(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    fragestellung = models.CharField(db_column='FRAGESTELLUNG', max_length=255, blank=True, null=True)
    symptomatik = models.CharField(db_column='SYMPTOMATIK', max_length=255, blank=True, null=True)
    zuweiser_id = models.IntegerField(db_column='ZUWEISER_ID', blank=True, null=True)
    region_id = models.ForeignKey("Region", db_column='REGION_ID', blank=True, null=True, on_delete=models.PROTECT)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AUFTRAG'


class AuftragGruppe(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    fragestellung = models.CharField(db_column='FRAGESTELLUNG', max_length=255, blank=True, null=True)
    symptomatik = models.CharField(db_column='SYMPTOMATIK', max_length=255, blank=True, null=True)
    zuweiser_id = models.IntegerField(db_column='ZUWEISER_ID', blank=True, null=True)
    region_id = models.IntegerField(db_column='REGION_ID', blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    uuid = models.CharField(db_column='UUID', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AUFTRAG_GRUPPE'


class AugeBrille(models.Model):
    doku_id = models.IntegerField(db_column='DOKU_ID', primary_key=True)
    l_sphaere_fern = models.FloatField(db_column='L_SPHAERE_FERN', blank=True, null=True)
    l_zylinder_fern = models.FloatField(db_column='L_ZYLINDER_FERN', blank=True, null=True)
    l_achse_fern = models.FloatField(db_column='L_ACHSE_FERN', blank=True, null=True)
    l_addition = models.FloatField(db_column='L_ADDITION', blank=True, null=True)
    l_prisma = models.FloatField(db_column='L_PRISMA', blank=True, null=True)
    l_basis = models.FloatField(db_column='L_BASIS', blank=True, null=True)
    pupillardistanz = models.FloatField(db_column='PUPILLARDISTANZ', blank=True, null=True)
    l_hornhautscheitelabstand = models.FloatField(db_column='L_HORNHAUTSCHEITELABSTAND', blank=True, null=True)
    r_sphaere_fern = models.FloatField(db_column='R_SPHAERE_FERN', blank=True, null=True)
    r_zylinder_fern = models.FloatField(db_column='R_ZYLINDER_FERN', blank=True, null=True)
    r_achse_fern = models.FloatField(db_column='R_ACHSE_FERN', blank=True, null=True)
    r_addition = models.FloatField(db_column='R_ADDITION', blank=True, null=True)
    r_prisma = models.FloatField(db_column='R_PRISMA', blank=True, null=True)
    r_basis = models.FloatField(db_column='R_BASIS', blank=True, null=True)
    r_hornhautscheitelabstand = models.FloatField(db_column='R_HORNHAUTSCHEITELABSTAND', blank=True, null=True)
    produktionsjahr = models.IntegerField(db_column='PRODUKTIONSJAHR', blank=True, null=True)
    l_sphaere_nah = models.FloatField(db_column='L_SPHAERE_NAH', blank=True, null=True)
    l_zylinder_nah = models.FloatField(db_column='L_ZYLINDER_NAH', blank=True, null=True)
    l_achse_nah = models.FloatField(db_column='L_ACHSE_NAH', blank=True, null=True)
    r_sphaere_nah = models.FloatField(db_column='R_SPHAERE_NAH', blank=True, null=True)
    r_zylinder_nah = models.FloatField(db_column='R_ZYLINDER_NAH', blank=True, null=True)
    r_achse_nah = models.FloatField(db_column='R_ACHSE_NAH', blank=True, null=True)
    bemerkung_messung = models.TextField(db_column='BEMERKUNG_MESSUNG', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AUGE_BRILLE'


class AugeDruck(models.Model):
    doku_id = models.IntegerField(db_column='DOKU_ID', primary_key=True)
    l_augendruck = models.FloatField(db_column='L_AUGENDRUCK', blank=True, null=True)
    r_augendruck = models.FloatField(db_column='R_AUGENDRUCK', blank=True, null=True)
    bemerkung_messung = models.TextField(db_column='BEMERKUNG_MESSUNG', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AUGE_DRUCK'


class AugeKeratometrie(models.Model):
    doku_id = models.IntegerField(db_column='DOKU_ID', primary_key=True)
    l_r1_hornhautkruemmung = models.FloatField(db_column='L_R1_HORNHAUTKRUEMMUNG', blank=True, null=True)
    l_r2_hornhautkruemmung = models.FloatField(db_column='L_R2_HORNHAUTKRUEMMUNG', blank=True, null=True)
    l_ave_hornhautkruemmung = models.FloatField(db_column='L_AVE_HORNHAUTKRUEMMUNG', blank=True, null=True)
    l_r1_brechwert = models.FloatField(db_column='L_R1_BRECHWERT', blank=True, null=True)
    l_r2_brechwert = models.FloatField(db_column='L_R2_BRECHWERT', blank=True, null=True)
    l_ave_brechwert = models.FloatField(db_column='L_AVE_BRECHWERT', blank=True, null=True)
    l_r1_zylinderachse = models.FloatField(db_column='L_R1_ZYLINDERACHSE', blank=True, null=True)
    l_r2_zylinderachse = models.FloatField(db_column='L_R2_ZYLINDERACHSE', blank=True, null=True)
    l_cyl_zylinderwert = models.FloatField(db_column='L_CYL_ZYLINDERWERT', blank=True, null=True)
    l_cyl_zylinderachse = models.FloatField(db_column='L_CYL_ZYLINDERACHSE', blank=True, null=True)
    r_r1_hornhautkruemmung = models.FloatField(db_column='R_R1_HORNHAUTKRUEMMUNG', blank=True, null=True)
    r_r2_hornhautkruemmung = models.FloatField(db_column='R_R2_HORNHAUTKRUEMMUNG', blank=True, null=True)
    r_ave_hornhautkruemmung = models.FloatField(db_column='R_AVE_HORNHAUTKRUEMMUNG', blank=True, null=True)
    r_r1_brechwert = models.FloatField(db_column='R_R1_BRECHWERT', blank=True, null=True)
    r_r2_brechwert = models.FloatField(db_column='R_R2_BRECHWERT', blank=True, null=True)
    r_ave_brechwert = models.FloatField(db_column='R_AVE_BRECHWERT', blank=True, null=True)
    r_r1_zylinderachse = models.FloatField(db_column='R_R1_ZYLINDERACHSE', blank=True, null=True)
    r_r2_zylinderachse = models.FloatField(db_column='R_R2_ZYLINDERACHSE', blank=True, null=True)
    r_cyl_zylinderwert = models.FloatField(db_column='R_CYL_ZYLINDERWERT', blank=True, null=True)
    r_cyl_zylinderachse = models.FloatField(db_column='R_CYL_ZYLINDERACHSE', blank=True, null=True)
    bemerkung_messung = models.TextField(db_column='BEMERKUNG_MESSUNG', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AUGE_KERATOMETRIE'


class AugeRefraktion(models.Model):
    doku_id = models.IntegerField(db_column='DOKU_ID', unique=True)
    typ = models.CharField(db_column='TYP', max_length=255, blank=True, null=True)
    l_sphaere_fern = models.FloatField(db_column='L_SPHAERE_FERN', blank=True, null=True)
    l_zylinder_fern = models.FloatField(db_column='L_ZYLINDER_FERN', blank=True, null=True)
    l_achse_fern = models.FloatField(db_column='L_ACHSE_FERN', blank=True, null=True)
    l_addition = models.FloatField(db_column='L_ADDITION', blank=True, null=True)
    l_prisma = models.FloatField(db_column='L_PRISMA', blank=True, null=True)
    l_basis = models.FloatField(db_column='L_BASIS', blank=True, null=True)
    pupillardistanz = models.FloatField(db_column='PUPILLARDISTANZ', blank=True, null=True)
    r_sphaere_fern = models.FloatField(db_column='R_SPHAERE_FERN', blank=True, null=True)
    r_zylinder_fern = models.FloatField(db_column='R_ZYLINDER_FERN', blank=True, null=True)
    r_achse_fern = models.FloatField(db_column='R_ACHSE_FERN', blank=True, null=True)
    r_addition = models.FloatField(db_column='R_ADDITION', blank=True, null=True)
    r_prisma = models.FloatField(db_column='R_PRISMA', blank=True, null=True)
    r_basis = models.FloatField(db_column='R_BASIS', blank=True, null=True)
    cycloplegie = models.IntegerField(db_column='CYCLOPLEGIE', blank=True, null=True)
    cycloplegie_tropfen = models.CharField(db_column='CYCLOPLEGIE_TROPFEN', max_length=255, blank=True, null=True)
    l_visus_sc = models.FloatField(db_column='L_VISUS_SC', blank=True, null=True)
    l_visus_cc = models.FloatField(db_column='L_VISUS_CC', blank=True, null=True)
    l_sphaere_nah = models.FloatField(db_column='L_SPHAERE_NAH', blank=True, null=True)
    l_zylinder_nah = models.FloatField(db_column='L_ZYLINDER_NAH', blank=True, null=True)
    l_achse_nah = models.FloatField(db_column='L_ACHSE_NAH', blank=True, null=True)
    r_visus_sc = models.FloatField(db_column='R_VISUS_SC', blank=True, null=True)
    r_visus_cc = models.FloatField(db_column='R_VISUS_CC', blank=True, null=True)
    r_sphaere_nah = models.FloatField(db_column='R_SPHAERE_NAH', blank=True, null=True)
    r_zylinder_nah = models.FloatField(db_column='R_ZYLINDER_NAH', blank=True, null=True)
    r_achse_nah = models.FloatField(db_column='R_ACHSE_NAH', blank=True, null=True)
    dominanz = models.CharField(db_column='DOMINANZ', max_length=255, blank=True, null=True)
    bemerkung_messung = models.TextField(db_column='BEMERKUNG_MESSUNG', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AUGE_REFRAKTION'

class AuswertungErgebnislisten(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    name = models.CharField(db_column='NAME', max_length=255)
    beschreibung = models.CharField(db_column='BESCHREIBUNG', max_length=255, blank=True, null=True)
    auswertung_name = models.CharField(db_column='AUSWERTUNG_NAME', max_length=255, blank=True, null=True)
    typ = models.CharField(db_column='TYP', max_length=255)
    id_liste = models.TextField(db_column='ID_LISTE', blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    datum_erstellt = models.DateField(db_column='DATUM_ERSTELLT', blank=True, null=True)
    aktiv = models.IntegerField(db_column='AKTIV', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AUSWERTUNG_ERGEBNISLISTEN'


class AuswertungKriterien(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    name = models.CharField(db_column='NAME', max_length=255)
    beschreibung = models.CharField(db_column='BESCHREIBUNG', max_length=255, blank=True, null=True)
    auswertung_name = models.CharField(db_column='AUSWERTUNG_NAME', max_length=255)
    suchfilter_xml = models.TextField(db_column='SUCHFILTER_XML', blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    nummer = models.IntegerField(db_column='NUMMER', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AUSWERTUNG_KRITERIEN'
        unique_together = (('auswertung_name', 'nummer'),)


class AutAbrechnungsstelle(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    mandant_id = models.IntegerField(db_column='MANDANT_ID', blank=True, null=True)
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=100, blank=True, null=True)
    kennung = models.CharField(db_column='KENNUNG', max_length=5, blank=True, null=True)
    bundesland = models.IntegerField(db_column='BUNDESLAND', blank=True, null=True)
    kassen_typ_liste = models.CharField(db_column='KASSEN_TYP_LISTE', max_length=255, blank=True, null=True)
    kassen_kennung_liste = models.CharField(db_column='KASSEN_KENNUNG_LISTE', max_length=255, blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)
    einstellungen = models.TextField(db_column='EINSTELLUNGEN', blank=True, null=True)
    elektronische_adresse = models.CharField(db_column='ELEKTRONISCHE_ADRESSE', max_length=255, blank=True, null=True)
    dienstleister_nr = models.CharField(db_column='DIENSTLEISTER_NR', max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AUT_ABRECHNUNGSSTELLE'
        unique_together = (('bezeichnung', 'mandant_id'),)


class AutAbrechnungStatistik(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)
    mandand_id = models.IntegerField(db_column='MANDAND_ID', blank=True, null=True)
    abrechnungsstellekennung = models.CharField(db_column='AbrechnungsstelleKennung', max_length=255, blank=True, null=True)
    abrechnungskassenkennungen = models.CharField(db_column='AbrechnungsKassenKennungen', max_length=255, blank=True, null=True)
    abrechnungskassentypen = models.CharField(db_column='AbrechnungsKassenTypen', max_length=255, blank=True, null=True)
    erstellung = models.DateTimeField(db_column='Erstellung', blank=True, null=True)
    zeitraumjahr = models.IntegerField(db_column='ZeitraumJahr', blank=True, null=True)
    zeitraumkennung = models.IntegerField(db_column='ZeitraumKennung', blank=True, null=True)
    datum_start = models.DateField(db_column='DATUM_START', blank=True, null=True)
    datum_ende = models.DateField(db_column='DATUM_ENDE', blank=True, null=True)
    anzahlscheine = models.IntegerField(db_column='AnzahlScheine', blank=True, null=True)
    gesamtsumme = models.FloatField(db_column='Gesamtsumme', blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AUT_ABRECHNUNG_STATISTIK'
        unique_together = (('mandand_id', 'abrechnungsstellekennung', 'datum_start', 'datum_ende'),)


class AutAbsmedikament(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    patient_id = models.IntegerField(db_column='PATIENT_ID')
    absdoku_id = models.IntegerField(db_column='ABSDOKU_ID')
    medikament_id = models.IntegerField(db_column='MEDIKAMENT_ID', blank=True, null=True)
    begruendung = models.TextField(db_column='BEGRUENDUNG', blank=True, null=True)
    diagnose = models.TextField(db_column='DIAGNOSE', blank=True, null=True)
    langzeitverordnung = models.CharField(db_column='LANGZEITVERORDNUNG', max_length=255, blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)
    antrag_medikament = models.CharField(db_column='ANTRAG_MEDIKAMENT', max_length=255, blank=True, null=True)
    pharmanummer = models.CharField(db_column='PHARMANUMMER', max_length=255, blank=True, null=True)
    magistralrezeptur = models.TextField(db_column='MAGISTRALREZEPTUR', blank=True, null=True)
    antrag_anzahl = models.FloatField(db_column='ANTRAG_ANZAHL', blank=True, null=True)
    dosisschema = models.TextField(db_column='DOSISSCHEMA', blank=True, null=True)
    bewilligt_anzahl = models.FloatField(db_column='BEWILLIGT_ANZAHL', blank=True, null=True)
    bewilligt_medikament = models.CharField(db_column='BEWILLIGT_MEDIKAMENT', max_length=255, blank=True, null=True)
    bewilligt_lzv_monate = models.IntegerField(db_column='BEWILLIGT_LZV_MONATE', blank=True, null=True)
    bewilligt_lzv_anzahl = models.IntegerField(db_column='BEWILLIGT_LZV_ANZAHL', blank=True, null=True)
    entscheidung = models.TextField(db_column='ENTSCHEIDUNG', blank=True, null=True)
    info_text = models.TextField(db_column='INFO_TEXT', blank=True, null=True)
    abs_anfrage_id = models.CharField(db_column='ABS_ANFRAGE_ID', max_length=255, blank=True, null=True)
    bewilligt_lzv_offen = models.IntegerField(db_column='BEWILLIGT_LZV_OFFEN', blank=True, null=True)
    bewilligt_lzv_bis = models.DateField(db_column='BEWILLIGT_LZV_BIS', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AUT_ABSMEDIKAMENT'


class AutApWarenverzeichnis(models.Model):
    pharma_nr = models.IntegerField(db_column='PHARMA_NR', primary_key=True)
    artikel_nr = models.IntegerField(db_column='ARTIKEL_NR', blank=True, null=True)
    aenderung_code = models.CharField(db_column='AENDERUNG_CODE', max_length=5, blank=True, null=True)
    gueltig_ab = models.DateField(db_column='GUELTIG_AB', blank=True, null=True)
    hersteller_code = models.CharField(db_column='HERSTELLER_CODE', max_length=5, blank=True, null=True)
    ap_ek_preis = models.FloatField(db_column='AP_EK_PREIS', blank=True, null=True)
    kassen_preis = models.FloatField(db_column='KASSEN_PREIS', blank=True, null=True)
    verkaufs_preis = models.FloatField(db_column='VERKAUFS_PREIS', blank=True, null=True)
    box = models.CharField(db_column='BOX', max_length=5, blank=True, null=True)
    artikel_kennzeichen = models.CharField(db_column='ARTIKEL_KENNZEICHEN', max_length=5, blank=True, null=True)
    lagervorschrift = models.CharField(db_column='LAGERVORSCHRIFT', max_length=5, blank=True, null=True)
    rezeptzeichen = models.CharField(db_column='REZEPTZEICHEN', max_length=5, blank=True, null=True)
    rzpt_gebuehr = models.CharField(db_column='RZPT_GEBUEHR', max_length=5, blank=True, null=True)
    kurztext = models.CharField(db_column='KURZTEXT', max_length=255, blank=True, null=True)
    menge = models.CharField(db_column='MENGE', max_length=10, blank=True, null=True)
    mengenart = models.CharField(db_column='MENGENART', max_length=5, blank=True, null=True)
    kassenzeichen = models.CharField(db_column='KASSENZEICHEN', max_length=5, blank=True, null=True)
    mwst_satz = models.IntegerField(db_column='MWST_SATZ', blank=True, null=True)
    liefer_code = models.CharField(db_column='LIEFER_CODE', max_length=5, blank=True, null=True)
    getraenke_steuer = models.CharField(db_column='GETRAENKE_STEUER', max_length=5, blank=True, null=True)
    warengruppe = models.CharField(db_column='WARENGRUPPE', max_length=5, blank=True, null=True)
    kassenzeichen_zusatz = models.CharField(db_column='KASSENZEICHEN_ZUSATZ', max_length=5, blank=True, null=True)
    registernummer = models.CharField(db_column='REGISTERNUMMER', max_length=6, blank=True, null=True)
    preiskennzeichen = models.IntegerField(db_column='PREISKENNZEICHEN', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AUT_AP_WARENVERZEICHNIS'


class AutAuMeldungen(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    doku_id = models.IntegerField(db_column='DOKU_ID')
    meldungsart = models.CharField(db_column='MELDUNGSART', max_length=50, blank=True, null=True)
    ausstellungsdatum = models.DateField(db_column='AUSSTELLUNGSDATUM', blank=True, null=True)
    quittung_id = models.IntegerField(db_column='QUITTUNG_ID', unique=True, blank=True, null=True)
    quittung_version = models.CharField(db_column='QUITTUNG_VERSION', max_length=50, blank=True, null=True)
    meldungsdaten = models.TextField(db_column='MELDUNGSDATEN', blank=True, null=True)
    personendaten = models.TextField(db_column='PERSONENDATEN', blank=True, null=True)
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=255, blank=True, null=True)
    au_von = models.DateField(db_column='AU_VON', blank=True, null=True)
    au_bis = models.DateField(db_column='AU_BIS', blank=True, null=True)
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)
    versicherungsnummer = models.CharField(db_column='VERSICHERUNGSNUMMER', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AUT_AU_MELDUNGEN'


class AutEcardMeldungen(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)
    datum = models.DateField(db_column='DATUM', blank=True, null=True)
    uhrzeit = models.TimeField(db_column='UHRZEIT', blank=True, null=True)
    meldung = models.TextField(db_column='MELDUNG', blank=True, null=True)
    category = models.CharField(db_column='CATEGORY', max_length=255, blank=True, null=True)
    daten = models.TextField(db_column='DATEN', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AUT_ECARD_MELDUNGEN'


class AutEcardQuittungen(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    formular_doku_id = models.IntegerField(db_column='FORMULAR_DOKU_ID')
    annahmezeitpunkt = models.CharField(db_column='ANNAHMEZEITPUNKT', max_length=255, blank=True, null=True)
    datenblatt_typ = models.CharField(db_column='DATENBLATT_TYP', max_length=255, blank=True, null=True)
    signatur = models.CharField(db_column='SIGNATUR', max_length=255, blank=True, null=True)
    sv_nummer = models.CharField(db_column='SV_NUMMER', max_length=255)
    svt = models.CharField(db_column='SVT', max_length=255, blank=True, null=True)
    untersuchungsdatum = models.CharField(db_column='UNTERSUCHUNGSDATUM', max_length=255, blank=True, null=True)
    vp_nummer = models.CharField(db_column='VP_NUMMER', max_length=255, blank=True, null=True)
    system_zeit = models.DateTimeField(db_column='SYSTEM_ZEIT')
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AUT_ECARD_QUITTUNGEN'


class AutGnwAufenthalt(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    patient_id = models.IntegerField(db_column='PATIENT_ID', blank=True, null=True)
    patient_name_aufenthalt = models.CharField(db_column='PATIENT_NAME_AUFENTHALT', max_length=255, blank=True, null=True)
    patient_vorname_aufenthalt = models.CharField(db_column='PATIENT_VORNAME_AUFENTHALT', max_length=255, blank=True, null=True)
    patient_gebdatum_aufenthalt = models.DateField(db_column='PATIENT_GEBDATUM_AUFENTHALT', blank=True, null=True)
    patient_versichnr_aufenthalt = models.CharField(db_column='PATIENT_VERSICHNR_AUFENTHALT', max_length=50, blank=True, null=True)
    aufnahmezeit = models.DateTimeField(db_column='AUFNAHMEZEIT', blank=True, null=True)
    entlassungszeit = models.DateTimeField(db_column='ENTLASSUNGSZEIT', blank=True, null=True)
    betriebsstellekey = models.CharField(db_column='BETRIEBSSTELLEKEY', max_length=100, blank=True, null=True)
    betriebsstelletext = models.CharField(db_column='BETRIEBSSTELLETEXT', max_length=255, blank=True, null=True)
    aufenthaltskey = models.CharField(db_column='AUFENTHALTSKEY', max_length=100, blank=True, null=True)
    aufenthaltsart = models.CharField(db_column='AUFENTHALTSART', max_length=255, blank=True, null=True)
    aufenthaltspfad = models.CharField(db_column='AUFENTHALTSPFAD', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AUT_GNW_AUFENTHALT'
        unique_together = (('betriebsstellekey', 'aufenthaltskey'),)


class AutGnwAufenthaltabfrage(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    typ = models.IntegerField(db_column='TYP')
    befundabfrage_id = models.IntegerField(db_column='BEFUNDABFRAGE_ID')
    betriebsstellekey = models.CharField(db_column='BETRIEBSSTELLEKEY', max_length=100, blank=True, null=True)
    aufenthaltskey = models.CharField(db_column='AUFENTHALTSKEY', max_length=100, blank=True, null=True)
    masterserver_betrkey = models.CharField(db_column='MASTERSERVER_BETRKEY', max_length=100, blank=True, null=True)
    masterserver_email = models.CharField(db_column='MASTERSERVER_EMAIL', max_length=100, blank=True, null=True)
    anfrage_kennung = models.CharField(db_column='ANFRAGE_KENNUNG', unique=True, max_length=100, blank=True, null=True)
    anfrage_status = models.IntegerField(db_column='ANFRAGE_STATUS')
    anfrage_zeit = models.DateTimeField(db_column='ANFRAGE_ZEIT', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AUT_GNW_AUFENTHALTABFRAGE'


class AutKonsultation(models.Model):
    id = models.BigIntegerField(db_column='ID', primary_key=True)
    kschein_id = models.ForeignKey("AutKschein", db_column='KSCHEIN_ID', on_delete=models.PROTECT)
    abrechnungsperiode = models.CharField(db_column='ABRECHNUNGSPERIODE', max_length=50, blank=True, null=True)
    abstimmungsbeleg = models.TextField(db_column='ABSTIMMUNGSBELEG', blank=True, null=True)
    anspruchsart = models.CharField(db_column='ANSPRUCHSART', max_length=50, blank=True, null=True)
    bearbeitungsdatum = models.DateField(db_column='BEARBEITUNGSDATUM', blank=True, null=True)
    behandlungsdatum = models.DateField(db_column='BEHANDLUNGSDATUM', blank=True, null=True)
    behandlungsfallcode = models.CharField(db_column='BEHANDLUNGSFALLCODE', max_length=50, blank=True, null=True)
    bezugsbereich = models.CharField(db_column='BEZUGSBEREICH', max_length=50, blank=True, null=True)
    fachgebietscode = models.CharField(db_column='FACHGEBIETSCODE', max_length=50, blank=True, null=True)
    konsultationsartcode = models.CharField(db_column='KONSULTATIONSARTCODE', max_length=50)
    limitgeprueft = models.CharField(db_column='LIMITGEPRUEFT', max_length=50, blank=True, null=True)
    zustaendigesvt = models.CharField(db_column='ZUSTAENDIGESVT', max_length=50, blank=True, null=True)
    ordinationsnr = models.IntegerField(db_column='ORDINATIONSNR', blank=True, null=True)
    originalsvtcode = models.CharField(db_column='ORIGINALSVTCODE', max_length=50, blank=True, null=True)
    status = models.CharField(db_column='STATUS', max_length=50)
    svnummer = models.CharField(db_column='SVNUMMER', max_length=50, blank=True, null=True)
    abgeleitetesvnr = models.CharField(db_column='ABGELEITETESVNR', max_length=50, blank=True, null=True)
    txnnummer = models.BigIntegerField(db_column='TXNNUMMER', blank=True, null=True)
    verrechnungssvtnr = models.CharField(db_column='VERRECHNUNGSSVTNR', max_length=50, blank=True, null=True)
    versichertenartcode = models.CharField(db_column='VERSICHERTENARTCODE', max_length=50, blank=True, null=True)
    versichertenkategorie = models.CharField(db_column='VERSICHERTENKATEGORIE', max_length=50, blank=True, null=True)
    version = models.IntegerField(db_column='VERSION', blank=True, null=True)
    vertragspartnernr = models.CharField(db_column='VERTRAGSPARTNERNR', max_length=20, blank=True, null=True)
    kstanteilbefreit = models.CharField(db_column='KSTANTEILBEFREIT', max_length=10, blank=True, null=True)
    overlimit = models.CharField(db_column='OVERLIMIT', max_length=10, blank=True, null=True)
    rzptgebfrei = models.CharField(db_column='RZPTGEBFREI', max_length=10, blank=True, null=True)
    storno = models.IntegerField(db_column='STORNO', blank=True, null=True)
    bearbeitungsuhrzeit = models.TimeField(db_column='BEARBEITUNGSUHRZEIT', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AUT_KONSULTATION'


class AutKostentraeger(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    typ = models.CharField(db_column='TYP', max_length=50, blank=True, null=True)
    kennung = models.IntegerField(db_column='KENNUNG', blank=True, null=True)
    kennung_s = models.CharField(db_column='KENNUNG_S', unique=True, max_length=50, blank=True, null=True)
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=100, blank=True, null=True)
    gruppe = models.CharField(db_column='GRUPPE', max_length=3, blank=True, null=True)
    kuerzel = models.CharField(db_column='KUERZEL', max_length=50, blank=True, null=True)
    notizen = models.TextField(db_column='NOTIZEN', blank=True, null=True)
    status = models.CharField(db_column='STATUS', max_length=50, blank=True, null=True)
    gueltig_von = models.DateField(db_column='GUELTIG_VON', blank=True, null=True)
    gueltig_bis = models.DateField(db_column='GUELTIG_BIS', blank=True, null=True)
    version = models.CharField(db_column='VERSION', max_length=5, blank=True, null=True)
    niederlassung = models.CharField(db_column='NIEDERLASSUNG', max_length=30, blank=True, null=True)
    bundesland = models.CharField(db_column='BUNDESLAND', max_length=50, blank=True, null=True)
    sortierung = models.IntegerField(db_column='SORTIERUNG', blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    letzte_aenderung_datum = models.DateField(db_column='LETZTE_AENDERUNG_DATUM', blank=True, null=True)
    letzte_aenderung_uhrzeit = models.TimeField(db_column='LETZTE_AENDERUNG_UHRZEIT', blank=True, null=True)
    strasse = models.CharField(db_column='STRASSE', max_length=255, blank=True, null=True)
    plz = models.CharField(db_column='PLZ', max_length=255, blank=True, null=True)
    ort = models.CharField(db_column='ORT', max_length=255, blank=True, null=True)
    ustid = models.CharField(db_column='USTID', max_length=255, blank=True, null=True)
    fallpauschale = models.FloatField(db_column='FALLPAUSCHALE', blank=True, null=True)
    leererschein = models.IntegerField(db_column='LEERERSCHEIN', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AUT_KOSTENTRAEGER'


class AutKostentraegerBewilligungsstellen(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    bewilligungsstellen_id = models.IntegerField(db_column='BEWILLIGUNGSSTELLEN_ID')
    aut_kostentraeger_id = models.ForeignKey(AutKostentraeger, db_column='AUT_KOSTENTRAEGER_ID', on_delete=models.PROTECT)
    aut_kschein_versich_kategorie_von = models.IntegerField(db_column='AUT_KSCHEIN_VERSICH_KATEGORIE_VON')
    aut_kschein_versich_kategorie_bis = models.IntegerField(db_column='AUT_KSCHEIN_VERSICH_KATEGORIE_BIS')

    class Meta:
        managed = False
        db_table = 'AUT_KOSTENTRAEGER_BEWILLIGUNGSSTELLEN'


class AutKschein(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    patient_id = models.ForeignKey("Patient", db_column='PATIENT_ID', on_delete=models.PROTECT)
    orgeinheit_id = models.IntegerField(db_column='ORGEINHEIT_ID')
    ermaechtigung_id = models.IntegerField(db_column='ERMAECHTIGUNG_ID')
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    status = models.IntegerField(db_column='STATUS')
    kschein_nr = models.IntegerField(db_column='KSCHEIN_NR')
    abgabe_datum = models.DateField(db_column='ABGABE_DATUM', blank=True, null=True)
    gueltigkeit = models.CharField(db_column='GUELTIGKEIT', max_length=10, blank=True, null=True)
    gueltigkeit_jahr = models.CharField(db_column='GUELTIGKEIT_JAHR', max_length=10, blank=True, null=True)
    kschein_art = models.IntegerField(db_column='KSCHEIN_ART', blank=True, null=True)
    versich_kategorie = models.IntegerField(db_column='VERSICH_KATEGORIE', blank=True, null=True)
    kennzeichen = models.CharField(db_column='KENNZEICHEN', max_length=10, blank=True, null=True)
    zuw_nr = models.CharField(db_column='ZUW_NR', max_length=50, blank=True, null=True)
    zuw_kom_anrede = models.CharField(db_column='ZUW_KOM_ANREDE', max_length=255, blank=True, null=True)
    zuw_adresse = models.CharField(db_column='ZUW_ADRESSE', max_length=255, blank=True, null=True)
    zuw_kommunikation = models.CharField(db_column='ZUW_KOMMUNIKATION', max_length=255, blank=True, null=True)
    zuw_fachgebiet = models.CharField(db_column='ZUW_FACHGEBIET', max_length=50, blank=True, null=True)
    ueberw_datum = models.DateField(db_column='UEBERW_DATUM', blank=True, null=True)
    gruvu = models.IntegerField(db_column='GRUVU', blank=True, null=True)
    kaution = models.FloatField(db_column='KAUTION', blank=True, null=True)
    kaution_waehrung = models.CharField(db_column='KAUTION_WAEHRUNG', max_length=50, blank=True, null=True)
    kst_code = models.CharField(db_column='KST_CODE', max_length=50, blank=True, null=True)
    kasse_id = models.IntegerField(db_column='KASSE_ID', blank=True, null=True)
    kasse_typ = models.CharField(db_column='KASSE_TYP', max_length=10, blank=True, null=True)
    kasse_bland = models.CharField(db_column='KASSE_BLAND', max_length=10, blank=True, null=True)
    rzptgeb_bis = models.DateField(db_column='RZPTGEB_BIS', blank=True, null=True)
    vers_nehm_vers_nr = models.CharField(db_column='VERS_NEHM_VERS_NR', max_length=50, blank=True, null=True)
    vers_nehm_gebdatum = models.DateField(db_column='VERS_NEHM_GEBDATUM', blank=True, null=True)
    vers_nehm_kom_anrede = Base64CharField(db_column='VERS_NEHM_KOM_ANREDE', max_length=255, blank=True, null=True)
    vers_nehm_adresse = Base64CharField(db_column='VERS_NEHM_ADRESSE', max_length=255, blank=True, null=True)
    vers_nehm_kommunikation = Base64CharField(db_column='VERS_NEHM_KOMMUNIKATION', max_length=255, blank=True, null=True)
    verw_verh = models.CharField(db_column='VERW_VERH', max_length=50, blank=True, null=True)
    zus_vers_id = models.CharField(db_column='ZUS_VERS_ID', max_length=50, blank=True, null=True)
    dienst_kom_anrede = Base64CharField(db_column='DIENST_KOM_ANREDE', max_length=255, blank=True, null=True)
    dienst_adresse = Base64TextField(db_column='DIENST_ADRESSE', blank=True, null=True)
    dienst_kommunikation = Base64CharField(db_column='DIENST_KOMMUNIKATION', max_length=255, blank=True, null=True)
    notizen = Base64TextField(db_column='NOTIZEN', blank=True, null=True)
    protokoll = Base64TextField(db_column='PROTOKOLL', blank=True, null=True)
    patienten_adresse = Base64CharField(db_column='PATIENTEN_ADRESSE', max_length=255, blank=True, null=True)
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)
    properties = Base64TextField(db_column='PROPERTIES', blank=True, null=True)
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)
    abr_monat = models.IntegerField(db_column='ABR_MONAT', blank=True, null=True)
    abr_jahr = models.IntegerField(db_column='ABR_JAHR', blank=True, null=True)
    vertragsart = models.IntegerField(db_column='VERTRAGSART', blank=True, null=True)
    zuweiser_id = models.IntegerField(db_column='ZUWEISER_ID', blank=True, null=True)
    zuweisungstext = models.TextField(db_column='ZUWEISUNGSTEXT', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AUT_KSCHEIN'
        verbose_name = "AUT-Krankenschein"
        verbose_name_plural = "AUT-Krankenscheine"

    def __str__(self):
        return "{}/{} ({})".format(self.gueltigkeit, self.gueltigkeit_jahr, self.kasse_typ)


class AutLaborstammZifferZuordnung(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    stammdaten_id = models.ForeignKey("Laborstamm", db_column='STAMMDATEN_ID', on_delete=models.PROTECT)
    anzahl = models.IntegerField(db_column='ANZAHL')
    typ = models.CharField(db_column='TYP', max_length=50, blank=True, null=True)
    ziffern_kasse_id = models.ForeignKey("AutZiffernKasse", db_column='ZIFFERN_KASSE_ID', blank=True, null=True, on_delete=models.PROTECT)
    ziffern_privat_id = models.ForeignKey("ZiffernPrivat", db_column='ZIFFERN_PRIVAT_ID', blank=True, null=True, on_delete=models.PROTECT)
    status = models.FloatField(db_column='STATUS', blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    vertragsart_kennung = models.IntegerField(db_column='VERTRAGSART_KENNUNG', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AUT_LABORSTAMM_ZIFFER_ZUORDNUNG'
        verbose_name_plural = "Laborstamm-Ziffernzuordnungen"


class AutTherapieZuordnung(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    phys_therapie_id = models.IntegerField(db_column='PHYS_THERAPIE_ID')
    anzahl = models.IntegerField(db_column='ANZAHL')
    typ = models.CharField(db_column='TYP', max_length=50, blank=True, null=True)
    aut_ziffern_kasse_id = models.IntegerField(db_column='AUT_ZIFFERN_KASSE_ID', blank=True, null=True)
    ziffern_privat_id = models.IntegerField(db_column='ZIFFERN_PRIVAT_ID', blank=True, null=True)
    status = models.FloatField(db_column='STATUS', blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    vertragsart_kennung = models.IntegerField(db_column='VERTRAGSART_KENNUNG', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AUT_THERAPIE_ZUORDNUNG'


class AutZiffernKasse(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    typ = models.CharField(db_column='TYP', max_length=255, blank=True, null=True)
    subtyp = models.CharField(db_column='SUBTYP', max_length=255, blank=True, null=True)
    ziffer = models.CharField(db_column='ZIFFER', max_length=255, blank=True, null=True)
    ziffer_text = models.TextField(db_column='ZIFFER_TEXT', blank=True, null=True)
    ziffer_text_kurz = models.CharField(db_column='ZIFFER_TEXT_KURZ', max_length=255, blank=True, null=True)
    punkte = models.FloatField(db_column='PUNKTE')
    gruppe = models.CharField(db_column='GRUPPE', max_length=255, blank=True, null=True)
    kuerzel = models.CharField(db_column='KUERZEL', max_length=255, blank=True, null=True)
    notizen = models.TextField(db_column='NOTIZEN', blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)
    status = models.FloatField(db_column='STATUS', blank=True, null=True)
    ziffer_von = models.DateField(db_column='ZIFFER_VON', blank=True, null=True)
    ziffer_bis = models.DateField(db_column='ZIFFER_BIS', blank=True, null=True)
    betrag = models.FloatField(db_column='BETRAG')
    waehrung = models.CharField(db_column='WAEHRUNG', max_length=255, blank=True, null=True)
    version = models.CharField(db_column='VERSION', max_length=255, blank=True, null=True)
    zeitstempel = models.DateTimeField(db_column='ZEITSTEMPEL', blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    regel_l = Base64TextField(db_column='REGEL_L', blank=True, null=True)
    erstattung_1 = models.FloatField(db_column='ERSTATTUNG_1', blank=True, null=True)
    erstattung_2 = models.FloatField(db_column='ERSTATTUNG_2', blank=True, null=True)
    fachgebiete = models.CharField(db_column='FACHGEBIETE', max_length=255, blank=True, null=True)
    betrag2 = models.FloatField(db_column='BETRAG2', blank=True, null=True)
    punktwert_kuerzel = models.CharField(db_column='PUNKTWERT_KUERZEL', max_length=20, blank=True, null=True)
    einstellungen = Base64TextField(db_column='EINSTELLUNGEN', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AUT_ZIFFERN_KASSE'
        verbose_name_plural = "Kassenziffern"
        unique_together = (('ziffer', 'typ', 'subtyp', 'ziffer_von'),
                           ('ziffer', 'typ', 'subtyp', 'ziffer_bis'),)

    def __str__(self):
        return "[{}] {}".format(
            self.ziffer,
            self.ziffer_text)


class BausteineText(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    typ = models.IntegerField(db_column='TYP')
    position = models.IntegerField(db_column='POSITION')
    kuerzel = models.CharField(db_column='KUERZEL', max_length=50, blank=True, null=True)
    langtext = Base64TextField(db_column='LANGTEXT', blank=True, null=True)
    tb_kategorie = models.CharField(db_column='TB_KATEGORIE', max_length=255, blank=True, null=True)
    wahlarzt = models.CharField(db_column='WAHLARZT', max_length=255, blank=True, null=True)
    kasse = models.CharField(db_column='KASSE', max_length=255, blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=255, blank=True, null=True)
    mandanten = models.CharField(db_column='MANDANTEN', max_length=255)
    bg = models.CharField(db_column='BG', max_length=255, blank=True, null=True)
    vertragsart = models.CharField(db_column='VERTRAGSART', max_length=255, blank=True, null=True)
    system_zeit = models.DateTimeField(db_column='SYSTEM_ZEIT', blank=True, null=True)
    liste = models.TextField(db_column='LISTE', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'BAUSTEINE_TEXT'
        verbose_name_plural = "Textbausteine"

    def __str__(self):
        return "'{}' ({})".format(
            self.kuerzel,
            self.langtext)


class BausteineUntersuchung(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    elter_id = models.CharField(db_column='ELTER_ID', max_length=50, blank=True, null=True)
    kuerzel = models.CharField(db_column='KUERZEL', max_length=50, blank=True, null=True)
    langtext = models.TextField(db_column='LANGTEXT', blank=True, null=True)
    typ = models.CharField(db_column='TYP', max_length=50, blank=True, null=True)
    wahlarzt = models.CharField(db_column='WAHLARZT', max_length=255, blank=True, null=True)
    kasse = models.CharField(db_column='KASSE', max_length=255, blank=True, null=True)
    ziffer = models.CharField(db_column='ZIFFER', max_length=255, blank=True, null=True)
    diagnose = models.CharField(db_column='DIAGNOSE', max_length=255, blank=True, null=True)
    therapie = models.TextField(db_column='THERAPIE', blank=True, null=True)
    befund = models.TextField(db_column='BEFUND', blank=True, null=True)
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=255, blank=True, null=True)
    mandanten = models.CharField(db_column='MANDANTEN', max_length=255, blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    untersuchung = models.TextField(db_column='UNTERSUCHUNG', blank=True, null=True)
    anamnese = models.TextField(db_column='ANAMNESE', blank=True, null=True)
    formular_1_id = models.IntegerField(db_column='FORMULAR_1_ID', blank=True, null=True)
    einstellungen = models.TextField(db_column='EINSTELLUNGEN', blank=True, null=True)
    roentgen = models.TextField(db_column='ROENTGEN', blank=True, null=True)
    sono = models.TextField(db_column='SONO', blank=True, null=True)
    formular_2_id = models.IntegerField(db_column='FORMULAR_2_ID', blank=True, null=True)
    bg = models.CharField(db_column='BG', max_length=255, blank=True, null=True)
    vertragsart = models.CharField(db_column='VERTRAGSART', max_length=255, blank=True, null=True)
    rezept = models.TextField(db_column='REZEPT', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'BAUSTEINE_UNTERSUCHUNG'


class BehandlungenEinzelleistungen(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    behandlungen_gruppen_id = models.ForeignKey("BehandlungenGruppen", db_column='BEHANDLUNGEN_GRUPPEN_ID', on_delete=models.PROTECT)
    therapie_id = models.IntegerField(db_column='THERAPIE_ID')
    region_id = models.ForeignKey("Region", db_column='REGION_ID', blank=True, null=True, on_delete=models.PROTECT)
    be_bemerkung = models.CharField(db_column='BE_BEMERKUNG', max_length=255, blank=True, null=True)
    vertragsart = models.IntegerField(db_column='VERTRAGSART', blank=True, null=True)
    be_reihe = models.SmallIntegerField(db_column='BE_REIHE')
    be_privatpreis = models.FloatField(db_column='BE_PRIVATPREIS', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'BEHANDLUNGEN_EINZELLEISTUNGEN'


class BehandlungenGruppen(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    doku_id = models.IntegerField(db_column='DOKU_ID')
    untersuchung_id = models.IntegerField(db_column='UNTERSUCHUNG_ID')
    bg_anzahl = models.SmallIntegerField(db_column='BG_ANZAHL')
    bg_erhalten = models.SmallIntegerField(db_column='BG_ERHALTEN')
    bg_status = models.CharField(db_column='BG_STATUS', max_length=2)
    bg_vorige_id = models.IntegerField(db_column='BG_VORIGE_ID', blank=True, null=True)
    benutzer = models.ForeignKey("Benutzer", db_column='BENUTZER_ID', blank=True, null=True, on_delete=models.PROTECT)
    bg_aktivierbar_bis = models.DateTimeField(db_column='BG_AKTIVIERBAR_BIS', blank=True, null=True)
    bg_aenr = models.SmallIntegerField(db_column='BG_AENR')
    bg_nr = models.SmallIntegerField(db_column='BG_NR')
    bg_name = models.CharField(db_column='BG_NAME', max_length=30, blank=True, null=True)
    faxstatus = models.CharField(db_column='FAXSTATUS', max_length=1, blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    bearbeitungs_status = models.IntegerField(db_column='BEARBEITUNGS_STATUS')
    bearbeitungs_instanz = models.IntegerField(db_column='BEARBEITUNGS_INSTANZ')
    bewilligungs_status = models.IntegerField(db_column='BEWILLIGUNGS_STATUS')

    class Meta:
        managed = False
        db_table = 'BEHANDLUNGEN_GRUPPEN'


class Behandlungsplaneinheit(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    behandlungsplandoku_id = models.IntegerField(db_column='BEHANDLUNGSPLANDOKU_ID', blank=True, null=True)
    daten = models.TextField(db_column='DATEN', blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'BEHANDLUNGSPLANEINHEIT'


class BehandlungsplaneinheitStamm(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    behandlungsplan_id = models.IntegerField(db_column='BEHANDLUNGSPLAN_ID', blank=True, null=True)
    kuerzel = models.CharField(db_column='KUERZEL', max_length=255, blank=True, null=True)
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)
    sortierung = models.IntegerField(db_column='SORTIERUNG', blank=True, null=True)
    beschreibung = models.TextField(db_column='BESCHREIBUNG', blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'BEHANDLUNGSPLANEINHEIT_STAMM'
        unique_together = (('kuerzel', 'behandlungsplan_id'),)


class BehandlungsplanStamm(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)
    mandant = models.CharField(db_column='MANDANT', max_length=255, blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'BEHANDLUNGSPLAN_STAMM'


class Benutzer(models.Model):
    id = models.IntegerField(db_column='ID', primary_key=True)  # maybe as AutoField...

    # umbenannt / weiter verwendet
    email = models.CharField(db_column='EMAIL', max_length=100, blank=True, null=True)
    kennwort = Base64CharField(db_column='KENNWORT', max_length=50, blank=True, null=True)  # kennwort

    kue = models.CharField(db_column='KUE', unique=True, max_length=255, blank=True, null=True)  # Kürzel
    anrede = models.CharField(db_column='ANREDE', max_length=255, blank=True, null=True)
    titel = models.CharField(db_column='TITEL', max_length=255, blank=True, null=True)
    vorname = models.CharField(db_column='VORNAME', max_length=255, blank=True, null=True)  # vorname
    wvorname = models.CharField(db_column='WVORNAME', max_length=255, blank=True, null=True)
    name = models.CharField(db_column='NAME', max_length=255, blank=True, null=True)  # name
    gebname = models.CharField(db_column='GEBNAME', max_length=255, blank=True, null=True)
    namenszusatz = models.CharField(db_column='NAMENSZUSATZ', max_length=255, blank=True, null=True)
    namensvorsatz = models.CharField(db_column='NAMENSVORSATZ', max_length=255, blank=True, null=True)
    gebdatum = models.DateField(db_column='GEBDATUM', blank=True, null=True)
    bild = models.TextField(db_column='BILD', blank=True, null=True)
    notizen = models.TextField(db_column='NOTIZEN', blank=True, null=True)
    protokoll = Base64TextField(db_column='PROTOKOLL', blank=True, null=True)
    strasse = models.CharField(db_column='STRASSE', max_length=255, blank=True, null=True)
    plz = models.CharField(db_column='PLZ', max_length=50, blank=True, null=True)
    ort = models.CharField(db_column='ORT', max_length=255, blank=True, null=True)
    land = models.CharField(db_column='LAND', max_length=255, blank=True, null=True)
    adresse_id1 = models.CharField(db_column='Adresse_ID1', max_length=50, blank=True, null=True)
    adresse_id2 = models.CharField(db_column='Adresse_ID2', max_length=50, blank=True, null=True)
    adresse_id3 = models.CharField(db_column='Adresse_ID3', max_length=50, blank=True, null=True)
    adresse_id4 = models.CharField(db_column='Adresse_ID4', max_length=50, blank=True, null=True)
    telefon = models.CharField(db_column='TELEFON', max_length=50, blank=True, null=True)
    telefon_2 = models.CharField(db_column='TELEFON_2', max_length=50, blank=True, null=True)
    fax = models.CharField(db_column='FAX', max_length=50, blank=True, null=True)
    mobil = models.CharField(db_column='MOBIL', max_length=50, blank=True, null=True)
    bank = models.CharField(db_column='BANK', max_length=255, blank=True, null=True)
    bank_plz = models.CharField(db_column='BANK_PLZ', max_length=50, blank=True, null=True)
    bank_ort = models.CharField(db_column='BANK_ORT', max_length=255, blank=True, null=True)
    bank_land = models.CharField(db_column='BANK_LAND', max_length=255, blank=True, null=True)
    kto_nr = models.CharField(db_column='KTO_NR', max_length=50, blank=True, null=True)
    blz = models.CharField(db_column='BLZ', max_length=50, blank=True, null=True)
    kto_inhaber = models.CharField(db_column='KTO_INHABER', max_length=255, blank=True, null=True)
    status = models.IntegerField(db_column='STATUS')
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    letzte_kennwoerter = Base64CharField(db_column='LETZTE_KENNWOERTER', max_length=255, blank=True, null=True)
    gueltig_ab = models.DateField(db_column='GUELTIG_AB', blank=True, null=True)
    gueltig_bis = models.DateField(db_column='GUELTIG_BIS', blank=True, null=True)
    organisation = models.CharField(db_column='ORGANISATION', max_length=255, blank=True, null=True)
    mandant = models.CharField(db_column='MANDANT', max_length=255, blank=True, null=True)
    gruppe = models.CharField(db_column='GRUPPE', max_length=255, blank=True, null=True)
    einstellung = Base64TextField(db_column='EINSTELLUNG', blank=True, null=True)
    sprache = models.CharField(db_column='SPRACHE', max_length=255, blank=True, null=True)
    werkzeugleiste = Base64TextField(db_column='WERKZEUGLEISTE', blank=True, null=True)
    arztnummer = models.CharField(db_column='ARZTNUMMER', max_length=25, blank=True, null=True)
    unterschrift_bildpfad = models.CharField(db_column='UNTERSCHRIFT_BILDPFAD', max_length=255, blank=True, null=True)

    kue.verbose_name = "Kürzel"

    class Meta:
        managed = False
        db_table = 'BENUTZER'
        verbose_name = "Benutzer"

    def __str__(self):
        return "{}, {} ({})".format(self.name, self.vorname, self.kue)


class Berechtigung(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    role = models.IntegerField(db_column='ROLE')
    benutzer = models.ForeignKey(Benutzer, db_column='BENUTZER_ID', on_delete=models.PROTECT)
    ermaechtigung_id = models.IntegerField(db_column='ERMAECHTIGUNG_ID')
    organisation_id = models.ForeignKey("Organisation", db_column='ORGANISATION_ID', on_delete=models.PROTECT)
    aktion = models.IntegerField(db_column='AKTION')
    berechtigung = models.IntegerField(db_column='BERECHTIGUNG')
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'BERECHTIGUNG'


class BewilligungslisteFaxBarcodeZaehler(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    fax_barcode = models.IntegerField(db_column='FAX_BARCODE', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'BEWILLIGUNGSLISTE_FAX_BARCODE_ZAEHLER'


class Bewilligungsstellen(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    kuerzel = models.CharField(db_column='KUERZEL', max_length=255, blank=True, null=True)
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)
    faxnummer = models.CharField(db_column='FAXNUMMER', max_length=255, blank=True, null=True)
    formulare_id = models.IntegerField(db_column='FORMULARE_ID', blank=True, null=True)
    kostentraeger_positionsnummern_ermitteln = models.IntegerField(db_column='KOSTENTRAEGER_POSITIONSNUMMERN_ERMITTELN')
    uhrzeit_erstes_fax = models.DateTimeField(db_column='UHRZEIT_ERSTES_FAX', blank=True, null=True)
    uhrzeit_letztes_fax = models.DateTimeField(db_column='UHRZEIT_LETZTES_FAX', blank=True, null=True)
    wiederholung_intervall = models.IntegerField(db_column='WIEDERHOLUNG_INTERVALL', blank=True, null=True)
    auto_vor_erster_leistung = models.IntegerField(db_column='AUTO_VOR_ERSTER_LEISTUNG')
    auto_bundesland_nicht = models.IntegerField(db_column='AUTO_BUNDESLAND_NICHT')
    auto_min_behandlungen_kalenderjahr = models.IntegerField(db_column='AUTO_MIN_BEHANDLUNGEN_KALENDERJAHR')
    auto_einzelleistungen_pro_behandlung_kalenderjahr = models.IntegerField(db_column='AUTO_EINZELLEISTUNGEN_PRO_BEHANDLUNG_KALENDERJAHR')
    automatisch_faxen = models.IntegerField(db_column='AUTOMATISCH_FAXEN')
    sammelsicht = models.IntegerField(db_column='SAMMELSICHT')
    status = models.IntegerField(db_column='STATUS')
    benutzer = models.ForeignKey(Benutzer, db_column='BENUTZER_ID', blank=True, null=True, on_delete=models.PROTECT)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'BEWILLIGUNGSSTELLEN'


class BlzAut(models.Model):
    lfd_nr = models.AutoField(db_column='Lfd_Nr', primary_key=True)
    blz = models.FloatField(db_column='BLZ', blank=True, null=True)
    eigen_blz = models.CharField(db_column='EIGEN_BLZ', max_length=255, blank=True, null=True)
    bisherige_blz = models.CharField(db_column='BISHERIGE_BLZ', max_length=255, blank=True, null=True)
    lzb_girokonto = models.CharField(db_column='LZB_GIROKONTO', max_length=255, blank=True, null=True)
    loeschungsdatum = models.CharField(db_column='LOESCHUNGSDATUM', max_length=255, blank=True, null=True)
    hinweis = models.CharField(db_column='HINWEIS', max_length=255, blank=True, null=True)
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)
    kurzbezeichnung = models.CharField(db_column='KURZBEZEICHNUNG', max_length=50, blank=True, null=True)
    strasse = models.CharField(db_column='STRASSE', max_length=255, blank=True, null=True)
    plz = models.CharField(db_column='PLZ', max_length=255, blank=True, null=True)
    ort = models.CharField(db_column='ORT', max_length=255, blank=True, null=True)
    bundesland = models.CharField(db_column='BUNDESLAND', max_length=255, blank=True, null=True)
    telefon = models.CharField(db_column='TELEFON', max_length=255, blank=True, null=True)
    fax = models.CharField(db_column='FAX', max_length=255, blank=True, null=True)
    btx_bezeichnung = models.CharField(db_column='BTX_BEZEICHNUNG', max_length=255, blank=True, null=True)
    pan = models.CharField(db_column='PAN', max_length=255, blank=True, null=True)
    veroef = models.CharField(db_column='VEROEF', max_length=255, blank=True, null=True)
    bic = models.CharField(db_column='BIC', max_length=255, blank=True, null=True)
    pruefziffer = models.CharField(db_column='PRUEFZIFFER', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'BLZ_AUT'
        verbose_name = 'BLZ (AT)'




class BlzDe(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    blz = models.IntegerField(db_column='BLZ', blank=True, null=True)
    merkmal = models.IntegerField(db_column='MERKMAL', blank=True, null=True)
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)
    plz = models.CharField(db_column='PLZ', max_length=255, blank=True, null=True)
    ort = models.CharField(db_column='ORT', max_length=255, blank=True, null=True)
    kurzbezeichnung = models.CharField(db_column='KURZBEZEICHNUNG', max_length=50, blank=True, null=True)
    pan = models.CharField(db_column='PAN', max_length=255, blank=True, null=True)
    bic = models.CharField(db_column='BIC', max_length=255, blank=True, null=True)
    pruefziffer = models.CharField(db_column='PRUEFZIFFER', max_length=10, blank=True, null=True)
    datensatz_id = models.CharField(db_column='DATENSATZ_ID', max_length=20, blank=True, null=True)
    aenderung_kz = models.CharField(db_column='AENDERUNG_KZ', max_length=10, blank=True, null=True)
    loeschung = models.CharField(db_column='LOESCHUNG', max_length=50, blank=True, null=True)
    nachfolge_blz = models.CharField(db_column='NACHFOLGE_BLZ', max_length=10, blank=True, null=True)
    hinweis = models.CharField(db_column='HINWEIS', max_length=255, blank=True, null=True)
    telefon = models.CharField(db_column='TELEFON', max_length=255, blank=True, null=True)
    fax = models.CharField(db_column='FAX', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'BLZ_DE'


class CdaDokument(models.Model):
    dokument_id = models.AutoField(db_column='DOKUMENT_ID', primary_key=True)
    datum = models.DateTimeField(db_column='DATUM', blank=True, null=True)
    format = models.CharField(db_column='FORMAT', max_length=255, blank=True, null=True)
    status = models.CharField(db_column='STATUS', max_length=255, blank=True, null=True)
    patient_id = models.IntegerField(db_column='PATIENT_ID', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'CDA_DOKUMENT'


class Combo(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    name = models.CharField(db_column='NAME', max_length=50, blank=True, null=True)
    text_kurz_de = models.CharField(db_column='TEXT_KURZ_DE', max_length=255, blank=True, null=True)
    text_kurz_en = models.CharField(db_column='TEXT_KURZ_EN', max_length=255, blank=True, null=True)
    text_kurz_fr = models.CharField(db_column='TEXT_KURZ_FR', max_length=255, blank=True, null=True)
    text_kurz_sp = models.CharField(db_column='TEXT_KURZ_SP', max_length=255, blank=True, null=True)
    text_de = models.CharField(db_column='TEXT_DE', max_length=255, blank=True, null=True)
    text_en = models.CharField(db_column='TEXT_EN', max_length=255, blank=True, null=True)
    text_fr = models.CharField(db_column='TEXT_FR', max_length=255, blank=True, null=True)
    text_sp = models.CharField(db_column='TEXT_SP', max_length=255, blank=True, null=True)
    kennung = models.IntegerField(db_column='KENNUNG', blank=True, null=True)
    zusatz_1 = Base64TextField(db_column='ZUSATZ_1', blank=True, null=True)
    zusatz_2 = Base64TextField(db_column='ZUSATZ_2', blank=True, null=True)
    zusatz_3 = Base64TextField(db_column='ZUSATZ_3', max_length=50, blank=True, null=True)
    zusatz_4 = Base64TextField(db_column='ZUSATZ_4', max_length=50, blank=True, null=True)
    kennung_s = models.CharField(db_column='KENNUNG_S', max_length=255, blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'COMBO'
        unique_together = (('name', 'kennung'), ('name', 'kennung_s'), ('name', 'kennung_s'),)

    def __str__(self):
        return self.text_de or ''


class Dermatest(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    patient_id = models.IntegerField(db_column='PATIENT_ID', blank=True, null=True)
    untersuchung_id = models.IntegerField(db_column='UNTERSUCHUNG_ID', blank=True, null=True)
    typ = models.CharField(db_column='TYP', max_length=50, blank=True, null=True)
    datum = models.DateField(db_column='DATUM', blank=True, null=True)
    test = models.CharField(db_column='TEST', max_length=50, blank=True, null=True)
    testergebnis = models.IntegerField(db_column='TESTERGEBNIS', blank=True, null=True)
    bemerkung = models.TextField(db_column='BEMERKUNG', blank=True, null=True)
    freitext1 = models.TextField(db_column='FREITEXT1', blank=True, null=True)
    freitext2 = models.TextField(db_column='FREITEXT2', blank=True, null=True)
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)
    system_datum = models.DateField(db_column='SYSTEM_DATUM', blank=True, null=True)
    system_uhrzeit = models.TimeField(db_column='SYSTEM_UHRZEIT', blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'DERMATEST'


class Diagnose(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    patient_id = models.ForeignKey("Patient", db_column='PATIENT_ID', on_delete=models.PROTECT)
    diagnosedoku_id = models.ForeignKey("Dokumentation", db_column='DIAGNOSEDOKU_ID', on_delete=models.PROTECT)
    untersuchung_id = models.IntegerField(db_column='UNTERSUCHUNG_ID')
    
    # "D", "DD", "ADD", ""
    typ = models.CharField(db_column='TYP', max_length=10)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    status = models.IntegerField(db_column='STATUS')
    datum = models.DateField(db_column='DATUM', blank=True, null=True)
    uhrzeit = models.TimeField(db_column='UHRZEIT', blank=True, null=True)
    schluessel = models.CharField(db_column='SCHLUESSEL', max_length=50, blank=True, null=True)
    diagnose_text = models.CharField(db_column='DIAGNOSE_TEXT', max_length=255, blank=True, null=True)
    diagnose_datum = models.DateField(db_column='DIAGNOSE_DATUM', blank=True, null=True)
    diagnose_uhrzeit = models.TimeField(db_column='DIAGNOSE_UHRZEIT', blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)
    notizen = models.TextField(db_column='NOTIZEN', blank=True, null=True)
    kuerzel = models.CharField(db_column='KUERZEL', max_length=50, blank=True, null=True)
    zusatz = models.TextField(db_column='ZUSATZ', blank=True, null=True)
    sicherheit = models.CharField(db_column='SICHERHEIT', max_length=10, blank=True, null=True)
    lokalisation = models.CharField(db_column='LOKALISATION', max_length=10, blank=True, null=True)
    erlauterung = models.CharField(db_column='ERLAUTERUNG', max_length=255, blank=True, null=True)
    schluessel_typ = models.IntegerField(db_column='SCHLUESSEL_TYP')
    sort = models.IntegerField(db_column='SORT')

    class Meta:
        managed = False
        db_table = 'DIAGNOSE'
        verbose_name_plural = "Diagnosen"

    def __str__(self):
        return "{}".format(self.diagnose_text)


class Dokumentation(models.Model):

    class Status(Enum):
        """An Enum that represents the status number in the Aceto database"""
        ANGELEGT = 1
        FREIGEGEBEN = 2
        ZU_DRUCKEN = 3
        UNGELESEN = 5
        GELESEN = 10   # = GEDRUCKT ???
        VERSENDET = 11
        ABGESCHLOSSEN = 15
        STORNIERT = 95
        ABGERECHNET = 90
        GELOESCHT = 99

    TYP_CHOICES = (("Untersuchung", "Untersuchung"),
                   ("Diagnose", "Diagnose"),
                   ("Anamnese", "Anamnese"),
                   ("Leistung", "Leistung"),
                   ("LaborBefund", "LaborBefund"),
                   ("Rezept", "Rezept"),
                   ("Befund", "Befund"),
                   ("Therapie", "Therapie"),
                   ("FremdBefund", "FremdBefund"),
                   ("Formular", "Formular"),
                   ("Rechnung", "Rechnung"),
                   ("Arztbrief", "Arztbrief"))

    id = models.AutoField(db_column='ID', primary_key=True)
    typ = models.CharField(db_column='TYP', choices=TYP_CHOICES, max_length=50, blank=True, null=True, )

    # Link zu ID in Fremdtabelle, je nach TYP ist das eine andere Tabelle...
    fremd_id = models.IntegerField(db_column='FREMD_ID')
    patient = models.ForeignKey("Patient", db_column='PATIENT_ID', on_delete=models.PROTECT)
    fall_nr = models.IntegerField(db_column='FALL_NR')
    untersuchung_nr = models.IntegerField(db_column='UNTERSUCHUNG_NR')
    orgeinheit_id = models.IntegerField(db_column='ORGEINHEIT_ID')
    ermaechtigung_id = models.ForeignKey("PraxisDaten", db_column='ERMAECHTIGUNG_ID', on_delete=models.CASCADE)
    kschein_id = models.ForeignKey("AutKschein", db_column='KSCHEIN_ID', on_delete=models.PROTECT)
    datum = models.DateField(db_column='DATUM', blank=True, null=True)

    # Der Text der in der Kartei erscheint
    doku_text = models.CharField(db_column='DOKU_TEXT', max_length=255, blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    protokoll = Base64TextField(db_column='PROTOKOLL', blank=True, null=True)
    notizen = models.TextField(db_column='NOTIZEN', blank=True, null=True)
    status = models.IntegerField(db_column='STATUS')
    kennwort = models.CharField(db_column='KENNWORT', max_length=255, blank=True, null=True)
    fall_zaehler = models.CharField(db_column='FALL_ZAEHLER', unique=True, max_length=50, blank=True, null=True)
    unters_zaehler = models.CharField(db_column='UNTERS_ZAEHLER', unique=True, max_length=50, blank=True, null=True)
    freitext1 = Base64TextField(db_column='FREITEXT1', blank=True, null=True)
    freitext2 = Base64TextField(db_column='FREITEXT2', blank=True, null=True)
    freitext3 = Base64TextField(db_column='FREITEXT3', blank=True, null=True)
    freitext4 = Base64TextField(db_column='FREITEXT4', blank=True, null=True)
    doku_properties = Base64TextField(db_column='DOKU_PROPERTIES', blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)
    arzt1 = models.CharField(db_column='ARZT1', max_length=50, blank=True, null=True)
    arzt2 = models.CharField(db_column='ARZT2', max_length=50, blank=True, null=True)
    subtyp = models.IntegerField(db_column='SUBTYP')
    letzte_aenderung_datum = models.DateField(db_column='LETZTE_AENDERUNG_DATUM', blank=True, null=True)
    letzte_aenderung_uhrzeit = models.TimeField(db_column='LETZTE_AENDERUNG_UHRZEIT', blank=True, null=True)
    abrechnung = models.CharField(db_column='ABRECHNUNG', max_length=255, blank=True, null=True)
    doku_uhrzeit = models.TimeField(db_column='DOKU_UHRZEIT', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'DOKUMENTATION'
        verbose_name = "Dokumentation"
        verbose_name_plural = "Dokumentationen"

    def __str__(self):
        if self.doku_text is None:
            return self.typ
        else:
            return self.doku_text


class DokuTyp(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    doku_typ = models.CharField(db_column='DOKU_TYP', max_length=50, blank=True, null=True)
    doku_sortierung = models.IntegerField(db_column='DOKU_SORTIERUNG')

    class Meta:
        managed = False
        db_table = 'DOKU_TYP'


class Druckereinstellung(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    typ = models.IntegerField(db_column='TYP')
    referenz_id = models.IntegerField(db_column='REFERENZ_ID')
    rechner_name = models.CharField(db_column='RECHNER_NAME', max_length=255)
    druckereinstellung = models.TextField(db_column='DRUCKEREINSTELLUNG', blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=255)
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG')
    protokoll = models.TextField(db_column='PROTOKOLL')

    class Meta:
        managed = False
        db_table = 'DRUCKEREINSTELLUNG'


class EhmvAtc(models.Model):
    atc = models.CharField(db_column='ATC', primary_key=True, max_length=7)
    bedeutung = models.CharField(db_column='Bedeutung', max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'EHMV_ATC'


class EhmvHinweis(models.Model):
    pharmanummer = models.IntegerField(db_column='Pharmanummer', primary_key=True)
    hinweise = models.TextField(db_column='Hinweise', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'EHMV_HINWEIS'


class EhmvIndtext(models.Model):
    pharmanummer = models.IntegerField(db_column='Pharmanummer', primary_key=True)
    kriterien = models.TextField(db_column='Kriterien', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'EHMV_INDTEXT'


class EhmvMedikament(models.Model):
    pharmanummer = models.IntegerField(db_column='Pharmanummer')
    regnrpre = models.CharField(db_column='RegNrPre', max_length=2, blank=True, null=True)
    regnr = models.CharField(db_column='RegNr', max_length=6, blank=True, null=True)
    regnreu = models.CharField(db_column='RegNrEU', max_length=3, blank=True, null=True)
    name = models.CharField(db_column='Name', max_length=100, blank=True, null=True)
    box = models.CharField(db_column='Box', max_length=3, blank=True, null=True)
    kassenzeichen = models.CharField(db_column='Kassenzeichen', max_length=3, blank=True, null=True)
    menge = models.CharField(db_column='Menge', max_length=6, blank=True, null=True)
    mengenart = models.CharField(db_column='Mengenart', max_length=2, blank=True, null=True)
    kvp = models.CharField(db_column='KVP', max_length=9, blank=True, null=True)
    kvp_einheit = models.CharField(db_column='KVP_Einheit', max_length=9, blank=True, null=True)
    darreichung = models.CharField(db_column='Darreichung', max_length=9, blank=True, null=True)
    teilbarkeit = models.CharField(db_column='Teilbarkeit', max_length=3, blank=True, null=True)
    refaktie = models.CharField(db_column='Refaktie', max_length=3, blank=True, null=True)
    abgabeanzahl = models.IntegerField(db_column='Abgabeanzahl', blank=True, null=True)
    hinweis = models.CharField(db_column='Hinweis', max_length=28, blank=True, null=True)
    langzeitbewilligung = models.CharField(db_column='Langzeitbewilligung', max_length=5, blank=True, null=True)
    suchtgift = models.CharField(db_column='Suchtgift', max_length=5, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'EHMV_MEDIKAMENT'


class EhmvPharma(models.Model):
    pharmanummer = models.IntegerField(db_column='Pharmanummer', blank=True, null=True)
    position = models.IntegerField(db_column='Position', blank=True, null=True)
    pharmanummervgl = models.IntegerField(db_column='PharmanummerVgl', blank=True, null=True)
    positionvgl = models.IntegerField(db_column='PositionVgl', blank=True, null=True)
    kennzeichenvgl = models.IntegerField(db_column='KennzeichenVgl', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'EHMV_PHARMA'


class EhmvRegeltexte(models.Model):
    pharmanummer = models.IntegerField(db_column='Pharmanummer', primary_key=True)
    hinweis = models.TextField(db_column='Hinweis', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'EHMV_REGELTEXTE'


class EhmvWirkstoff(models.Model):
    pharmanummer = models.IntegerField(db_column='Pharmanummer')
    atc_who = models.CharField(db_column='ATC_WHO', max_length=7, blank=True, null=True)
    laufnummer = models.IntegerField(db_column='Laufnummer', blank=True, null=True)
    atc_erg = models.CharField(db_column='ATC_ERG', max_length=7, blank=True, null=True)
    staerke = models.FloatField(db_column='Staerke', blank=True, null=True)
    dimension = models.CharField(db_column='Dimension', max_length=9, blank=True, null=True)
    eigenschaft = models.CharField(db_column='Eigenschaft', max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'EHMV_WIRKSTOFF'


class Email(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    emaildoku_id = models.IntegerField(db_column='EMAILDOKU_ID', unique=True, blank=True, null=True)
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)
    zeit = models.DateTimeField(db_column='ZEIT', blank=True, null=True)
    content_type = models.CharField(db_column='CONTENT_TYPE', max_length=255)
    mime_version = models.CharField(db_column='MIME_VERSION', max_length=255, blank=True, null=True)
    absender = models.CharField(db_column='ABSENDER', max_length=255, blank=True, null=True)
    empfaenger = models.CharField(db_column='EMPFAENGER', max_length=255, blank=True, null=True)
    message_id = models.CharField(db_column='MESSAGE_ID', max_length=255, blank=True, null=True)
    betreff = models.TextField(db_column='BETREFF', blank=True, null=True)
    nachricht_text = models.TextField(db_column='NACHRICHT_TEXT', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'EMAIL'


class ErinnerungTermin(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    mandant_id = models.IntegerField(db_column='MANDANT_ID', blank=True, null=True)
    patient = models.ForeignKey("Patient", db_column='PATIENT_ID', blank=True, null=True, on_delete=models.CASCADE)
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)
    datum = models.DateField(db_column='DATUM', blank=True, null=True)
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)
    bemerkung = Base64TextField(db_column='BEMERKUNG', blank=True, null=True)
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)
    system_zeit = models.DateTimeField(db_column='SYSTEM_ZEIT', blank=True, null=True)
    anlage_zeit = models.DateTimeField(db_column='ANLAGE_ZEIT', blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)
    erinnerung_kennung = models.IntegerField(db_column='ERINNERUNG_KENNUNG', blank=True, null=True)
    referenzdoku_id = models.IntegerField(db_column='REFERENZDOKU_ID', blank=True, null=True)
    will_email_erinnerung = models.IntegerField(db_column='WILL_EMAIL_ERINNERUNG', blank=True, null=True)

    will_sms_erinnerung = models.IntegerField(db_column='WILL_SMS_ERINNERUNG', blank=True, null=True)
    will_sms_erinnerung.verbose_name = "Will SMS-Erinnerung"

    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ERINNERUNG_TERMIN'
        verbose_name_plural = 'Terminerinnerungen'
        verbose_name = 'Terminerinnerung'

    def __str__(self):
        return "{}: {}, {} ({})".format(self.datum, self.patient.name, self.patient.vorname, self.patient.gebdatum)


class Fachbefund(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    patient_id = models.ForeignKey("Patient", db_column='PATIENT_ID', on_delete=models.PROTECT)
    doku_id = models.IntegerField(db_column='DOKU_ID')
    #FIXME: Untersuchung - Dokumentation? FK?
    untersuchung_id = models.ForeignKey("Dokumentation", db_column='UNTERSUCHUNG_ID', on_delete=models.PROTECT)
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)
    anschrift = models.TextField(db_column='ANSCHRIFT', blank=True, null=True)
    email = models.CharField(db_column='EMAIL', max_length=255, blank=True, null=True)
    begruessung = models.TextField(db_column='BEGRUESSUNG', blank=True, null=True)
    einleitung = models.TextField(db_column='EINLEITUNG', blank=True, null=True)
    anamnese = models.TextField(db_column='ANAMNESE', blank=True, null=True)
    text_1 = models.TextField(db_column='TEXT_1', blank=True, null=True)
    text_2 = models.TextField(db_column='TEXT_2', blank=True, null=True)
    text_3 = models.TextField(db_column='TEXT_3', blank=True, null=True)
    text_4 = models.TextField(db_column='TEXT_4', blank=True, null=True)
    text_5 = models.TextField(db_column='TEXT_5', blank=True, null=True)
    text_6 = models.TextField(db_column='TEXT_6', blank=True, null=True)
    text_7 = models.TextField(db_column='TEXT_7', blank=True, null=True)
    text_8 = models.TextField(db_column='TEXT_8', blank=True, null=True)
    text_9 = models.TextField(db_column='TEXT_9', blank=True, null=True)
    text_10 = models.TextField(db_column='TEXT_10', blank=True, null=True)
    text_11 = models.TextField(db_column='TEXT_11', blank=True, null=True)
    text_12 = models.TextField(db_column='TEXT_12', blank=True, null=True)
    text_13 = models.TextField(db_column='TEXT_13', blank=True, null=True)
    text_14 = models.TextField(db_column='TEXT_14', blank=True, null=True)
    text_15 = models.TextField(db_column='TEXT_15', blank=True, null=True)
    text_16 = models.TextField(db_column='TEXT_16', blank=True, null=True)
    schlussformel = models.TextField(db_column='SCHLUSSFORMEL', blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)
    notizen = models.TextField(db_column='NOTIZEN', blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)
    letzte_aenderung_datum = models.DateField(db_column='LETZTE_AENDERUNG_DATUM', blank=True, null=True)
    letzte_aenderung_uhrzeit = models.TimeField(db_column='LETZTE_AENDERUNG_UHRZEIT', blank=True, null=True)
    zusatz = models.TextField(db_column='ZUSATZ', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'FACHBEFUND'
        verbose_name_plural = "Fachbefunde"


class Fallnummer(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    fallnummer = models.CharField(db_column='FALLNUMMER', max_length=50)
    patient_id = models.IntegerField(db_column='PATIENT_ID')
    aufnahme_zeit = models.DateTimeField(db_column='AUFNAHME_ZEIT', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'FALLNUMMER'
        unique_together = (('fallnummer', 'patient_id'),)
        verbose_name_plural = "Fallnummern"


class Firma(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    name = models.CharField(db_column='NAME', max_length=255, blank=True, null=True)
    abteilung = models.CharField(db_column='ABTEILUNG', max_length=255, blank=True, null=True)
    kurzname = models.CharField(db_column='KURZNAME', max_length=255, blank=True, null=True)
    kuerzel = models.CharField(db_column='KUERZEL', max_length=255, blank=True, null=True)
    adresse = models.IntegerField(db_column='ADRESSE')
    post_adresse = models.IntegerField(db_column='POST_ADRESSE')
    liefer_adresse = models.IntegerField(db_column='LIEFER_ADRESSE')
    rechnung_adresse = models.IntegerField(db_column='RECHNUNG_ADRESSE')
    kommunikation = models.IntegerField(db_column='KOMMUNIKATION')
    konto = models.IntegerField(db_column='KONTO')
    ansprechpartner = models.IntegerField(db_column='ANSPRECHPARTNER')
    notizen = models.TextField(db_column='NOTIZEN', blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    status = models.IntegerField(db_column='STATUS')

    class Meta:
        managed = False
        db_table = 'FIRMA'


class Formulare(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    aceto_nr = models.IntegerField(db_column='ACETO_NR', unique=True, blank=True, null=True)
    position = models.IntegerField(db_column='POSITION', blank=True, null=True)
    name = models.CharField(db_column='NAME', max_length=100, blank=True, null=True)
    beschreibung = models.CharField(db_column='BESCHREIBUNG', max_length=255, blank=True, null=True)
    report = models.CharField(db_column='REPORT', max_length=50, blank=True, null=True)
    farbe = models.CharField(db_column='FARBE', max_length=50, blank=True, null=True)
    logo = models.CharField(db_column='LOGO', max_length=50, blank=True, null=True)
    einstellungen = Base64TextField(db_column='EINSTELLUNGEN', blank=True, null=True)

    # XML Feld mit Daten der Felder
    felder = Base64TextField(db_column='FELDER', blank=True, null=True)
    wahlarzt = models.CharField(db_column='WAHLARZT', max_length=255, blank=True, null=True)
    kasse = models.CharField(db_column='KASSE', max_length=255, blank=True, null=True)
    mandanten = models.CharField(db_column='MANDANTEN', max_length=255, blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)
    protokoll = Base64TextField(db_column='PROTOKOLL', blank=True, null=True)
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)
    schnell_knopf = models.IntegerField(db_column='SCHNELL_KNOPF', unique=True, blank=True, null=True, verbose_name="Schnellknopf")
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)
    version = models.FloatField(db_column='VERSION', blank=True, null=True)
    pdftitel = models.CharField(db_column='PDFTITEL', max_length=255, blank=True, null=True, verbose_name="PDF-Titel")
    pdfdokumentformat = models.CharField(db_column='PDFDOKUMENTFORMAT', max_length=50, blank=True, null=True)
    pdfcssexpert = models.TextField(db_column='PDFCSSEXPERT', blank=True, null=True)
    htmldokument = models.TextField(db_column='HTMLDOKUMENT', blank=True, null=True)
    logo_seite2 = models.CharField(db_column='LOGO_SEITE2', max_length=50, blank=True, null=True)
    notizen = models.TextField(db_column='NOTIZEN', blank=True, null=True)
    druck_css = models.CharField(db_column='DRUCK_CSS', max_length=50, blank=True, null=True)
    email_css = models.CharField(db_column='EMAIL_CSS', max_length=50, blank=True, null=True)
    fax_css = models.CharField(db_column='FAX_CSS', max_length=50, blank=True, null=True)
    serienbrief_typen = models.TextField(db_column='SERIENBRIEF_TYPEN', blank=True, null=True)
    serienbrief_adressat_typ = models.CharField(db_column='SERIENBRIEF_ADRESSAT_TYP', max_length=50, blank=True, null=True)
    vertragsart = models.CharField(db_column='VERTRAGSART', max_length=255, blank=True, null=True)
    sprache = models.CharField(db_column='SPRACHE', max_length=10, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'FORMULARE'
        verbose_name_plural = 'Formulare'

    def __str__(self):
        return '{} (id: {})'.format(self.name, self.id)


class FtpArchiv(models.Model):
    ftp_archiv_id = models.AutoField(db_column='FTP_ARCHIV_ID', primary_key=True)
    datei_name = models.CharField(db_column='DATEI_NAME', max_length=255, blank=True, null=True)
    pfad = models.CharField(db_column='PFAD', max_length=255, blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    zugriff_von = models.CharField(db_column='ZUGRIFF_VON', max_length=255, blank=True, null=True)
    datum_zugriff = models.DateField(db_column='DATUM_ZUGRIFF', blank=True, null=True)
    uhrzeit_zugriff = models.TimeField(db_column='UHRZEIT_ZUGRIFF', blank=True, null=True)
    zugriff_computername = models.CharField(db_column='ZUGRIFF_COMPUTERNAME', max_length=255, blank=True, null=True)
    aktion = models.IntegerField(db_column='AKTION', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'FTP_ARCHIV'


class Geraete(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    gt_typ = models.CharField(db_column='GT_TYP', max_length=50, blank=True, null=True)
    gr_kuerzel = models.CharField(db_column='GR_KUERZEL', unique=True, max_length=255, blank=True, null=True)
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)
    gr_beschreibung = models.TextField(db_column='GR_BESCHREIBUNG', blank=True, null=True)
    gruppe = models.IntegerField(db_column='GRUPPE', blank=True, null=True)
    ermaechtigungen = models.CharField(db_column='ERMAECHTIGUNGEN', max_length=255, blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)
    gr_timestart = models.DateTimeField(db_column='GR_TIMESTART')
    gr_rechenraster = models.IntegerField(db_column='GR_RECHENRASTER')
    gr_gm = models.IntegerField(db_column='GR_GM')
    modalitaet = models.IntegerField(db_column='MODALITAET', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'GERAETE'


class GeraeteCode(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    geraet_id = models.IntegerField(db_column='GERAET_ID', blank=True, null=True)
    procedure_code = models.CharField(db_column='PROCEDURE_CODE', max_length=255, blank=True, null=True)
    beschreibung = models.TextField(db_column='BESCHREIBUNG', blank=True, null=True)
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'GERAETE_CODE'
        unique_together = (('geraet_id', 'procedure_code'),)


class GeraeteTypen(models.Model):
    gt_typ = models.CharField(db_column='GT_TYP', primary_key=True, max_length=50)

    class Meta:
        managed = False
        db_table = 'GERAETE_TYPEN'


class Grafik(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    patient_id = models.IntegerField(db_column='PATIENT_ID', blank=True, null=True)
    typ = models.CharField(db_column='TYP', max_length=50, blank=True, null=True)
    datum = models.DateField(db_column='DATUM', blank=True, null=True)
    bild = models.CharField(db_column='BILD', max_length=50, blank=True, null=True)
    position = models.CharField(db_column='POSITION', max_length=50, blank=True, null=True)
    markierung_text = models.TextField(db_column='MARKIERUNG_TEXT', blank=True, null=True)
    freitext1 = models.TextField(db_column='FREITEXT1', blank=True, null=True)
    freitext2 = models.TextField(db_column='FREITEXT2', blank=True, null=True)
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)
    system_datum = models.DateField(db_column='SYSTEM_DATUM', blank=True, null=True)
    system_uhrzeit = models.TimeField(db_column='SYSTEM_UHRZEIT', blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'GRAFIK'


class Gruppen(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    typ = models.CharField(db_column='TYP', max_length=11, blank=True, null=True)
    name = models.CharField(db_column='NAME', max_length=50, blank=True, null=True)
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)
    kennung = models.IntegerField(db_column='KENNUNG', blank=True, null=True)
    beschreibung = models.CharField(db_column='BESCHREIBUNG', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'GRUPPEN'


class Heilmittel(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    typ = models.CharField(db_column='TYP', max_length=50, blank=True, null=True)
    position = models.IntegerField(db_column='POSITION', blank=True, null=True)
    kuerzel = models.CharField(db_column='KUERZEL', max_length=255, blank=True, null=True)
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)
    anzahl_standard = models.IntegerField(db_column='ANZAHL_STANDARD', blank=True, null=True)
    frequenz_standard = models.IntegerField(db_column='FREQUENZ_STANDARD', blank=True, null=True)
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=255, blank=True, null=True)
    wahlarzt = models.CharField(db_column='WAHLARZT', max_length=255, blank=True, null=True)
    vertragsart = models.CharField(db_column='VERTRAGSART', max_length=255, blank=True, null=True)
    bg = models.CharField(db_column='BG', max_length=255, blank=True, null=True)
    kasse = models.CharField(db_column='KASSE', max_length=255, blank=True, null=True)
    mandanten = models.CharField(db_column='MANDANTEN', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'HEILMITTEL'
        unique_together = (('kuerzel', 'typ'),)


class Hilfsmittel(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    position = models.IntegerField(db_column='POSITION')
    typ = models.CharField(db_column='TYP', max_length=50, blank=True, null=True)
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)
    rezeptur = models.TextField(db_column='REZEPTUR', blank=True, null=True)
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)
    letzte_aenderung_datum = models.DateField(db_column='LETZTE_AENDERUNG_DATUM', blank=True, null=True)
    letzte_aenderung_uhrzeit = models.TimeField(db_column='LETZTE_AENDERUNG_UHRZEIT', blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)
    dosisschema = models.TextField(db_column='DOSISSCHEMA', blank=True, null=True)
    ermaechtigung = models.CharField(db_column='ERMAECHTIGUNG', max_length=255)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    kasse = models.CharField(db_column='KASSE', max_length=255, blank=True, null=True)
    privat = models.CharField(db_column='PRIVAT', max_length=255, blank=True, null=True)
    einstellung = models.TextField(db_column='EINSTELLUNG', blank=True, null=True)
    medikament_box = models.CharField(db_column='MEDIKAMENT_BOX', max_length=5, blank=True, null=True)
    vertragsart = models.CharField(db_column='VERTRAGSART', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'HILFSMITTEL'


class Historie(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    doku_typ = models.CharField(db_column='DOKU_TYP', max_length=255, blank=True, null=True)
    doku_id = models.IntegerField(db_column='DOKU_ID', blank=True, null=True)
    zeit = models.DateTimeField(db_column='ZEIT', blank=True, null=True)
    benutzer = models.CharField(db_column='BENUTZER', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'HISTORIE'


class HistoBefunde(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    patient_id = models.IntegerField(db_column='PATIENT_ID', blank=True, null=True)
    untersuchung_id = models.IntegerField(db_column='UNTERSUCHUNG_ID', blank=True, null=True)
    typ = models.CharField(db_column='TYP', max_length=50, blank=True, null=True)
    datum = models.DateField(db_column='DATUM', blank=True, null=True)
    abstrich_freitext = models.TextField(db_column='ABSTRICH_FREITEXT', blank=True, null=True)
    befund_ergebnis = models.CharField(db_column='BEFUND_ERGEBNIS', max_length=50, blank=True, null=True)
    befund_freitext = models.TextField(db_column='BEFUND_FREITEXT', blank=True, null=True)
    bemerkung = models.TextField(db_column='BEMERKUNG', blank=True, null=True)
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)
    system_datum = models.DateField(db_column='SYSTEM_DATUM', blank=True, null=True)
    system_uhrzeit = models.TimeField(db_column='SYSTEM_UHRZEIT', blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)
    ermaechtigung_id = models.IntegerField(db_column='ERMAECHTIGUNG_ID', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'HISTO_BEFUNDE'


class Hl7Auftragsdaten(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    typ = models.CharField(db_column='TYP', max_length=20, blank=True, null=True)
    trigger_event = models.CharField(db_column='TRIGGER_EVENT', max_length=20, blank=True, null=True)
    message_id = models.CharField(db_column='MESSAGE_ID', max_length=255, blank=True, null=True)
    dateiname = models.CharField(db_column='DATEINAME', max_length=255, blank=True, null=True)
    patient_id = models.IntegerField(db_column='PATIENT_ID', blank=True, null=True)
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)
    message = models.TextField(db_column='MESSAGE', blank=True, null=True)
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'HL7_AUFTRAGSDATEN'


class Hl7Leistungsdaten(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    typ = models.CharField(db_column='TYP', max_length=20, blank=True, null=True)
    trigger_event = models.CharField(db_column='TRIGGER_EVENT', max_length=20, blank=True, null=True)
    segment = models.CharField(db_column='SEGMENT', max_length=20, blank=True, null=True)
    patient_id = models.IntegerField(db_column='PATIENT_ID')
    datum = models.DateField(db_column='DATUM', blank=True, null=True)
    producer_id = models.CharField(db_column='PRODUCER_ID', unique=True, max_length=255, blank=True, null=True)
    ergebnis = models.CharField(db_column='ERGEBNIS', max_length=50, blank=True, null=True)
    ergebnis_typ = models.CharField(db_column='ERGEBNIS_TYP', max_length=10, blank=True, null=True)
    ergebnis_identifier = models.CharField(db_column='ERGEBNIS_IDENTIFIER', max_length=255, blank=True, null=True)
    ergebnis_status = models.CharField(db_column='ERGEBNIS_STATUS', max_length=10, blank=True, null=True)
    ergebnis_einheit = models.CharField(db_column='ERGEBNIS_EINHEIT', max_length=50, blank=True, null=True)
    beschreibung = models.CharField(db_column='BESCHREIBUNG', max_length=255, blank=True, null=True)
    status = models.IntegerField(db_column='STATUS')
    ermaechtigung_id = models.IntegerField(db_column='ERMAECHTIGUNG_ID')
    dateiname = models.CharField(db_column='DATEINAME', max_length=255, blank=True, null=True)
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'HL7_LEISTUNGSDATEN'


class Hl7Sendeprotokoll(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    typ = models.CharField(db_column='TYP', max_length=20, blank=True, null=True)
    trigger_event = models.CharField(db_column='TRIGGER_EVENT', max_length=20, blank=True, null=True)
    dateiname = models.CharField(db_column='DATEINAME', max_length=255, blank=True, null=True)
    patient_id = models.IntegerField(db_column='PATIENT_ID', blank=True, null=True)
    doku_id = models.IntegerField(db_column='DOKU_ID', blank=True, null=True)
    zeit = models.DateTimeField(db_column='ZEIT', blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=255, blank=True, null=True)
    nachricht = models.TextField(db_column='NACHRICHT', blank=True, null=True)
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'HL7_SENDEPROTOKOLL'


class IcdKapitel(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    hierarchie = models.IntegerField(db_column='HIERARCHIE')
    nummer = models.CharField(db_column='NUMMER', max_length=20, blank=True, null=True)
    von_icd = models.CharField(db_column='VON_ICD', max_length=20, blank=True, null=True)
    bis_icd = models.CharField(db_column='BIS_ICD', max_length=20, blank=True, null=True)
    bezeichnung = models.TextField(db_column='BEZEICHNUNG', blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ICD_KAPITEL'


class IcdKatalog(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    typ = models.CharField(db_column='TYP', max_length=10, blank=True, null=True)
    icd_kode = models.CharField(db_column='ICD_KODE', max_length=6, blank=True, null=True)
    icd_text = models.TextField(db_column='ICD_TEXT', blank=True, null=True)
    icd_typ = models.IntegerField(db_column='ICD_TYP', blank=True, null=True)
    notation = models.CharField(db_column='NOTATION', max_length=1, blank=True, null=True)
    geschlechtsbezug = models.CharField(db_column='GESCHLECHTSBEZUG', max_length=2, blank=True, null=True)
    alter_unten = models.CharField(db_column='ALTER_UNTEN', max_length=10, blank=True, null=True)
    alter_oben = models.CharField(db_column='ALTER_OBEN', max_length=10, blank=True, null=True)
    alter_fehler = models.CharField(db_column='ALTER_FEHLER', max_length=1, blank=True, null=True)
    exotik = models.CharField(db_column='EXOTIK', max_length=1, blank=True, null=True)
    kode_belegt = models.CharField(db_column='KODE_BELEGT', max_length=1, blank=True, null=True)
    meldepflicht = models.CharField(db_column='MELDEPFLICHT', max_length=1, blank=True, null=True)
    ebm_kombi = models.CharField(db_column='EBM_KOMBI', max_length=1, blank=True, null=True)
    gruppe = models.CharField(db_column='GRUPPE', max_length=255, blank=True, null=True)
    kuerzel = models.CharField(db_column='KUERZEL', max_length=255, blank=True, null=True)
    notizen = models.CharField(db_column='NOTIZEN', max_length=255, blank=True, null=True)
    protokoll = models.CharField(db_column='PROTOKOLL', max_length=255, blank=True, null=True)
    status = models.CharField(db_column='STATUS', max_length=255, blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=255, blank=True, null=True)
    ziffer_von = models.CharField(db_column='ZIFFER_VON', max_length=255, blank=True, null=True)
    ziffer_bis = models.CharField(db_column='ZIFFER_BIS', max_length=255, blank=True, null=True)
    version = models.CharField(db_column='VERSION', max_length=255, blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)
    geschlecht_fehler = models.CharField(db_column='GESCHLECHT_FEHLER', max_length=10)
    akr_ref = models.CharField(db_column='AKR_REF', max_length=255)

    class Meta:
        managed = False
        db_table = 'ICD_KATALOG'


class IcdThesaurus(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    icd_kode = models.CharField(db_column='ICD_KODE', max_length=6, blank=True, null=True)
    thesaurus = models.CharField(db_column='THESAURUS', max_length=255, blank=True, null=True)
    hausarztkodierung = models.CharField(db_column='HAUSARZTKODIERUNG', max_length=5, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ICD_THESAURUS'


class Impfbuch(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    ermaechtigung_id = models.IntegerField(db_column='ERMAECHTIGUNG_ID')
    patient_id = models.IntegerField(db_column='PATIENT_ID')
    untersuchung_id = models.IntegerField(db_column='UNTERSUCHUNG_ID', blank=True, null=True)
    typ = models.IntegerField(db_column='TYP')
    subtyp = models.IntegerField(db_column='SUBTYP')
    datum = models.DateField(db_column='DATUM', blank=True, null=True)
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)
    ergebnis = models.IntegerField(db_column='ERGEBNIS')
    bemerkung = models.TextField(db_column='BEMERKUNG', blank=True, null=True)
    freitext1 = models.TextField(db_column='FREITEXT1', blank=True, null=True)
    freitext2 = models.TextField(db_column='FREITEXT2', blank=True, null=True)
    status = models.IntegerField(db_column='STATUS')
    system_datum = models.DateField(db_column='SYSTEM_DATUM', blank=True, null=True)
    system_uhrzeit = models.TimeField(db_column='SYSTEM_UHRZEIT', blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)
    chargennr = models.CharField(db_column='CHARGENNR', max_length=255, blank=True, null=True)
    impfart = models.IntegerField(db_column='IMPFART')
    impf_kennung = models.IntegerField(db_column='IMPF_KENNUNG')
    impftyp = models.IntegerField(db_column='IMPFTYP')
    impfstoff = models.CharField(db_column='IMPFSTOFF', max_length=255, blank=True, null=True)
    hersteller = models.CharField(db_column='HERSTELLER', max_length=255, blank=True, null=True)
    dosierung = models.CharField(db_column='DOSIERUNG', max_length=255, blank=True, null=True)
    dosierung2 = models.CharField(db_column='DOSIERUNG2', max_length=255, blank=True, null=True)
    impf_dokuid = models.IntegerField(db_column='IMPF_DOKUID', blank=True, null=True)
    impfer = models.CharField(db_column='IMPFER', max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'IMPFBUCH'


class Import(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    altsystem_id = models.BigIntegerField(db_column='ALTSYSTEM_ID', blank=True, null=True)
    text = models.TextField(db_column='TEXT', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'IMPORT'


class Import2(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    altsystem_id = models.BigIntegerField(db_column='ALTSYSTEM_ID', blank=True, null=True)
    datum = models.DateField(db_column='DATUM', blank=True, null=True)
    text = models.TextField(db_column='TEXT', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'IMPORT2'


class IvfEmbryokryo(models.Model):
    embryokryo_dokuid = models.IntegerField(db_column='EMBRYOKRYO_DOKUID', unique=True, blank=True, null=True)
    kommentar = models.TextField(db_column='KOMMENTAR', blank=True, null=True)
    labor = models.CharField(db_column='LABOR', max_length=255, blank=True, null=True)
    externekryo = models.IntegerField(db_column='EXTERNEKRYO', blank=True, null=True)
    invitro_zyklus_id = models.IntegerField(db_column='INVITRO_ZYKLUS_ID', blank=True, null=True)
    embryonen_anzahl = models.IntegerField(db_column='EMBRYONEN_ANZAHL', blank=True, null=True)
    embryonen_qualitaet = models.CharField(db_column='EMBRYONEN_QUALITAET', max_length=255, blank=True, null=True)
    befund = models.CharField(db_column='BEFUND', max_length=255, blank=True, null=True)
    befund_sonstiges = models.CharField(db_column='BEFUND_SONSTIGES', max_length=255, blank=True, null=True)
    lager_typ = models.IntegerField(db_column='LAGER_TYP', blank=True, null=True)
    lager_id = models.CharField(db_column='LAGER_ID', max_length=255, blank=True, null=True)
    lager_code = models.CharField(db_column='LAGER_CODE', max_length=255, blank=True, null=True)
    lager_behaelter = models.IntegerField(db_column='LAGER_BEHAELTER', blank=True, null=True)
    lager_position = models.CharField(db_column='LAGER_POSITION', max_length=255, blank=True, null=True)
    tag = models.IntegerField(db_column='TAG', blank=True, null=True)
    lager_anzahl = models.CharField(db_column='LAGER_ANZAHL', max_length=100, blank=True, null=True)
    lager_typfarbe = models.CharField(db_column='LAGER_TYPFARBE', max_length=100, blank=True, null=True)
    lager_positionfarbe = models.CharField(db_column='LAGER_POSITIONFARBE', max_length=100, blank=True, null=True)
    methode_gefrieren_pbs_min = models.IntegerField(db_column='METHODE_GEFRIEREN_PBS_MIN', blank=True, null=True)
    methode_gefrieren_pbs_sek = models.IntegerField(db_column='METHODE_GEFRIEREN_PBS_SEK', blank=True, null=True)
    methode_gefrieren_55_min = models.IntegerField(db_column='METHODE_GEFRIEREN_55_MIN', blank=True, null=True)
    methode_gefrieren_55_sek = models.IntegerField(db_column='METHODE_GEFRIEREN_55_SEK', blank=True, null=True)
    methode_gefrieren_1010_min = models.IntegerField(db_column='METHODE_GEFRIEREN_1010_MIN', blank=True, null=True)
    methode_gefrieren_1010_sek = models.IntegerField(db_column='METHODE_GEFRIEREN_1010_SEK', blank=True, null=True)
    methode_gefrieren_1515_min = models.IntegerField(db_column='METHODE_GEFRIEREN_1515_MIN', blank=True, null=True)
    methode_gefrieren_1515_sek = models.IntegerField(db_column='METHODE_GEFRIEREN_1515_SEK', blank=True, null=True)
    methode_gefrieren_2020_min = models.IntegerField(db_column='METHODE_GEFRIEREN_2020_MIN', blank=True, null=True)
    methode_gefrieren_2020_sek = models.IntegerField(db_column='METHODE_GEFRIEREN_2020_SEK', blank=True, null=True)
    methode_auftauen_suc1m_min = models.IntegerField(db_column='METHODE_AUFTAUEN_SUC1M_MIN', blank=True, null=True)
    methode_auftauen_suc1m_sek = models.IntegerField(db_column='METHODE_AUFTAUEN_SUC1M_SEK', blank=True, null=True)
    methode_auftauen_suc05m_min = models.IntegerField(db_column='METHODE_AUFTAUEN_SUC05M_MIN', blank=True, null=True)
    methode_auftauen_suc05m_sek = models.IntegerField(db_column='METHODE_AUFTAUEN_SUC05M_SEK', blank=True, null=True)
    methode_auftauen_suc025m_min = models.IntegerField(db_column='METHODE_AUFTAUEN_SUC025M_MIN', blank=True, null=True)
    methode_auftauen_suc025m_sek = models.IntegerField(db_column='METHODE_AUFTAUEN_SUC025M_SEK', blank=True, null=True)
    methode_auftauen_pbs_min = models.IntegerField(db_column='METHODE_AUFTAUEN_PBS_MIN', blank=True, null=True)
    methode_auftauen_pbs_sek = models.IntegerField(db_column='METHODE_AUFTAUEN_PBS_SEK', blank=True, null=True)
    methode_auftauen_temperatur = models.IntegerField(db_column='METHODE_AUFTAUEN_TEMPERATUR', blank=True, null=True)
    methode_auftauen_suc0125m_min = models.IntegerField(db_column='METHODE_AUFTAUEN_SUC0125M_MIN', blank=True, null=True)
    methode_auftauen_suc0125m_sek = models.IntegerField(db_column='METHODE_AUFTAUEN_SUC0125M_SEK', blank=True, null=True)
    methode_auftauen_laserhatching = models.IntegerField(db_column='METHODE_AUFTAUEN_LASERHATCHING', blank=True, null=True)
    methode_gefrieren_temperatur = models.IntegerField(db_column='METHODE_GEFRIEREN_TEMPERATUR', blank=True, null=True)
    kryo_abgeschlossen = models.IntegerField(db_column='KRYO_ABGESCHLOSSEN', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'IVF_EMBRYOKRYO'


class IvfEmbryotransfer(models.Model):
    embryotransfer_dokuid = models.IntegerField(db_column='EMBRYOTRANSFER_DOKUID', unique=True, blank=True, null=True)
    kommentar = models.TextField(db_column='KOMMENTAR', blank=True, null=True)
    invitro_zyklus_id = models.IntegerField(db_column='INVITRO_ZYKLUS_ID', blank=True, null=True)
    tag = models.IntegerField(db_column='TAG', blank=True, null=True)
    labor = models.CharField(db_column='LABOR', max_length=255, blank=True, null=True)
    embryonen_anz = models.IntegerField(db_column='EMBRYONEN_ANZ', blank=True, null=True)
    embryonen_typ = models.IntegerField(db_column='EMBRYONEN_TYP', blank=True, null=True)
    embryonen_qualitaet = models.CharField(db_column='EMBRYONEN_QUALITAET', max_length=255, blank=True, null=True)
    intern = models.TextField(db_column='INTERN', blank=True, null=True)
    sondenlaenge = models.CharField(db_column='SONDENLAENGE', max_length=50, blank=True, null=True)
    position_uterus = models.IntegerField(db_column='POSITION_UTERUS', blank=True, null=True)
    schwierigkeit = models.IntegerField(db_column='SCHWIERIGKEIT', blank=True, null=True)
    transferset = models.IntegerField(db_column='TRANSFERSET', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'IVF_EMBRYOTRANSFER'


class IvfInvitroZyklus(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    patient_id = models.IntegerField(db_column='PATIENT_ID', blank=True, null=True)
    datum = models.DateField(db_column='DATUM', blank=True, null=True)
    mandant_id = models.IntegerField(db_column='MANDANT_ID', blank=True, null=True)
    bearbeiter_id = models.IntegerField(db_column='BEARBEITER_ID', blank=True, null=True)
    arzt_id = models.IntegerField(db_column='ARZT_ID', blank=True, null=True)
    start_down_reg = models.DateField(db_column='START_DOWN_REG', blank=True, null=True)
    down_reg_bemerkung = models.CharField(db_column='DOWN_REG_BEMERKUNG', max_length=255, blank=True, null=True)
    letzte_menstruation = models.DateField(db_column='LETZTE_MENSTRUATION', blank=True, null=True)
    start_stimulation = models.DateField(db_column='START_STIMULATION', blank=True, null=True)
    ende_stimulation = models.DateField(db_column='ENDE_STIMULATION', blank=True, null=True)
    ultraschall_1 = models.DateField(db_column='ULTRASCHALL_1', blank=True, null=True)
    ultraschall_imhaus = models.IntegerField(db_column='ULTRASCHALL_IMHAUS', blank=True, null=True)
    aktivierung_datum = models.DateField(db_column='AKTIVIERUNG_DATUM', blank=True, null=True)
    aktivierung_uhrzeit = models.TimeField(db_column='AKTIVIERUNG_UHRZEIT', blank=True, null=True)
    punktion_datum = models.DateField(db_column='PUNKTION_DATUM', blank=True, null=True)
    punktion_uhrzeit = models.TimeField(db_column='PUNKTION_UHRZEIT', blank=True, null=True)
    punktion_imhaus = models.IntegerField(db_column='PUNKTION_IMHAUS', blank=True, null=True)
    behandlungen = models.CharField(db_column='BEHANDLUNGEN', max_length=255, blank=True, null=True)
    bemerkungen = models.TextField(db_column='BEMERKUNGEN', blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    therapieplan_titel = models.CharField(db_column='THERAPIEPLAN_TITEL', max_length=255, blank=True, null=True)
    therapieplan_nummer = models.IntegerField(db_column='THERAPIEPLAN_NUMMER', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'IVF_INVITRO_ZYKLUS'


class IvfInvitroZyklusTabelleneintraege(models.Model):
    zyklus_id = models.IntegerField(db_column='ZYKLUS_ID', blank=True, null=True)
    name = models.CharField(db_column='NAME', max_length=255, blank=True, null=True)
    us_seite = models.CharField(db_column='US_SEITE', max_length=1, blank=True, null=True)
    datum_string = models.CharField(db_column='DATUM_STRING', max_length=100, blank=True, null=True)
    wert = models.CharField(db_column='WERT', max_length=255, blank=True, null=True)
    typ = models.CharField(db_column='TYP', max_length=3, blank=True, null=True)
    zeilenposition = models.IntegerField(db_column='ZEILENPOSITION', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'IVF_INVITRO_ZYKLUS_TABELLENEINTRAEGE'


class IvfKultur(models.Model):
    kultur_dokuid = models.IntegerField(db_column='KULTUR_DOKUID', unique=True, blank=True, null=True)
    kommentar = models.TextField(db_column='KOMMENTAR', blank=True, null=True)
    invitro_zyklus_id = models.IntegerField(db_column='INVITRO_ZYKLUS_ID', blank=True, null=True)
    punktiondoku_id = models.IntegerField(db_column='PUNKTIONDOKU_ID', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'IVF_KULTUR'


class IvfKulturDaten(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    kultur_dokuid = models.IntegerField(db_column='KULTUR_DOKUID', blank=True, null=True)
    eizelle_nummer = models.IntegerField(db_column='EIZELLE_NUMMER', blank=True, null=True)
    tag0_datum = models.DateField(db_column='TAG0_DATUM', blank=True, null=True)
    tag0_uhrzeit = models.TimeField(db_column='TAG0_UHRZEIT', blank=True, null=True)
    tag0_daten = models.TextField(db_column='TAG0_DATEN', blank=True, null=True)
    tag1_datum = models.DateField(db_column='TAG1_DATUM', blank=True, null=True)
    tag1_uhrzeit = models.TimeField(db_column='TAG1_UHRZEIT', blank=True, null=True)
    tag1_daten = models.TextField(db_column='TAG1_DATEN', blank=True, null=True)
    tag2_datum = models.DateField(db_column='TAG2_DATUM', blank=True, null=True)
    tag2_uhrzeit = models.TimeField(db_column='TAG2_UHRZEIT', blank=True, null=True)
    tag2_daten = models.TextField(db_column='TAG2_DATEN', blank=True, null=True)
    tag3_datum = models.DateField(db_column='TAG3_DATUM', blank=True, null=True)
    tag3_uhrzeit = models.TimeField(db_column='TAG3_UHRZEIT', blank=True, null=True)
    tag3_daten = models.TextField(db_column='TAG3_DATEN', blank=True, null=True)
    tag4_datum = models.DateField(db_column='TAG4_DATUM', blank=True, null=True)
    tag4_uhrzeit = models.TimeField(db_column='TAG4_UHRZEIT', blank=True, null=True)
    tag4_daten = models.TextField(db_column='TAG4_DATEN', blank=True, null=True)
    tag5_datum = models.DateField(db_column='TAG5_DATUM', blank=True, null=True)
    tag5_uhrzeit = models.TimeField(db_column='TAG5_UHRZEIT', blank=True, null=True)
    tag5_daten = models.TextField(db_column='TAG5_DATEN', blank=True, null=True)
    tag6_datum = models.DateField(db_column='TAG6_DATUM', blank=True, null=True)
    tag6_uhrzeit = models.TimeField(db_column='TAG6_UHRZEIT', blank=True, null=True)
    tag6_daten = models.TextField(db_column='TAG6_DATEN', blank=True, null=True)
    endablauf_transfer = models.IntegerField(db_column='ENDABLAUF_TRANSFER', blank=True, null=True)
    endablauf_hatch = models.IntegerField(db_column='ENDABLAUF_HATCH', blank=True, null=True)
    endablauf_kryo = models.IntegerField(db_column='ENDABLAUF_KRYO', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'IVF_KULTUR_DATEN'
        unique_together = (('kultur_dokuid', 'eizelle_nummer'),)


class IvfPunktion(models.Model):
    punktion_dokuid = models.IntegerField(db_column='PUNKTION_DOKUID', unique=True)
    kommentar = models.TextField(db_column='KOMMENTAR', blank=True, null=True)
    labor = models.CharField(db_column='LABOR', max_length=255, blank=True, null=True)
    punktionen = models.TextField(db_column='PUNKTIONEN', blank=True, null=True)
    invitro_zyklus_id = models.IntegerField(db_column='INVITRO_ZYKLUS_ID', blank=True, null=True)
    hyaluronidase = models.IntegerField(db_column='HYALURONIDASE', blank=True, null=True)
    uhrzeit = models.TimeField(db_column='UHRZEIT', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'IVF_PUNKTION'


class IvfSamenkryo(models.Model):
    samenkryo_dokuid = models.IntegerField(db_column='SAMENKRYO_DOKUID', unique=True, blank=True, null=True)
    kommentar = models.TextField(db_column='KOMMENTAR', blank=True, null=True)
    labor = models.CharField(db_column='LABOR', max_length=255, blank=True, null=True)
    externekryo = models.IntegerField(db_column='EXTERNEKRYO', blank=True, null=True)
    invitro_zyklus_id = models.IntegerField(db_column='INVITRO_ZYKLUS_ID', blank=True, null=True)
    befund = models.CharField(db_column='BEFUND', max_length=255, blank=True, null=True)
    befund_sonstiges = models.CharField(db_column='BEFUND_SONSTIGES', max_length=255, blank=True, null=True)
    lager_typ = models.IntegerField(db_column='LAGER_TYP', blank=True, null=True)
    lager_id = models.CharField(db_column='LAGER_ID', max_length=255, blank=True, null=True)
    lager_code = models.CharField(db_column='LAGER_CODE', max_length=255, blank=True, null=True)
    lager_behaelter = models.IntegerField(db_column='LAGER_BEHAELTER', blank=True, null=True)
    lager_position = models.CharField(db_column='LAGER_POSITION', max_length=255, blank=True, null=True)
    lager_anzahl = models.CharField(db_column='LAGER_ANZAHL', max_length=100, blank=True, null=True)
    lager_typfarbe = models.CharField(db_column='LAGER_TYPFARBE', max_length=100, blank=True, null=True)
    lager_positionfarbe = models.CharField(db_column='LAGER_POSITIONFARBE', max_length=100, blank=True, null=True)
    methode = models.CharField(db_column='METHODE', max_length=255, blank=True, null=True)
    methode_kommentar = models.TextField(db_column='METHODE_KOMMENTAR', blank=True, null=True)
    spermiogramm_id = models.IntegerField(db_column='SPERMIOGRAMM_ID', blank=True, null=True)
    tese = models.IntegerField(db_column='TESE', blank=True, null=True)
    tese_50er = models.CharField(db_column='TESE_50ER', max_length=255, blank=True, null=True)
    tese_90er = models.CharField(db_column='TESE_90ER', max_length=255, blank=True, null=True)
    tese_70er = models.CharField(db_column='TESE_70ER', max_length=255, blank=True, null=True)
    tese_ergebnisse = models.TextField(db_column='TESE_ERGEBNISSE', blank=True, null=True)
    tese_ueberstand = models.CharField(db_column='TESE_UEBERSTAND', max_length=255, blank=True, null=True)
    kryo_abgeschlossen = models.IntegerField(db_column='KRYO_ABGESCHLOSSEN', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'IVF_SAMENKRYO'


class IvfSpermiogramm(models.Model):
    spermiogramm_dokuid = models.IntegerField(db_column='SPERMIOGRAMM_DOKUID', unique=True, blank=True, null=True)
    kommentar = models.TextField(db_column='KOMMENTAR', blank=True, null=True)
    invitro_zyklus_id = models.IntegerField(db_column='INVITRO_ZYKLUS_ID', blank=True, null=True)
    labor = models.CharField(db_column='LABOR', max_length=255, blank=True, null=True)
    spermiengewinnung = models.IntegerField(db_column='SPERMIENGEWINNUNG', blank=True, null=True)
    spermiengewinnung_sontiges = models.TextField(db_column='SPERMIENGEWINNUNG_SONTIGES', blank=True, null=True)
    samenpraeparationfuer = models.IntegerField(db_column='SAMENPRAEPARATIONFUER', blank=True, null=True)
    number_3gradientensperma = models.CharField(db_column='3GRADIENTENSPERMA', max_length=100, blank=True, null=True)
    generation = models.CharField(db_column='GENERATION', max_length=255, blank=True, null=True)
    tese_obstruktion = models.IntegerField(db_column='TESE_OBSTRUKTION', blank=True, null=True)
    tese_ergebnis = models.TextField(db_column='TESE_ERGEBNIS', blank=True, null=True)
    samen_volumen = models.CharField(db_column='SAMEN_VOLUMEN', max_length=255, blank=True, null=True)
    samen_viskositaet = models.IntegerField(db_column='SAMEN_VISKOSITAET', blank=True, null=True)
    samen_verfluessigen = models.CharField(db_column='SAMEN_VERFLUESSIGEN', max_length=255, blank=True, null=True)
    samen_konzentration = models.CharField(db_column='SAMEN_KONZENTRATION', max_length=255, blank=True, null=True)
    samen_konzentration_einheit = models.CharField(db_column='SAMEN_KONZENTRATION_EINHEIT', max_length=100, blank=True, null=True)
    samen_totalemotilitaetprozent = models.CharField(db_column='SAMEN_TOTALEMOTILITAETPROZENT', max_length=50, blank=True, null=True)
    samen_schnellprogressivprozent = models.CharField(db_column='SAMEN_SCHNELLPROGRESSIVPROZENT', max_length=50, blank=True, null=True)
    samen_langsamprogressivprozent = models.CharField(db_column='SAMEN_LANGSAMPROGRESSIVPROZENT', max_length=50, blank=True, null=True)
    samen_beweglichprozent = models.CharField(db_column='SAMEN_BEWEGLICHPROZENT', max_length=50, blank=True, null=True)
    samen_unbeweglichprozent = models.CharField(db_column='SAMEN_UNBEWEGLICHPROZENT', max_length=50, blank=True, null=True)
    samen_pathologischprozent = models.CharField(db_column='SAMEN_PATHOLOGISCHPROZENT', max_length=50, blank=True, null=True)
    samen_rundzellenleukosanzahl = models.CharField(db_column='SAMEN_RUNDZELLENLEUKOSANZAHL', max_length=100, blank=True, null=True)
    samen_agglutination = models.IntegerField(db_column='SAMEN_AGGLUTINATION', blank=True, null=True)
    tese_seite = models.CharField(db_column='TESE_SEITE', max_length=50, blank=True, null=True)
    imsi_klasse1prozent = models.CharField(db_column='IMSI_KLASSE1PROZENT', max_length=50, blank=True, null=True)
    imsi_klasse2prozent = models.CharField(db_column='IMSI_KLASSE2PROZENT', max_length=50, blank=True, null=True)
    imsi_klasse3prozent = models.CharField(db_column='IMSI_KLASSE3PROZENT', max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'IVF_SPERMIOGRAMM'


class Kalenderstamm(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    name = models.CharField(db_column='NAME', max_length=50, blank=True, null=True)
    beschreibung = models.CharField(db_column='BESCHREIBUNG', max_length=255, blank=True, null=True)
    kennung = models.IntegerField(db_column='KENNUNG', unique=True, blank=True, null=True)
    zusatz_1 = models.TextField(db_column='ZUSATZ_1', blank=True, null=True)
    zusatz_2 = models.TextField(db_column='ZUSATZ_2', blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'KALENDERSTAMM'


class Kalendervorlage(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    kalender_kennung = models.IntegerField(db_column='KALENDER_KENNUNG')
    vorlagen_kennung = models.IntegerField(db_column='VORLAGEN_KENNUNG')
    gueltig_ab = models.DateField(db_column='GUELTIG_AB', blank=True, null=True)
    gueltig_bis = models.DateField(db_column='GUELTIG_BIS', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'KALENDERVORLAGE'


class KatalogIcd(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    typ = models.CharField(db_column='TYP', max_length=255, blank=True, null=True)
    typ_1 = models.CharField(db_column='TYP_1', max_length=255, blank=True, null=True)
    typ_2 = models.CharField(db_column='TYP_2', max_length=255, blank=True, null=True)
    ziffer = models.CharField(db_column='ZIFFER', max_length=255, blank=True, null=True)
    ziffer_text_1 = models.CharField(db_column='ZIFFER_TEXT_1', max_length=255, blank=True, null=True)
    ziffer_text_2 = models.CharField(db_column='ZIFFER_TEXT_2', max_length=255, blank=True, null=True)
    ziffer_text_3 = models.CharField(db_column='ZIFFER_TEXT_3', max_length=255, blank=True, null=True)
    ziffer_text = models.CharField(db_column='ZIFFER_TEXT', max_length=255, blank=True, null=True)
    ziffer_text_kurz = models.CharField(db_column='ZIFFER_TEXT_KURZ', max_length=255, blank=True, null=True)
    punkte = models.CharField(db_column='PUNKTE', max_length=255, blank=True, null=True)
    gruppe = models.CharField(db_column='GRUPPE', max_length=255, blank=True, null=True)
    kuerzel = models.CharField(db_column='KUERZEL', max_length=255, blank=True, null=True)
    notizen = models.CharField(db_column='NOTIZEN', max_length=255, blank=True, null=True)
    protokoll = models.CharField(db_column='PROTOKOLL', max_length=255, blank=True, null=True)
    status = models.CharField(db_column='STATUS', max_length=255, blank=True, null=True)
    ziffer_von = models.CharField(db_column='ZIFFER_VON', max_length=255, blank=True, null=True)
    ziffer_bis = models.CharField(db_column='ZIFFER_BIS', max_length=255, blank=True, null=True)
    betrag = models.CharField(db_column='BETRAG', max_length=255, blank=True, null=True)
    waehrung = models.CharField(db_column='WAEHRUNG', max_length=255, blank=True, null=True)
    version = models.CharField(db_column='VERSION', max_length=255, blank=True, null=True)
    zeitstempel = models.CharField(db_column='ZEITSTEMPEL', max_length=255, blank=True, null=True)
    lock = models.CharField(db_column='LOCK', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'KATALOG_ICD'


class KatalogKapitel(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    typ = models.IntegerField(db_column='TYP')
    index = models.CharField(db_column='INDEX', max_length=15, blank=True, null=True)
    bereich = models.CharField(db_column='BEREICH', max_length=15, blank=True, null=True)
    kapitel = models.CharField(db_column='KAPITEL', max_length=15, blank=True, null=True)
    abschnitt = models.CharField(db_column='ABSCHNITT', max_length=15, blank=True, null=True)
    uabschnitt = models.CharField(db_column='UABSCHNITT', max_length=15, blank=True, null=True)
    block = models.CharField(db_column='BLOCK', max_length=15, blank=True, null=True)
    titel = models.CharField(db_column='TITEL', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'KATALOG_KAPITEL'
        unique_together = (('index', 'typ'),)


class Kontraindikationenauftrag(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    anamnesefragenstamm_id = models.IntegerField(db_column='ANAMNESEFRAGENSTAMM_ID')
    auftragstamm_id = models.IntegerField(db_column='AUFTRAGSTAMM_ID')

    class Meta:
        managed = False
        db_table = 'KONTRAINDIKATIONENAUFTRAG'
        unique_together = (('anamnesefragenstamm_id', 'auftragstamm_id'),)


class Kreislauf(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    patient_id = models.IntegerField(db_column='PATIENT_ID')
    typ = models.CharField(db_column='TYP', max_length=50)
    datum = models.DateField(db_column='DATUM', blank=True, null=True)
    systole = models.FloatField(db_column='SYSTOLE', blank=True, null=True)
    diastole = models.FloatField(db_column='DIASTOLE', blank=True, null=True)
    puls = models.FloatField(db_column='PULS', blank=True, null=True)
    bemerkung = models.TextField(db_column='BEMERKUNG', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'KREISLAUF'


class Kultur(models.Model):
    kultur_dokuid = models.IntegerField(db_column='KULTUR_DOKUID', unique=True, blank=True, null=True)
    kommentar = models.TextField(db_column='KOMMENTAR', blank=True, null=True)
    invitro_zyklus_id = models.IntegerField(db_column='INVITRO_ZYKLUS_ID', blank=True, null=True)
    tag0_datum = models.DateField(db_column='TAG0_DATUM', blank=True, null=True)
    tag0_uhrzeit = models.TimeField(db_column='TAG0_UHRZEIT', blank=True, null=True)
    tag0_daten = models.TextField(db_column='TAG0_DATEN', blank=True, null=True)
    tag1_datum = models.DateField(db_column='TAG1_DATUM', blank=True, null=True)
    tag1_uhrzeit = models.TimeField(db_column='TAG1_UHRZEIT', blank=True, null=True)
    tag1_daten = models.TextField(db_column='TAG1_DATEN', blank=True, null=True)
    tag2_datum = models.DateField(db_column='TAG2_DATUM', blank=True, null=True)
    tag2_uhrzeit = models.TimeField(db_column='TAG2_UHRZEIT', blank=True, null=True)
    tag2_daten = models.TextField(db_column='TAG2_DATEN', blank=True, null=True)
    tag3_datum = models.DateField(db_column='TAG3_DATUM', blank=True, null=True)
    tag3_uhrzeit = models.TimeField(db_column='TAG3_UHRZEIT', blank=True, null=True)
    tag3_daten = models.TextField(db_column='TAG3_DATEN', blank=True, null=True)
    tag4_datum = models.DateField(db_column='TAG4_DATUM', blank=True, null=True)
    tag4_uhrzeit = models.TimeField(db_column='TAG4_UHRZEIT', blank=True, null=True)
    tag4_daten = models.TextField(db_column='TAG4_DATEN', blank=True, null=True)
    tag5_datum = models.DateField(db_column='TAG5_DATUM', blank=True, null=True)
    tag5_uhrzeit = models.TimeField(db_column='TAG5_UHRZEIT', blank=True, null=True)
    tag5_daten = models.TextField(db_column='TAG5_DATEN', blank=True, null=True)
    tag6_datum = models.DateField(db_column='TAG6_DATUM', blank=True, null=True)
    tag6_uhrzeit = models.TimeField(db_column='TAG6_UHRZEIT', blank=True, null=True)
    tag6_daten = models.TextField(db_column='TAG6_DATEN', blank=True, null=True)
    endablauf_transfer = models.IntegerField(db_column='ENDABLAUF_TRANSFER', blank=True, null=True)
    endablauf_hatch = models.IntegerField(db_column='ENDABLAUF_HATCH', blank=True, null=True)
    endablauf_kryo = models.IntegerField(db_column='ENDABLAUF_KRYO', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'KULTUR'


class Laborauftrag(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    laborauftragdoku_id = models.IntegerField(db_column='LABORAUFTRAGDOKU_ID', unique=True)
    datum_versendet = models.DateField(db_column='DATUM_VERSENDET', blank=True, null=True)
    status = models.IntegerField(db_column='STATUS')
    diagnose = models.CharField(db_column='DIAGNOSE', max_length=255, blank=True, null=True)
    kommentar = models.CharField(db_column='KOMMENTAR', max_length=255, blank=True, null=True)
    auftragsnummer = models.IntegerField(db_column='AUFTRAGSNUMMER', unique=True, blank=True, null=True)
    probenentnahme_datum = models.DateField(db_column='PROBENENTNAHME_DATUM', blank=True, null=True)
    etiketten_zaehler = models.IntegerField(db_column='ETIKETTEN_ZAEHLER', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'LABORAUFTRAG'


class Laborauftragwert(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    laborauftragdoku_id = models.IntegerField(db_column='LABORAUFTRAGDOKU_ID', blank=True, null=True)
    code_im_labor = models.CharField(db_column='CODE_IM_LABOR', max_length=255, blank=True, null=True)
    laborauftrag_zuweiser_id = models.IntegerField(db_column='LABORAUFTRAG_ZUWEISER_ID', blank=True, null=True)
    referenz_dokuid = models.IntegerField(db_column='REFERENZ_DOKUID', blank=True, null=True)
    nachforderung = models.IntegerField(db_column='NACHFORDERUNG', blank=True, null=True)
    kuerzel = models.CharField(db_column='KUERZEL', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'LABORAUFTRAGWERT'


class Laborauftragwertstamm(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    code_im_labor = models.CharField(db_column='CODE_IM_LABOR', max_length=255, blank=True, null=True)
    kuerzel = models.CharField(db_column='KUERZEL', max_length=255, blank=True, null=True)
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)
    typ = models.CharField(db_column='TYP', max_length=255, blank=True, null=True)
    spezimen = models.CharField(db_column='SPEZIMEN', max_length=255, blank=True, null=True)
    geb_kuerzel = models.CharField(db_column='GEB_KUERZEL', max_length=255, blank=True, null=True)
    bereich = models.CharField(db_column='BEREICH', max_length=255, blank=True, null=True)
    nachforderung = models.IntegerField(db_column='NACHFORDERUNG', blank=True, null=True)
    kasse = models.CharField(db_column='KASSE', max_length=255, blank=True, null=True)
    privattarif = models.FloatField(db_column='PRIVATTARIF', blank=True, null=True)
    flags = models.CharField(db_column='FLAGS', max_length=255, blank=True, null=True)
    mandanten = models.CharField(db_column='MANDANTEN', max_length=255, blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)
    gueltig_ab = models.DateField(db_column='GUELTIG_AB', blank=True, null=True)
    gueltig_bis = models.DateField(db_column='GUELTIG_BIS', blank=True, null=True)
    laborauftrag_zuweiser_id = models.IntegerField(db_column='LABORAUFTRAG_ZUWEISER_ID', blank=True, null=True)
    profil_parameter_ids = models.CharField(db_column='PROFIL_PARAMETER_IDS', max_length=500, blank=True, null=True)
    geb_bezeichnung = models.CharField(db_column='GEB_BEZEICHNUNG', max_length=255, blank=True, null=True)
    verrech_diagnosen = models.CharField(db_column='VERRECH_DIAGNOSEN', max_length=255, blank=True, null=True)
    prob_inf = models.TextField(db_column='PROB_INF', blank=True, null=True)
    mic_biol = models.IntegerField(db_column='MIC_BIOL', blank=True, null=True)
    zwingend_param = models.CharField(db_column='ZWINGEND_PARAM', max_length=255, blank=True, null=True)
    exclude_param = models.CharField(db_column='EXCLUDE_PARAM', max_length=255, blank=True, null=True)
    app_gem = models.IntegerField(db_column='APP_GEM', blank=True, null=True)
    method_id = models.CharField(db_column='METHOD_ID', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'LABORAUFTRAGWERTSTAMM'
        unique_together = (('code_im_labor', 'laborauftrag_zuweiser_id'), ('code_im_labor', 'laborauftrag_zuweiser_id'),)


class LaborauftragNummern(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    auftragsnummer = models.CharField(db_column='AUFTRAGSNUMMER', max_length=20, blank=True, null=True)
    labor = models.CharField(db_column='LABOR', max_length=100, blank=True, null=True)
    mandant_id = models.IntegerField(db_column='MANDANT_ID', blank=True, null=True)
    gueltig_bis = models.DateField(db_column='GUELTIG_BIS', blank=True, null=True)
    doku_id = models.IntegerField(db_column='DOKU_ID', unique=True, blank=True, null=True)
    status = models.IntegerField(db_column='STATUS')
    jahr = models.IntegerField(db_column='JAHR', blank=True, null=True)
    nummern_typ = models.IntegerField(db_column='NUMMERN_TYP', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'LABORAUFTRAG_NUMMERN'
        unique_together = (('auftragsnummer', 'labor', 'jahr'),)


class Laborbefund(models.Model):
    doku_id = models.IntegerField(db_column='DOKU_ID', unique=True, primary_key=True)
    senden = models.IntegerField(db_column='SENDEN', blank=True, null=True)
    sende_status = models.IntegerField(db_column='SENDE_STATUS')
    sende_zeit = models.TimeField(db_column='SENDE_ZEIT', blank=True, null=True)
    sende_empfaenger = models.CharField(db_column='SENDE_EMPFAENGER', max_length=255, blank=True, null=True)
    senden_auftragsnummer = models.CharField(db_column='SENDEN_AUFTRAGSNUMMER', max_length=255, blank=True, null=True)
    empfangen = models.IntegerField(db_column='EMPFANGEN', blank=True, null=True)
    empfangen_status = models.IntegerField(db_column='EMPFANGEN_STATUS', blank=True, null=True)
    empfangen_zeit = models.DateTimeField(db_column='EMPFANGEN_ZEIT', blank=True, null=True)
    empfangen_absender = models.CharField(db_column='EMPFANGEN_ABSENDER', max_length=100, blank=True, null=True)
    empfangen_auftragsnummer = models.CharField(db_column='EMPFANGEN_AUFTRAGSNUMMER', max_length=50, blank=True, null=True)
    auftragsinformation = models.TextField(db_column='AUFTRAGSINFORMATION', blank=True, null=True)
    laborauftrag_dokuid = models.IntegerField(db_column='LABORAUFTRAG_DOKUID', blank=True, null=True)
    laborbefund_typ = models.IntegerField(db_column='LABORBEFUND_TYP', blank=True, null=True)
    laborbefund_art = models.CharField(db_column='LABORBEFUND_ART', max_length=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'LABORBEFUND'
        unique_together = (('empfangen_absender', 'empfangen_auftragsnummer', 'laborbefund_typ', 'laborbefund_art'),)
        verbose_name_plural = "Laborbefunde"


class Labornorm(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    laborstamm_id = models.ForeignKey("Laborstamm", db_column='LABORSTAMM_ID', blank=True, null=True, on_delete=models.PROTECT)
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)

    # 0,1,2
    geschlecht = models.IntegerField(db_column='GESCHLECHT', blank=True, null=True)
    alter_von = models.IntegerField(db_column='ALTER_VON', blank=True, null=True)
    alter_bis = models.IntegerField(db_column='ALTER_BIS', blank=True, null=True)
    alter_einheit = models.IntegerField(db_column='ALTER_EINHEIT')
    normbereich_unten = models.FloatField(db_column='NORMBEREICH_UNTEN', blank=True, null=True)
    normbereich_oben = models.FloatField(db_column='NORMBEREICH_OBEN', blank=True, null=True)
    normbereich = models.CharField(db_column='NORMBEREICH', max_length=50, blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'LABORNORM'
        verbose_name_plural = "Labornormen"

    def __str__(self):
        return "Alter: {} bis {} {}, {}".format(
            self.alter_von,
            self.alter_bis,
            self.alter_einheit,
            self.geschlecht)


class Laborstamm(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    einheit = models.CharField(db_column='EINHEIT', max_length=255, blank=True, null=True)
    kuerzel = models.CharField(db_column='KUERZEL', unique=True, max_length=255, blank=True, null=True)
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)
    gruppe = models.IntegerField(db_column='GRUPPE', blank=True, null=True)
    ermaechtigung = models.CharField(db_column='ERMAECHTIGUNG', max_length=255, blank=True, null=True)
    privat = models.CharField(db_column='PRIVAT', max_length=255, blank=True, null=True)
    kasse = models.CharField(db_column='KASSE', max_length=255, blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    letzte_aenderung_datum = models.DateField(db_column='LETZTE_AENDERUNG_DATUM', blank=True, null=True)
    letzte_aenderung_uhrzeit = models.TimeField(db_column='LETZTE_AENDERUNG_UHRZEIT', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'LABORSTAMM'
        verbose_name_plural = "Laborstammdaten"

    def __str__(self):
        return "{} ({}, {})".format(
            self.kuerzel,
            self.bezeichnung,
            self.einheit)


class Laborwerte(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    # ??? Das verstehe ich nicht. verlinkt das auf Dokumentation, oder Laborbefund?
    laborbefund_id = models.ForeignKey(Laborbefund, to_field="doku_id", db_column='LABORBEFUND_ID', blank=True, null=True, on_delete=models.PROTECT)
    untersuchung_id = models.IntegerField(db_column='UNTERSUCHUNG_ID', blank=True, null=True)
    sortierung = models.IntegerField(db_column='SORTIERUNG')
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)
    warnung = models.CharField(db_column='WARNUNG', max_length=50, blank=True, null=True)
    abweichung = models.CharField(db_column='ABWEICHUNG', max_length=50, blank=True, null=True)
    ergebniswert = models.CharField(db_column='ERGEBNISWERT', max_length=255, blank=True, null=True)
    einheit = models.CharField(db_column='EINHEIT', max_length=50, blank=True, null=True)
    normbereich = models.TextField(db_column='NORMBEREICH', blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)
    gruppe = models.CharField(db_column='GRUPPE', max_length=50, blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)
    testbezeichnung = models.TextField(db_column='TESTBEZEICHNUNG', blank=True, null=True)
    ergebnistext = models.TextField(db_column='ERGEBNISTEXT', blank=True, null=True)
    probenident = models.CharField(db_column='PROBENIDENT', max_length=30, blank=True, null=True)
    probenindex = models.CharField(db_column='PROBENINDEX', max_length=10, blank=True, null=True)
    probenbezeichnung = models.CharField(db_column='PROBENBEZEICHNUNG', max_length=255, blank=True, null=True)
    probenspezifikation = models.CharField(db_column='PROBENSPEZIFIKATION', max_length=255, blank=True, null=True)
    norm_unten = models.CharField(db_column='NORM_UNTEN', max_length=255, blank=True, null=True)
    norm_oben = models.CharField(db_column='NORM_OBEN', max_length=255, blank=True, null=True)
    entnahmedatum = models.DateField(db_column='ENTNAHMEDATUM', blank=True, null=True)
    entnahmeuhrzeit = models.TimeField(db_column='ENTNAHMEUHRZEIT', blank=True, null=True)
    hinweistext = models.TextField(db_column='HINWEISTEXT', blank=True, null=True)
    laborstamm_id = models.IntegerField(db_column='LABORSTAMM_ID', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'LABORWERTE'
        verbose_name_plural = "Laborwerte"

    def __str__(self):
        return "{} {}".format(
            self.ergebniswert,
            self.einheit if self.einheit is not None else ""
            )


class LaborwertstammLaborkuerzelZuordnung(models.Model):
    laborwertstamm_id = models.IntegerField(db_column='LABORWERTSTAMM_ID', blank=True, null=True)
    anforderungslabor_id = models.IntegerField(db_column='ANFORDERUNGSLABOR_ID', blank=True, null=True)
    anforderungslabor_kuerzel = models.CharField(db_column='ANFORDERUNGSLABOR_KUERZEL', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'LABORWERTSTAMM_LABORKUERZEL_ZUORDNUNG'


class Lagerbewegung(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    typ = models.IntegerField(db_column='TYP')
    datum = models.DateField(db_column='DATUM', blank=True, null=True)
    uhrzeit = models.TimeField(db_column='UHRZEIT', blank=True, null=True)
    artikel = models.CharField(db_column='ARTIKEL', max_length=255, blank=True, null=True)
    bewegung_text = models.TextField(db_column='BEWEGUNG_TEXT', blank=True, null=True)
    menge = models.FloatField(db_column='MENGE')
    chargen_nr = models.CharField(db_column='CHARGEN_NR', max_length=255, blank=True, null=True)
    haltbar = models.DateField(db_column='HALTBAR', blank=True, null=True)
    standort = models.CharField(db_column='STANDORT', max_length=255, blank=True, null=True)
    ek_preis = models.FloatField(db_column='EK_PREIS')
    vk_preis = models.FloatField(db_column='VK_PREIS')
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)
    system_zeit = models.DateTimeField(db_column='SYSTEM_ZEIT', blank=True, null=True)
    ziffer_id = models.IntegerField(db_column='ZIFFER_ID', blank=True, null=True)
    ek_waehrung = models.CharField(db_column='EK_WAEHRUNG', max_length=255)
    vk_waehrung = models.CharField(db_column='VK_WAEHRUNG', max_length=255)

    class Meta:
        managed = False
        db_table = 'LAGERBEWEGUNG'


class Lagerstamm(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    kuerzel = models.CharField(db_column='KUERZEL', unique=True, max_length=255, blank=True, null=True)
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)
    gruppe = models.IntegerField(db_column='GRUPPE', blank=True, null=True)
    ermaechtigungen = models.CharField(db_column='ERMAECHTIGUNGEN', max_length=255, blank=True, null=True)
    privat = models.CharField(db_column='PRIVAT', max_length=255, blank=True, null=True)
    kasse = models.CharField(db_column='KASSE', max_length=255, blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)
    artikel_nr = models.CharField(db_column='ARTIKEL_NR', max_length=50, blank=True, null=True)
    einheit = models.CharField(db_column='EINHEIT', max_length=255, blank=True, null=True)
    mindestmenge = models.FloatField(db_column='MINDESTMENGE')
    lieferant = models.IntegerField(db_column='LIEFERANT', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'LAGERSTAMM'


class Leistung(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    leistungsdoku_id = models.IntegerField(db_column='LEISTUNGSDOKU_ID')
    auftragstamm_id = models.IntegerField(db_column='AUFTRAGSTAMM_ID')
    anzahl = models.IntegerField(db_column='ANZAHL')
    datum = models.DateField(db_column='DATUM', blank=True, null=True)
    uhrzeit = models.TimeField(db_column='UHRZEIT', blank=True, null=True)
    vertragsart = models.IntegerField(db_column='VERTRAGSART', blank=True, null=True)
    privatpreis = models.FloatField(db_column='PRIVATPREIS')
    auftrag_einzelleistungen_id = models.IntegerField(db_column='AUFTRAG_EINZELLEISTUNGEN_ID', blank=True, null=True)
    benutzer = models.ForeignKey(Benutzer, db_column='BENUTZER_ID', blank=True, null=True, on_delete=models.PROTECT)
    status = models.IntegerField(db_column='STATUS')
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)
    begruendung = models.CharField(db_column='BEGRUENDUNG', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'LEISTUNG'


class Magistraliter(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    position = models.IntegerField(db_column='POSITION')
    typ = models.CharField(db_column='TYP', max_length=50, blank=True, null=True)
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)
    rezeptur = models.TextField(db_column='REZEPTUR', blank=True, null=True)
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)
    letzte_aenderung_datum = models.DateField(db_column='LETZTE_AENDERUNG_DATUM', blank=True, null=True)
    letzte_aenderung_uhrzeit = models.TimeField(db_column='LETZTE_AENDERUNG_UHRZEIT', blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)
    dosisschema = models.TextField(db_column='DOSISSCHEMA', blank=True, null=True)
    ermaechtigung = models.CharField(db_column='ERMAECHTIGUNG', max_length=255)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    kasse = models.CharField(db_column='KASSE', max_length=255, blank=True, null=True)
    privat = models.CharField(db_column='PRIVAT', max_length=255, blank=True, null=True)
    einstellung = models.TextField(db_column='EINSTELLUNG', blank=True, null=True)
    medikament_box = models.CharField(db_column='MEDIKAMENT_BOX', max_length=5, blank=True, null=True)
    vertragsart = models.CharField(db_column='VERTRAGSART', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'MAGISTRALITER'


class Mahnung(models.Model):
    mahnungdoku_id = models.IntegerField(db_column='MAHNUNGDOKU_ID', primary_key=True)
    untersuchung_id = models.IntegerField(db_column='UNTERSUCHUNG_ID')
    anschrift_xml = models.TextField(db_column='ANSCHRIFT_XML', blank=True, null=True)
    absende_anschrift = models.TextField(db_column='ABSENDE_ANSCHRIFT', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'MAHNUNG'


class Medikament(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    patient_id = models.ForeignKey("Patient", db_column='PATIENT_ID', on_delete=models.PROTECT)
    doku_id = models.ForeignKey(Dokumentation, db_column='DOKU_ID', on_delete=models.PROTECT)
    untersuchung_id = models.ForeignKey("Untersuchung", db_column='UNTERSUCHUNG_ID',  on_delete=models.PROTECT)
    typ = models.CharField(db_column='TYP', max_length=10)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    status = models.CharField(db_column='STATUS', max_length=50, blank=True, null=True)
    datum = models.DateField(db_column='DATUM', blank=True, null=True)
    uhrzeit = models.TimeField(db_column='UHRZEIT', blank=True, null=True)
    medikament = models.CharField(db_column='MEDIKAMENT', max_length=255, blank=True, null=True)
    menge = models.FloatField(db_column='MENGE', blank=True, null=True)
    mengenart = models.CharField(db_column='MENGENART', max_length=255, blank=True, null=True)
    anzahl = models.FloatField(db_column='ANZAHL', blank=True, null=True)
    dosisschema = models.TextField(db_column='DOSISSCHEMA', blank=True, null=True)
    medikament_ca_kz = models.CharField(db_column='MEDIKAMENT_CA_KZ', max_length=50, blank=True, null=True)
    rezeptur = models.TextField(db_column='REZEPTUR', blank=True, null=True)
    reserve2 = models.CharField(db_column='RESERVE2', max_length=50, blank=True, null=True)
    reserve3 = models.TextField(db_column='RESERVE3', blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)
    rezept_datum = models.DateField(db_column='REZEPT_DATUM', blank=True, null=True)
    kuerzel = models.CharField(db_column='KUERZEL', max_length=50, unique=True, blank=True, null=True)
    pharma_nr = models.IntegerField(db_column='PHARMA_NR', blank=True, null=True)
    katalog = models.IntegerField(db_column='KATALOG', blank=True, null=True)
    box = models.CharField(db_column='BOX', max_length=5, blank=True, null=True)
    end_datum = models.DateField(db_column='END_DATUM', blank=True, null=True)
    absetzgrund = models.CharField(db_column='ABSETZGRUND', max_length=255, blank=True, null=True)
    anmerkung = models.TextField(db_column='ANMERKUNG', blank=True, null=True)
    rhythmus = models.IntegerField(db_column='RHYTHMUS')
    anzeige = models.IntegerField(db_column='ANZEIGE', blank=True, null=True)
    nachts = models.FloatField(db_column='NACHTS')
    fruehnachts = models.FloatField(db_column='FRUEHNACHTS')
    morgens = models.FloatField(db_column='MORGENS')
    mittags = models.FloatField(db_column='MITTAGS')
    nachmittags = models.FloatField(db_column='NACHMITTAGS')
    abends = models.FloatField(db_column='ABENDS')
    verschreibungsgrund = models.CharField(db_column='VERSCHREIBUNGSGRUND', max_length=255, blank=True, null=True)
    fremdarzt = models.CharField(db_column='FREMDARZT', max_length=255, blank=True, null=True)
    dm_end_datum = models.DateField(db_column='DM_END_DATUM', blank=True, null=True)
    rezeptur_rezept_name = models.CharField(db_column='REZEPTUR_REZEPT_NAME', max_length=255, blank=True, null=True)
    elga = models.IntegerField(db_column='ELGA', blank=True, null=True)
    verordnung_id = models.CharField(db_column='VERORDNUNG_ID', max_length=255, blank=True, null=True)
    verordnung_aenderung = models.DateTimeField(db_column='VERORDNUNG_AENDERUNG', blank=True,null=True)
    interaktion_check = models.IntegerField(db_column='INTERAKTION_CHECK', blank=True, null=True)
    fremd_medik = models.IntegerField(db_column='FREMD_MEDIK', blank=True, null=True)
    preisfindung = models.IntegerField(db_column='PREISFINDUNG', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'MEDIKAMENT'
        verbose_name_plural = "Medikamente"

    def __str__(self):
        return "{}, {} {}, S: {}-{}-{}".format(
            self.medikament,
            self.menge, self.mengenart,
            self.morgens, self.mittags, self.abends
            )


class MedikamentMeldung(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    benutzer = models.CharField(db_column='BENUTZER', max_length=255)
    pzn = models.IntegerField(db_column='PZN')
    wert = models.IntegerField(db_column='WERT')
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'MEDIKAMENT_MELDUNG'
        unique_together = (('benutzer', 'pzn'),)


class Medikationsplan(models.Model):
    doku_id = models.IntegerField(db_column='DOKU_ID', primary_key=True)  # Field name made lowercase.
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'MEDIKATIONSPLAN'


class MedikationsplanEintrag(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    doku_id = models.IntegerField(db_column='DOKU_ID', blank=True, null=True)  # Field name made lowercase.
    pzn = models.CharField(db_column='PZN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    wirkstoffe = models.CharField(db_column='WIRKSTOFFE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    handelsname = models.CharField(db_column='HANDELSNAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    staerke = models.CharField(db_column='STAERKE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    darr_form = models.CharField(db_column='DARR_FORM', max_length=255, blank=True, null=True)  # Field name made lowercase.
    nachts = models.FloatField(db_column='NACHTS', blank=True, null=True)  # Field name made lowercase.
    fruehnachts = models.FloatField(db_column='FRUEHNACHTS', blank=True, null=True)  # Field name made lowercase.
    morgens = models.FloatField(db_column='MORGENS', blank=True, null=True)  # Field name made lowercase.
    mittags = models.FloatField(db_column='MITTAGS', blank=True, null=True)  # Field name made lowercase.
    nachmittags = models.FloatField(db_column='NACHMITTAGS', blank=True, null=True)  # Field name made lowercase.
    abends = models.FloatField(db_column='ABENDS', blank=True, null=True)  # Field name made lowercase.
    einheit = models.CharField(db_column='EINHEIT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    hinweise = models.CharField(db_column='HINWEISE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    verschr_grund = models.CharField(db_column='VERSCHR_GRUND', max_length=255, blank=True, null=True)  # Field name made lowercase.
    position = models.IntegerField(db_column='POSITION', blank=True, null=True)  # Field name made lowercase.
    drucken = models.CharField(db_column='DRUCKEN', max_length=255, blank=True, null=True)  # Field name made lowercase.
    fremd_arzt = models.CharField(db_column='FREMD_ARZT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    absetz_grund = models.CharField(db_column='ABSETZ_GRUND', max_length=255, blank=True, null=True)  # Field name made lowercase.
    absetz_datum = models.DateField(db_column='ABSETZ_DATUM', blank=True, null=True)  # Field name made lowercase.
    einnahmeende = models.DateField(db_column='EINNAHMEENDE', blank=True, null=True)  # Field name made lowercase.
    typ = models.CharField(db_column='TYP', max_length=255, blank=True, null=True)  # Field name made lowercase.
    freitext = models.CharField(db_column='FREITEXT', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'MEDIKATIONSPLAN_EINTRAG'


class MedikationsplanUeberschriften(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    code = models.IntegerField(db_column='CODE', blank=True, null=True)  # Field name made lowercase.
    text = models.CharField(db_column='TEXT', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'MEDIKATIONSPLAN_UEBERSCHRIFTEN'



class MedizinfragebogenFragenStamm(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    fragebogen_id = models.IntegerField(db_column='FRAGEBOGEN_ID', blank=True, null=True)
    fragen_kuerzel = models.CharField(db_column='FRAGEN_KUERZEL', max_length=50)
    fragen_sprache = models.CharField(db_column='FRAGEN_SPRACHE', max_length=11)
    reihenfolge_nummer = models.IntegerField(db_column='REIHENFOLGE_NUMMER', blank=True, null=True)
    aktiv = models.IntegerField(db_column='AKTIV', blank=True, null=True)
    trennlinie = models.IntegerField(db_column='TRENNLINIE', blank=True, null=True)
    nummerierung = models.IntegerField(db_column='NUMMERIERUNG', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'MEDIZINFRAGEBOGEN_FRAGEN_STAMM'
        unique_together = (('fragebogen_id', 'fragen_kuerzel', 'fragen_sprache'),)


class MedizinfragebogenStamm(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)
    mandanten = models.CharField(db_column='MANDANTEN', max_length=255, blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    einstellungen = models.TextField(db_column='EINSTELLUNGEN', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'MEDIZINFRAGEBOGEN_STAMM'


class MedizinfrageAntworten(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    patient_id = models.IntegerField(db_column='PATIENT_ID', blank=True, null=True)
    medizinfragebogen_fragen_id = models.IntegerField(db_column='MEDIZINFRAGEBOGEN_FRAGEN_ID', blank=True, null=True)
    fragen_kuerzel = models.CharField(db_column='FRAGEN_KUERZEL', max_length=50, blank=True, null=True)
    fragen_sprache = models.CharField(db_column='FRAGEN_SPRACHE', max_length=11, blank=True, null=True)
    fragen_antwort = models.IntegerField(db_column='FRAGEN_ANTWORT', blank=True, null=True)
    fragen_bemerkung = models.CharField(db_column='FRAGEN_BEMERKUNG', max_length=255, blank=True, null=True)
    fragen_status = models.IntegerField(db_column='FRAGEN_STATUS', blank=True, null=True)
    fragen_aenderung = models.IntegerField(db_column='FRAGEN_AENDERUNG', blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    doku_id = models.IntegerField(db_column='DOKU_ID', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'MEDIZINFRAGE_ANTWORTEN'
        unique_together = (('patient_id', 'fragen_kuerzel', 'fragen_sprache'),
                           ('fragen_kuerzel', 'fragen_sprache', 'patient_id'),
                           ('fragen_kuerzel', 'fragen_sprache', 'doku_id'),)


class MedizinfrageStamm(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    kuerzel = models.CharField(db_column='KUERZEL', max_length=255, blank=True, null=True)
    frage_text = models.TextField(db_column='FRAGE_TEXT', blank=True, null=True)
    sprache = models.CharField(db_column='SPRACHE', max_length=10, blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)
    kategorie = models.IntegerField(db_column='KATEGORIE', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'MEDIZINFRAGE_STAMM'
        unique_together = (('kuerzel', 'sprache'),)


class Medizinstatus(models.Model):
    doku_id = models.IntegerField(db_column='DOKU_ID', unique=True)
    medizinstatus_typ = models.IntegerField(db_column='MEDIZINSTATUS_TYP', blank=True, null=True)
    medizinstatus_status = models.IntegerField(db_column='MEDIZINSTATUS_STATUS', blank=True, null=True)
    sprache = models.CharField(db_column='SPRACHE', max_length=11, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'MEDIZINSTATUS'


class MedizinstatusAntworten(models.Model):
    medizinstatusdokuid = models.IntegerField(db_column='MedizinStatusDokuID', primary_key=True)
    medizinfragestammid = models.IntegerField(db_column='MedizinFrageStammID', blank=True, null=True)
    medizinfragestammkuerzel = models.CharField(db_column='MedizinFrageStammKuerzel', max_length=50)
    medizinfragestammsprache = models.CharField(db_column='MedizinFrageStammSprache', max_length=11)
    antwort = models.IntegerField(db_column='Antwort', blank=True, null=True)
    antwortbemerkung = models.TextField(db_column='AntwortBemerkung', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'MEDIZINSTATUS_ANTWORTEN'
        unique_together = (('medizinstatusdokuid', 'medizinfragestammkuerzel', 'medizinfragestammsprache'),)


class Mkpbuch(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    ermaechtigung_id = models.IntegerField(db_column='ERMAECHTIGUNG_ID', blank=True, null=True)
    patient_id = models.IntegerField(db_column='PATIENT_ID', blank=True, null=True)
    untersuchung_id = models.IntegerField(db_column='UNTERSUCHUNG_ID', blank=True, null=True)
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)
    subtyp = models.IntegerField(db_column='SUBTYP', blank=True, null=True)
    datum = models.DateField(db_column='DATUM', blank=True, null=True)
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)
    ergebnis = models.IntegerField(db_column='ERGEBNIS', blank=True, null=True)
    bemerkung = models.TextField(db_column='BEMERKUNG', blank=True, null=True)
    freitext1 = models.TextField(db_column='FREITEXT1', blank=True, null=True)
    freitext2 = models.TextField(db_column='FREITEXT2', blank=True, null=True)
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)
    system_datum = models.DateField(db_column='SYSTEM_DATUM', blank=True, null=True)
    system_uhrzeit = models.TimeField(db_column='SYSTEM_UHRZEIT', blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)
    mkp_kennung = models.IntegerField(db_column='MKP_KENNUNG', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'MKPBUCH'


class NachrichtVersand(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    nachricht_art = models.IntegerField(db_column='NACHRICHT_ART', blank=True, null=True)
    lokale_daten_id = models.IntegerField(db_column='LOKALE_DATEN_ID', blank=True, null=True)
    nachricht_typ = models.IntegerField(db_column='NACHRICHT_TYP', blank=True, null=True)
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)
    empfaenger = models.CharField(db_column='EMPFAENGER', max_length=255, blank=True, null=True)
    betreff = models.CharField(db_column='BETREFF', max_length=255, blank=True, null=True)
    nachricht = models.TextField(db_column='NACHRICHT', blank=True, null=True)
    versand_datum = models.DateField(db_column='VERSAND_DATUM', blank=True, null=True)
    versand_ablaufdatum = models.DateField(db_column='VERSAND_ABLAUFDATUM', blank=True, null=True)
    web_status = models.IntegerField(db_column='WEB_STATUS', blank=True, null=True)
    web_id = models.IntegerField(db_column='WEB_ID', blank=True, null=True)
    web_angelegt = models.DateTimeField(db_column='WEB_ANGELEGT', blank=True, null=True)
    web_versand = models.DateTimeField(db_column='WEB_VERSAND', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'NACHRICHT_VERSAND'
        unique_together = (('nachricht_art', 'lokale_daten_id', 'nachricht_typ'),)


class Nlg(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    patient_id = models.IntegerField(db_column='PATIENT_ID')
    doku_id = models.IntegerField(db_column='DOKU_ID')
    nummer = models.IntegerField(db_column='NUMMER', blank=True, null=True)
    geschwindigkeit_re = models.FloatField(db_column='GESCHWINDIGKEIT_RE', blank=True, null=True)
    strecke_re = models.FloatField(db_column='STRECKE_RE', blank=True, null=True)
    geschwindigkeit_li = models.FloatField(db_column='GESCHWINDIGKEIT_LI', blank=True, null=True)
    strecke_li = models.FloatField(db_column='STRECKE_LI', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'NLG'


class Operation(models.Model):
    operation_dokuid = models.IntegerField(db_column='OPERATION_DOKUID', unique=True)
    op_team = models.TextField(db_column='OP_TEAM', blank=True, null=True)
    op_bezeichnung = models.CharField(db_column='OP_BEZEICHNUNG', max_length=255, blank=True, null=True)
    op_startuhrzeit = models.TimeField(db_column='OP_STARTUHRZEIT', blank=True, null=True)
    op_enduhrzeit = models.TimeField(db_column='OP_ENDUHRZEIT', blank=True, null=True)
    komplikationen = models.TextField(db_column='KOMPLIKATIONEN', blank=True, null=True)
    lagerung = models.CharField(db_column='LAGERUNG', max_length=255, blank=True, null=True)
    narkose_art = models.CharField(db_column='NARKOSE_ART', max_length=255, blank=True, null=True)
    narkose_mittel = models.CharField(db_column='NARKOSE_MITTEL', max_length=255, blank=True, null=True)
    narkose_zusatztext = models.TextField(db_column='NARKOSE_ZUSATZTEXT', blank=True, null=True)
    op_verlauf = models.TextField(db_column='OP_VERLAUF', blank=True, null=True)
    diagnose = models.TextField(db_column='DIAGNOSE', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'OPERATION'


class Ops(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    patient_id = models.IntegerField(db_column='PATIENT_ID')
    opdoku_id = models.IntegerField(db_column='OPDOKU_ID')
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    status = models.IntegerField(db_column='STATUS')
    datum = models.DateField(db_column='DATUM', blank=True, null=True)
    uhrzeit = models.TimeField(db_column='UHRZEIT', blank=True, null=True)
    schluessel = models.CharField(db_column='SCHLUESSEL', max_length=50, blank=True, null=True)
    seitenlokalisation = models.CharField(db_column='SEITENLOKALISATION', max_length=5)
    ops_text = models.CharField(db_column='OPS_TEXT', max_length=255, blank=True, null=True)
    ops_datum = models.DateField(db_column='OPS_DATUM', blank=True, null=True)
    ops_uhrzeit = models.TimeField(db_column='OPS_UHRZEIT', blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'OPS'


class Organisation(models.Model):
    id = models.IntegerField(db_column='ID', primary_key=True)
    typ = models.CharField(db_column='TYP', max_length=50, blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)
    notiz = models.TextField(db_column='NOTIZ', blank=True, null=True)
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)
    kennwort = models.CharField(db_column='KENNWORT', max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ORGANISATION'


class Patient(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    kue = models.CharField(db_column='KUE', max_length=255, blank=True, null=True)
    anrede = models.CharField(db_column='ANREDE', max_length=255, blank=True, null=True)
    titel = models.CharField(db_column='TITEL', max_length=255, blank=True, null=True)
    vorname = models.CharField(db_column='VORNAME', max_length=255, blank=True, null=True)
    wvorname = models.CharField(db_column='WVORNAME', max_length=255, blank=True, null=True)
    name = models.CharField(db_column='NAME', max_length=255, blank=True, null=True)
    gebname = models.CharField(db_column='GEBNAME', max_length=255, blank=True, null=True)
    namenszusatz = models.CharField(db_column='NAMENSZUSATZ', max_length=255, blank=True, null=True)
    namensvorsatz = models.CharField(db_column='NAMENSVORSATZ', max_length=255, blank=True, null=True)
    gebdatum = models.DateField(db_column='GEBDATUM', blank=True, null=True)
    bild = models.FileField(db_column='BILD', blank=True, null=True)
    notizen = models.TextField(db_column='NOTIZEN', blank=True, null=True)
    protokoll = Base64TextField(db_column='PROTOKOLL', blank=True, null=True)
    adresse_typ = models.IntegerField(db_column='ADRESSE_TYP', blank=True, null=True)
    strasse = models.CharField(db_column='STRASSE', max_length=255, blank=True, null=True)
    hausnummer = models.CharField(db_column='HAUSNUMMER', max_length=30, blank=True, null=True)
    adresszusatz = models.CharField(db_column='ADRESSZUSATZ', max_length=255, blank=True, null=True)
    plz = models.CharField(db_column='PLZ', max_length=50, blank=True, null=True)
    ort = models.CharField(db_column='ORT', max_length=255, blank=True, null=True)
    land = models.CharField(db_column='LAND', max_length=255, blank=True, null=True)
    anamnese1 = models.TextField(db_column='ANAMNESE1', blank=True, null=True)
    anamnese2 = models.TextField(db_column='ANAMNESE2', blank=True, null=True)
    anamnese3 = models.TextField(db_column='ANAMNESE3', blank=True, null=True)
    telefon = models.CharField(db_column='TELEFON', max_length=50, blank=True, null=True)
    telefon_2 = models.CharField(db_column='TELEFON_2', max_length=50, blank=True, null=True)
    fax = models.CharField(db_column='FAX', max_length=50, blank=True, null=True)
    mobil = models.CharField(db_column='MOBIL', max_length=50, blank=True, null=True)
    email = models.CharField(db_column='EMAIL', max_length=100, blank=True, null=True)
    bank = models.CharField(db_column='BANK', max_length=255, blank=True, null=True)
    bank_plz = models.CharField(db_column='BANK_PLZ', max_length=50, blank=True, null=True)
    bank_ort = models.CharField(db_column='BANK_ORT', max_length=255, blank=True, null=True)
    bank_land = models.CharField(db_column='BANK_LAND', max_length=255, blank=True, null=True)
    kto_nr = models.CharField(db_column='KTO_NR', max_length=50, blank=True, null=True)
    blz = models.CharField(db_column='BLZ', max_length=50, blank=True, null=True)
    kto_inhaber = models.CharField(db_column='KTO_INHABER', max_length=255, blank=True, null=True)
    status = models.IntegerField(db_column='STATUS')
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    versicherungsnr = models.CharField(db_column='VERSICHERUNGSNR', max_length=50, blank=True, null=True)
    cave = models.CharField(db_column='CAVE', max_length=255, blank=True, null=True)
    info = models.CharField(db_column='INFO', max_length=255, blank=True, null=True)
    letzte_aenderung_datum = models.DateField(db_column='LETZTE_AENDERUNG_DATUM', blank=True, null=True)
    letzte_aenderung_uhrzeit = models.TimeField(db_column='LETZTE_AENDERUNG_UHRZEIT', blank=True, null=True)
    freitext1 = models.TextField(db_column='FREITEXT1', blank=True, null=True)
    freitext2 = models.TextField(db_column='FREITEXT2', blank=True, null=True)
    sterbedatum = models.DateField(db_column='STERBEDATUM', blank=True, null=True)
    entbindungsdatum = models.DateField(db_column='ENTBINDUNGSDATUM', blank=True, null=True)
    nationalitaet = models.CharField(db_column='NATIONALITAET', max_length=255, blank=True, null=True)
    familienstand = models.CharField(db_column='FAMILIENSTAND', max_length=255, blank=True, null=True)
    kennzeichen1 = models.BigIntegerField(db_column='KENNZEICHEN1')
    kennzeichen2 = models.IntegerField(db_column='KENNZEICHEN2')
    zusatz = Base64TextField(db_column='ZUSATZ', blank=True, null=True)
    einstellungen = Base64TextField(db_column='EINSTELLUNGEN')
    briefdaten = Base64TextField(db_column='BRIEFDATEN', blank=True, null=True)
    altsystem_id = models.BigIntegerField(db_column='ALTSYSTEM_ID', unique=True, blank=True, null=True)
    kis_id = models.BigIntegerField(db_column='KIS_ID', unique=True, blank=True, null=True)
    kennzeichen3 = models.IntegerField(db_column='KENNZEICHEN3', blank=True, null=True)
    hausarzt_vpnr = models.CharField(db_column='HAUSARZT_VPNR', max_length=50, blank=True, null=True)
    abweichende_anschrift = models.TextField(db_column='ABWEICHENDE_ANSCHRIFT', blank=True, null=True)
    brief_versand = Base64TextField(db_column='BRIEF_VERSAND', blank=True, null=True)
    bank_iban = models.CharField(db_column='BANK_IBAN', max_length=50, blank=True, null=True)
    bank_bic = models.CharField(db_column='BANK_BIC', max_length=50, blank=True, null=True)
    aufnahme_zeit = models.DateTimeField(db_column='AUFNAHME_ZEIT', blank=True, null=True)
    geschlecht = models.IntegerField(db_column='GESCHLECHT')

    class Meta:
        managed = False
        db_table = 'PATIENT'
        verbose_name_plural = "Patienten"

    def __str__(self):
        return "{}, {} ({})".format(self.name, self.vorname, self.gebdatum)


class Patientmedizin(models.Model):
    patient_id = models.OneToOneField(Patient, db_column='PATIENT_ID', primary_key=True, on_delete=models.PROTECT)
    anzschwangerschaften = models.IntegerField(db_column='ANZSCHWANGERSCHAFTEN', blank=True, null=True)
    anzgeburten = models.IntegerField(db_column='ANZGEBURTEN', blank=True, null=True)
    anzaborte = models.IntegerField(db_column='ANZABORTE', blank=True, null=True)
    anzkinder = models.IntegerField(db_column='ANZKINDER', blank=True, null=True)
    blutgruppe = models.CharField(db_column='BLUTGRUPPE', max_length=5, blank=True, null=True)
    anamnese1 = Base64TextField(db_column='ANAMNESE1', blank=True, null=True)
    anam1_lock = models.CharField(db_column='ANAM1_LOCK', max_length=255, blank=True, null=True)
    anamnese2 = Base64TextField(db_column='ANAMNESE2', blank=True, null=True)
    anam2_lock = models.CharField(db_column='ANAM2_LOCK', max_length=255, blank=True, null=True)
    anamnese3 = Base64TextField(db_column='ANAMNESE3', blank=True, null=True)
    anam3_lock = models.CharField(db_column='ANAM3_LOCK', max_length=255, blank=True, null=True)
    cave = models.CharField(db_column='CAVE', max_length=255, blank=True, null=True)
    info = Base64CharField(db_column='INFO', max_length=255, blank=True, null=True)
    beruf = models.CharField(db_column='BERUF', max_length=255, blank=True, null=True)
    familienstand = models.CharField(db_column='FAMILIENSTAND', max_length=255, blank=True, null=True)
    risikofaktoren = models.CharField(db_column='RISIKOFAKTOREN', max_length=255, blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    einstellungen = Base64TextField(db_column='EINSTELLUNGEN', blank=True, null=True)
    letzte_regel = models.DateField(db_column='LETZTE_REGEL', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'PATIENTMEDIZIN'


class Patientzusatz(models.Model):
    patient_id = models.OneToOneField(Patient, db_column='PATIENT_ID', primary_key=True, on_delete=models.CASCADE)
    nsf = models.IntegerField(db_column='NSF', blank=True, null=True)
    th_stop = models.IntegerField(db_column='TH_Stop', blank=True, null=True)
    anam1_lock = models.CharField(db_column='ANAM1_LOCK', max_length=255, blank=True, null=True)
    anam2_lock = models.CharField(db_column='ANAM2_LOCK', max_length=255, blank=True, null=True)
    anam3_lock = models.CharField(db_column='ANAM3_LOCK', max_length=255, blank=True, null=True)
    sprache = models.CharField(db_column='SPRACHE', max_length=5, blank=True, null=True)
    hausarzt = models.TextField(db_column='HAUSARZT', blank=True, null=True)
    anzschwangerschaften = models.IntegerField(db_column='ANZSCHWANGERSCHAFTEN', blank=True, null=True)
    anzgeburten = models.IntegerField(db_column='ANZGEBURTEN', blank=True, null=True)
    anzkinder = models.IntegerField(db_column='ANZKINDER', blank=True, null=True)
    kennzeichen1 = models.BigIntegerField(db_column='KENNZEICHEN1', blank=True, null=True)
    kennzeichen2 = models.IntegerField(db_column='KENNZEICHEN2', blank=True, null=True)
    kennzeichen3 = models.IntegerField(db_column='KENNZEICHEN3', blank=True, null=True)
    kennzeichen4 = models.IntegerField(db_column='KENNZEICHEN4', blank=True, null=True)
    kennzeichen5 = models.IntegerField(db_column='KENNZEICHEN5', blank=True, null=True)
    kennzeichen6 = models.IntegerField(db_column='KENNZEICHEN6', blank=True, null=True)
    kennzeichen7 = models.IntegerField(db_column='KENNZEICHEN7', blank=True, null=True)
    kennzeichen8 = models.IntegerField(db_column='KENNZEICHEN8', blank=True, null=True)
    kennzeichen9 = models.IntegerField(db_column='KENNZEICHEN9', blank=True, null=True)
    kennzeichen10 = models.IntegerField(db_column='KENNZEICHEN10', blank=True, null=True)
    nationalitaet = models.CharField(db_column='NATIONALITAET', max_length=5, blank=True, null=True)
    blutgruppe = models.CharField(db_column='BLUTGRUPPE', max_length=5, blank=True, null=True)
    quittung = models.IntegerField(db_column='QUITTUNG')
    suchname = models.CharField(db_column='SUCHNAME', max_length=255, blank=True, null=True)
    serienbrief_versandarten = models.CharField(db_column='SERIENBRIEF_VERSANDARTEN', max_length=255, blank=True, null=True)
    serienbrief_stdversandart = models.CharField(db_column='SERIENBRIEF_STDVERSANDART', max_length=50, blank=True, null=True)
    serienbrief_typen = models.TextField(db_column='SERIENBRIEF_TYPEN', blank=True, null=True)
    partner_patient_id = models.IntegerField(db_column='PARTNER_PATIENT_ID', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'PATIENTZUSATZ'


class Person(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    anrede = models.CharField(db_column='ANREDE', max_length=255, blank=True, null=True)
    titel = models.CharField(db_column='TITEL', max_length=255, blank=True, null=True)
    name = models.CharField(db_column='NAME', max_length=255, blank=True, null=True)
    vorname = models.CharField(db_column='VORNAME', max_length=255, blank=True, null=True)
    wvorname = models.CharField(db_column='WVORNAME', max_length=255, blank=True, null=True)
    geburtsname = models.CharField(db_column='GEBURTSNAME', max_length=255, blank=True, null=True)
    name_vorsatz = models.CharField(db_column='NAME_VORSATZ', max_length=255, blank=True, null=True)
    name_zusatz = models.CharField(db_column='NAME_ZUSATZ', max_length=255, blank=True, null=True)
    kuerzel = models.CharField(db_column='KUERZEL', max_length=255, blank=True, null=True)
    geburtsdatum = models.DateField(db_column='GEBURTSDATUM', blank=True, null=True)
    sterbedatum = models.DateField(db_column='STERBEDATUM', blank=True, null=True)
    adresse = models.IntegerField(db_column='ADRESSE', blank=True, null=True)
    post_adresse = models.IntegerField(db_column='POST_ADRESSE', blank=True, null=True)
    liefer_adresse = models.IntegerField(db_column='LIEFER_ADRESSE', blank=True, null=True)
    rechnung_adresse = models.IntegerField(db_column='RECHNUNG_ADRESSE', blank=True, null=True)
    kommunikation = models.IntegerField(db_column='KOMMUNIKATION', blank=True, null=True)
    konto = models.IntegerField(db_column='KONTO', blank=True, null=True)
    bild = models.TextField(db_column='BILD', blank=True, null=True)
    notizen = models.TextField(db_column='NOTIZEN', blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'PERSON'
        verbose_name_plural = 'Personen'


class Phlebostatus(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    patient_id = models.ForeignKey(Patient, db_column='PATIENT_ID', blank=True, null=True, on_delete=models.PROTECT)
    doku_id = models.IntegerField(db_column='DOKU_ID', blank=True, null=True)
    typ = models.CharField(db_column='TYP', max_length=50, blank=True, null=True)
    datum = models.DateField(db_column='DATUM', blank=True, null=True)
    properties = models.TextField(db_column='PROPERTIES', blank=True, null=True)
    bemerkung = models.TextField(db_column='BEMERKUNG', blank=True, null=True)
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)
    system_datum = models.DateField(db_column='SYSTEM_DATUM', blank=True, null=True)
    system_uhrzeit = models.TimeField(db_column='SYSTEM_UHRZEIT', blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'PHLEBOSTATUS'


class Physis(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    patient_id = models.ForeignKey(Patient, db_column='PATIENT_ID', on_delete=models.PROTECT)
    typ = models.CharField(db_column='TYP', max_length=50)
    datum = models.DateField(db_column='DATUM', blank=True, null=True)
    monat = models.FloatField(db_column='MONAT')
    groesse = models.FloatField(db_column='GROESSE', blank=True, null=True)
    kopfumfang = models.FloatField(db_column='KOPFUMFANG', blank=True, null=True)
    gewicht = models.FloatField(db_column='GEWICHT', blank=True, null=True)
    percentile = models.IntegerField(db_column='PERCENTILE', blank=True, null=True)
    referenz = models.IntegerField(db_column='REFERENZ', blank=True, null=True)
    bemerkung = models.TextField(db_column='BEMERKUNG', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'PHYSIS'


class PhysTherapie(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    th_kuerzel = models.CharField(db_column='TH_KUERZEL', unique=True, max_length=30)
    th_bezeichnung = models.CharField(db_column='TH_BEZEICHNUNG', max_length=255, blank=True, null=True)
    th_kkbezeich = models.CharField(db_column='TH_KKBEZEICH', max_length=50, blank=True, null=True)
    th_typ = models.CharField(db_column='TH_TYP', max_length=1, blank=True, null=True)
    th_max = models.IntegerField(db_column='TH_MAX')
    th_def_verrechnungsart = models.IntegerField(db_column='TH_DEF_VERRECHNUNGSART', blank=True, null=True)
    th_char = models.CharField(db_column='TH_CHAR', max_length=1, blank=True, null=True)
    th_tm = models.FloatField(db_column='TH_TM')
    th_km = models.FloatField(db_column='TH_KM')
    th_einzeltermin = models.IntegerField(db_column='TH_EINZELTERMIN')
    gt_typ = models.CharField(db_column='GT_TYP', max_length=50, blank=True, null=True)
    th_vf = models.IntegerField(db_column='TH_VF')
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    gruppe = models.IntegerField(db_column='GRUPPE')
    einstellung = models.TextField(db_column='EINSTELLUNG', blank=True, null=True)
    benutzer = models.ForeignKey(Benutzer, db_column='BENUTZER_ID', blank=True, null=True, on_delete=models.PROTECT)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)
    privat = models.CharField(db_column='PRIVAT', max_length=255, blank=True, null=True)
    kasse = models.CharField(db_column='KASSE', max_length=255, blank=True, null=True)
    ermaechtigung = models.CharField(db_column='ERMAECHTIGUNG', max_length=255, blank=True, null=True)
    feiertagstermin = models.IntegerField(db_column='FEIERTAGSTERMIN')
    kalender = models.CharField(db_column='KALENDER', max_length=255, blank=True, null=True)
    warteliste = models.CharField(db_column='WARTELISTE', max_length=255, blank=True, null=True)
    web_name = models.CharField(db_column='WEB_NAME', max_length=255, blank=True, null=True)
    web_beschreibung = models.TextField(db_column='WEB_BESCHREIBUNG', blank=True, null=True)
    web_hinweistext = models.TextField(db_column='WEB_HINWEISTEXT', blank=True, null=True)
    web_properties = models.TextField(db_column='WEB_PROPERTIES', blank=True, null=True)
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)
    geraet = models.CharField(db_column='GERAET', max_length=255, blank=True, null=True)
    modalitaet = models.IntegerField(db_column='MODALITAET', blank=True, null=True)
    typ_profil = models.CharField(db_column='TYP_PROFIL', max_length=255, blank=True, null=True)
    profil_parameter_ids = models.CharField(db_column='PROFIL_PARAMETER_IDS', max_length=255, blank=True, null=True)
    favorit_position = models.IntegerField(db_column='FAVORIT_POSITION', blank=True, null=True)
    ae_title = models.CharField(db_column='AE_TITLE', max_length=255, blank=True, null=True)
    ae_stationname = models.CharField(db_column='AE_STATIONNAME', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'PHYS_THERAPIE'


class PhysTherapieRegion(models.Model):
    therapie_id = models.IntegerField(db_column='THERAPIE_ID', primary_key=True)
    region_id = models.IntegerField(db_column='REGION_ID')

    class Meta:
        managed = False
        db_table = 'PHYS_THERAPIE_REGION'
        unique_together = (('therapie_id', 'region_id'),)


class PlzAut(models.Model):
    plz = models.CharField(db_column='PLZ', max_length=10, primary_key=True)
    ort = models.CharField(db_column='ORT', max_length=255, blank=True, null=True)
    bundesland = models.CharField(db_column='BUNDESLAND', max_length=255, blank=True, null=True)
    zusatz = models.CharField(db_column='ZUSATZ', max_length=255, blank=True, null=True)
    ort2 = models.CharField(db_column='ORT2', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'PLZ_AUT'
        unique_together = (('plz', 'ort'),)
        verbose_name = 'PLZ (at)'

    def __str__(self):
        return '{} {}'.format(self.plz, self.ort)


class PlzDe(models.Model):
    plz = models.CharField(db_column='PLZ', max_length=5, primary_key=True)
    ort = models.CharField(db_column='ORT', max_length=31, blank=True, null=True)
    bundesland = models.CharField(db_column='BUNDESLAND', max_length=50, blank=True, null=True)
    zusatz = models.CharField(db_column='ZUSATZ', max_length=40, blank=True, null=True)
    ort2 = models.CharField(db_column='ORT2', max_length=24, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'PLZ_DE'
        verbose_name = 'PLZ (de)'

    def __str__(self):
        return '{} {}'.format(self.plz, self.ort)


class PraxisDaten(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    kuerzel = models.CharField(db_column='KUERZEL', unique=True, max_length=50)
    einrichtung_typ = models.IntegerField(db_column='EINRICHTUNG_TYP', blank=True, null=True)
    hv_nummer = models.CharField(db_column='HV_NUMMER', max_length=50, blank=True, null=True)
    dv_nummer = models.CharField(db_column='DV_NUMMER', max_length=50, blank=True, null=True)
    adress_code = models.CharField(db_column='ADRESS_CODE', max_length=50, blank=True, null=True)
    fachgebiet = models.IntegerField(db_column='FACHGEBIET')
    kom_anrede = Base64CharField(db_column='KOM_ANREDE', max_length=255, blank=True, null=True)
    adresse = Base64TextField(db_column='ADRESSE', blank=True, null=True)
    kommunikation = Base64CharField(db_column='KOMMUNIKATION', max_length=255, blank=True, null=True)
    konto = Base64TextField(db_column='KONTO', blank=True, null=True)
    datum = models.DateField(db_column='DATUM', blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    status = models.IntegerField(db_column='STATUS')
    typ = models.IntegerField(db_column='TYP')
    gueltigkeit = models.CharField(db_column='GUELTIGKEIT', max_length=50, blank=True, null=True)
    notizen = models.TextField(db_column='NOTIZEN', blank=True, null=True)
    protokoll = Base64TextField(db_column='PROTOKOLL', blank=True, null=True)
    bundesland_kasse = models.CharField(db_column='BUNDESLAND_KASSE', max_length=50, blank=True, null=True)
    briefdaten = Base64TextField(db_column='BRIEFDATEN', blank=True, null=True)
    letzte_aenderung_datum = models.DateField(db_column='LETZTE_AENDERUNG_DATUM', blank=True, null=True)
    letzte_aenderung_uhrzeit = models.TimeField(db_column='LETZTE_AENDERUNG_UHRZEIT', blank=True, null=True)
    einstellungen = Base64TextField(db_column='EINSTELLUNGEN', blank=True, null=True)
    gnw_einstellungen = Base64TextField(db_column='GNW_EINSTELLUNGEN', blank=True, null=True)
    pvs_einstellungen = Base64TextField(db_column='PVS_EINSTELLUNGEN', blank=True, null=True)
    anschrift_zeile1 = models.CharField(db_column='ANSCHRIFT_ZEILE1', max_length=255, blank=True, null=True)
    anschrift_zeile2 = models.CharField(db_column='ANSCHRIFT_ZEILE2', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'PRAXIS_DATEN'
        verbose_name_plural = 'PraxisDaten'

    def __str__(self):
        firstname, tmp, lastname,tmp, tmp, tmp, title, salutation = \
            self.kom_anrede.split('|')
        return '{hvnr}: {title} {lastname}, {firstname}, ({fach})'.format(
            hvnr=self.hv_nummer,
            title=title,
            lastname=lastname,
            firstname=firstname,
            fach=Fachgebiete[self.fachgebiet])


class Produkt(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    modul = models.CharField(db_column='MODUL', max_length=50, blank=True, null=True)
    einstellung = models.CharField(db_column='EINSTELLUNG', max_length=50, blank=True, null=True)
    wert = models.CharField(db_column='WERT', max_length=50, blank=True, null=True)
    typ = models.CharField(db_column='TYP', max_length=50, blank=True, null=True)
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)
    beschreibung = models.CharField(db_column='BESCHREIBUNG', max_length=255, blank=True, null=True)
    produkt = models.IntegerField(db_column='PRODUKT', blank=True, null=True)
    land = models.CharField(db_column='LAND', max_length=10, blank=True, null=True)
    lizenz = models.CharField(db_column='LIZENZ', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'PRODUKT'


class ProtokollAbrechnung(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    typ = models.IntegerField(db_column='TYP')
    mandant_id = models.IntegerField(db_column='MANDANT_ID')
    sitzungs_nr = models.BigIntegerField(db_column='SITZUNGS_NR', blank=True, null=True)
    datum = models.DateField(db_column='DATUM', blank=True, null=True)
    uhrzeit = models.TimeField(db_column='UHRZEIT', blank=True, null=True)
    abr_datei = models.CharField(db_column='ABR_DATEI', max_length=255, blank=True, null=True)
    protokoll = models.CharField(db_column='PROTOKOLL', max_length=255, blank=True, null=True)
    quartal = models.IntegerField(db_column='QUARTAL', blank=True, null=True)
    jahr = models.IntegerField(db_column='JAHR', blank=True, null=True)
    kasse = models.CharField(db_column='KASSE', max_length=255, blank=True, null=True)
    status = models.IntegerField(db_column='STATUS')
    uuid = models.CharField(db_column='UUID', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'PROTOKOLL_ABRECHNUNG'


class ProtokollDaten(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    daten_id = models.IntegerField(db_column='DATEN_ID')
    daten_typ = models.IntegerField(db_column='DATEN_TYP')
    typ = models.IntegerField(db_column='TYP')
    datum = models.DateField(db_column='DATUM', blank=True, null=True)
    uhrzeit = models.TimeField(db_column='UHRZEIT', blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)
    protokoll_kurztext = models.TextField(db_column='PROTOKOLL_KURZTEXT', blank=True, null=True)
    sitzungs_nr = models.BigIntegerField(db_column='SITZUNGS_NR', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'PROTOKOLL_DATEN'


class ProtokollDoku(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    doku_id = models.IntegerField(db_column='DOKU_ID')
    typ = models.IntegerField(db_column='TYP')
    datum = models.DateField(db_column='DATUM', blank=True, null=True)
    uhrzeit = models.TimeField(db_column='UHRZEIT', blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)
    protokoll_kurztext = models.TextField(db_column='PROTOKOLL_KURZTEXT', blank=True, null=True)
    sitzungs_nr = models.BigIntegerField(db_column='SITZUNGS_NR', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'PROTOKOLL_DOKU'


class ProtokollEintragSerienbrief(models.Model):
    protokoll_serienbrief_id = models.IntegerField(db_column='PROTOKOLL_SERIENBRIEF_ID')
    adressat_id = models.IntegerField(db_column='ADRESSAT_ID', blank=True, null=True)
    versandart = models.CharField(db_column='VERSANDART', max_length=255, blank=True, null=True)
    status_text = models.TextField(db_column='STATUS_TEXT', blank=True, null=True)
    adressat_name = models.CharField(db_column='ADRESSAT_NAME', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'PROTOKOLL_EINTRAG_SERIENBRIEF'


class ProtokollImport(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    typ = models.IntegerField(db_column='TYP')
    ermaechtigung_id = models.IntegerField(db_column='ERMAECHTIGUNG_ID')
    benutzer = models.CharField(db_column='BENUTZER', max_length=50, blank=True, null=True)
    datum = models.DateField(db_column='DATUM', blank=True, null=True)
    uhrzeit = models.TimeField(db_column='UHRZEIT', blank=True, null=True)
    sitzungs_nr = models.BigIntegerField(db_column='SITZUNGS_NR')
    protokoll = Base64TextField(db_column='PROTOKOLL')

    class Meta:
        managed = False
        db_table = 'PROTOKOLL_IMPORT'

    def __str__(self):
        return "{} {} ({})".format(
            self.datum,
            self.uhrzeit,
            self.benutzer)


class ProtokollRechner(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    rechner = models.CharField(db_column='RECHNER', max_length=255, blank=True, null=True)
    hash = models.CharField(db_column='HASH', max_length=255, blank=True, null=True)
    details = models.TextField(db_column='DETAILS', blank=True, null=True)
    aktiv = models.IntegerField(db_column='AKTIV', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'PROTOKOLL_RECHNER'


class ProtokollSenden(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    typ = models.IntegerField(db_column='TYP')
    ermaechtigung_id = models.IntegerField(db_column='ERMAECHTIGUNG_ID')
    sitzungs_nr = models.BigIntegerField(db_column='SITZUNGS_NR', blank=True, null=True)
    datum = models.DateField(db_column='DATUM', blank=True, null=True)
    uhrzeit = models.TimeField(db_column='UHRZEIT', blank=True, null=True)
    sende_alb = models.CharField(db_column='SENDE_ALB', max_length=255, blank=True, null=True)
    info_text = models.TextField(db_column='INFO_TEXT', blank=True, null=True)
    status = models.IntegerField(db_column='STATUS')

    class Meta:
        managed = False
        db_table = 'PROTOKOLL_SENDEN'


class ProtokollSerienbrief(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    auswertung_liste_id = models.IntegerField(db_column='AUSWERTUNG_LISTE_ID', blank=True, null=True)
    typ = models.CharField(db_column='TYP', max_length=50, blank=True, null=True)
    datum = models.DateField(db_column='DATUM', blank=True, null=True)
    uhrzeit = models.TimeField(db_column='UHRZEIT', blank=True, null=True)
    benutzer = models.ForeignKey(Benutzer, db_column='BENUTZER_ID', blank=True, null=True, on_delete=models.PROTECT)
    mandant_id = models.IntegerField(db_column='MANDANT_ID', blank=True, null=True)
    druckvorlage_id = models.IntegerField(db_column='DRUCKVORLAGE_ID', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'PROTOKOLL_SERIENBRIEF'


class ProtokollSitzung(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)
    rechner = models.CharField(db_column='RECHNER', max_length=50, blank=True, null=True)
    os = models.CharField(db_column='OS', max_length=50, blank=True, null=True)
    java = models.CharField(db_column='JAVA', max_length=50, blank=True, null=True)
    aceto_version = models.CharField(db_column='ACETO_VERSION', max_length=50, blank=True, null=True)
    benutzer = models.CharField(db_column='BENUTZER', max_length=50, blank=True, null=True)
    datum_an = models.DateField(db_column='DATUM_AN', blank=True, null=True)
    uhrzeit_an = models.TimeField(db_column='UHRZEIT_AN', blank=True, null=True)
    uhrzeit_aktion = models.TimeField(db_column='UHRZEIT_AKTION', blank=True, null=True)
    datum_ab = models.DateField(db_column='DATUM_AB', blank=True, null=True)
    uhrzeit_ab = models.TimeField(db_column='UHRZEIT_AB', blank=True, null=True)
    dauer = models.BigIntegerField(db_column='DAUER', blank=True, null=True)
    sitzungs_nr = models.BigIntegerField(db_column='SITZUNGS_NR', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'PROTOKOLL_SITZUNG'


class ProtokollSpvm(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    sitzungs_nr = models.BigIntegerField(db_column='SITZUNGS_NR', blank=True, null=True)
    maximal = models.IntegerField(db_column='MAXIMAL', blank=True, null=True)
    frei = models.IntegerField(db_column='FREI', blank=True, null=True)
    total = models.IntegerField(db_column='TOTAL', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'PROTOKOLL_SPVM'


class ProtokollStamm(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    stamm_id = models.IntegerField(db_column='STAMM_ID')
    stamm_typ = models.IntegerField(db_column='STAMM_TYP')
    typ = models.IntegerField(db_column='TYP')
    datum = models.DateField(db_column='DATUM', blank=True, null=True)
    uhrzeit = models.TimeField(db_column='UHRZEIT', blank=True, null=True)
    protokoll = Base64TextField(db_column='PROTOKOLL', blank=True, null=True)
    protokoll_kurztext = Base64TextField(db_column='PROTOKOLL_KURZTEXT', blank=True, null=True)
    sitzungs_nr = models.BigIntegerField(db_column='SITZUNGS_NR', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'PROTOKOLL_STAMM'


class ProtokollWebtermine(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    sitzungs_nr = models.BigIntegerField(db_column='SITZUNGS_NR', blank=True, null=True)
    typ = models.IntegerField(db_column='TYP')
    zeit = models.DateTimeField(db_column='ZEIT', blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)
    termin_id_liste = models.TextField(db_column='TERMIN_ID_LISTE', blank=True, null=True)
    start_datum = models.DateField(db_column='START_DATUM', blank=True, null=True)
    end_datum = models.DateField(db_column='END_DATUM', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'PROTOKOLL_WEBTERMINE'


class Rechnung(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    rechnungdoku_id = models.IntegerField(db_column='RECHNUNGDOKU_ID', unique=True)
    untersuchung_id = models.IntegerField(db_column='UNTERSUCHUNG_ID')
    re_nr = models.CharField(db_column='RE_NR', unique=True, max_length=50, blank=True, null=True)
    re_text = models.TextField(db_column='RE_TEXT', blank=True, null=True)
    re_anschrift = models.TextField(db_column='RE_ANSCHRIFT', blank=True, null=True)
    re_anschrift_xml = models.TextField(db_column='RE_ANSCHRIFT_XML', blank=True, null=True)
    re_absende_anschrift = models.TextField(db_column='RE_ABSENDE_ANSCHRIFT', blank=True, null=True)
    re_versandart = models.CharField(db_column='RE_VERSANDART', max_length=255, blank=True, null=True)
    re_positionen = models.TextField(db_column='RE_POSITIONEN', blank=True, null=True)
    re_anzahl_pos = models.IntegerField(db_column='RE_ANZAHL_POS')
    re_summe_netto = models.FloatField(db_column='RE_SUMME_NETTO', blank=True, null=True)
    re_summe_netto_1 = models.FloatField(db_column='RE_SUMME_NETTO_1', blank=True, null=True)
    re_summe_netto_2 = models.FloatField(db_column='RE_SUMME_NETTO_2', blank=True, null=True)
    re_summe_brutto = models.FloatField(db_column='RE_SUMME_BRUTTO', blank=True, null=True)
    re_summe_brutto_1 = models.FloatField(db_column='RE_SUMME_BRUTTO_1', blank=True, null=True)
    re_summe_brutto_2 = models.FloatField(db_column='RE_SUMME_BRUTTO_2', blank=True, null=True)
    re_mwst_1 = models.FloatField(db_column='RE_MWST_1', blank=True, null=True)
    re_mwst_2 = models.FloatField(db_column='RE_MWST_2', blank=True, null=True)
    re_summe_mwst = models.FloatField(db_column='RE_SUMME_MWST', blank=True, null=True)
    re_summe_mwst_1 = models.FloatField(db_column='RE_SUMME_MWST_1', blank=True, null=True)
    re_summe_mwst_2 = models.FloatField(db_column='RE_SUMME_MWST_2', blank=True, null=True)
    re_waehrung = models.CharField(db_column='RE_WAEHRUNG', max_length=50, blank=True, null=True)
    re_unterschreiber = models.CharField(db_column='RE_UNTERSCHREIBER', max_length=50, blank=True, null=True)
    re_signed = models.CharField(db_column='RE_SIGNED', max_length=255, blank=True, null=True)
    re_offene_summe_brutto = models.FloatField(db_column='RE_OFFENE_SUMME_BRUTTO')
    re_zaehler = models.IntegerField(db_column='RE_ZAEHLER', blank=True, null=True)
    datum_versendet = models.DateField(db_column='DATUM_VERSENDET', blank=True, null=True)
    datum_storniert = models.DateField(db_column='DATUM_STORNIERT', blank=True, null=True)
    status_zusatz = models.TextField(db_column='STATUS_ZUSATZ', blank=True, null=True)
    mahn_stufe = models.IntegerField(db_column='MAHN_STUFE')
    re_jahr = models.IntegerField(db_column='RE_JAHR', blank=True, null=True)
    re_mandant_id = models.IntegerField(db_column='RE_MANDANT_ID', blank=True, null=True)
    abrechnungsschein_id = models.IntegerField(db_column='ABRECHNUNGSSCHEIN_ID', blank=True, null=True)
    diagnosen_xml = models.TextField(db_column='DIAGNOSEN_XML', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'RECHNUNG'
        unique_together = (('re_zaehler', 're_jahr', 're_mandant_id'),)


class Rechnungposition(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    rechnungdoku_id = models.IntegerField(db_column='RECHNUNGDOKU_ID', blank=True, null=True)
    pos_nr = models.IntegerField(db_column='POS_NR', blank=True, null=True)
    anzahl = models.FloatField(db_column='ANZAHL', blank=True, null=True)
    position_datum = models.DateField(db_column='POSITION_DATUM', blank=True, null=True)
    einzelpreis = models.FloatField(db_column='EINZELPREIS', blank=True, null=True)
    mwst_satz = models.IntegerField(db_column='MWST_SATZ', blank=True, null=True)
    kontierung = models.IntegerField(db_column='KONTIERUNG', blank=True, null=True)
    leistung = models.CharField(db_column='LEISTUNG', max_length=255, blank=True, null=True)
    rechnung_text = models.TextField(db_column='RECHNUNG_TEXT', blank=True, null=True)
    begruendung = models.TextField(db_column='BEGRUENDUNG', blank=True, null=True)
    mwst_preis = models.FloatField(db_column='MWST_PREIS', blank=True, null=True)
    brutto_preis = models.FloatField(db_column='BRUTTO_PREIS', blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)
    system_datum = models.DateField(db_column='SYSTEM_DATUM', blank=True, null=True)
    system_uhrzeit = models.TimeField(db_column='SYSTEM_UHRZEIT', blank=True, null=True)
    faktor = models.FloatField(db_column='FAKTOR')
    abzug = models.FloatField(db_column='ABZUG')
    typ_kosten = models.IntegerField(db_column='TYP_KOSTEN')
    ziffer_id = models.IntegerField(db_column='ZIFFER_ID', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'RECHNUNGPOSITION'


class Region(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    re_kuerzel = models.CharField(db_column='RE_KUERZEL', unique=True, max_length=20)
    re_bezeichnung = models.CharField(db_column='RE_BEZEICHNUNG', max_length=35, blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'REGION'


class Rezept(models.Model):
    rezeptdoku_id = models.IntegerField(db_column='REZEPTDOKU_ID', unique=True)
    status_rezept = models.IntegerField(db_column='STATUS_REZEPT', blank=True, null=True)
    gebuehr_befreit = models.IntegerField(db_column='GEBUEHR_BEFREIT', blank=True, null=True)
    rezept_typ = models.CharField(db_column='REZEPT_TYP', max_length=50, blank=True, null=True)
    hausapotheke = models.IntegerField(db_column='HAUSAPOTHEKE', blank=True, null=True)
    elga = models.IntegerField(db_column='ELGA', blank=True, null=True)
    emed_id = models.CharField(db_column='EMED_ID', max_length=255, blank=True, null=True)
    emed_datamatrix = models.TextField(db_column='EMED_DATAMATRIX', blank=True, null=True)
    datum_gedruckt = models.DateField(db_column='DATUM_GEDRUCKT', blank=True, null=True)
    datum_storniert = models.DateField(db_column='DATUM_STORNIERT', blank=True, null=True)
    bemerkung_stornierung = models.CharField(db_column='BEMERKUNG_STORNIERUNG', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'REZEPT'


class Rohdrogen(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)
    latein = models.CharField(db_column='LATEIN', max_length=255, blank=True, null=True)
    pinyin = models.CharField(db_column='PINYIN', max_length=255, blank=True, null=True)
    pinyin_lautschrift = models.CharField(db_column='PINYIN_LAUTSCHRIFT', max_length=255, blank=True, null=True)
    warnhinweis = models.CharField(db_column='WARNHINWEIS', max_length=30, blank=True, null=True)
    benskykategorie = models.IntegerField(db_column='BENSKYKATEGORIE', blank=True, null=True)
    chinazeichen_tradition = models.CharField(db_column='CHINAZEICHEN_TRADITION', max_length=30, blank=True, null=True)
    chinazeichen_kurzform = models.CharField(db_column='CHINAZEICHEN_KURZFORM', max_length=30, blank=True, null=True)
    notiz = models.TextField(db_column='NOTIZ', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ROHDROGEN'


class Schwangerschaft(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    nr = models.IntegerField(db_column='NR')
    letzte_regel_datum = models.DateField(db_column='LETZTE_REGEL_DATUM', blank=True, null=True)
    geplant_geburt_datum = models.DateField(db_column='GEPLANT_GEBURT_DATUM', blank=True, null=True)
    herztaetigkeit_datum = models.DateField(db_column='HERZTAETIGKEIT_DATUM', blank=True, null=True)
    geburt_abort_datum = models.DateField(db_column='GEBURT_ABORT_DATUM', blank=True, null=True)
    geburt_abort_ssw = models.CharField(db_column='GEBURT_ABORT_SSW', max_length=255, blank=True, null=True)
    geburt_typ = models.IntegerField(db_column='GEBURT_TYP', blank=True, null=True)
    geburt_art = models.CharField(db_column='GEBURT_ART', max_length=255, blank=True, null=True)
    abort_genetik = models.TextField(db_column='ABORT_GENETIK', blank=True, null=True)
    untersuchung_text = models.TextField(db_column='UNTERSUCHUNG_TEXT', blank=True, null=True)
    saeuglinge = models.TextField(db_column='SAEUGLINGE', blank=True, null=True)
    geburt_abort_bemerkung = models.TextField(db_column='GEBURT_ABORT_BEMERKUNG', blank=True, null=True)
    schwangerschafts_dokuid = models.IntegerField(db_column='SCHWANGERSCHAFTS_DOKUID', blank=True, null=True)
    zyklus_tage = models.IntegerField(db_column='ZYKLUS_TAGE', blank=True, null=True)
    schwangerschaftstest = models.IntegerField(db_column='SCHWANGERSCHAFTSTEST', blank=True, null=True)
    errechneter_gebtermin = models.CharField(db_column='ERRECHNETER_GEBTERMIN', max_length=100, blank=True, null=True)
    invitro_zyklus_id = models.IntegerField(db_column='INVITRO_ZYKLUS_ID', blank=True, null=True)
    mutterschutz_beginn_datum = models.DateField(db_column='MUTTERSCHUTZ_BEGINN_DATUM', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'SCHWANGERSCHAFT'


class Sendeliste(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    doku_id = models.IntegerField(db_column='DOKU_ID')
    sendetyp = models.IntegerField(db_column='SENDETYP')
    sendestatus = models.IntegerField(db_column='SENDESTATUS')
    webstatus = models.IntegerField(db_column='WEBSTATUS')
    absender = models.CharField(db_column='ABSENDER', max_length=255, blank=True, null=True)
    empfaenger = models.CharField(db_column='EMPFAENGER', max_length=255, blank=True, null=True)
    betreff = models.CharField(db_column='BETREFF', max_length=255, blank=True, null=True)
    text = models.TextField(db_column='TEXT', blank=True, null=True)
    dateiname = models.CharField(db_column='DATEINAME', max_length=255, blank=True, null=True)
    benutzer = models.ForeignKey(Benutzer, db_column='BENUTZER_ID', on_delete=models.PROTECT)
    datum_erstellt = models.DateTimeField(db_column='DATUM_ERSTELLT', blank=True, null=True)
    datum_uebertragen = models.DateTimeField(db_column='DATUM_UEBERTRAGEN', blank=True, null=True)
    datum_gesendet = models.DateTimeField(db_column='DATUM_GESENDET', blank=True, null=True)
    rechner = models.CharField(db_column='RECHNER', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'SENDELISTE'


class Serienbrief(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    aceto_nr = models.IntegerField(db_column='ACETO_NR')
    position = models.IntegerField(db_column='POSITION', blank=True, null=True)
    name = models.CharField(db_column='NAME', max_length=255, blank=True, null=True)
    report = models.CharField(db_column='REPORT', max_length=50, blank=True, null=True)
    logo = models.CharField(db_column='LOGO', max_length=50, blank=True, null=True)
    einstellungen = models.TextField(db_column='EINSTELLUNGEN', blank=True, null=True)
    privat = models.CharField(db_column='PRIVAT', max_length=255, blank=True, null=True)
    kasse = models.CharField(db_column='KASSE', max_length=255, blank=True, null=True)
    ermaechtigung = models.CharField(db_column='ERMAECHTIGUNG', max_length=255, blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)
    letzte_aenderung_datum = models.DateField(db_column='LETZTE_AENDERUNG_DATUM', blank=True, null=True)
    letzte_aenderung_uhrzeit = models.TimeField(db_column='LETZTE_AENDERUNG_UHRZEIT', blank=True, null=True)
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)
    begruessung = models.TextField(db_column='BEGRUESSUNG', blank=True, null=True)
    einleitung = models.TextField(db_column='EINLEITUNG', blank=True, null=True)
    absatz1 = models.TextField(db_column='ABSATZ1', blank=True, null=True)
    absatz2 = models.TextField(db_column='ABSATZ2', blank=True, null=True)
    absatz3 = models.TextField(db_column='ABSATZ3', blank=True, null=True)
    schlussformel = models.TextField(db_column='SCHLUSSFORMEL', blank=True, null=True)
    unterschrift = models.TextField(db_column='UNTERSCHRIFT', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'SERIENBRIEF'


class SisDok(models.Model):
    doknummer = models.CharField(db_column='DOKNUMMER', max_length=10, blank=True, null=True)
    doktyp = models.CharField(db_column='DOKTYP', max_length=10, blank=True, null=True)
    dokreddat = models.CharField(db_column='DOKREDDAT', max_length=10, blank=True, null=True)
    doktitel = models.CharField(db_column='DOKTITEL', max_length=255, blank=True, null=True)
    dokurl = models.CharField(db_column='DOKURL', max_length=255, blank=True, null=True)
    doktxt = models.TextField(db_column='DOKTXT', blank=True, null=True)
    doktxk = models.TextField(db_column='DOKTXK', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'SIS_DOK'


class SisDokId(models.Model):
    indnr = models.CharField(db_column='INDNR', max_length=10, blank=True, null=True)
    doknummer = models.CharField(db_column='DOKNUMMER', max_length=10, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'SIS_DOK_ID'


class SisPicto(models.Model):
    zlnumm = models.CharField(db_column='ZLNUMM', max_length=10)
    znumm = models.CharField(db_column='ZNUMM', primary_key=True, max_length=10)
    zbez = models.CharField(db_column='ZBEZ', max_length=100, blank=True, null=True)
    picto = models.CharField(db_column='PICTO', max_length=20, blank=True, null=True)
    sst = models.CharField(db_column='SST', max_length=10, blank=True, null=True)
    sst2 = models.CharField(db_column='SST2', max_length=10, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'SIS_PICTO'


class SisZarzst(models.Model):
    yhnummer = models.CharField(db_column='YHNUMMER', primary_key=True, max_length=10)
    ykz = models.CharField(db_column='YKZ', max_length=10, blank=True, null=True)
    ytxt = models.CharField(db_column='YTXT', max_length=255, blank=True, null=True)
    yhnummer1 = models.CharField(db_column='YHNUMMER1', max_length=10, blank=True, null=True)
    ydh = models.CharField(db_column='YDH', max_length=10, blank=True, null=True)
    ydu = models.CharField(db_column='YDU', max_length=10, blank=True, null=True)
    ydruck = models.CharField(db_column='YDRUCK', max_length=5, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'SIS_ZARZST'


class SisZatc(models.Model):
    zatc = models.CharField(db_column='ZATC', primary_key=True, max_length=10)
    zatcbez = models.CharField(db_column='ZATCBEZ', max_length=255, blank=True, null=True)
    zatcbeze = models.CharField(db_column='ZATCBEZE', max_length=255, blank=True, null=True)
    zdruck = models.CharField(db_column='ZDRUCK', max_length=5, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'SIS_ZATC'


class SisZatcl(models.Model):
    znumm = models.CharField(db_column='ZNUMM', primary_key=True, max_length=10)
    zatc = models.CharField(db_column='ZATC', max_length=10)
    zsort = models.CharField(db_column='ZSORT', max_length=10, blank=True, null=True)
    zdruck = models.CharField(db_column='ZDRUCK', max_length=5, blank=True, null=True)
    zkz1 = models.CharField(db_column='ZKZ1', max_length=5, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'SIS_ZATCL'
        unique_together = (('znumm', 'zatc'),)


class SisZdar(models.Model):
    ydar = models.CharField(db_column='YDAR', primary_key=True, max_length=10)
    ybez = models.CharField(db_column='YBEZ', max_length=255, blank=True, null=True)
    yhgruppe = models.CharField(db_column='YHGRUPPE', max_length=10)
    yugruppe = models.CharField(db_column='YUGRUPPE', max_length=10)

    class Meta:
        managed = False
        db_table = 'SIS_ZDAR'
        unique_together = (('ydar', 'yhgruppe', 'yugruppe'),)


class SisZherst(models.Model):
    ycode = models.CharField(db_column='YCODE', primary_key=True, max_length=10)
    yname = models.CharField(db_column='YNAME', max_length=100, blank=True, null=True)
    ytel = models.CharField(db_column='YTEL', max_length=100, blank=True, null=True)
    yplz = models.CharField(db_column='YPLZ', max_length=10, blank=True, null=True)
    yort = models.CharField(db_column='YORT', max_length=100, blank=True, null=True)
    ystrasse = models.CharField(db_column='YSTRASSE', max_length=100, blank=True, null=True)
    yadr2 = models.CharField(db_column='YADR2', max_length=100, blank=True, null=True)
    ydruck = models.CharField(db_column='YDRUCK', max_length=5, blank=True, null=True)
    yfax = models.CharField(db_column='YFAX', max_length=100, blank=True, null=True)
    ymail = models.CharField(db_column='YMAIL', max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'SIS_ZHERST'


class SisZindgrst(models.Model):
    zindnr = models.CharField(db_column='ZINDNR', primary_key=True, max_length=10)
    zindbez = models.CharField(db_column='ZINDBEZ', max_length=100, blank=True, null=True)
    zindnr2 = models.CharField(db_column='ZINDNR2', max_length=10, blank=True, null=True)
    zdruck = models.CharField(db_column='ZDRUCK', max_length=5, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'SIS_ZINDGRST'


class SisZindl(models.Model):
    znumm = models.CharField(db_column='ZNUMM', primary_key=True, max_length=10)
    zindnr = models.CharField(db_column='ZINDNR', max_length=10)
    zsort = models.CharField(db_column='ZSORT', max_length=10, blank=True, null=True)
    zdruck = models.CharField(db_column='ZDRUCK', max_length=5, blank=True, null=True)
    zkz1 = models.CharField(db_column='ZKZ1', max_length=5, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'SIS_ZINDL'
        unique_together = (('znumm', 'zindnr'),)


class SisZinterl(models.Model):
    zinternr = models.CharField(db_column='ZINTERNR', primary_key=True, max_length=10)
    zintergr = models.CharField(db_column='ZINTERGR', max_length=5)
    zinterart = models.CharField(db_column='ZINTERART', max_length=5)
    yhnummer = models.CharField(db_column='YHNUMMER', max_length=10)
    yhnummer1 = models.CharField(db_column='YHNUMMER1', max_length=10, blank=True, null=True)
    zsort = models.CharField(db_column='ZSORT', max_length=10, blank=True, null=True)
    zdruck = models.CharField(db_column='ZDRUCK', max_length=5, blank=True, null=True)
    yhnummer3 = models.CharField(db_column='YHNUMMER3', max_length=10, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'SIS_ZINTERL'
        unique_together = (('zinternr', 'zintergr', 'zinterart', 'yhnummer'),)


class SisZinterst(models.Model):
    zinternr = models.CharField(db_column='ZINTERNR', primary_key=True, max_length=10)
    zinternr2 = models.CharField(db_column='ZINTERNR2', max_length=10, blank=True, null=True)
    zintergr1 = models.CharField(db_column='ZINTERGR1', max_length=10, blank=True, null=True)
    zintergr2 = models.CharField(db_column='ZINTERGR2', max_length=10, blank=True, null=True)
    zgruppe = models.CharField(db_column='ZGRUPPE', max_length=10, blank=True, null=True)
    zkurz = models.CharField(db_column='ZKURZ', max_length=100, blank=True, null=True)
    zart = models.CharField(db_column='ZART', max_length=5, blank=True, null=True)
    zdruck = models.CharField(db_column='ZDRUCK', max_length=5, blank=True, null=True)
    ztext = models.TextField(db_column='ZTEXT', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'SIS_ZINTERST'


class SisZspez(models.Model):
    znumm = models.CharField(db_column='ZNUMM', primary_key=True, max_length=10)
    zlnumm = models.CharField(db_column='ZLNUMM', max_length=10, blank=True, null=True)
    zbez = models.CharField(db_column='ZBEZ', max_length=100, blank=True, null=True)
    zland = models.CharField(db_column='ZLAND', max_length=5, blank=True, null=True)
    zmono = models.CharField(db_column='ZMONO', max_length=5, blank=True, null=True)
    zanzahl = models.CharField(db_column='ZANZAHL', max_length=5, blank=True, null=True)
    zherst = models.CharField(db_column='ZHERST', max_length=10, blank=True, null=True)
    zvertr = models.CharField(db_column='ZVERTR', max_length=10, blank=True, null=True)
    zedat = models.CharField(db_column='ZEDAT', max_length=20, blank=True, null=True)
    zbdat = models.CharField(db_column='ZBDAT', max_length=20, blank=True, null=True)
    zbezgr = models.CharField(db_column='ZBEZGR', max_length=40, blank=True, null=True)
    zverfall = models.CharField(db_column='ZVERFALL', max_length=5, blank=True, null=True)
    zverfall2 = models.CharField(db_column='ZVERFALL2', max_length=10, blank=True, null=True)
    zlager = models.CharField(db_column='ZLAGER', max_length=5, blank=True, null=True)
    zloesen = models.CharField(db_column='ZLOESEN', max_length=5, blank=True, null=True)
    zwartez = models.CharField(db_column='ZWARTEZ', max_length=5, blank=True, null=True)
    zabgabe = models.CharField(db_column='ZABGABE', max_length=5, blank=True, null=True)
    zna = models.CharField(db_column='ZNA', max_length=5, blank=True, null=True)
    zrp = models.CharField(db_column='ZRP', max_length=10, blank=True, null=True)
    zwarn = models.CharField(db_column='ZWARN', max_length=5, blank=True, null=True)
    zsucht = models.CharField(db_column='ZSUCHT', max_length=10, blank=True, null=True)
    zdruck = models.CharField(db_column='ZDRUCK', max_length=5, blank=True, null=True)
    zdruck2 = models.CharField(db_column='ZDRUCK2', max_length=20, blank=True, null=True)
    zanwend = models.TextField(db_column='ZANWEND', blank=True, null=True)
    zhinw = models.TextField(db_column='ZHINW', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'SIS_ZSPEZ'


class SisZstoffl(models.Model):
    znumm = models.CharField(db_column='ZNUMM', primary_key=True, max_length=10)
    yhnummer = models.CharField(db_column='YHNUMMER', max_length=10, blank=True, null=True)
    yhnummer1 = models.CharField(db_column='YHNUMMER1', max_length=10, blank=True, null=True)
    zkz = models.CharField(db_column='ZKZ', max_length=10, blank=True, null=True)
    zmenge = models.CharField(db_column='ZMENGE', max_length=20, blank=True, null=True)
    zmengeinh = models.CharField(db_column='ZMENGEINH', max_length=10, blank=True, null=True)
    ztext = models.CharField(db_column='ZTEXT', max_length=20, blank=True, null=True)
    zsort = models.CharField(db_column='ZSORT', max_length=10, blank=True, null=True)
    znr = models.CharField(db_column='ZNR', max_length=10)
    zdruck = models.CharField(db_column='ZDRUCK', max_length=5, blank=True, null=True)
    zkz1 = models.CharField(db_column='ZKZ1', max_length=5, blank=True, null=True)
    zkz2 = models.CharField(db_column='ZKZ2', max_length=5, blank=True, null=True)
    yhnummer3 = models.CharField(db_column='YHNUMMER3', max_length=10, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'SIS_ZSTOFFL'
        unique_together = (('znumm', 'znr'),)


class SisZsubstan(models.Model):
    zintergr = models.CharField(db_column='ZINTERGR', primary_key=True, max_length=10)
    zgrbez = models.CharField(db_column='ZGRBEZ', max_length=100, blank=True, null=True)
    zart = models.CharField(db_column='ZART', max_length=10, blank=True, null=True)
    zintergr1 = models.CharField(db_column='ZINTERGR1', max_length=10, blank=True, null=True)
    zdruck = models.CharField(db_column='ZDRUCK', max_length=5, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'SIS_ZSUBSTAN'


class Stddiag(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    position = models.IntegerField(db_column='POSITION', blank=True, null=True)
    kuerzel = models.CharField(db_column='KUERZEL', max_length=50, blank=True, null=True)
    diagnosen = models.CharField(db_column='DIAGNOSEN', max_length=255, blank=True, null=True)
    icd_kodes = models.CharField(db_column='ICD_KODES', max_length=255, blank=True, null=True)
    mandanten = models.CharField(db_column='MANDANTEN', max_length=255, blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)
    letzte_aenderung_datum = models.DateField(db_column='LETZTE_AENDERUNG_DATUM', blank=True, null=True)
    letzte_aenderung_uhrzeit = models.TimeField(db_column='LETZTE_AENDERUNG_UHRZEIT', blank=True, null=True)
    wahlarzt = models.CharField(db_column='WAHLARZT', max_length=255, blank=True, null=True)
    kasse = models.CharField(db_column='KASSE', max_length=255, blank=True, null=True)
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)
    bg = models.CharField(db_column='BG', max_length=255, blank=True, null=True)
    vertragsart = models.CharField(db_column='VERTRAGSART', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'STDDIAG'


class StddiagGyn(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    position = models.IntegerField(db_column='POSITION', blank=True, null=True)
    kuerzel = models.CharField(db_column='KUERZEL', max_length=50, blank=True, null=True)
    diagnosen = models.CharField(db_column='DIAGNOSEN', max_length=255, blank=True, null=True)
    icd_kodes = models.CharField(db_column='ICD_KODES', max_length=255, blank=True, null=True)
    ermaechtigung = models.CharField(db_column='ERMAECHTIGUNG', max_length=255, blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)
    letzte_aenderung_datum = models.DateField(db_column='LETZTE_AENDERUNG_DATUM', blank=True, null=True)
    letzte_aenderung_uhrzeit = models.TimeField(db_column='LETZTE_AENDERUNG_UHRZEIT', blank=True, null=True)
    privat = models.CharField(db_column='PRIVAT', max_length=255, blank=True, null=True)
    kasse = models.CharField(db_column='KASSE', max_length=255, blank=True, null=True)
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)
    bg = models.CharField(db_column='BG', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'STDDIAG_GYN'


class Stdleist(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    position = models.IntegerField(db_column='POSITION', blank=True, null=True)
    kasse_ziffern_kette = models.TextField(db_column='KASSE_ZIFFERN_KETTE', blank=True, null=True)
    privat_ziffern_kette = models.TextField(db_column='PRIVAT_ZIFFERN_KETTE', blank=True, null=True)
    leistung_kette = models.CharField(db_column='LEISTUNG_KETTE', max_length=255, blank=True, null=True)
    bg_ziffern_kette = models.TextField(db_column='BG_ZIFFERN_KETTE', blank=True, null=True)
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)
    mandanten = models.CharField(db_column='MANDANTEN', max_length=255, blank=True, null=True)
    schein_kennzeichen = models.CharField(db_column='SCHEIN_KENNZEICHEN', max_length=255, blank=True, null=True)
    schein_art = models.CharField(db_column='SCHEIN_ART', max_length=255, blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)
    letzte_aenderung_datum = models.DateField(db_column='LETZTE_AENDERUNG_DATUM', blank=True, null=True)
    letzte_aenderung_uhrzeit = models.TimeField(db_column='LETZTE_AENDERUNG_UHRZEIT', blank=True, null=True)
    kasse = models.CharField(db_column='KASSE', max_length=255, blank=True, null=True)
    modalitaet = models.CharField(db_column='MODALITAET', max_length=255, blank=True, null=True)
    einstellung = Base64TextField(db_column='EINSTELLUNG', blank=True, null=True)
    wahlarzt = models.CharField(db_column='WAHLARZT', max_length=255, blank=True, null=True)
    bg = models.CharField(db_column='BG', max_length=255, blank=True, null=True)
    vertragsart = models.CharField(db_column='VERTRAGSART', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'STDLEIST'
        verbose_name = "Standardleistung"
        verbose_name_plural = "Standardleistungen"

    def __str__(self):
        return "{} ({})".format(self.bezeichnung, self.kasse_ziffern_kette)


class StdleistGyn(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    position = models.IntegerField(db_column='POSITION', blank=True, null=True)
    kasse_ziffern_kette = models.CharField(db_column='KASSE_ZIFFERN_KETTE', max_length=255, blank=True, null=True)
    privat_ziffern_kette = models.CharField(db_column='PRIVAT_ZIFFERN_KETTE', max_length=255, blank=True, null=True)
    leistung_kette = models.CharField(db_column='LEISTUNG_KETTE', max_length=255, blank=True, null=True)
    bg_ziffern_kette = models.CharField(db_column='BG_ZIFFERN_KETTE', max_length=255, blank=True, null=True)
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)
    ermaechtigung = models.CharField(db_column='ERMAECHTIGUNG', max_length=255, blank=True, null=True)
    schein_kennzeichen = models.CharField(db_column='SCHEIN_KENNZEICHEN', max_length=255, blank=True, null=True)
    schein_art = models.CharField(db_column='SCHEIN_ART', max_length=255, blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)
    letzte_aenderung_datum = models.DateField(db_column='LETZTE_AENDERUNG_DATUM', blank=True, null=True)
    letzte_aenderung_uhrzeit = models.TimeField(db_column='LETZTE_AENDERUNG_UHRZEIT', blank=True, null=True)
    kasse = models.CharField(db_column='KASSE', max_length=255, blank=True, null=True)
    modalitaet = models.CharField(db_column='MODALITAET', max_length=255, blank=True, null=True)
    einstellung = models.TextField(db_column='EINSTELLUNG', blank=True, null=True)
    privat = models.CharField(db_column='PRIVAT', max_length=255, blank=True, null=True)
    bg = models.CharField(db_column='BG', max_length=255, blank=True, null=True)
    vertragsart = models.CharField(db_column='VERTRAGSART', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'STDLEIST_GYN'


class Stdsig(models.Model):
    pharmanummer = models.IntegerField(db_column='PHARMANUMMER', primary_key=True)
    dosisschema = models.TextField(db_column='DOSISSCHEMA', blank=True, null=True)
    ermaechtigung_id = models.IntegerField(db_column='ERMAECHTIGUNG_ID')

    class Meta:
        managed = False
        db_table = 'STDSIG'
        unique_together = (('pharmanummer', 'ermaechtigung_id'),)


class StdsigGyn(models.Model):
    pharmanummer = models.IntegerField(db_column='PHARMANUMMER', primary_key=True)
    dosisschema = models.TextField(db_column='DOSISSCHEMA', blank=True, null=True)
    ermaechtigung_id = models.IntegerField(db_column='ERMAECHTIGUNG_ID')

    class Meta:
        managed = False
        db_table = 'STDSIG_GYN'
        unique_together = (('pharmanummer', 'ermaechtigung_id'),)


class Tagesinfo(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    kalender_nr = models.IntegerField(db_column='KALENDER_NR')
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)
    ermaechtigung_id = models.IntegerField(db_column='ERMAECHTIGUNG_ID')
    info_datum = models.DateField(db_column='INFO_DATUM', blank=True, null=True)
    info_text = models.CharField(db_column='INFO_TEXT', max_length=255, blank=True, null=True)
    letzte_aenderung_datum = models.DateField(db_column='LETZTE_AENDERUNG_DATUM', blank=True, null=True)
    letzte_aenderung_uhrzeit = models.TimeField(db_column='LETZTE_AENDERUNG_UHRZEIT', blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=255, blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'TAGESINFO'


class Terminreservierung(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    kalendertermin_id = models.IntegerField(db_column='KALENDERTERMIN_ID', blank=True, null=True)
    web_reservierungsdaten_id = models.IntegerField(db_column='WEB_RESERVIERUNGSDATEN_ID', blank=True, null=True)
    reserviert_am = models.DateTimeField(db_column='RESERVIERT_AM', blank=True, null=True)
    storniert_am = models.DateTimeField(db_column='STORNIERT_AM', blank=True, null=True)
    patient_id = models.IntegerField(db_column='PATIENT_ID', blank=True, null=True)
    anrede = models.CharField(db_column='ANREDE', max_length=255, blank=True, null=True)
    geschlecht = models.IntegerField(db_column='GESCHLECHT', blank=True, null=True)
    titel = models.CharField(db_column='TITEL', max_length=255, blank=True, null=True)
    vorname = models.CharField(db_column='VORNAME', max_length=255, blank=True, null=True)
    name = models.CharField(db_column='NAME', max_length=255, blank=True, null=True)
    geburtsdatum = models.DateField(db_column='GEBURTSDATUM', blank=True, null=True)
    versichertennummer = models.CharField(db_column='VERSICHERTENNUMMER', max_length=255, blank=True, null=True)
    versicherung = models.CharField(db_column='VERSICHERUNG', max_length=255, blank=True, null=True)
    ist_neupatient = models.IntegerField(db_column='IST_NEUPATIENT', blank=True, null=True)
    strasse = models.CharField(db_column='STRASSE', max_length=255, blank=True, null=True)
    plz = models.CharField(db_column='PLZ', max_length=255, blank=True, null=True)
    ort = models.CharField(db_column='ORT', max_length=255, blank=True, null=True)
    telefon = models.CharField(db_column='TELEFON', max_length=255, blank=True, null=True)
    mobil = models.CharField(db_column='MOBIL', max_length=255, blank=True, null=True)
    fax = models.CharField(db_column='FAX', max_length=255, blank=True, null=True)
    email = models.CharField(db_column='EMAIL', max_length=255, blank=True, null=True)
    mitteilung = models.TextField(db_column='MITTEILUNG', blank=True, null=True)
    sms_erinnerung = models.IntegerField(db_column='SMS_ERINNERUNG', blank=True, null=True)
    email_erinnerung = models.IntegerField(db_column='EMAIL_ERINNERUNG', blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'TERMINRESERVIERUNG'


class Untersuchung(models.Model):
    class Status(Enum):
        BEAUFTRAGT = 2
        BEGONNEN = 5
        UNTERBROCHEN = 10
        BEENDET = 15
        ABGEBROCHEN = 20

    untersuchung_id = models.IntegerField(db_column='UNTERSUCHUNG_ID', unique=True, primary_key=True)
    status_untersuchung = models.IntegerField(db_column='STATUS_UNTERSUCHUNG', blank=True, null=True)
    arztbrief_id = models.ForeignKey(Arztbrief, db_column='ARZTBRIEF_ID', blank=True, null=True, on_delete=models.SET_NULL)
    auftraggruppe_id = models.IntegerField(db_column='AUFTRAGGRUPPE_ID',
                                           blank=True, null=True)
    auftrag_stamm_id = models.IntegerField(db_column='AUFTRAG_STAMM_ID', blank=True, null=True)
    uuid = models.CharField(db_column='UUID', max_length=255, blank=True,
                            null=True)

    class Meta:
        managed = False
        db_table = 'UNTERSUCHUNG'

    def __str__(self):
        return "Untersuchung {} (Auftrag: {}, Status: {})".format(
            self.untersuchung_id,
            self.auftrag_id or "-",
            Untersuchung.Status(self.status_untersuchung).name)


class UntersuchungTyp(models.Model):
    id = models.IntegerField(db_column='ID', primary_key=True)
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)
    u_text = models.CharField(db_column='U_TEXT', max_length=100, blank=True, null=True)
    u_sortierung = models.IntegerField(db_column='U_SORTIERUNG', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'UNTERSUCHUNG_TYP'


class Vertragsart(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    elter_id = models.IntegerField(db_column='ELTER_ID')
    typ = models.IntegerField(db_column='TYP')
    position = models.IntegerField(db_column='POSITION')
    kuerzel = models.CharField(db_column='KUERZEL', unique=True, max_length=50, blank=True, null=True)
    kennung = models.IntegerField(db_column='KENNUNG', unique=True)
    langtext = models.TextField(db_column='LANGTEXT', blank=True, null=True)
    adresstext = models.TextField(db_column='ADRESSTEXT', blank=True, null=True)
    adresse = models.TextField(db_column='ADRESSE', blank=True, null=True)
    re_anschrift_xml = models.TextField(db_column='RE_ANSCHRIFT_XML', blank=True, null=True)
    mandant = models.CharField(db_column='MANDANT', max_length=255)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=255, blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)
    ziffer_typ = models.CharField(db_column='ZIFFER_TYP', max_length=50, blank=True, null=True)
    faktor = models.FloatField(db_column='FAKTOR', blank=True, null=True)
    faktor_tech = models.FloatField(db_column='FAKTOR_TECH', blank=True, null=True)
    faktor_lab = models.FloatField(db_column='FAKTOR_LAB', blank=True, null=True)
    einstellungen = models.TextField(db_column='EINSTELLUNGEN', blank=True, null=True)
    kurzkuerzel = models.CharField(db_column='KURZKUERZEL', unique=True, max_length=5, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'VERTRAGSART'


class VidalDaten(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    checksum = models.IntegerField(db_column='CHECKSUM', blank=True, null=True)
    type = models.CharField(db_column='TYPE', max_length=3, blank=True, null=True)
    gueltig = models.DateField(db_column='GUELTIG', blank=True, null=True)
    verfuegbar = models.CharField(db_column='VERFUEGBAR', max_length=3, blank=True, null=True)
    pharma_nr = models.IntegerField(db_column='PHARMA_NR', blank=True, null=True)
    zulassung_nr_string = models.CharField(db_column='ZULASSUNG_NR_STRING', max_length=50, blank=True, null=True)
    zulassung_nr = models.IntegerField(db_column='ZULASSUNG_NR', blank=True, null=True)
    bezeichnung_kurz = models.CharField(db_column='BEZEICHNUNG_KURZ', max_length=50, blank=True, null=True)
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)
    letzte_aenderung = models.DateField(db_column='LETZTE_AENDERUNG', blank=True, null=True)
    rsigns = models.CharField(db_column='RSIGNS', max_length=50, blank=True, null=True)
    storage = models.CharField(db_column='STORAGE', max_length=255, blank=True, null=True)
    remb = models.IntegerField(db_column='REMB', blank=True, null=True)
    box = models.CharField(db_column='BOX', max_length=1, blank=True, null=True)
    ssigns = models.CharField(db_column='SSIGNS', max_length=50, blank=True, null=True)
    indtext = models.TextField(db_column='INDTEXT', blank=True, null=True)
    ruletext = models.TextField(db_column='RULETEXT', blank=True, null=True)
    remarktext = models.TextField(db_column='REMARKTEXT', blank=True, null=True)
    quantity = models.IntegerField(db_column='QUANTITY', blank=True, null=True)
    unit = models.CharField(db_column='UNIT', max_length=50, blank=True, null=True)
    enhunit = models.CharField(db_column='ENHUNIT', max_length=255, blank=True, null=True)
    kvp = models.FloatField(db_column='KVP', blank=True, null=True)
    avp = models.FloatField(db_column='AVP', blank=True, null=True)
    zinh = models.IntegerField(db_column='ZINH', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'VIDAL_DATEN'


class VidalReferenzen(models.Model):
    zinh = models.IntegerField(db_column='ZINH', primary_key=True)
    bezeichnung = models.CharField(db_column='BEZEICHNUNG', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'VIDAL_REFERENZEN'


class Warteliste(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    patient_id = models.ForeignKey(Patient, db_column='PATIENT_ID', blank=True, null=True, on_delete=models.PROTECT)
    termin_start_datum = models.DateField(db_column='TERMIN_START_DATUM', blank=True, null=True)
    termin_start_uhrzeit = models.TimeField(db_column='TERMIN_START_UHRZEIT', blank=True, null=True)
    termin_text = models.CharField(db_column='TERMIN_TEXT', max_length=255, blank=True, null=True)
    warte_liste = models.CharField(db_column='WARTE_LISTE', max_length=50, blank=True, null=True)
    warte_datum = models.DateField(db_column='WARTE_DATUM', blank=True, null=True)
    warte_uhrzeit = models.TimeField(db_column='WARTE_UHRZEIT', blank=True, null=True)
    letzte_aenderung_datum = models.DateField(db_column='LETZTE_AENDERUNG_DATUM', blank=True, null=True)
    letzte_aenderung_uhrzeit = models.TimeField(db_column='LETZTE_AENDERUNG_UHRZEIT', blank=True, null=True)
    termin_ende_datum = models.DateField(db_column='TERMIN_ENDE_DATUM', blank=True, null=True)
    termin_ende_uhrzeit = models.TimeField(db_column='TERMIN_ENDE_UHRZEIT', blank=True, null=True)
    typ = models.IntegerField(db_column='TYP', blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=255, blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)
    patient_name = models.CharField(db_column='PATIENT_NAME', max_length=255, blank=True, null=True)
    mandant_id = models.IntegerField(db_column='MANDANT_ID', blank=True, null=True)
    kalender_nr = models.IntegerField(db_column='KALENDER_NR', blank=True, null=True)
    position = models.BigIntegerField(db_column='POSITION', blank=True, null=True)
    erledigt = models.IntegerField(db_column='ERLEDIGT')
    kennzeichen = models.IntegerField(db_column='KENNZEICHEN', blank=True, null=True)
    einstellungen = models.TextField(db_column='EINSTELLUNGEN', blank=True, null=True)
    termin_anlage = models.DateTimeField(db_column='TERMIN_ANLAGE', blank=True, null=True)
    warte_entlassen = models.DateTimeField(db_column='WARTE_ENTLASSEN', blank=True, null=True)
    webstatus = models.IntegerField(db_column='WEBSTATUS', blank=True, null=True)
    webversion = models.IntegerField(db_column='WEBVERSION', blank=True, null=True)
    webanlage = models.DateTimeField(db_column='WEBANLAGE', blank=True, null=True)
    webreservierung_id = models.IntegerField(db_column='WEBRESERVIERUNG_ID', blank=True, null=True)
    auftragliste = models.CharField(db_column='AUFTRAGLISTE', max_length=255, blank=True, null=True)
    wegzusenden = models.IntegerField(db_column='WEGZUSENDEN')
    gewaehlterauftrag = models.IntegerField(db_column='GEWAEHLTERAUFTRAG', blank=True, null=True)
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)
    email = models.CharField(db_column='EMAIL', max_length=255, blank=True, null=True)
    mobil = models.CharField(db_column='MOBIL', max_length=255, blank=True, null=True)
    ist_webtermin = models.IntegerField(db_column='IST_WEBTERMIN', blank=True, null=True)
    webtermin_titel = models.CharField(db_column='WEBTERMIN_TITEL', max_length=255, blank=True, null=True)
    will_email_erinnerung = models.IntegerField(db_column='WILL_EMAIL_ERINNERUNG', blank=True, null=True)
    will_sms_erinnerung = models.IntegerField(db_column='WILL_SMS_ERINNERUNG', blank=True, null=True)
    notizen = models.TextField(db_column='NOTIZEN', blank=True, null=True)
    ist_weberinnerung = models.IntegerField(db_column='IST_WEBERINNERUNG', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'WARTELISTE'
        unique_together = (
            ('termin_ende_datum', 'termin_ende_uhrzeit', 'kalender_nr'),
            ('termin_start_datum', 'termin_start_uhrzeit', 'kalender_nr'),
        )


class Woerterbuch(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    sprache = models.CharField(db_column='SPRACHE', max_length=10, blank=True, null=True)
    name = models.CharField(db_column='NAME', max_length=20, blank=True, null=True)
    text = models.CharField(db_column='TEXT', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'WOERTERBUCH'
        unique_together = (('sprache', 'name'),)


class Xadresse(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    typ = models.IntegerField(db_column='TYP')
    strasse = models.CharField(db_column='STRASSE', max_length=255, blank=True, null=True)
    plz = models.CharField(db_column='PLZ', max_length=255, blank=True, null=True)
    ort = models.CharField(db_column='ORT', max_length=255, blank=True, null=True)
    bundesland = models.CharField(db_column='BUNDESLAND', max_length=255, blank=True, null=True)
    land_code = models.CharField(db_column='LAND_CODE', max_length=255, blank=True, null=True)
    postfach = models.CharField(db_column='POSTFACH', max_length=255, blank=True, null=True)
    postfach_plz = models.CharField(db_column='POSTFACH_PLZ', max_length=255, blank=True, null=True)
    postfach_ort = models.CharField(db_column='POSTFACH_ORT', max_length=255, blank=True, null=True)
    notizen = models.TextField(db_column='NOTIZEN', blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)
    letzte_aenderung = models.DateTimeField(db_column='LETZTE_AENDERUNG', blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)
    hausnummer = models.CharField(db_column='HAUSNUMMER', max_length=255)
    tabelle = models.IntegerField(db_column='TABELLE')

    class Meta:
        managed = False
        db_table = 'XADRESSE'

class Zahlungen(models.Model):

    class Meta:
        managed = False
        db_table = 'ZAHLUNGEN'
        unique_together = (('storno', 'belegart'), ('zaehler', 'mandant_id', 'jahr'),)

    id = models.AutoField(db_column='ID', primary_key=True)
    rechnungdoku_id = models.IntegerField(db_column='RECHNUNGDOKU_ID', blank=True, null=True)
    zahlung_zeit = models.DateTimeField(db_column='ZAHLUNG_ZEIT', blank=True, null=True)
    betrag = models.FloatField(db_column='BETRAG')
    zahlung_text = models.CharField(db_column='ZAHLUNG_TEXT', max_length=255, blank=True, null=True)
    zahlung_art = models.CharField(db_column='ZAHLUNG_ART', max_length=255, blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)
    system_zeit = models.DateTimeField(db_column='SYSTEM_ZEIT', blank=True, null=True)
    mandant_id = models.IntegerField(db_column='MANDANT_ID')
    waehrung = models.CharField(db_column='WAEHRUNG', max_length=50, blank=True, null=True)
    scheindoku_id = models.IntegerField(db_column='SCHEINDOKU_ID', blank=True, null=True)
    zaehler = models.IntegerField(db_column='ZAEHLER', blank=True, null=True)
    jahr = models.IntegerField(db_column='JAHR', blank=True, null=True)
    umsatzzaehler_bar = models.FloatField(db_column='UMSATZZAEHLER_BAR', blank=True, null=True)
    storno = models.IntegerField(db_column='STORNO', blank=True, null=True)
    belegart = models.IntegerField(db_column='BELEGART', blank=True, null=True)
    signatur = models.TextField(db_column='SIGNATUR', blank=True, null=True)
    betrag_mwst = models.FloatField(db_column='BETRAG_MWST')
    journal = models.TextField(db_column='JOURNAL', blank=True, null=True)
    seriennummer_sigzert = models.CharField(db_column='SERIENNUMMER_SIGZERT', max_length=255, blank=True, null=True)
    sigzert_ausgefallen = models.IntegerField(db_column='SIGZERT_AUSGEFALLEN', blank=True, null=True)
    registrierkasse_nr = models.CharField(db_column='REGISTRIERKASSE_NR', max_length=255, blank=True, null=True)
    letztes_kettenglied_id = models.IntegerField(db_column='LETZTES_KETTENGLIED_ID', unique=True, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ZAHLUNGEN'
        unique_together = (('storno', 'belegart'), ('zaehler', 'mandant_id', 'jahr'),)


class Zahlungsplan(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    scheindoku_id = models.IntegerField(db_column='SCHEINDOKU_ID')
    zahlung_datum = models.DateField(db_column='ZAHLUNG_DATUM', blank=True, null=True)
    betrag = models.FloatField(db_column='BETRAG')
    zahlung_text = models.CharField(db_column='ZAHLUNG_TEXT', max_length=255)
    zahlung_art = models.CharField(db_column='ZAHLUNG_ART', max_length=20, blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=100, blank=True, null=True)
    system_datum = models.DateField(db_column='SYSTEM_DATUM', blank=True, null=True)
    system_uhrzeit = models.TimeField(db_column='SYSTEM_UHRZEIT', blank=True, null=True)
    status = models.IntegerField(db_column='STATUS')
    re_nr = models.CharField(db_column='RE_NR', unique=True, max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ZAHLUNGSPLAN'


class Ziffer(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    leistungsdoku_id = models.IntegerField(db_column='LEISTUNGSDOKU_ID', blank=True, null=True)
    untersuchung_id = models.IntegerField(db_column='UNTERSUCHUNG_ID', blank=True, null=True)
    typ = models.CharField(db_column='TYP', max_length=50, blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)
    system_datum = models.DateField(db_column='SYSTEM_DATUM', blank=True, null=True)
    system_uhrzeit = models.TimeField(db_column='SYSTEM_UHRZEIT', blank=True, null=True)
    ziffer_nr = models.CharField(db_column='ZIFFER_NR', max_length=50, blank=True, null=True)
    ziffer_anzahl = models.FloatField(db_column='ZIFFER_ANZAHL', blank=True, null=True)
    ziffer_betrag = models.FloatField(db_column='ZIFFER_BETRAG', blank=True, null=True)
    ziffer_waehrung = models.CharField(db_column='ZIFFER_WAEHRUNG', max_length=50, blank=True, null=True)
    ziffer_punkte = models.FloatField(db_column='ZIFFER_PUNKTE', blank=True, null=True)
    ziffer_km = models.FloatField(db_column='ZIFFER_KM', blank=True, null=True)
    ziffer_kurztext = models.CharField(db_column='ZIFFER_KURZTEXT', max_length=50, blank=True, null=True)
    ziffer_text = models.TextField(db_column='ZIFFER_TEXT', blank=True, null=True)
    ziffer_begr = models.CharField(db_column='ZIFFER_BEGR', max_length=255, blank=True, null=True)
    ziffer_datum = models.DateField(db_column='ZIFFER_DATUM', blank=True, null=True)
    ziffer_uhrzeit = models.TimeField(db_column='ZIFFER_UHRZEIT', blank=True, null=True)
    ziffer_ca_kennzeichen = models.CharField(db_column='ZIFFER_CA_KENNZEICHEN', max_length=50, blank=True, null=True)
    zusatzkennzeichen = models.CharField(db_column='ZUSATZKENNZEICHEN', max_length=50, blank=True, null=True)
    reserve1 = models.CharField(db_column='RESERVE1', max_length=50, blank=True, null=True)
    mwst_satz = models.IntegerField(db_column='MWST_SATZ', blank=True, null=True)
    kostentyp = models.IntegerField(db_column='KOSTENTYP', blank=True, null=True)
    kontierung = models.IntegerField(db_column='KONTIERUNG', blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)
    visitenadresse = models.TextField(db_column='VISITENADRESSE', blank=True, null=True)
    sachkosten = models.TextField(db_column='SACHKOSTEN', blank=True, null=True)
    zusatz = models.TextField(db_column='ZUSATZ', blank=True, null=True)
    organ = models.TextField(db_column='ORGAN', blank=True, null=True)
    begruendung_arzt = models.TextField(db_column='BEGRUENDUNG_ARZT', blank=True, null=True)
    begruendung_gnr = models.CharField(db_column='BEGRUENDUNG_GNR', max_length=255, blank=True, null=True)
    stationaer_von = models.DateField(db_column='STATIONAER_VON', blank=True, null=True)
    stationaer_bis = models.DateField(db_column='STATIONAER_BIS', blank=True, null=True)
    op_datum = models.DateField(db_column='OP_DATUM', blank=True, null=True)
    ops = models.TextField(db_column='OPS', blank=True, null=True)
    komplikation = models.TextField(db_column='KOMPLIKATION', blank=True, null=True)
    privat_faktor = models.FloatField(db_column='PRIVAT_FAKTOR')
    rabatt = models.FloatField(db_column='RABATT', blank=True, null=True)
    op_dauer = models.FloatField(db_column='OP_DAUER', blank=True, null=True)
    leistung_id = models.IntegerField(db_column='LEISTUNG_ID', blank=True, null=True)
    vertragsart = models.IntegerField(db_column='VERTRAGSART', blank=True, null=True)
    dokumentation_id = models.IntegerField(db_column='DOKUMENTATION_ID', blank=True, null=True)
    sortierung = models.IntegerField(db_column='SORTIERUNG', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ZIFFER'


class ZiffernPrivat(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    typ = models.CharField(db_column='TYP', max_length=255, blank=True, null=True)
    subtyp = models.CharField(db_column='SUBTYP', max_length=255, blank=True, null=True)
    ziffer = models.CharField(db_column='ZIFFER', max_length=255, blank=True, null=True)
    ziffer_text = models.TextField(db_column='ZIFFER_TEXT', blank=True, null=True)
    ziffer_text_kurz = models.CharField(db_column='ZIFFER_TEXT_KURZ', max_length=255, blank=True, null=True)
    punkte = models.FloatField(db_column='PUNKTE')
    gruppe = models.CharField(db_column='GRUPPE', max_length=255, blank=True, null=True)
    kuerzel = models.CharField(db_column='KUERZEL', max_length=255, blank=True, null=True)
    notizen = models.TextField(db_column='NOTIZEN', blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)
    status = models.IntegerField(db_column='STATUS', blank=True, null=True)
    ziffer_von = models.DateField(db_column='ZIFFER_VON', blank=True, null=True)
    ziffer_bis = models.DateField(db_column='ZIFFER_BIS', blank=True, null=True)
    betrag = models.FloatField(db_column='BETRAG')
    waehrung = models.CharField(db_column='WAEHRUNG', max_length=255, blank=True, null=True)
    version = models.CharField(db_column='VERSION', max_length=255, blank=True, null=True)
    zeitstempel = models.TextField(db_column='ZEITSTEMPEL', blank=True, null=True)
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    regel_l = Base64TextField(db_column='REGEL_L', blank=True, null=True)
    mwst_satz = models.IntegerField(db_column='MWST_SATZ', blank=True, null=True)
    kostentyp = models.IntegerField(db_column='KOSTENTYP', blank=True, null=True)
    kontierung = models.IntegerField(db_column='KONTIERUNG', blank=True, null=True)
    lager_kuerzel = models.CharField(db_column='LAGER_KUERZEL', max_length=255, blank=True, null=True)
    lager_menge = models.FloatField(db_column='LAGER_MENGE')
    wa_texte = Base64TextField(db_column='WA_TEXTE', blank=True, null=True)
    angebot_gruppe = models.CharField(db_column='ANGEBOT_GRUPPE', max_length=255, blank=True, null=True)
    angebot_ueberschrift = models.CharField(db_column='ANGEBOT_UEBERSCHRIFT', max_length=255, blank=True, null=True)
    angebot_text = models.TextField(db_column='ANGEBOT_TEXT', blank=True, null=True)
    angebot_bild = models.CharField(db_column='ANGEBOT_BILD', max_length=255, blank=True, null=True)
    angebot_details = Base64TextField(db_column='ANGEBOT_DETAILS', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ZIFFERN_PRIVAT'
        verbose_name_plural = "Privatziffern"

    def __str__(self):
        return "[{}] {}".format(
            self.ziffer,
            self.ziffer_text,
            )


class ZifferGruppe(models.Model):
    id = models.CharField(db_column='ID', primary_key=True, max_length=50)
    gruppe = models.CharField(db_column='GRUPPE', max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ZIFFER_GRUPPE'


class ZifferTyp(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    typ = models.CharField(db_column='TYP', max_length=50, blank=True, null=True)
    ziffer_sortierung = models.IntegerField(db_column='ZIFFER_SORTIERUNG', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ZIFFER_TYP'


class Zuweiser(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    kue = models.CharField(db_column='KUE', unique=True, max_length=50, blank=True, null=True)
    anrede = models.CharField(db_column='ANREDE', max_length=50, blank=True, null=True)
    titel = models.CharField(db_column='TITEL', max_length=50, blank=True, null=True)
    vorname = models.CharField(db_column='VORNAME', max_length=255, blank=True, null=True)
    wvorname = models.CharField(db_column='WVORNAME', max_length=50, blank=True, null=True)
    name = models.CharField(db_column='NAME', max_length=255, blank=True, null=True)
    gebname = models.CharField(db_column='GEBNAME', max_length=50, blank=True, null=True)
    namenszusatz = models.CharField(db_column='NAMENSZUSATZ', max_length=50, blank=True, null=True)
    namensvorsatz = models.CharField(db_column='NAMENSVORSATZ', max_length=50, blank=True, null=True)
    gebdatum = models.DateField(db_column='GEBDATUM', blank=True, null=True)
    bild = models.TextField(db_column='BILD', blank=True, null=True)
    notizen = models.TextField(db_column='NOTIZEN', blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)
    strasse = models.CharField(db_column='STRASSE', max_length=255, blank=True, null=True)
    plz = models.CharField(db_column='PLZ', max_length=50, blank=True, null=True)
    ort = models.CharField(db_column='ORT', max_length=255, blank=True, null=True)
    land = models.CharField(db_column='LAND', max_length=50, blank=True, null=True)
    adresse_id1 = models.CharField(db_column='Adresse_ID1', max_length=50, blank=True, null=True)
    adresse_id2 = models.CharField(db_column='Adresse_ID2', max_length=50, blank=True, null=True)
    adresse_id3 = models.CharField(db_column='Adresse_ID3', max_length=50, blank=True, null=True)
    adresse_id4 = models.CharField(db_column='Adresse_ID4', max_length=50, blank=True, null=True)
    telefon = models.CharField(db_column='TELEFON', max_length=50, blank=True, null=True)
    telefon_2 = models.CharField(db_column='TELEFON_2', max_length=50, blank=True, null=True)
    fax = models.CharField(db_column='FAX', max_length=50, blank=True, null=True)
    mobil = models.CharField(db_column='MOBIL', max_length=50, blank=True, null=True)
    email = models.CharField(db_column='EMAIL', max_length=50, blank=True, null=True)
    bank = models.CharField(db_column='BANK', max_length=255, blank=True, null=True)
    bank_plz = models.CharField(db_column='BANK_PLZ', max_length=50, blank=True, null=True)
    bank_ort = models.CharField(db_column='BANK_ORT', max_length=255, blank=True, null=True)
    bank_land = models.CharField(db_column='BANK_LAND', max_length=50, blank=True, null=True)
    kto_nr = models.CharField(db_column='KTO_NR', max_length=50, blank=True, null=True)
    blz = models.CharField(db_column='BLZ', max_length=50, blank=True, null=True)
    kto_inhaber = models.CharField(db_column='KTO_INHABER', max_length=50, blank=True, null=True)
    status = models.IntegerField(db_column='STATUS')
    zeile_lock = models.CharField(db_column='ZEILE_LOCK', max_length=255, blank=True, null=True)
    vertragspartnernr = models.CharField(db_column='VERTRAGSPARTNERNR', max_length=50, blank=True, null=True)
    fachgebiet = models.CharField(db_column='FACHGEBIET', max_length=50, blank=True, null=True)
    letzte_aenderung_datum = models.DateField(db_column='LETZTE_AENDERUNG_DATUM', blank=True, null=True)
    letzte_aenderung_uhrzeit = models.TimeField(db_column='LETZTE_AENDERUNG_UHRZEIT', blank=True, null=True)
    briefdaten = Base64TextField(db_column='BRIEFDATEN', blank=True, null=True)
    anschrift_zeile1 = models.CharField(db_column='ANSCHRIFT_ZEILE1', max_length=255, blank=True, null=True)
    anschrift_zeile2 = models.CharField(db_column='ANSCHRIFT_ZEILE2', max_length=255, blank=True, null=True)
    befundkomm_betrkey = models.CharField(db_column='BEFUNDKOMM_BETRKEY', max_length=50, blank=True, null=True)
    befundkomm_email = models.CharField(db_column='BEFUNDKOMM_EMAIL', max_length=50, blank=True, null=True)
    befundkomm_menr = models.CharField(db_column='BEFUNDKOMM_MENR', max_length=50, blank=True, null=True)
    praxis_typ = models.IntegerField(db_column='PRAXIS_TYP')
    bsnr = models.CharField(db_column='BSNR', max_length=50, blank=True, null=True)
    befundkomm_beftypen = models.CharField(db_column='BEFUNDKOMM_BEFTYPEN', max_length=255, blank=True, null=True)
    praxiszeiten = models.TextField(db_column='PRAXISZEITEN', blank=True, null=True)
    lanr = models.CharField(db_column='LANR', max_length=50, blank=True, null=True)
    anforderungslabor = models.IntegerField(db_column='ANFORDERUNGSLABOR', blank=True, null=True)
    komm_versandtyp = models.CharField(db_column='KOMM_VERSANDTYP', max_length=50, blank=True, null=True)
    komm_technik = models.CharField(db_column='KOMM_TECHNIK', max_length=50, blank=True, null=True)
    verrechnungsstelle = models.CharField(db_column='VERRECHNUNGSSTELLE', max_length=255, blank=True, null=True)
    befundkomm_teilnehmer = models.IntegerField(db_column='BEFUNDKOMM_TEILNEHMER', blank=True, null=True)
    befunde_immer_erhalten = models.IntegerField(db_column='BEFUNDE_IMMER_ERHALTEN', blank=True, null=True)
    komm_datenformat_laborauftrag = models.CharField(db_column='KOMM_DATENFORMAT_LABORAUFTRAG', max_length=50, blank=True, null=True)
    komm_technik_laborauftrag = models.CharField(db_column='KOMM_TECHNIK_LABORAUFTRAG', max_length=50, blank=True, null=True)
    sprache = models.CharField(db_column='SPRACHE', max_length=50, blank=True, null=True)
    serienbrief_versandarten = models.CharField(db_column='SERIENBRIEF_VERSANDARTEN', max_length=255, blank=True, null=True)
    serienbrief_stdversandart = models.CharField(db_column='SERIENBRIEF_STDVERSANDART', max_length=50, blank=True, null=True)
    hausnummer = models.CharField(db_column='HAUSNUMMER', max_length=50, blank=True, null=True)
    serienbrief_typen = models.TextField(db_column='SERIENBRIEF_TYPEN', blank=True, null=True)
    einstellungen = Base64TextField(db_column='EINSTELLUNGEN', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ZUWEISER'
        unique_together = (('vertragspartnernr', 'lanr'),)
        verbose_name_plural = 'Zuweiser'

    def __str__(self):
        return "{}, {}".format(
            self.name if not self.name == " " else "[" + self.anschrift_zeile1 + "]",
            self.vorname if not self.name == " " else self.anschrift_zeile2,
            )


class ZytoBefunde(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)
    patient_id = models.ForeignKey(Patient, db_column='PATIENT_ID', on_delete=models.PROTECT)
    untersuchung_id = models.IntegerField(db_column='UNTERSUCHUNG_ID')
    typ = models.IntegerField(db_column='TYP')
    datum = models.DateField(db_column='DATUM', blank=True, null=True)
    abstrich_freitext = models.TextField(db_column='ABSTRICH_FREITEXT', blank=True, null=True)
    befund_ergebnis = models.CharField(db_column='BEFUND_ERGEBNIS', max_length=50, blank=True, null=True)
    befund_freitext = models.TextField(db_column='BEFUND_FREITEXT', blank=True, null=True)
    bemerkung = models.TextField(db_column='BEMERKUNG', blank=True, null=True)
    status = models.IntegerField(db_column='STATUS')
    system_datum = models.DateField(db_column='SYSTEM_DATUM', blank=True, null=True)
    system_uhrzeit = models.TimeField(db_column='SYSTEM_UHRZEIT', blank=True, null=True)
    bearbeiter = models.CharField(db_column='BEARBEITER', max_length=50, blank=True, null=True)
    protokoll = models.TextField(db_column='PROTOKOLL', blank=True, null=True)
    ermaechtigung_id = models.IntegerField(db_column='ERMAECHTIGUNG_ID')
    bild = models.CharField(db_column='BILD', max_length=50, blank=True, null=True)
    position = models.CharField(db_column='POSITION', max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ZYTO_BEFUNDE'
