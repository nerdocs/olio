"""olio URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from django.urls import path, include

import olio.acetomed.views as views
from olio.acetomed.views import PatientSearchView, NotSentVUsView, ProblemsView

app_name = 'acetomed'

urlpatterns = [
    path('', views.index, name='index'),
    path('<int:pat_id>/', views.file, name='detail'),
    path('search/<str:patient_search_string>/', login_required(PatientSearchView.as_view()), name="patientsearch"),
    path('search/', login_required(PatientSearchView.as_view()), name="patientsearch"),
    path('problems/', login_required(ProblemsView.as_view()), name='problems'),
    path('problems/notsentvus', login_required(NotSentVUsView.as_view()), name="notsentvus"),
    path('clinical_reports/', login_required(
        views.UnreadClinicalFindingsView.as_view()), name='clinical_reports'),
]
