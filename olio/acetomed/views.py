from django.shortcuts import render
from django.http import Http404

from .models import Patient, Dokumentation, Medikament, Combo
from django.contrib import messages
from django.views.generic.list import ListView
from django.views.generic.base import TemplateView
from django.contrib.auth.decorators import login_required


@login_required(login_url="/admin/login")
def index(request):
    return render(request, "acetomed/index.html")


@login_required(login_url="/admin/login")
def file(request, pat_id):
    try:
        p = Patient.objects.get(pk=pat_id)
    except Patient.DoesNotExist:
        messages.ERROR(request,
                       "Der Patient mit der ID {} wurde nicht gefunden.".
                       format(pat_id))
        return render(request, "")

    if p.geschlecht == 1:
        sex = "male"
    elif p.geschlecht == 2:
        sex = "female"
    else:
        sex = "unknown"

    kennzeichen = []
    for i in range(10):
        kzname = 'kennzeichen{}'.format(i+1)
        kennung = getattr(p.patientzusatz, kzname)
        kz = (
            kennung,
            Combo.objects.filter(name=kzname, kennung=kennung).first()
              )
        kennzeichen.append(kz)

    return render(request, "acetomed/patients/file.html", {
        "patient": p,
        "kennzeichen": kennzeichen,
        "doku": p.dokumentation_set.all().exclude(
                    status=Dokumentation.Status.GELOESCHT.value
                ).order_by("-untersuchung_nr", "-datum"),

        # FIXME: offensichtlich ist patient_id nicht sinnvoll verknüpft.
        "medikamente": p.medikament_set.filter(typ="DM"),
        "diagnosen": p.diagnose_set.filter(typ="DD"),
        "sex": sex,
        "medizin": p.patientmedizin,
        })


class PatientSearchView(ListView):
    model = Patient
    context_object_name = "patients"
    template_name = "acetomed/patients/patient_list.html"

    def get_queryset(self):
        searchstring = self.request.GET.get("patient_search_string", "").strip()
        if not searchstring:
            messages.error(self.request,
                           "Gesucht werden kann mittels 'Name', oder "
                           "'Name, Vorname', "
                           "oder Teilen davon, z.B findet 'mus,m' alle"
                           " Personen, deren "
                           "Nachname mit 'Mus' und deren Vorname mit "
                           "'M' beginnen. Groß- Kleinschreibung "
                           "wird ignoriert. Es kann auch eine SVNR "
                           "als Suchparameter angegeben werden, z.B. '1234'")
            return []
        else:
            try:
                # try to parse searchstring as SVNR
                svnr = int(searchstring)
                return Patient.objects.filter(
                    versicherungsnr__startswith=str(svnr))
            except ValueError:
                # try to parse searchstring as "lastname,firstname"
                if "," in searchstring:
                    if searchstring.count(",") != 1:
                        raise Http404("Fehler in Sucheingabe")

                    lastname, firstname = searchstring.split(",")
                    return Patient.objects.filter(
                        name__istartswith=lastname,
                        vorname__istartswith=firstname)
                else:
                    return Patient.objects.filter(
                            name__istartswith=searchstring)


class MedicationSearchView(ListView):
    model = Medikament
    context_object_name = "medikamente"
    #template_name = "acetomed/patient_list.html"

    def get_queryset(self):
        searchstring = self.request.GET.get("searchstring", "").strip()
        if not searchstring:
            messages.error(self.request,
                           "Es kann nach Medimanentennamen gesucht werden, "
                           "so wie sie in der Kartei gespeichert sind. "
                           "Groß- Kleinschreibung "
                           "wird ignoriert.")
            return []
        else:
            try:
                drugname = searchstring.strip()
                return Medikament.objects.filter(
                        patient_id=1,
                        medikament__istartswith=drugname)
            except ValueError:
                return []


class ProblemsView(TemplateView):
    template_name = "acetomed/problems.html"


class NotSentVUsView(ListView):
    """Lists all VUs that have not been sent."""
    template_name = "acetomed/dokumentation_list_notsentvus.html"
    model = Dokumentation
    queryset = Dokumentation.objects.filter(typ="AT_F_VU2017_ALLG") \
                .exclude(status=Dokumentation.Status.VERSENDET.value) \
                .exclude(status=Dokumentation.Status.GELOESCHT.value) \
                .exclude(status=Dokumentation.Status.STORNIERT.value) \
                .order_by("datum")



class UnreadClinicalFindingsView(ListView):
    """Lists all new, unread clinical findings."""
    model = Dokumentation
    context_object_name = "dokumentation"
    template_name = "acetomed/dokumentation_list_unreadclinfindings.html"
    queryset = Dokumentation.objects.select_related('patient').filter(
        typ__in=["Laborbefund", "Fremdbefund"]).exclude(status=Dokumentation.Status.GELOESCHT.value)

    #def get_queryset(self):
#        return Dokumentation.objects.filter(typ__in=["Laborbefund", "Fremdbefund"]) \
#                .exclude(status=Dokumentation.Status.VERSENDET.value) \
#                .exclude(status=Dokumentation.Status.GELOESCHT.value) \
#                .exclude(status=Dokumentation.Status.STORNIERT.value)
