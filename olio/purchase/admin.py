from django.contrib import admin
from olio.purchase.models import *


admin.site.register(Vendor)
admin.site.register(Product)
admin.site.register(ProductLink)
admin.site.register(Tag)
admin.site.register(List)

