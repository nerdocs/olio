from django.db import models


class Tag(models.Model):
    name = models.CharField(max_length=32)

    def __str__(self):
        return self.name


class Vendor(models.Model):
    name = models.CharField(max_length=100)
    url = models.CharField(max_length=244)
    phone = models.CharField(max_length=50, blank=True)
    email = models.CharField(max_length=100, blank=True)

    class Meta:
        verbose_name = "Händler"
        verbose_name_plural = "Händler"

    def __str__(self):
        return self.name


class ProductLink(models.Model):
    url = models.CharField(max_length=244, verbose_name="URL")
    vendor = models.ForeignKey(Vendor, on_delete=models.CASCADE, verbose_name="Händler")

    price = models.DecimalField(max_digits=7, decimal_places=2, verbose_name="Preis")

    # the amount of items per package
    packagesize = models.PositiveIntegerField(verbose_name="Packungsgröße")

    # the unit of this item, like pcs., l (litres), mg etc.
    unit = models.CharField(max_length=32, verbose_name="Einheit",
                            help_text="Einheit des Produkts, z.B. l, mg, Stk. etc.")

    class Meta:
        verbose_name = "Produkt-Link"
        verbose_name_plural = "Produkt-Links"

    def __str__(self):
        return "{} ({})".format(self.vendor.name, self.price)

    @property
    def cost_per_piece(self):
        return self.price / self.packagesize if self.packagesize > 0 else 0


class Product(models.Model):
    """Represents a product which is needed during work.

    This can be bought in several shops, with a different price and amount/package in each shop."""

    name = models.CharField(max_length=100)
    links = models.ManyToManyField(ProductLink)

    tags = models.ManyToManyField(Tag)

    class Meta:
        verbose_name = "Produkt"
        verbose_name_plural = "Produkte"

    def __str__(self):
        return self.name

    def cheapest_vendor(self):
        """Returns the vendor with the cheapest price"""
        return self.links.filter().order_by("price").first()


class List(models.Model):
    """Represents a buying list."""

    class Meta:
        verbose_name = "Liste"
        verbose_name_plural = "Listen"

    name = models.CharField(max_length=255, blank=True)
    items = models.ManyToManyField(Product)


