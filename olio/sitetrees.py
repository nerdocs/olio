from sitetree.utils import tree, item

sitetrees = (
    tree("maintree", items=[
        item("Patient", "acetomed:index", children=[
            item("Neu", "acetomed:index", hint="Neuen Patienten anlegen"),
        ]),
        item("Extras", "#", children=[
            item('Nicht abgeschickte VUs', 'acetomed:notsentvus'),
            item('Ungelesene Befunde', 'acetomed:clinical_reports')
        ]),
        item("Logout", "/admin/logout")
    ]),
)