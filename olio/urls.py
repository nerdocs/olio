"""olio URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.urls import path, include
from django.contrib import admin
from olio.acetomed.views import index as patient_index

# TODO make acetomed app's URLs relative
urlpatterns = [
    path('', patient_index),
    path('admin/', admin.site.urls),
    path('acetomed/', include('olio.acetomed.urls')),
    path('purchase/', include('olio.purchase.urls')),
    path('acetobugs/', include('olio.acetobugs.urls')),
]
